/**********************************************************************
 *
 * Archivo     : hora.c
 *
 * Proposito   : Implementacion de la clase GfiHora.
 *
 * Clase       : GfiHora
 *
 * Acronimo
 * de la clase : GfiHora
 *
 * Autor       : Reynaldo Martinez
 *
 * Supervision : Enrique Ibarra
 *
 * Historia    : 11/Nov/93 -> Creacion
 *               25/Nov/93 -> Modificacion por Jose Rafael Torres C.
 *               08/Oct/98 -> Modificacion por Antonio Gerardo Cano D.
 *                            para soportar el formato de horas de DB2.

 **********************************************************************/

#include "hora.h"

#if defined (GFI_VERSION)

/* Identificacion para el control de versiones RCS */

static char RCS_GFI_HORA_C[] = "$Id$";

/* Mensajes de log de RCS
 *
 * $Log$
 */

#endif

struct GfiHora_attr
{
  int segundos;			/*  seconds after the minute  [0,59]  */
  int minutos;			/*  minutes after hour   [0,59]  */
  int hora;			/*  hours   [0,23]  */
};

typedef struct GfiHora_attr GfiHora_attr;

/**********************************************************************
 *
 * GfiHora::GfiHora_Create_1
 *
 *********************************************************************/

int
GfiHora_Create_1 (GfiHora * self, int hora, int minutos, int segundos)
{
  int result = GFI_ERRC_OK;

  if (self == 0)
    return GFI_ERRC_NULLOBJPTR;

  if (hora >= 0 && hora <= 23)
    if (minutos >= 0 && minutos <= 59)
      if (segundos >= 0 && segundos <= 59)
      {
	result = GfiMalloc ((void **) self, sizeof (GfiHora_attr));
	if (result == GFI_ERRC_OK)
	{
	  (*self)->hora = hora;
	  (*self)->minutos = minutos;
	  (*self)->segundos = segundos;
	}
	return result;
      }

  return GFI_ERRC_BADARG;

}				/* Fin de GfiHora::GfiHora_Create_1 */

/**********************************************************************
 *
 * GfiHora::GfiHora_Create_2
 *
 *********************************************************************/

int
GfiHora_Create_2 (GfiHora * self)
{
  int result = GFI_ERRC_OK;
  time_t bintime;
  struct tm *ptr_tm;

  if (self == 0)
    return GFI_ERRC_NULLOBJPTR;

  result = GfiMalloc ((void **) self, sizeof (GfiHora_attr));
  if (result == GFI_ERRC_OK)
  {
    (void) time (&bintime);
    ptr_tm = localtime (&bintime);
    (*self)->hora = ptr_tm->tm_hour;
    (*self)->minutos = ptr_tm->tm_min;
    (*self)->segundos = ptr_tm->tm_sec;
  }

  return result;

}				/* Fin de GfiHora::GfiHora_Create_2 */

/**********************************************************************
 *
 * GfiHora::GfiHora_Create_3
 *
 *********************************************************************/

int
GfiHora_Create_3 (GfiHora *self, const long hora)
{
  long hora_local;
  int result;

#if defined(TRACE)
  (void) fprintf (stderr, "Entrando a \"GfiHora_Create_3\"\n");
#endif

  if (self == 0)
    return GFI_ERRC_NULLOBJ;

  result = GfiMalloc ((void **) self, sizeof (GfiHora_attr));
  if (result == GFI_ERRC_OK)
    {
      (*self)->hora = 0;
      (*self)->minutos = 0;
      (*self)->segundos = 0;

      if (hora < 100)
	{
	  (*self)->hora = hora;
	}
      else
	if (hora < 10000)
	{
	  (*self)->minutos = hora % 100;
	  (*self)->hora = hora / 100;
	}
	else
	  {
	    (*self)->segundos = hora % 100;
	    hora_local = hora / 100;
	    (*self)->minutos = hora_local % 100;
	    (*self)->hora = hora_local / 100;
	  }
    }

#if defined(TRACE)
  (void) fprintf (stderr, "Saliendo de \"GfiHora_Create_3\"\n");
#endif

  return (result);

}				/*  Fin de GfiHora::GfiHora_Create_3  */

/**********************************************************************
 *
 * GfiHora::GfiHora_Create_4
 *
 *********************************************************************/
/* Crea una instancia de la clase hora, apartir de una cadena de carecteres
   en el formato: hh:mm:ss */

int
GfiHora_Create_4 (GfiHora *self, const char *horac)
{
  int   result = GFI_ERRC_OK,
        hor = 0,
        min = 0,
        seg = 0;
  char *hora      = 0,
#if defined (DB2)
       *separador = ".",
#else
       *separador = ":",
#endif /*--- DB2 ---*/
       *separador_final = '\0',

       *indice    = 0;

#if defined(TRACE)
  (void) fprintf (stderr, "Entrando a \"GfiHora_Create_4\"\n");
#endif

  if (self == 0)
    return GFI_ERRC_NULLOBJ;
  else
    if (horac == 0)
      return GFI_ERRC_NULLPTR;
    else /*--- para considerar el caso: h:m:s en ORACLE y h.m.s en DB2 ---*/
      if (strlen (horac) < 5)
	return GFI_ERRC_BADARG;

  result = GfiMalloc ((void **)&hora, (strlen (horac)+1)*sizeof(char));
  if (result == GFI_ERRC_OK)
  {
    (void) strcpy (hora, horac);

#if defined (DB2)
    /*--- coloca el nulo porque la hora viene hh.mm.ss.nnnnn en DB2 --*/
    if (strlen (hora) > 8)
      *(hora + 8) = '\0';
#endif  /*--- DB2 ---*/

    /*--- Obtiene la hora ---*/
    indice = strtok (hora, separador);
    if (indice != 0)
      hor = atoi (indice);
    else
      result = GFI_ERRC_BADARG;

    /*--- Obtiene los minutos ---*/
    if (result == GFI_ERRC_OK)
    {
      indice = strtok (0, separador);
      if (indice != 0)
	min = atoi (indice);
      else
	result = GFI_ERRC_BADARG;
    }

    /*--- Obtiene los segundos ---*/
    if (result == GFI_ERRC_OK)
    {
      indice = strtok (0, separador_final);
      if (indice != 0)
	seg = atoi (indice);
      else
	result = GFI_ERRC_BADARG;
    }
  }

  if (result == GFI_ERRC_OK)
    result = GfiHora_Create_1 (self, hor, min, seg);

  if (hora != 0)
    (void) GfiFree ((void **) &hora);

#if defined(TRACE)
  (void) fprintf (stderr, "Saliendo de \"GfiHora_Create_4\"\n");
#endif

  return (result);

}				/*  Fin de GfiHora::GfiHora_Create_4  */

/**********************************************************************
 *
 * GfiHora::GfiHora_Delete
 *
 *********************************************************************/

int
GfiHora_Delete (GfiHora * self)
{
  int result = GFI_ERRC_OK;

  if (self == 0)
    return GFI_ERRC_NULLOBJPTR;
  else if (*self == 0)
    return result;

  GfiFree ((void **) self);

  return result;
}				/* Fin de GfiHora::GfiHora_Delete */

/**********************************************************************
 *
 * GfiHora::GfiHora_Hora
 *
 *********************************************************************/

int
GfiHora_Hora (const GfiHora self, int *hora)
{
  int result = GFI_ERRC_OK;

  if (self == 0)
    return GFI_ERRC_NULLOBJ;
  else if (hora == 0)
    return GFI_ERRC_NULLPTR;

  *hora = self->hora;

  return result;

}				/* Fin de GfiHora::GfiHora_Hora */

/**********************************************************************
 *
 * GfiHora::GfiHora_Minutos
 *
 *********************************************************************/

int
GfiHora_Minutos (const GfiHora self, int *minutos)
{
  int result = GFI_ERRC_OK;

  if (self == 0)
    return GFI_ERRC_NULLOBJ;
  else if (minutos == 0)
    return GFI_ERRC_NULLPTR;

  *minutos = self->minutos;

  return result;

}				/* Fin de GfiHora::GfiHora_Minutos */

/**********************************************************************
 *
 * GfiHora::GfiHora_Segundos
 *
 *********************************************************************/

int
GfiHora_Segundos (const GfiHora self, int *segundos)
{
  int result = GFI_ERRC_OK;

  if (self == 0)
    return GFI_ERRC_NULLOBJ;
  else if (segundos == 0)
    return GFI_ERRC_NULLPTR;

  *segundos = self->segundos;

  return result;

}				/* Fin de GfiHora::GfiHora_Segundos */

/**********************************************************************
 *
 * GfiHora::GfiHora_ComparaHoras
 *
 *********************************************************************/

int
GfiHora_ComparaHoras (const GfiHora self,
		      const GfiHora self2, int *valor)
{
  int result = GFI_ERRC_OK;

  if (self == 0 || self2 == 0)
    return GFI_ERRC_NULLOBJ;
  else if (valor == 0)
    return GFI_ERRC_NULLPTR;

  if (self->hora == self2->hora)
    if (self->minutos == self2->minutos)
      if (self->segundos == self2->segundos)
	*valor = 0;
      else if (self->segundos < self2->segundos)
	*valor = -1;
      else
	*valor = 1;
    else if (self->minutos < self2->minutos)
      *valor = -1;
    else
      *valor = 1;
  else if (self->hora < self2->hora)
    *valor = -1;
  else
    *valor = 1;

  return result;

}				/* Fin de GfiHora::GfiHora_ComparaHoras */

/**********************************************************************
 *
 * GfiHora::GfiHora_ComparaConHoraActual
 *
 *********************************************************************/

int
GfiHora_ComparaConHoraActual (const GfiHora self, int *valor)
{
  int result = GFI_ERRC_OK;
  time_t bintime;
  struct tm *ptr_tm;

  if (self == 0)
    return GFI_ERRC_NULLOBJPTR;
  else if (valor == 0)
    return GFI_ERRC_NULLPTR;

  (void) time (&bintime);
  ptr_tm = localtime (&bintime);

  if (self->hora == ptr_tm->tm_hour)
    if (self->minutos == ptr_tm->tm_min)
      if (self->segundos == ptr_tm->tm_sec)
	*valor = 0;
      else if (self->segundos < ptr_tm->tm_sec)
	*valor = -1;
      else
	*valor = 1;
    else if (self->minutos < ptr_tm->tm_min)
      *valor = -1;
    else
      *valor = 1;
  else if (self->hora < ptr_tm->tm_hour)
    *valor = -1;
  else
    *valor = 1;

  return result;

}			/* Fin de GfiHora::GfiHora_ComparaConHoraActual */

/**********************************************************************

 * GfiHora::GfiHora_CopiaHoraNueva
 *
 *********************************************************************/

int
GfiHora_CopiaHoraNueva (const GfiHora self, const GfiHora src)
{
  int result = GFI_ERRC_OK;

  if (self == 0 || src == 0)
    return GFI_ERRC_NULLOBJPTR;

  self->segundos = src->segundos;
  self->minutos = src->minutos;
  self->hora = src->hora;

  return result;

}                               /* Fin de GfiHora::GfiHora_CopiaHoraNueva */

/**********************************************************************
 *
 * GfiHora::GfiHora_2_Long
 *
 *********************************************************************/

int
GfiHora_2_Long (const GfiHora self, long *hour)
{

  int result = GFI_ERRC_OK;
  int hora, min, seg;

  if (self == 0)
    return GFI_ERRC_NULLOBJ;
  else
    if (hour == 0)
      return GFI_ERRC_NULLPTR;

  result = GfiHora_Hora (self, &hora);
  if (result == GFI_ERRC_OK)
  {
    result = GfiHora_Minutos (self, &min);
    if (result == GFI_ERRC_OK)
    {
      result = GfiHora_Segundos (self, &seg);
      if (result == GFI_ERRC_OK)
      {
	*hour = ((long) seg);
	*hour = (((long) min) * 100) + *hour;
	*hour = (((long) hora) *10000) + *hour;
      }
    }
  }

  return result;

}		                 /* Fin de GfiHora_2_Long */

/**********************************************************************
 *
 * GfiHora::GfiHora_2_String
 *
 *********************************************************************/

int
GfiHora_2_String (const GfiHora self, char *str, int str_len)
{

  int  result = GFI_ERRC_OK,
       hora, min, seg, i;
  char str_hora [ 3 ],
       str_min  [ 3 ],
       str_seg  [ 3 ];

  if (self == 0)
    return GFI_ERRC_NULLOBJ;
  else
    if (str == 0)
      return GFI_ERRC_NULLPTR;
    else
      if (str_len < 9)
	return GFI_ERRC_BADARG;

  for (i = 0; i < 9; i++)
    str [ i ] = '\0';

  result = GfiHora_Hora (self, &hora);
  if (result == GFI_ERRC_OK)
  {
    result = GfiHora_Minutos (self, &min);
    if (result == GFI_ERRC_OK)
    {
      result = GfiHora_Segundos (self, &seg);
      if (result == GFI_ERRC_OK)
      {
	(void) sprintf (str_hora, "%.2d", hora);
	(void) sprintf (str_min, "%.2d", min);
	(void) sprintf (str_seg, "%.2d", seg);
	(void) strcat(str, str_hora);
#if defined (DB2)
	(void) strcat(str, ".");
#else
	(void) strcat(str, ":");
#endif  /*--- DB2 ---*/
	(void) strcat(str, str_min);
#if defined (DB2)
	(void) strcat(str, ".");
#else
	(void) strcat(str, ":");
#endif
	(void) strcat(str, str_seg);
	str [ 8 ] = '\0';
      }
    }
  }

  return result;

}		                 /* Fin de GfiHora_2_String */

/**********************************************************************
 *
 * GfiHora::GfiHora_Print
 *
 *********************************************************************/

int
GfiHora_Print (const GfiHora self, FILE * stream)
{
  int result = GFI_ERRC_OK;

  if (self == 0)
    return GFI_ERRC_NULLOBJ;
  else if (stream == 0)
    return GFI_ERRC_NULLPTR;

  (void) fprintf (stream, "%d:%.2d:%.2d\n", self->hora,
		  self->minutos, self->segundos);

  return result;

}				/* Fin de GfiHora_Print */