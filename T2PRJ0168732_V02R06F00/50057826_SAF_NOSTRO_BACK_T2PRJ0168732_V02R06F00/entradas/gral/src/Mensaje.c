/*
////////////////////////////////////////////////////////////
//
//  Libreria:
//
//              Message.c
//
//  Creado por:
//
//              German Gutierrez Galan
//
//  Compania:
//
//              SOLTIS
//
//  Descripcion:
//
//              Libreria de Mensajes
//
////////////////////////////////////////////////////////////
*/
#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "../include/Mensaje.h"
#include "sys/errno.h"




int open ( int key , int opc )
{
int perms    = 0600;
int flag     = IPC_CREAT;
int flagSend = IPC_NOWAIT;
int id       = -1;
struct myMsg   msg;
pid_t pid;


   msg.text [ 0 ] = 0;
   msg.type  = 1;

   if ( ( id = msgget( key , flag | perms ) ) == -1 )
   {
      perror ( "openQueue" );
      return 1;
   }

   if ( opc )
   {
      while ( ( pid = getpid ( ) ) == -1 );
      sprintf ( msg.text , "%d" , pid );
   }
   else
      strcpy ( msg.text , "**SALI**" );

   printf ( "pid=(%s)\n" , msg.text );

   if ( msgsnd ( id , &msg , 94 , flagSend ) == -1 )
   {
      perror ( "sendMessage" );
      return 1;
   }
   return 0;


}

char * cadWithCero( short no )
{
   char aux[10];
   int j = 0;
   int i = 0;
   char *cad = 0;

   cad =(char*) malloc( sizeof ( char)*4 );

        sprintf( aux , "%d" , no );
        for ( i = 0; i < 3; i++)
        {
                if ( ( 3 - i ) <= ( (int) strlen ( aux ) ) )
                {
                        cad[i] = aux [ j ];
                        j++;

                }
                else
                        cad[i] = 48;
        }
        cad [ i ] = 0;
        return cad;
}

char * findNoFile( char *path , char * name )
{
  char *cad = 0;
  char *ext = 0;
  FILE *fp = 0;

  cad = (char*) malloc( sizeof ( char)* 200 );

  for ( int i = 0; i < 999; i++ )
  {
    ext = cadWithCero( i );
    sprintf ( cad , "%s/%s.%s" , path , name , ext );
    fp = fopen ( cad , "r" );
    free( ext );
    ext = 0;
    if ( fp == 0 )
    {
      printf ( "archivo(%s)\n" , cad );
      return cad;
    }
    fclose ( fp );
  }

  return 0;
}



