

extern int giNIVEL_TRACE;
/*=============================
 Fn Formatosaldo: Cambia punto(.) por coma(,)
 Si saldo es cero formatea como 0,
=============================*/
char *Formatosaldo(char *saldo)
{
  char  cSaldoFmt[17+1]="";
  char  *stpos=NULL;

   if( giNIVEL_TRACE >= 10 )
  {
    printf("Inicia Fn. Formatosaldo()\n");
    printf("saldo [%s]\n", saldo);
  }
  if( atof(saldo) != 0 )
  {
    stpos=strchr(saldo, PUNTO);
    if( giNIVEL_TRACE >= 10 )
    {
      printf("stpos [%s]\n", stpos);
    }

    if ( stpos != NULL )
    {
       *stpos=',';
       if (saldo[0] == COMA )
          strcpy(cSaldoFmt,"0");
       strcat(cSaldoFmt, saldo);
    }
    else if( strchr(saldo, COMA) == NULL )
    {
      strcpy(cSaldoFmt, saldo);
      strcat(cSaldoFmt,",0");
    }
    else
    {
      strcpy(cSaldoFmt, saldo);
    }
  }
  else
  {
    strcpy(cSaldoFmt,"0,00");
  }
  if ( giNIVEL_TRACE >= 10 )
  {
    printf("termina Fn. Formatosaldo(). cSaldoFmt [%s]\n", cSaldoFmt);
  }
  return cSaldoFmt;
}

/*===============================================
 *   Fn. BsmStrTok
 *     Desc: Obtiene los datos de una cadena,
 *===============================================*/
char *BsmStrTok(char *origen,char sep,int ntokens)
{
   static char buffer1[LONG_LINE] = "";
   char *buffer = 0;
   char *p1 = 0;
   int cont_sep = 0;

   buffer = (char *)&buffer1;

   strncpy(buffer,origen,LONG_LINE-1);
   *(buffer + LONG_LINE-1) = 0;

   while(*buffer >= 0x20){
      if(cont_sep == ntokens -1)
      {
        break;
      }
      if(*buffer == sep)
      {
         cont_sep++;
         if(cont_sep == (ntokens -1) )
         {
            buffer++;
            break;
         }
      }
      buffer++;
   }

   if( (*buffer == sep) || (*buffer < 0x20) )
   {
      *buffer = 0;
      return(buffer);
   }
   else{
      p1 = buffer;
      while( (*p1 >=0x20) && (*p1 != sep) )
      {
        p1++;
      }
      *p1 = 0;
   }
   return(buffer);
}

/*=================================================
   Fn Creanodo(): genera nodos para la lista ligada
===================================================*/
tMsgSwift *Creanodo(void)
{
  tMsgSwift *lptr= NULL;

  lptr= (tMsgSwift *)malloc(sizeof(tMsgSwift));
  if( lptr == NULL )
  {
    printf("Error al asignar memoria en Fn: Creanodo()\n");
    lptr=NULL;
  }
  return lptr;
}

/*======================
    Elimina Lista Ligada
======================*/
void BorraLista(void)
{
  if( !gphead )
  {
    return;
  }
  gpptr= gphead;
  for(;;)
  {
    gplist= gpptr;
    gpptr= gplist->next;
    if( !gpptr )
    {
      break;
    }
    free(gplist);
  }
  gphead= NULL;
  gplist= NULL;
  gpptr= NULL;
}

/*===============================================
 Fn. AddFieldSwift()
 Arma estructura de mensaje swift en lista
==================================================*/
int AddFieldSwift(char *campo, char *value1, char *value2)
{
  int iresult=0;

  gpptr= Creanodo();
  if( !gpptr )
  {
    iresult= -2;
  }

  if( !iresult )
  {
    if( !strcmp(gplist->eBo, "-}" ) )
    {
      memset(gplist->eBo, '\0', sizeof(gplist->eBo));
      /*==Conteo de bytes, se considera mas 1 por eq caracter ascii $==*/
      gi_numbytes=gi_numbytes + 1;
      ms_numbytes=ms_numbytes + 1;
    }

    gplist->next= gpptr;
    gplist= gpptr;
    /*==Los siguientes valores son nulos==*/
    memset(gplist->BHB, '\0', sizeof(gplist->BHB));
    memset(gplist->AHB, '\0', sizeof(gplist->AHB));
    memset(gplist->iBo, '\0', sizeof(gplist->iBo));

    /*==Conteo de bytes, se considera mas 2 por los carateres ascii 13 y 10==*/
    gi_numbytes=gi_numbytes + strlen(campo) + strlen(value1)
                  + strlen(value2) + 2;
    ms_numbytes=ms_numbytes + strlen(campo) + strlen(value1)
                  + strlen(value2) + 2;

    /*==los siguientes valores son el Msg Swift==*/
    strcpy(gplist->Field, campo);
    strcpy(gplist->Value1, value1);
    strcpy(gplist->Value2, value2);
    strcpy(gplist->eBo, "-}");
    gplist->next=NULL;
  }
  return iresult;
}

/*===================================================
   Fn: CreaHeaderSwift()
     Genera el Header del Mensaje Swift
=====================================================*/
int CreaHeaderSwift(char *inMsgSwift, char *inCveBicEnv, char *inCveBicSant)
{
  int iresult=0;

  if( gphead == NULL )
  {
    if( giNIVEL_TRACE >= 8 )
    {
      printf("Inicia Fn. CreaHeaderSwift()\n");
      printf("inMsgSwift [%s]\n", inMsgSwift);
      printf("inCveBicEnv[%s]\n", inCveBicEnv);
    }
    gpptr= Creanodo();
    if( !gpptr )
    {
      iresult= -1;
    }

    if( !iresult )
    {
      gplist= gpptr;
      /*==Basic Header Block==*/
      sprintf(gplist->BHB,"{1:F01%-12s0000000000}", inCveBicSant);

      /*==Application Header Block==*/
      if( strlen(inCveBicEnv) == 11 )
      {
        strcat(inCveBicEnv,"X");
      }
      sprintf(gplist->AHB,"{2:I%s%sN}", inMsgSwift, inCveBicEnv);

      /*==Initial Body ==*/
      strcpy(gplist->iBo,"{4:");

      /*==Conteo de bytes, se considera mas 2 por los carateres ascii 13 y 10==*/
      gi_numbytes=gi_numbytes + strlen(gplist->BHB) + strlen(gplist->AHB)
                  + strlen(gplist->iBo) + 2;
      ms_numbytes=ms_numbytes + strlen(gplist->BHB) + strlen(gplist->AHB)
                  + strlen(gplist->iBo) + 2;

      /*==los siguientes valores se dejan en null==*/
      memset(gplist->Field, '\0', sizeof(gplist->Field));
      memset(gplist->Value1, '\0', sizeof(gplist->Value1));
      memset(gplist->Value2, '\0', sizeof(gplist->Value2));
      memset(gplist->eBo, '\0', sizeof(gplist->eBo));

      gplist->next=NULL;
      gphead= gplist;
    }
    if( giNIVEL_TRACE >= 8 )
    {
      printf("Fn. CreaHeaderSwift(). gi_numbytes: %d ms_numbytes %d\n", gi_numbytes,ms_numbytes);
      printf("termina Fn. CreaHeaderSwift(). result: %d\n\n", iresult);
    }
  }
  return iresult;
}

/*======================================================================================
  Fn. GeneraMsgSwift()
  Escribe mensaje en swift en archivo
=========================================================================================*/
int GeneraMsgSwift(void)
{
  int  ienc=0;
  int  iresult=0;
  int giMaxSizeMsg=0;
  FILE *afMsgSw=NULL;

  if( giNIVEL_TRACE >= 5 )
  {
    printf("Inicia GeneraMsgSwift\n");
    printf("gi_first_message= %02d\n", gi_first_message);

  }

  gpptr= gphead;
  gplist= gphead;
  while(iresult==0)
  {
    if( ienc == 0 )
    {
      if( gi_first_message == 1 )
      {
        fprintf(afMsgSw,"%s","$" );
      }
      fprintf(afMsgSw, "%s%s%s%c%c",
              gplist->BHB, gplist->AHB,gplist->iBo,(char)13, (char)10);
      ienc= 1;
    }
    else
    {
      if( (strcmp(gplist->Field,":61:")==0) && (strlen(gplist->Value2) > 0) )
      {
        fprintf(afMsgSw, "%s%s%c%c%s%c%c", gplist->Field, gplist->Value1, (char)13, (char)10,
                gplist->Value2, (char)13, (char)10);
      }
      else if( (strcmp(gplist->Field,":86:")==0) && (strlen(gplist->Value2) > 0) )
      {
        fprintf(afMsgSw, "%s%s\\%c%c%s%c%c", gplist->Field, gplist->Value1,(char)13, (char)10,
                gplist->Value2, (char)13, (char)10);
      }
      else
      {
        fprintf(afMsgSw, "%s%s%c%c", gplist->Field, gplist->Value1, (char)13, (char)10);
      }
      fprintf(afMsgSw, "%s", gplist->eBo);
    }
    gpptr= gplist->next;
    if( !gpptr )
    {
      /*==Bandera en 1, indica que se escribio 1er mensaje==*/
      gi_first_message=1;
      break;
    }
    else
    {
      gplist= gpptr;
    }
    fflush(afMsgSw);
  }

  if (giMaxSizeMsg==1){
   gi_numbytes=0;
   giMaxSizeMsg=0;
   ms_numbytes=0;
   msMaxSizeMsg=0;
   gi_first_message=0;
   fclose (afMsgSw);
   printf ("\n\nAlcanzo el maximo tamano el archivo se envia a Swift [%s] y se crea siguiente archivo..........\n\n",gcnamefileSwiftSecuenciaExt);
   if( !iresult )
   {
     iresult= EnviaArchivoSwift(gcShellFtp, gcDirFtp, gcPathBin, gcnamefileSwiftSecuenciaExt);
     if( iresult )
     {
       printf("Error Fn. EnviaArchivoSwift(). result=%d\n", iresult);
     }
   }
   if( !iresult )
   {
      iresult=nombreArchivoSig();
   }
   if ( iresult==0 &&
        (afMsgSw=fopen(gcnamefileSwiftSecuenciaExt,"w"))==NULL){
        printf ("ERROR AL ABRIR EL ARCHIVO [[[%s]]]\n",gcnamefileSwiftSecuenciaExt);
      iresult=ERROR_OPEN_FILE;
   }
  }

  if( giNIVEL_TRACE >= 5 )
  {
    printf("Termina GeneraMsgSwift\n");
  }
  return iresult;
}

/*=================================================================
  Fn. EnviaArchivoSwift()
  Realiza la ejecucion del proceso para el envio del archivo Swift
=================================================================*/
int EnviaArchivoSwift(char *inShellFtp, char *inDirFtp, char *inPathBin, char *inArch)
{
  int iresult= GFI_ERRC_OK;
  char cmd[255]="";

  if( giNIVEL_TRACE >= 10 )
  {
    printf("Inicia Fn. EnviaArchivoSwift()\n");
    printf("ShellFtp [%s]\n", inShellFtp);
    printf("DirFtp   [%s]\n", inDirFtp);
    printf("PathBin  [%s]\n", inPathBin);
    printf("Arch     [%s]\n", inArch);

  }

  sprintf(cmd, "%s/%s %s %s %s", inPathBin, inShellFtp, inArch, inDirFtp, "940");
  iresult= EjecucionSystem("admfedocta", cmd);
  if( iresult )
  {
    printf("Fn. EnviaArchivoSwift(). Error al enviar archivo [%s]\n", inArch);
    printf("Fn. EnviaArchivoSwift(). cmd [%s]\n\n", cmd);
  }else{

    sprintf(cmd, "mv %s %s.pro",inArch,inArch);
    iresult= EjecucionSystem("admfedocta", cmd);
    if( iresult )
    {
      printf("Fn. EnviaArchivoSwift(). Error al renombrar archivo [%s]\n", inArch);
      printf("Fn. EnviaArchivoSwift(). cmd [%s]\n\n", cmd);
    }
  }

  if( giNIVEL_TRACE >= 10 )
  {
    printf("Termina Fn. EnviaArchivoSwift(). result: %d\n", iresult);
  }
  return iresult;
}

/*===============================================
  Fn. EjecucionSystem()
  Realiza la ejecucion de un comando, atraves de la
  funcion system
================================================*/
int EjecucionSystem( char *nombre, char *comando)
{
  int resultado_system;
  char texto_error[ 502];
  char texto_nombre[ 101];
  char texto_comando[ 341];
  int longitud_nombre;

  resultado_system = system( comando);

  if ( resultado_system > 255)
  {
    resultado_system /= 256;
  }
  if ( resultado_system != 0)
  {
    if ( (strlen( nombre) + strlen( comando)) > 440)
    {
      if ( strlen( nombre) > 100)
      {
        strncpy( texto_nombre, nombre, 100);
        texto_nombre[ 100] = '\0';
      }
      else
      {
        strcpy( texto_nombre, nombre);
      }
      longitud_nombre = strlen( texto_nombre);
      if ( strlen( comando) > (340 + 100 - longitud_nombre))
      {
        strncpy( texto_comando, comando, (340 + 100 - longitud_nombre));
        texto_comando[ (340 + 100 - longitud_nombre)] = '\0';
      }
      else
      {
        strcpy( texto_comando, comando);
      }
    }
    else
    {
      strcpy( texto_nombre, nombre);
      strcpy( texto_comando, comando);
    }

    if ( resultado_system != -1)
    {
      printf( "%s error dentro del comando ejecutado >%s<\n", texto_nombre, texto_comando);
    }
    else
    {
      sprintf( texto_error, "%s error de sistema operativo al ejecutar el system para >%s< ", texto_nombre, texto_comando);
      perror( texto_error);
    }
  }

  return( resultado_system);
}


/*=======================================================
  Fn: CreaEncSigPag()
  Crea Encabezado de pagina siguiente de un mensaje Swift
=======================================================*/
int CreaEncSigPag(char *inTypeMsg, char *inBicDest, char *inBicBco, char *inCta,
        char *inFchSist, int inSec, int inNumPag, double inSdoIni, char *inFchsdoini,
        char *inDiv)
{
  int iresult=GFI_ERRC_OK;
  char cValue1[101]="", cValue2[101]="", csdoini[20]="";

   /*==Header Mensaje Swift==*/
  iresult= CreaHeaderSwift(inTypeMsg, inBicDest, inBicBco);

  /*== Tag 20 Transaction Reference Number ==*/
  if( !iresult )
  {
    if( strlen(inCta) > 9 )
    {
      strncpy(cValue1, inCta, 9);
      cValue1[9]='\0';
      sprintf(cValue1,"%s/%s",cValue1, inFchSist);
    }
    else
    {
      sprintf(cValue1,"%s/%s",inCta, inFchSist);
    }
    if( giNIVEL_TRACE >= 8 )
    {
      printf(":20:%s\n", cValue1);
    }
    iresult=AddFieldSwift(":20:",cValue1, cValue2);
  }
  /*== Tag 25 Account Identification ==*/
  if( !iresult )
  {
    strcpy(cValue1, inCta);
    if( giNIVEL_TRACE >= 8 )
    {
      printf(":25:%s\n", cValue1);
    }
    iresult=AddFieldSwift(":25:",cValue1, cValue2);
  }
  /*== Tag 28 Account Identification==*/
  if( !iresult )
  {
    sprintf(cValue1,"%d/%04d", inSec, inNumPag);
    if( giNIVEL_TRACE >= 8 )
    {
      printf(":28C:%s\n", cValue1);
    }
    iresult=AddFieldSwift(":28C:",cValue1, cValue2);
  }
  /*== Tag 60a Opening Balance ==*/
  if( !iresult )
  {
    if( inSdoIni >= 0.00 ) /*=Credit=*/
    {
      sprintf(csdoini, "%.2f", inSdoIni);
      sprintf(cValue1,"C%s%s%s", inFchsdoini, inDiv, Formatosaldo(csdoini));
    }
    else /*=Debit=*/
    {
      inSdoIni= inSdoIni * (-1);
      sprintf(csdoini,"%.2f", inSdoIni);
      sprintf(cValue1,"D%s%s%s", inFchsdoini, inDiv, Formatosaldo(csdoini));
    }

    if( giNIVEL_TRACE >= 8 )
    {
      printf(":60M:%s\n", cValue1);
    }
    iresult=AddFieldSwift(":60M:",cValue1, cValue2);
  }

  return iresult;
}

int nombreArchivoSig(void){
   FILE   *fp=NULL;
   int    isecuencia=0;
   int    iresult=0;
   DIR    *directorio = NULL;
   struct dirent *dentry;
   char   listaNomArch [400][100];
   char   nombreTmp [100];
   size_t i=1;
   int    numArch=0;
   int    ciclo=0;
   char   path_swift[100]="";
   char   nombre_swift[100];
   char   *apNombre;
   printf ("Inicio de programa nombreArchivoSig\n");
   memset (listaNomArch, '\0', (sizeof (char) * (400*100)));
   printf ("busqueda de caracter [%s]\n",gcnamefileSwift);
   apNombre=strrchr(gcnamefileSwift,'/');
   apNombre++;
   printf ("Encontrado [%s]\n",apNombre);
   strncpy(path_swift,gcnamefileSwift,apNombre-gcnamefileSwift);
   path_swift[apNombre-gcnamefileSwift]='\0';
   strcpy(nombre_swift,apNombre);
   printf ("PATH    [[%s]]\n",path_swift);
   printf ("NOMBRE  [[%s]]\n",nombre_swift);
   directorio = opendir(path_swift);
   if (directorio == NULL){
      iresult = ERROR_OPEN_DIR;
   }
   while( iresult == 0 &&
         (dentry=readdir(directorio))!=NULL)
   {
      if ( strlen (dentry->d_name) >= (strlen (nombre_swift)) ){
         if ( strncmp (dentry->d_name,nombre_swift,strlen(nombre_swift)) == 0 ){
            strncpy (listaNomArch[numArch],dentry->d_name,(strlen(nombre_swift)+3));
            listaNomArch[numArch][(strlen(nombre_swift)+3)]='\0';
            printf ("Agregado a la lista : >%s<",listaNomArch[numArch]);
            numArch++;
         }
      }
   }
   closedir(directorio);
   gcnamefileSwiftSecuenciaExt[0]='\0';
   if ( iresult == 0 )
   {
      do{
         sprintf(nombreTmp,"%s%03d",nombre_swift,isecuencia);
         ciclo =0;
         while (ciclo < numArch ){
            printf ("COMPARANDO : >%s< Vs >%s<\n",nombreTmp,listaNomArch[ciclo]);
            if ( strcmp(nombreTmp,listaNomArch[ciclo]) == 0 )
            {
               break;
            }
            ciclo ++;
         }
         if (ciclo == numArch){
            sprintf(gcnamefileSwiftSecuenciaExt,"%s%s%03d_%s%s",path_swift,nombre_swift,isecuencia,HoraSys(),gcExtArch);
            printf ("nombreArchivoSig : >%s<\n",gcnamefileSwiftSecuenciaExt);
         }
         isecuencia++;
      }while ( gcnamefileSwiftSecuenciaExt[0]== '\0' && isecuencia < 400 );
   }
   if (isecuencia >= 400){
      iresult=ERROR_FIND_FILE;
   }
   printf ("Salida de nombreArchivoSig %d >%s<\n",iresult,gcnamefileSwiftSecuenciaExt);
   return iresult;
}

char *HoraSys (void)
{
   time_t FechaHoy  = time(0);
   struct tm *tlocal;
   char   horaSys[8];

   tlocal = localtime(&FechaHoy);
   strftime(horaSys,12,"%H%M%S",tlocal);
   return horaSys;
}