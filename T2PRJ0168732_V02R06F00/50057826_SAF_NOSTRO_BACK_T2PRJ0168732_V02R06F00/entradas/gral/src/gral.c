/**********************************************************************
 *
 * Grupo Financiero InverMexico
 *
 * Archivo     : gral.c
 *
 * Proposito   : Definicion de Rutinas comunes.
 *
 * Autor       : Enrique Ibarra A.
 *
 * Supervision : Enrique Ibarra A.
 *
 **********************************************************************/

#include  "gral.h"
#include  <stdio.h>
#include  <string>
#include  <stdlib.h>
#include  <ctype.h>
#include  <time.h>
#include  "gfi-err.h"

/* Identificacion para el control de versiones RCS */

/*
   static char RCS_GRAL_C[] = "$Id$";
*/

/* Mensajes de Log de RCS
 *
 * $Log$
 *
 */

#define STR_LEN  512

static char STR1[STR_LEN], STR2[STR_LEN];

/******************************************************************
 *
 * FUNCTION: GfiFree
 *
 *******************************************************************/

void
GfiFree (void **mem)
{
  if (mem == 0 || *mem == 0)
    return;

#if defined(__cplusplus) || defined(c_plusplus)

  delete [] *mem;

#else

  (void) free (*mem);

#endif

  *mem = 0;

} /* End of function GfiFree */

/******************************************************************
 *
 * FUNCTION: GfiMalloc
 *
 *******************************************************************/

int
GfiMalloc (void **mem, const int size)
{
  int result = GFI_ERRC_OK;

  if (mem == 0)
    return GFI_ERRC_NULLPTR;

  *mem = 0;

  if (size <= 0)
    result = GFI_ERRC_BADARG;
  else
  {

#if defined (__cplusplus) || defined (c_plusplus)

    *mem = new char [ size ];

#else

    *mem = (void *) malloc (size);

#endif

    if (*mem == 0)
      result = GFI_ERRC_NOMEM;
  }

  return result;

}				/* end of function GfiMalloc */

/******************************************************************
 *
 * FUNCTION: GfiStrcmp
 *
 *******************************************************************/

int
GfiStrcmp (const char *s1, const char *s2)
{
  char *a;
  int i, result;

  if (s1 != 0 && s2 != 0)
  {
    (void) strncpy (STR1, s1, STR_LEN);
    (void) strncpy (STR2, s2, STR_LEN);
    STR1[STR_LEN - 1] = STR2[STR_LEN - 1] = '\0';

    for (a = STR1, i = 0; *a && i < STR_LEN; ++a, ++i)
      if (isalpha ((int) *a) && isupper ((int) *a))
	*a = (char) tolower ((int) *a);

    for (a = STR2, i = 0; *a && i < STR_LEN; ++a, ++i)
      if (isalpha ((int) *a) && isupper ((int) *a))
	*a = (char) tolower ((int) *a);

    result = strcmp (STR1, STR2);
  }
  else if (s1 == 0 && s2 == 0)
    result = 0;
  else if (s1 == 0)
    result = -1;
  else
    result = 1;

  return result;

}				/* end of function GfiStrcmp */


/**********************************************************************
 *
 * General  Gfi_StrLTrim
 *
 **********************************************************************/

int Gfi_StrLTrim (char *cadena)
{
  int   result = GFI_ERRC_OK;
  char  *p;

#if defined(TRACE)
  (void) fprintf (stderr, "Entrando a \"Gfi_StrLTrim\"\n");
#endif

  if (cadena == 0)
    result = GFI_ERRC_NULLPTR;

  if (result == GFI_ERRC_OK)
  {
    for (p = cadena; *p == ' '; p++)
      ;

    (void) strcpy (cadena, p);
  }

#if defined(TRACE)
  (void) fprintf (stderr, "Saliendo de \"Gfi_StrLTrim\", resultado= %d\n",
		  result);
#endif

  return (result);
}				/*  Fin de la operacion Gfi_StrLTrim  */


/**********************************************************************
 *
 * General  Gfi_StrRTrim
 *
 **********************************************************************/

int
Gfi_StrRTrim (char *str_regreso)
{

  int result = GFI_ERRC_OK;
  short i, found = 0, tamano;

#if defined(TRACE)
  (void) fprintf (stderr, "Entrando a \"Gfi_StrRTrim\"\n");
#endif

  if (str_regreso == 0)
    result = GFI_ERRC_NULLPTR;

  if (result == GFI_ERRC_OK)
  {
    tamano = strlen (str_regreso) - 1;
    for (i = tamano; i >= 0 && found == 0; --i)
    {
      if (str_regreso[i] != ' ')
      {
	found = 1;
	if (i != tamano)
	  str_regreso[i + 1] = '\0';
      }
    }

    if (found == 0 && i < 0)
      str_regreso[0] = '\0';
  }

#if defined(TRACE)
  (void) fprintf (stderr, "Saliendo de \"Gfi_StrRTrim\", resultado= %d\n",
		  result);
#endif

  return (result);
}				/*  Fin de la operacion Gfi_StrRTrim  */
