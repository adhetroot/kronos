///////////////////////////////////////////////////////////////////
//                                                               //
//  Programa auxiliar para manejo TRANSACCIONAL de los Procesos  //
//                                                               //
//  CTRANSACCIONALCDL.H                                          //
//                          Heriberto Guapo (G. Tecnis) Mar-2010 //
//                                                               //
///////////////////////////////////////////////////////////////////

#ifndef CTRANSACCIONALCDL_H
#define CTRANSACCIONALCDL_H

#if defined(__cplusplus) || defined(c_plusplus)
extern "C"
{
#endif

int  DeclaraCursorsH(void);
int  ProcesaBaja( char *clave_inst,  char *clave_sis, char *clave_rast, char *clave_tran,
                  char *fecha_recep, char *tipo,      char *status );
int  ProcesaSala( char *clave_inst,  char *clave_sis,     char *clave_rast,   char *clave_tran,
                  char *fecha_recep, char *tipo,          char *folio_origen, char *tipo_oper,
                  char *banco_ord,   char *cve_banco_ord, char *nombre_ord,   char *cuenta_ord,
                  char *banco_ben,   char *cve_banco_ben, char *nombre_ben,   char *cuenta_ben,
                  double importe,    char *observacion,   char *ref_concil,   char *ref_med,
                  char *fecha_cap,   char *fecha_venc,    char *fecha_flujo,  char *fecha_reg,
                  char *fecha_baja,  char *status );
int  ProcesaReal( char *clave_inst,  char *clave_sis,     char *clave_rast,   char *clave_tran,
                  char *fecha_recep, char *tipo,          char *folio_origen, char *tipo_oper,
                  char *banco_ord,   char *cve_banco_ord, char *nombre_ord,   char *cuenta_ord,
                  char *banco_ben,   char *cve_banco_ben, char *nombre_ben,   char *cuenta_ben,
                  double importe,    char *observacion,   char *ref_concil,   char *ref_med,
                  char *fecha_cap,   char *fecha_venc,    char *fecha_flujo,  char *fecha_reg,
                  char *fecha_baja,  char *status );
int  ProcesaProg( char *clave_inst,  char *clave_sis,     char *clave_rast,   char *clave_tran,
                  char *fecha_recep, char *tipo,          char *folio_origen, char *tipo_oper,
                  char *banco_ord,   char *cve_banco_ord, char *nombre_ord,   char *cuenta_ord,
                  char *banco_ben,   char *cve_banco_ben, char *nombre_ben,   char *cuenta_ben,
                  double importe,    char *observacion,   char *ref_concil,   char *ref_med,
                  char *fecha_cap,   char *fecha_venc,    char *fecha_flujo,  char *fecha_reg,
                  char *fecha_baja,  char *status );
int  ProcesaProgBaja( char *clave_inst,  char *clave_sis, char *clave_rast, char *clave_tran,
                      char *fecha_recep, char *tipo,      char *status );
int  ProcesaSubTipo( char *clave_inst, char *clave_sis, char *clave_rast, char *clave_tran, char *fecha_recep,
                     char *tipo);
int  CargaContrapartes(char *clave_inst,  char *clave_sis); // HGT 26-May-2010

#if defined(__cplusplus) || defined(c_plusplus)
}
#endif

#endif
