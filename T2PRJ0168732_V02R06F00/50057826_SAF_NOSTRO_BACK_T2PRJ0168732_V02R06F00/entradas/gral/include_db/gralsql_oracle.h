
/**********************************************************************
 *
 * Grupo Financiero InverMexico
 *
 * Archivo include comun a todos los proyectos
 *
 * Archivo     : gralsql_oracle.h
 *
 * Proposito   : Definicion de includes comunes.
 *
 * Autor       : Antonio Gerardo Cano D.
 *
 * Supervision : Enrique Ibarra A.
 *
 * Historia    :  29-Julio-1996 -> Creacion
 *
 **********************************************************************/

#if !defined (_GRALSQLINCL_H_)
#define _GRALSQLINCL_H_

/* Seccion de Includes */

#include  <stdio.h>
#include  <string.h>
#include  <string>
#include  <stdlib.h>
#include  <ctype.h>
#include  <time.h>
#include  <gfi-err.h>


/* Identificacion para el control de versiones RCS */

/* static char RCS_GRALSQL_H[] = "$Id$"; */

/* Mensajes de log de RCS
 *
 * $Log$
 */

#if defined(__cplusplus) || defined(c_plusplus)
extern "C"
{
#endif

#define          SQL_VALOR_U_VERSION           "!"
#define          SQL_LONG_U_VERSION             2
#define          SQL_LONG_FECHAS                26


/* Funciones utilitarias comunes */

int ChecaErrorOracle (const int,  const int, long *);

int OpenDB (char *str, int str_len);

int CloseDB (void);

int ChecaErrorMsg(void);

#if defined(__cplusplus) || defined(c_plusplus)
}
#endif

#endif
