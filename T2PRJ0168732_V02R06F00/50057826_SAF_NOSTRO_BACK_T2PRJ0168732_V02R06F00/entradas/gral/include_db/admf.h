/*****************************************************/
/* admf.h                                            */
/* Autor : Victor Hugo Guti‰rrez Ch.                 */
/* Fecha : 12 de Noviembre de 2002                   */
/*                                                   */
/*****************************************************/
#include <time.h>
#include <math.h>
#include <iostream>
#include <gfi-err.h>

/*****************************************************/
/*           Variables                               */
/*****************************************************/


/*
#define PATH_RECEPCION_DEFAULT  "../recepcion"
#define PATH_ENVIOS_DEFAULT     "../envio"
#define PATH_SHELLS_DEFAULT     "../shells"
#define PATH_CFG_DEFAULT        "../cfg"
#define PATH_LOG_DEFAULT        "../"
#define PATH_BIN                "."
*/


#define PATH_RECEPCION_DEFAULT  "/plancdl/procesos/admf"
#define PATH_ENVIOS_DEFAULT     "/plancdl/procesos/admf"
#define PATH_SHELLS_DEFAULT     "/plancdl/procesos/admf"
#define PATH_CFG_DEFAULT        "/plancdl/procesos/admf"
#define PATH_LOG_DEFAULT        "/plancdl/procesos/admf"
#define PATH_BIN                "/afs/gfi/altair/mdinero/upgrade_sw_base/saf/bin"


int strcount(char *);


//////////////////////////////////////////////////////////////////////
//           SUBSTR                                                 //
// Parametros : char[] .- Cadena de donde se obtendra la subcadena. //
//              int    .- Posicion inicial.                         //
//              int    .- numero de caracteres a extraer.           //
//              char[] .- cadena resultante.                        //
//////////////////////////////////////////////////////////////////////
void substr(char *cadena, int ini, int fin, char *resultado)
{
 int i, long_cadena, primera;

 primera = 0;
 resultado[0] = '\0';
 long_cadena = strlen(cadena);
 if (long_cadena < (fin+ini))
	 printf( "\n Error: La longitud de la cadena %s es menor a la posicion definida %d \n", cadena, long_cadena);
    /* Optimizacion a migracion linux
		cout << "\n Error: La longitud de la cadena " << cadena << " es menor a la posicion definida " << long_cadena << "\n";
	*/
 else
    {
     for (i=ini; i < ini+fin; i++)
        {
         if (primera == 0)
            {
             sprintf(resultado, "%c", cadena[i]);
             primera = 1;
            }
         else
            sprintf(resultado, "%s%c", resultado, cadena[i]);
        }
    }
}

//////////////////////////////////////////////////////////////////////
//           strcount                                               //
//                                                                  //
//////////////////////////////////////////////////////////////////////
int strcount(char *cadena)
{
 int i = 0;

 while (cadena[i] != '\0')
   i++;
 return(i+1);
}


///////////////////////////////////////////////////////////////////
//                                                               //
//  Funcion:      Obtiene_hora_actual                            //
//                                                               //
///////////////////////////////////////////////////////////////////
void Obtiene_hora_actual(char *HH_MM_SS)
  {
   char    str_hora_actual[12];
   HH_MM_SS[0]='\0';
   GfiHora hora_actual;
   if (GfiHora_Create_2(&hora_actual) == GFI_ERRC_OK)
    {
     if (GfiHora_2_String(hora_actual, str_hora_actual,
         SQL_LONG_FECHAS) == GFI_ERRC_OK)
      {
       Gfi_StrRTrim(str_hora_actual);
       strcpy(HH_MM_SS,str_hora_actual);
      }
     GfiHora_Delete(&hora_actual);
    }
   else
    fprintf(stderr,"Error al obtener la hora del sistema\n");
  }



///////////////////////////////////////////////////////////////////
//                                                               //
//  Funcion : StrRTrim                                           //
//                                                               //
///////////////////////////////////////////////////////////////////
char *StrRTrim(char *st)
 {
  int i;
  i=strlen(st)-1;
  while ((i>0) && (st[i]==' '))
   --i;
  st[i+1]='\0';
  return st;
 }

///////////////////////////////////////////////////////////////////
//                                                               //
//  Funcion : crea_archivo                                       //
//                                                               //
///////////////////////////////////////////////////////////////////
FILE *crea_archivo(char *archivo,char *arch, int contador)
{
 FILE *Swift;

 sprintf(archivo, "%s/%s.%03i", PATH_ENVIOS_DEFAULT, arch, contador);
 if ((Swift=fopen(archivo, "w")) == NULL)
	 printf("No se pudo crear el archivo %s \n",archivo);
	 /* Optimizacion a migracion linux
		cout << "No se pudo crear el archivo " << archivo << " \n";
	*/
 return (Swift);
}


///////////////////////////////////////////////////////////////////
//                                                               //
//  Funcion : chrustr     (Busca ultimo caracter)                //
//                                                               //
///////////////////////////////////////////////////////////////////
int chrustr(char *cadena, char caracter[2])
{
 int i;

 i = strlen(cadena)-1;
 while (i > -1)
   {
    if (cadena[i] == caracter[0])
       break;
    else
       i--;
   }
 if (i == -1)
    return (-1);
 else
   return (i);
}




///////////////////////////////////////////////////////////////////
//                                                               //
//  Funcion : chrpstr     (Busca primer caracter)                //
//                                                               //
///////////////////////////////////////////////////////////////////
int chrpstr(char *cadena)
{
 int i;

 i = 0;
while (cadena[i] != '\0')
   {
    if (cadena[i] == '\n' || cadena[i] == '\r')
       break;
    else
       i++;
   }
 if (cadena[i] == '\0')
    return (-1);
 else
    return (i);
}


///////////////////////////////////////////////////////////////////
//                                                               //
//  Funcion : VariablesCfg   (Extrae datos del archivos cfg)     //
//                                                               //
///////////////////////////////////////////////////////////////////
int VariablesCfg(char *Archivo, char *Variable, char *Valor)
{
   FILE *CFG;
   char CadValor[120] = "";
   char buffer[51]    = "";
   int i;

   int result = GFI_ERRC_OK;

   if((CFG=fopen(Archivo,"r")) == NULL)
   {
	    printf("Error no se encontro el achivo de configuracion %s \n",Archivo);
      /* Optimizacion a migracion linux
		cout << "Error no se encontro el achivo de configuracion " << Archivo << "\n";
	  */
      result = GFI_ERRC_NULLPTR;
   }

   /*************************************/
   /* Extrae valor                      */
   /*************************************/
   do
   {
      buffer[0] = 0;
      fgets(buffer,50,CFG);
   }while(!strstr(buffer,Variable) && buffer[0] != 0);


   fgets(buffer,50,CFG);
   for(i=0;buffer[i]!='\n' && buffer[i]!=13 && buffer[i]!=10;++i)
      CadValor[i]=buffer[i];

   CadValor[i]=0;
   strcpy(Valor,CadValor);

   if (CadValor[0] == 0)
   {
	   printf("Error no se encontro el valor para la variable %s \n",Variable);
      /* Optimizacion a migracion linux
		cout << "Error no se encontro el valor para la variable " << Variable << "\n";
	*/
      result = GFI_ERRC_NULLPTR;
   }

   fclose(CFG);
   return (result);
}