////////////////////////////////////////////////////////////
//
//  Headers:
//
//              admfcbocaducteb.h
//
//  Creado por:
//
//              Roberto Villa Villalobos
//
//  Compania:
//
//              Grupo Tecnis
//
////////////////////////////////////////////////////////////


#ifndef ADMFCBOCADUCTEB_H
#define ADMFCBOCADUCTEB_H

#include <sys/time.h>

#if defined(__cplusplus) || defined(c_plusplus)
extern "C"
{
#endif

void SpAdmfClaveBancos(char[], char[]);

void SpAdmfChecaAduana(char[], char[], char [], char[], double,
                       char[], char[], char[], char [], char&);

void SpAdmfVosCteBen(char[], char[], char[],
                     char[], char[], int&);

//HGT Agosto-2008
void SpAdmfValidaCMP(char[], char[], char[], char[],
                     char[], int&, int&, int&, int); //HGT Octubre-2008

//HGT Septiembre-2008
int SpAdmfRegEviMsj(char [], char [], char& ,  char [],
                    char [], char [], char [], double ,
                    char [], char [], char [], char [],
                    char [], char [], char [], char [],
                    char [], char [], char [],
                    char [], char [], char [],
                    char [], char []);


					
#if defined(__cplusplus) || defined(c_plusplus)
}
#endif

#endif
