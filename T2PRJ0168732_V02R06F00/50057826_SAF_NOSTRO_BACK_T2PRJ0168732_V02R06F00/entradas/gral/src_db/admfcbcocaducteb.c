
/* Result Sets Interface */
#ifndef SQL_CRSR
#  define SQL_CRSR
  struct sql_cursor
  {
    unsigned int curocn;
    void *ptr1;
    void *ptr2;
    unsigned int magic;
  };
  typedef struct sql_cursor sql_cursor;
  typedef struct sql_cursor SQL_CURSOR;
#endif /* SQL_CRSR */

/* Thread Safety */
typedef void * sql_context;
typedef void * SQL_CONTEXT;

/* Object support */
struct sqltvn
{
  unsigned char *tvnvsn; 
  unsigned short tvnvsnl; 
  unsigned char *tvnnm;
  unsigned short tvnnml; 
  unsigned char *tvnsnm;
  unsigned short tvnsnml;
};
typedef struct sqltvn sqltvn;

struct sqladts
{
  unsigned int adtvsn; 
  unsigned short adtmode; 
  unsigned short adtnum;  
  sqltvn adttvn[1];       
};
typedef struct sqladts sqladts;

static struct sqladts sqladt = {
  1,1,0,
};

/* Binding to PL/SQL Records */
struct sqltdss
{
  unsigned int tdsvsn; 
  unsigned short tdsnum; 
  unsigned char *tdsval[1]; 
};
typedef struct sqltdss sqltdss;
static struct sqltdss sqltds =
{
  1,
  0,
};

/* File name & Package Name */
struct sqlcxp
{
  unsigned short fillen;
           char  filnam[38];
};
static const struct sqlcxp sqlfpn =
{
    37,
    "../../gral/src_db/admfcbcocaducteb.pc"
};


static unsigned int sqlctx = 311048261;


static struct sqlexd {
   unsigned long  sqlvsn;
   unsigned int   arrsiz;
   unsigned int   iters;
   unsigned int   offset;
   unsigned short selerr;
   unsigned short sqlety;
   unsigned int   occurs;
      const short *cud;
   unsigned char  *sqlest;
      const char  *stmt;
   sqladts *sqladtp;
   sqltdss *sqltdsp;
   unsigned char  **sqphsv;
   unsigned long  *sqphsl;
            int   *sqphss;
            short **sqpind;
            int   *sqpins;
   unsigned long  *sqparm;
   unsigned long  **sqparc;
   unsigned short  *sqpadto;
   unsigned short  *sqptdso;
   unsigned int   sqlcmax;
   unsigned int   sqlcmin;
   unsigned int   sqlcincr;
   unsigned int   sqlctimeout;
   unsigned int   sqlcnowait;
            int   sqfoff;
   unsigned int   sqcmod;
   unsigned int   sqfmod;
   unsigned int   sqlpfmem;
   unsigned char  *sqhstv[33];
   unsigned long  sqhstl[33];
            int   sqhsts[33];
            short *sqindv[33];
            int   sqinds[33];
   unsigned long  sqharm[33];
   unsigned long  *sqharc[33];
   unsigned short  sqadto[33];
   unsigned short  sqtdso[33];
} sqlstm = {13,33};

// Prototypes
extern "C" {
  void sqlcxt (void **, unsigned int *,
               struct sqlexd *, const struct sqlcxp *);
  void sqlcx2t(void **, unsigned int *,
               struct sqlexd *, const struct sqlcxp *);
  void sqlbuft(void **, char *);
  void sqlgs2t(void **, char *);
  void sqlorat(void **, unsigned int *, void *);
}

// Forms Interface
static const int IAPSUCC = 0;
static const int IAPFAIL = 1403;
static const int IAPFTL  = 535;
extern "C" { void sqliem(unsigned char *, signed int *); }

 static const char *sq0012 = 
"select NO_CRITERIO ,DS_BCOORD ,CUENTA_ID ,REFERENCIA ,TO_CHAR(TO_DATE(FEC_VA\
L,'dd/mm/yyyy'),'yyyymmdd') ,IMPORTE_DE ,IMPORTE_A ,DS_BCO_BEN ,CTA_BENVOS ,CT\
A_BENCLI ,DS_CLIBEN  from ADMF_ADUANA_TRASPASOS            ";

typedef struct { unsigned short len; unsigned char arr[1]; } VARCHAR;
typedef struct { unsigned short len; unsigned char arr[1]; } varchar;

/* cud (compilation unit data) array */
static const short sqlcud0[] =
{13,4146,1,0,0,
5,0,0,1,105,0,4,204,0,0,2,1,0,1,0,2,3,0,0,1,97,0,0,
28,0,0,2,120,0,4,213,0,0,2,1,0,1,0,2,97,0,0,1,97,0,0,
51,0,0,3,130,0,4,224,0,0,2,1,0,1,0,2,3,0,0,1,97,0,0,
74,0,0,4,145,0,4,233,0,0,2,1,0,1,0,2,97,0,0,1,97,0,0,
97,0,0,5,133,0,4,248,0,0,2,1,0,1,0,2,3,0,0,1,97,0,0,
120,0,0,6,155,0,4,257,0,0,2,1,0,1,0,2,97,0,0,1,97,0,0,
143,0,0,7,103,0,4,342,0,0,2,1,0,1,0,2,3,0,0,1,97,0,0,
166,0,0,8,120,0,4,350,0,0,2,1,0,1,0,2,97,0,0,1,97,0,0,
189,0,0,9,139,0,3,372,0,0,2,2,0,1,0,1,97,0,0,1,97,0,0,
212,0,0,10,0,0,29,378,0,0,0,0,0,1,0,
227,0,0,11,221,0,4,384,0,0,3,1,0,1,0,2,97,0,0,2,3,0,0,1,97,0,0,
254,0,0,12,213,0,9,419,0,0,0,0,0,1,0,
269,0,0,12,0,0,13,423,0,0,11,0,0,1,0,2,3,0,0,2,9,0,0,2,9,0,0,2,9,0,0,2,9,0,0,2,
4,0,0,2,4,0,0,2,9,0,0,2,9,0,0,2,9,0,0,2,9,0,0,
328,0,0,12,0,0,15,496,0,0,0,0,0,1,0,
343,0,0,13,1047,0,3,923,0,0,33,33,0,1,0,1,9,0,0,1,9,0,0,1,9,0,0,1,9,0,0,1,9,0,
0,1,9,0,0,1,9,0,0,1,9,0,0,1,9,0,0,1,9,0,0,1,1,0,0,1,9,0,0,1,97,0,0,1,9,0,0,1,9,
0,0,1,4,0,0,1,9,0,0,1,9,0,0,1,9,0,0,1,9,0,0,1,9,0,0,1,9,0,0,1,97,0,0,1,9,0,0,1,
97,0,0,1,97,0,0,1,9,0,0,1,97,0,0,1,97,0,0,1,9,0,0,1,9,0,0,1,97,0,0,1,97,0,0,
490,0,0,14,0,0,29,946,0,0,0,0,0,1,0,
};


#line 1 "../../gral/src_db/admfcbcocaducteb.pc"
///////////////////////////////////////////////////////////////////
//                                                               //
//  Conjunto de stored procdures auxiliares                      //
//                                                               //
//                                                               //
//   ADMFCBCOCADUCTEB.PC              Roberto Villa Villalobos   //
//                    DHJ 10-04-2008 (Correccion cmp57 o cmp58)  //
//                   HGT Agosto-2008 (Se incluye nueva funcion   //
//                                    SpAdmfValidaCMP)           //
//               HGT Septiembre-2008 (Se incluye nueva funcion   //
//                                    SpAdmfRegEviMsj)           //
//                  HGT Octubre-2008 (Mejoras Funcionales R55)   //
//       HGT AMX-10-01186-0A  (Octubre-2010) Longitud Campo 70   //
//   ASLS M/022481 Actualización de estándares SWIFT 2015. Se    //
//   adecua la funcion SpAdmfValidaCMP para el uso del campo 59F //
//                                                               //
///////////////////////////////////////////////////////////////////

#define SQLCA_STORAGE_CLASS extern
#define ORACA_STORAGE_CLASS extern

//Hasta ahora estos son los headers necesarios
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <signal.h>
#include <unistd.h>
#include <iostream>
#include <string>
#include <sys/msg.h>
#include <ctype.h>
#include "../../gral/include_db/gralsql_oracle.h"
#include "../../gral/include/gfi-err.h"
#include "../../gral/include/gral.h"
#include "../../gral/include/hora.h"
#include "../../gral/include_db/admfcbocaducteb.h"

#if defined (__cplusplus) || defined (c_plusplus)
extern "C"
{
#endif

#include <sqlda.h>
#include <sqlcpr.h>

#if defined (__cplusplus) || defined (c_plusplus)
}
#endif

//Constante para identificar si un campo es nulo
#define  IS_NULL     == -1
#define  IS_NOT_NULL ==  0

using namespace std;

extern int sql_error(void);
extern void substr(char*, int, int, char*);

/* EXEC SQL BEGIN DECLARE SECTION; */ 
#line 59 "../../gral/src_db/admfcbcocaducteb.pc"

  /* Variables para admf_clave_bancos */
  char v_cadena[100],
       v_banco_id[12],
       v_cve_spei[11];

  int  v_entra,
       v_cuantos,
       v_posicion;

  /* Registro ADMF_ADUANA_TRASPASOS*/
  int     NO_CRITERIO;
  /* VARCHAR DS_BCOORD[36]; */ 
struct { unsigned short len; unsigned char arr[36]; } DS_BCOORD;
#line 71 "../../gral/src_db/admfcbcocaducteb.pc"

  /* VARCHAR CUENTA_ID[ 9]; */ 
struct { unsigned short len; unsigned char arr[9]; } CUENTA_ID;
#line 72 "../../gral/src_db/admfcbcocaducteb.pc"

  /* VARCHAR REFERENCIA[17]; */ 
struct { unsigned short len; unsigned char arr[17]; } REFERENCIA;
#line 73 "../../gral/src_db/admfcbcocaducteb.pc"

  /* VARCHAR FEC_VAL[11]; */ 
struct { unsigned short len; unsigned char arr[11]; } FEC_VAL;
#line 74 "../../gral/src_db/admfcbcocaducteb.pc"

  double  IMPORTE_DE;
  double  IMPORTE_A;
  /* VARCHAR DS_BCO_BEN[36]; */ 
struct { unsigned short len; unsigned char arr[36]; } DS_BCO_BEN;
#line 77 "../../gral/src_db/admfcbcocaducteb.pc"

  /* VARCHAR CTA_BENVOS[ 9]; */ 
struct { unsigned short len; unsigned char arr[9]; } CTA_BENVOS;
#line 78 "../../gral/src_db/admfcbcocaducteb.pc"

  /* VARCHAR CTA_BENCLI[21]; */ 
struct { unsigned short len; unsigned char arr[21]; } CTA_BENCLI;
#line 79 "../../gral/src_db/admfcbcocaducteb.pc"

  /* VARCHAR DS_CLIBEN[36]; */ 
struct { unsigned short len; unsigned char arr[36]; } DS_CLIBEN;
#line 80 "../../gral/src_db/admfcbcocaducteb.pc"


  /* indicadores de Valor NULL para registro ADMF_ADUANA_TRASPASOS */
  short   ind_NO_CRITERIO,
          ind_DS_BCOORD,
          ind_CUENTA_ID,
          ind_REFERENCIA,
          ind_FEC_VAL,
          ind_IMPORTE_DE,
          ind_IMPORTE_A,
          ind_DS_BCO_BEN,
          ind_CTA_BENVOS,
          ind_CTA_BENCLI,
          ind_DS_CLIBEN;

 // HGT Septiembre-2008
  char v_stts,
       v_cuenta[9],
       v_anio[3],
       v_tip_ope[9],
       v_cmp_corregido[13],
       v_tipoctaord[3],
       v_tipoctaben[3],
       v_nom2doben[51],
       v_conpago2doben[41],
       v_tipocta2doben[3];

  double v_importe;

  /* VARCHAR v_CMP2     [48],
          v_CMPF21   [23],
          CTA_CLI    [35],
          NOM_CLI   [176],
          INS_SPE   [211],
          C_ORIGINAL[211],
          CTA_2DO_BEN[34],
          NOM_2DO_BEN[173]; */ 
struct { unsigned short len; unsigned char arr[48]; } v_CMP2;
#line 109 "../../gral/src_db/admfcbcocaducteb.pc"

struct { unsigned short len; unsigned char arr[23]; } v_CMPF21;
#line 110 "../../gral/src_db/admfcbcocaducteb.pc"

struct { unsigned short len; unsigned char arr[35]; } CTA_CLI;
#line 111 "../../gral/src_db/admfcbcocaducteb.pc"

struct { unsigned short len; unsigned char arr[176]; } NOM_CLI;
#line 112 "../../gral/src_db/admfcbcocaducteb.pc"

struct { unsigned short len; unsigned char arr[211]; } INS_SPE;
#line 113 "../../gral/src_db/admfcbcocaducteb.pc"

struct { unsigned short len; unsigned char arr[211]; } C_ORIGINAL;
#line 114 "../../gral/src_db/admfcbcocaducteb.pc"

struct { unsigned short len; unsigned char arr[34]; } CTA_2DO_BEN;
#line 115 "../../gral/src_db/admfcbcocaducteb.pc"

struct { unsigned short len; unsigned char arr[173]; } NOM_2DO_BEN;
#line 116 "../../gral/src_db/admfcbcocaducteb.pc"


  // HGT Octubre-2008
  char    v_DESCBANCO[36];

/* EXEC SQL END DECLARE SECTION; */ 
#line 121 "../../gral/src_db/admfcbcocaducteb.pc"


// HGT Octubre-2008
int ChecaAprox(char *str1, char *str2, int porc)
{
  int len1 = 0, len2 = 0;
  int id1 = 0, id2 = 0;
  int  result = 0, dif = 0;
  char v_str[10];
  len1 = strlen( str1 );
  len2 = strlen( str2 );
  // Inicia ciclo, evalua cadenas
  do
  {
    if (str1[id1] != str2[id2] )
    {
      if (id2+1 <= len2)
      {
        if (str1[id1] !=  str2[id2+1] )
		    {
		      if (id2-1 >= 0)
          {
            if (str1[id1] !=  str2[id2-1] )
              dif++;
		      }
		      else
            dif++;
		    }
		  }
		  else
        dif++;
	  }

    if (id1 <= len1)
      id1++;

    if (id2 <= len2)
      id2++;

  }while( (id1 < len1) || (id2 < len2));
  // Termina ciclo, evalua resultado
  if (len2 > len1)
    result = len2;
  else
    result = len1;

  if (result <= 10)
    result = result * 2;

  result = 100 - (dif * 100) / result;
  if (result >= porc)
    result =  GFI_ERRC_OK;
  else
    result =  GFI_ERRC_FATAL;

  return result;
}

///////////////////////////////////////////////////////////////////
//                                                               //
//  Funcion : SpAdmfClaveBancos                                  //
//  HGT Octubre-2008                                             //
//                                                               //
///////////////////////////////////////////////////////////////////
void SpAdmfClaveBancos(char in_descrip[], char v_clave[])
{
  int result = GFI_ERRC_FATAL,
      porc = 85, //Porcentaje de Aproximación
      v_encontrado = 0,
      v_inserta = 0;

  /* EXEC SQL WHENEVER SQLERROR DO sql_error(); */ 
#line 192 "../../gral/src_db/admfcbcocaducteb.pc"


  v_cuantos = 0;
  v_entra = 1;
  Gfi_StrRTrim(in_descrip);
  v_posicion = strlen(in_descrip);

  substr(in_descrip, 0, v_posicion, v_cadena);

  if (v_posicion <= 11) // La DESCRIPCION es BIC CODE, así lo busca
  {
    // Busca por CLAVE
    /* EXEC SQL SELECT COUNT(1) INTO :v_cuantos
             FROM ADMF_ENVIO
             WHERE ubic_envio = :v_cadena
               AND tipo_envio_id = 'SWI'
               AND status = 'A'; */ 
#line 208 "../../gral/src_db/admfcbcocaducteb.pc"

{
#line 204 "../../gral/src_db/admfcbcocaducteb.pc"
    struct sqlexd sqlstm;
#line 204 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqlvsn = 13;
#line 204 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.arrsiz = 2;
#line 204 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqladtp = &sqladt;
#line 204 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqltdsp = &sqltds;
#line 204 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.stmt = "select count(1) into :b0  from ADMF_ENVIO where ((ubic_en\
vio=:b1 and tipo_envio_id='SWI') and status='A')";
#line 204 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.iters = (unsigned int  )1;
#line 204 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.offset = (unsigned int  )5;
#line 204 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.selerr = (unsigned short)1;
#line 204 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqlpfmem = (unsigned int  )0;
#line 204 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.cud = sqlcud0;
#line 204 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 204 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqlety = (unsigned short)4352;
#line 204 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.occurs = (unsigned int  )0;
#line 204 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhstv[0] = (unsigned char  *)&v_cuantos;
#line 204 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhstl[0] = (unsigned long )sizeof(int);
#line 204 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhsts[0] = (         int  )0;
#line 204 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqindv[0] = (         short *)0;
#line 204 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqinds[0] = (         int  )0;
#line 204 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqharm[0] = (unsigned long )0;
#line 204 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqadto[0] = (unsigned short )0;
#line 204 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqtdso[0] = (unsigned short )0;
#line 204 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhstv[1] = (unsigned char  *)v_cadena;
#line 204 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhstl[1] = (unsigned long )100;
#line 204 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhsts[1] = (         int  )0;
#line 204 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqindv[1] = (         short *)0;
#line 204 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqinds[1] = (         int  )0;
#line 204 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqharm[1] = (unsigned long )0;
#line 204 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqadto[1] = (unsigned short )0;
#line 204 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqtdso[1] = (unsigned short )0;
#line 204 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqphsv = sqlstm.sqhstv;
#line 204 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqphsl = sqlstm.sqhstl;
#line 204 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqphss = sqlstm.sqhsts;
#line 204 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqpind = sqlstm.sqindv;
#line 204 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqpins = sqlstm.sqinds;
#line 204 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqparm = sqlstm.sqharm;
#line 204 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqparc = sqlstm.sqharc;
#line 204 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqpadto = sqlstm.sqadto;
#line 204 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqptdso = sqlstm.sqtdso;
#line 204 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 204 "../../gral/src_db/admfcbcocaducteb.pc"
    if (sqlca.sqlcode < 0) sql_error();
#line 204 "../../gral/src_db/admfcbcocaducteb.pc"
}

#line 208 "../../gral/src_db/admfcbcocaducteb.pc"


    if (v_cuantos > 0)
    {
      v_banco_id[0] = '\0';
      /* EXEC SQL SELECT banco_id INTO :v_banco_id
               FROM ADMF_ENVIO
               WHERE ubic_envio = :v_cadena
                 AND tipo_envio_id = 'SWI'
                 AND status = 'A'
                 AND ROWNUM = 1; */ 
#line 218 "../../gral/src_db/admfcbcocaducteb.pc"

{
#line 213 "../../gral/src_db/admfcbcocaducteb.pc"
      struct sqlexd sqlstm;
#line 213 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqlvsn = 13;
#line 213 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.arrsiz = 2;
#line 213 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqladtp = &sqladt;
#line 213 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqltdsp = &sqltds;
#line 213 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.stmt = "select banco_id into :b0  from ADMF_ENVIO where (((ubic\
_envio=:b1 and tipo_envio_id='SWI') and status='A') and ROWNUM=1)";
#line 213 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.iters = (unsigned int  )1;
#line 213 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.offset = (unsigned int  )28;
#line 213 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.selerr = (unsigned short)1;
#line 213 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqlpfmem = (unsigned int  )0;
#line 213 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.cud = sqlcud0;
#line 213 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 213 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqlety = (unsigned short)4352;
#line 213 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.occurs = (unsigned int  )0;
#line 213 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqhstv[0] = (unsigned char  *)v_banco_id;
#line 213 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqhstl[0] = (unsigned long )12;
#line 213 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqhsts[0] = (         int  )0;
#line 213 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqindv[0] = (         short *)0;
#line 213 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqinds[0] = (         int  )0;
#line 213 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqharm[0] = (unsigned long )0;
#line 213 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqadto[0] = (unsigned short )0;
#line 213 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqtdso[0] = (unsigned short )0;
#line 213 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqhstv[1] = (unsigned char  *)v_cadena;
#line 213 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqhstl[1] = (unsigned long )100;
#line 213 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqhsts[1] = (         int  )0;
#line 213 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqindv[1] = (         short *)0;
#line 213 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqinds[1] = (         int  )0;
#line 213 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqharm[1] = (unsigned long )0;
#line 213 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqadto[1] = (unsigned short )0;
#line 213 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqtdso[1] = (unsigned short )0;
#line 213 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqphsv = sqlstm.sqhstv;
#line 213 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqphsl = sqlstm.sqhstl;
#line 213 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqphss = sqlstm.sqhsts;
#line 213 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqpind = sqlstm.sqindv;
#line 213 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqpins = sqlstm.sqinds;
#line 213 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqparm = sqlstm.sqharm;
#line 213 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqparc = sqlstm.sqharc;
#line 213 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqpadto = sqlstm.sqadto;
#line 213 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqptdso = sqlstm.sqtdso;
#line 213 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 213 "../../gral/src_db/admfcbcocaducteb.pc"
      if (sqlca.sqlcode < 0) sql_error();
#line 213 "../../gral/src_db/admfcbcocaducteb.pc"
}

#line 218 "../../gral/src_db/admfcbcocaducteb.pc"


      v_encontrado = 1;
    }
    else
    {
      /* EXEC SQL SELECT COUNT(1) INTO :v_cuantos
               FROM ADMF_ENVIO
               WHERE ubic_envio = RPAD(SUBSTR(:v_cadena,1,8),11,'X')
                 AND tipo_envio_id = 'SWI'
                 AND status = 'A'; */ 
#line 228 "../../gral/src_db/admfcbcocaducteb.pc"

{
#line 224 "../../gral/src_db/admfcbcocaducteb.pc"
      struct sqlexd sqlstm;
#line 224 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqlvsn = 13;
#line 224 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.arrsiz = 2;
#line 224 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqladtp = &sqladt;
#line 224 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqltdsp = &sqltds;
#line 224 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.stmt = "select count(1) into :b0  from ADMF_ENVIO where ((ubic_\
envio=RPAD(SUBSTR(:b1,1,8),11,'X') and tipo_envio_id='SWI') and status='A')";
#line 224 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.iters = (unsigned int  )1;
#line 224 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.offset = (unsigned int  )51;
#line 224 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.selerr = (unsigned short)1;
#line 224 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqlpfmem = (unsigned int  )0;
#line 224 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.cud = sqlcud0;
#line 224 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 224 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqlety = (unsigned short)4352;
#line 224 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.occurs = (unsigned int  )0;
#line 224 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqhstv[0] = (unsigned char  *)&v_cuantos;
#line 224 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqhstl[0] = (unsigned long )sizeof(int);
#line 224 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqhsts[0] = (         int  )0;
#line 224 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqindv[0] = (         short *)0;
#line 224 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqinds[0] = (         int  )0;
#line 224 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqharm[0] = (unsigned long )0;
#line 224 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqadto[0] = (unsigned short )0;
#line 224 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqtdso[0] = (unsigned short )0;
#line 224 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqhstv[1] = (unsigned char  *)v_cadena;
#line 224 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqhstl[1] = (unsigned long )100;
#line 224 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqhsts[1] = (         int  )0;
#line 224 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqindv[1] = (         short *)0;
#line 224 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqinds[1] = (         int  )0;
#line 224 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqharm[1] = (unsigned long )0;
#line 224 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqadto[1] = (unsigned short )0;
#line 224 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqtdso[1] = (unsigned short )0;
#line 224 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqphsv = sqlstm.sqhstv;
#line 224 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqphsl = sqlstm.sqhstl;
#line 224 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqphss = sqlstm.sqhsts;
#line 224 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqpind = sqlstm.sqindv;
#line 224 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqpins = sqlstm.sqinds;
#line 224 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqparm = sqlstm.sqharm;
#line 224 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqparc = sqlstm.sqharc;
#line 224 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqpadto = sqlstm.sqadto;
#line 224 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqptdso = sqlstm.sqtdso;
#line 224 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 224 "../../gral/src_db/admfcbcocaducteb.pc"
      if (sqlca.sqlcode < 0) sql_error();
#line 224 "../../gral/src_db/admfcbcocaducteb.pc"
}

#line 228 "../../gral/src_db/admfcbcocaducteb.pc"


      if (v_cuantos > 0)
      {
        v_banco_id[0] = '\0';
        /* EXEC SQL SELECT banco_id INTO :v_banco_id
                 FROM ADMF_ENVIO
                 WHERE ubic_envio = RPAD(SUBSTR(:v_cadena,1,8),11,'X')
                   AND tipo_envio_id = 'SWI'
                   AND status = 'A'
                   AND ROWNUM = 1; */ 
#line 238 "../../gral/src_db/admfcbcocaducteb.pc"

{
#line 233 "../../gral/src_db/admfcbcocaducteb.pc"
        struct sqlexd sqlstm;
#line 233 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqlvsn = 13;
#line 233 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.arrsiz = 2;
#line 233 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqladtp = &sqladt;
#line 233 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqltdsp = &sqltds;
#line 233 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.stmt = "select banco_id into :b0  from ADMF_ENVIO where (((ub\
ic_envio=RPAD(SUBSTR(:b1,1,8),11,'X') and tipo_envio_id='SWI') and status='A')\
 and ROWNUM=1)";
#line 233 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.iters = (unsigned int  )1;
#line 233 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.offset = (unsigned int  )74;
#line 233 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.selerr = (unsigned short)1;
#line 233 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqlpfmem = (unsigned int  )0;
#line 233 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.cud = sqlcud0;
#line 233 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 233 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqlety = (unsigned short)4352;
#line 233 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.occurs = (unsigned int  )0;
#line 233 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqhstv[0] = (unsigned char  *)v_banco_id;
#line 233 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqhstl[0] = (unsigned long )12;
#line 233 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqhsts[0] = (         int  )0;
#line 233 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqindv[0] = (         short *)0;
#line 233 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqinds[0] = (         int  )0;
#line 233 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqharm[0] = (unsigned long )0;
#line 233 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqadto[0] = (unsigned short )0;
#line 233 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqtdso[0] = (unsigned short )0;
#line 233 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqhstv[1] = (unsigned char  *)v_cadena;
#line 233 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqhstl[1] = (unsigned long )100;
#line 233 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqhsts[1] = (         int  )0;
#line 233 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqindv[1] = (         short *)0;
#line 233 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqinds[1] = (         int  )0;
#line 233 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqharm[1] = (unsigned long )0;
#line 233 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqadto[1] = (unsigned short )0;
#line 233 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqtdso[1] = (unsigned short )0;
#line 233 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqphsv = sqlstm.sqhstv;
#line 233 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqphsl = sqlstm.sqhstl;
#line 233 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqphss = sqlstm.sqhsts;
#line 233 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqpind = sqlstm.sqindv;
#line 233 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqpins = sqlstm.sqinds;
#line 233 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqparm = sqlstm.sqharm;
#line 233 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqparc = sqlstm.sqharc;
#line 233 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqpadto = sqlstm.sqadto;
#line 233 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqptdso = sqlstm.sqtdso;
#line 233 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 233 "../../gral/src_db/admfcbcocaducteb.pc"
        if (sqlca.sqlcode < 0) sql_error();
#line 233 "../../gral/src_db/admfcbcocaducteb.pc"
}

#line 238 "../../gral/src_db/admfcbcocaducteb.pc"


        v_encontrado = 1;
      }
    }
  }

  if (v_encontrado == 0) // Lo busca como DESCRIPCION
  { //1
    // Busca en ADMF_CVE_INTERME_REC
    /* EXEC SQL SELECT COUNT(1) INTO :v_cuantos
             FROM ADMF_CVE_INTERME_REC
                 WHERE TRIM(desc_interme_rec) = TRIM(:v_cadena)
                   AND tipo_envio_id = 'TRA'
                   AND status  = 'A'; */ 
#line 252 "../../gral/src_db/admfcbcocaducteb.pc"

{
#line 248 "../../gral/src_db/admfcbcocaducteb.pc"
    struct sqlexd sqlstm;
#line 248 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqlvsn = 13;
#line 248 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.arrsiz = 2;
#line 248 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqladtp = &sqladt;
#line 248 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqltdsp = &sqltds;
#line 248 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.stmt = "select count(1) into :b0  from ADMF_CVE_INTERME_REC where\
 ((trim(desc_interme_rec)=trim(:b1) and tipo_envio_id='TRA') and status='A')";
#line 248 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.iters = (unsigned int  )1;
#line 248 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.offset = (unsigned int  )97;
#line 248 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.selerr = (unsigned short)1;
#line 248 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqlpfmem = (unsigned int  )0;
#line 248 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.cud = sqlcud0;
#line 248 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 248 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqlety = (unsigned short)4352;
#line 248 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.occurs = (unsigned int  )0;
#line 248 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhstv[0] = (unsigned char  *)&v_cuantos;
#line 248 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhstl[0] = (unsigned long )sizeof(int);
#line 248 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhsts[0] = (         int  )0;
#line 248 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqindv[0] = (         short *)0;
#line 248 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqinds[0] = (         int  )0;
#line 248 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqharm[0] = (unsigned long )0;
#line 248 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqadto[0] = (unsigned short )0;
#line 248 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqtdso[0] = (unsigned short )0;
#line 248 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhstv[1] = (unsigned char  *)v_cadena;
#line 248 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhstl[1] = (unsigned long )100;
#line 248 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhsts[1] = (         int  )0;
#line 248 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqindv[1] = (         short *)0;
#line 248 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqinds[1] = (         int  )0;
#line 248 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqharm[1] = (unsigned long )0;
#line 248 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqadto[1] = (unsigned short )0;
#line 248 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqtdso[1] = (unsigned short )0;
#line 248 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqphsv = sqlstm.sqhstv;
#line 248 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqphsl = sqlstm.sqhstl;
#line 248 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqphss = sqlstm.sqhsts;
#line 248 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqpind = sqlstm.sqindv;
#line 248 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqpins = sqlstm.sqinds;
#line 248 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqparm = sqlstm.sqharm;
#line 248 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqparc = sqlstm.sqharc;
#line 248 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqpadto = sqlstm.sqadto;
#line 248 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqptdso = sqlstm.sqtdso;
#line 248 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 248 "../../gral/src_db/admfcbcocaducteb.pc"
    if (sqlca.sqlcode < 0) sql_error();
#line 248 "../../gral/src_db/admfcbcocaducteb.pc"
}

#line 252 "../../gral/src_db/admfcbcocaducteb.pc"


    if (v_cuantos > 0)
    {
        v_banco_id[0] = '\0';
        /* EXEC SQL SELECT cve_interme_rec INTO :v_cve_spei
                   FROM ADMF_CVE_INTERME_REC
                  WHERE TRIM(desc_interme_rec) = TRIM(:v_cadena)
                    AND tipo_envio_id = 'TRA'
                    AND status  = 'A'
                    AND ROWNUM = 1; */ 
#line 262 "../../gral/src_db/admfcbcocaducteb.pc"

{
#line 257 "../../gral/src_db/admfcbcocaducteb.pc"
        struct sqlexd sqlstm;
#line 257 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqlvsn = 13;
#line 257 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.arrsiz = 2;
#line 257 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqladtp = &sqladt;
#line 257 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqltdsp = &sqltds;
#line 257 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.stmt = "select cve_interme_rec into :b0  from ADMF_CVE_INTERM\
E_REC where (((trim(desc_interme_rec)=trim(:b1) and tipo_envio_id='TRA') and s\
tatus='A') and ROWNUM=1)";
#line 257 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.iters = (unsigned int  )1;
#line 257 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.offset = (unsigned int  )120;
#line 257 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.selerr = (unsigned short)1;
#line 257 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqlpfmem = (unsigned int  )0;
#line 257 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.cud = sqlcud0;
#line 257 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 257 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqlety = (unsigned short)4352;
#line 257 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.occurs = (unsigned int  )0;
#line 257 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqhstv[0] = (unsigned char  *)v_cve_spei;
#line 257 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqhstl[0] = (unsigned long )11;
#line 257 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqhsts[0] = (         int  )0;
#line 257 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqindv[0] = (         short *)0;
#line 257 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqinds[0] = (         int  )0;
#line 257 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqharm[0] = (unsigned long )0;
#line 257 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqadto[0] = (unsigned short )0;
#line 257 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqtdso[0] = (unsigned short )0;
#line 257 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqhstv[1] = (unsigned char  *)v_cadena;
#line 257 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqhstl[1] = (unsigned long )100;
#line 257 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqhsts[1] = (         int  )0;
#line 257 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqindv[1] = (         short *)0;
#line 257 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqinds[1] = (         int  )0;
#line 257 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqharm[1] = (unsigned long )0;
#line 257 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqadto[1] = (unsigned short )0;
#line 257 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqtdso[1] = (unsigned short )0;
#line 257 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqphsv = sqlstm.sqhstv;
#line 257 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqphsl = sqlstm.sqhstl;
#line 257 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqphss = sqlstm.sqhsts;
#line 257 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqpind = sqlstm.sqindv;
#line 257 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqpins = sqlstm.sqinds;
#line 257 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqparm = sqlstm.sqharm;
#line 257 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqparc = sqlstm.sqharc;
#line 257 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqpadto = sqlstm.sqadto;
#line 257 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlstm.sqptdso = sqlstm.sqtdso;
#line 257 "../../gral/src_db/admfcbcocaducteb.pc"
        sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 257 "../../gral/src_db/admfcbcocaducteb.pc"
        if (sqlca.sqlcode < 0) sql_error();
#line 257 "../../gral/src_db/admfcbcocaducteb.pc"
}

#line 262 "../../gral/src_db/admfcbcocaducteb.pc"


        v_encontrado = 2;
    }
    else
      v_inserta = 1; // Activa bandera para registrar en INTERME_REC aun si no se encuentra en CICLOS
  }
  /*
  if (v_encontrado == 0) // Lo busca como DESCRIPCION aún no se corrige
  { //2
    while(v_encontrado == 0 && v_posicion >= 5) // Busca por ciclo Decreciente
    {
      substr(in_descrip, 0, v_posicion, v_cadena);
      v_cuantos = 0;

      EXEC SQL SELECT COUNT(1) INTO :v_cuantos
               FROM ADMF_BANCO
               WHERE descripcion LIKE '%'||TRIM(:v_cadena)||'%'
                 AND status  = 'A'
                 AND pais_id = 'MX';

      if (v_cuantos > 0)
      {
        EXEC SQL SELECT id INTO :v_banco_id
               FROM ADMF_BANCO
               WHERE descripcion LIKE '%'||TRIM(:v_cadena)||'%'
                 AND status  = 'A'
                 AND pais_id = 'MX'
                 AND ROWNUM = 1;

        v_encontrado = 1;
      }
      v_posicion--;
    }
  }

  if (v_encontrado == 0) // Busca por Porcentaje de Aprox.
  {
    v_posicion = strlen(in_descrip);
    substr(in_descrip, 0, v_posicion, v_cadena);
    EXEC SQL DECLARE cB1 CURSOR FOR
             SELECT NVL(c.desc_interme_rec, a.descripcion), b.ubic_envio
             FROM ADMF_BANCO a, ADMF_ENVIO b, ADMF_CVE_INTERME_REC c
             WHERE a.status  = 'A'
               AND a.pais_id = 'MX'
               AND b.banco_id = a.id
               AND b.tipo_envio_id = 'SPE'
               AND b.status  = 'A'
               AND trim(c.cve_interme_rec(+)) = b.ubic_envio
               AND c.tipo_envio_id(+) = 'TRA'
               AND c.status(+) = 'A'
             ORDER BY UPPER(NVL(c.desc_interme_rec,a.descripcion));

    EXEC SQL OPEN cB1;
    do
    {
      EXEC SQL FETCH cB1 INTO :v_DESCBANCO, :v_cve_spei;

      if (sqlca.sqlcode != 0)
        break;

      Gfi_StrRTrim(v_DESCBANCO);
      result = ChecaAprox(v_cadena, v_DESCBANCO, porc);

      if (result == GFI_ERRC_OK)
      {
        v_encontrado = 2;
        break;
      }

      memset(v_DESCBANCO, '\0', sizeof(v_DESCBANCO));
      memset(v_cve_spei, '\0', sizeof(v_cve_spei));

    }while(!sqlca.sqlcode);
    EXEC SQL CLOSE cB1;
  }
  */
  if (v_encontrado == 1) // Lo encontró!!, carga el BIC SPEI
  {
    v_cuantos = 0;
    /* EXEC SQL SELECT COUNT(1) INTO :v_cuantos
             FROM ADMF_ENVIO
             WHERE banco_id = :v_banco_id
               AND tipo_envio_id = 'SPE'
               AND status = 'A'; */ 
#line 346 "../../gral/src_db/admfcbcocaducteb.pc"

{
#line 342 "../../gral/src_db/admfcbcocaducteb.pc"
    struct sqlexd sqlstm;
#line 342 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqlvsn = 13;
#line 342 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.arrsiz = 2;
#line 342 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqladtp = &sqladt;
#line 342 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqltdsp = &sqltds;
#line 342 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.stmt = "select count(1) into :b0  from ADMF_ENVIO where ((banco_i\
d=:b1 and tipo_envio_id='SPE') and status='A')";
#line 342 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.iters = (unsigned int  )1;
#line 342 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.offset = (unsigned int  )143;
#line 342 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.selerr = (unsigned short)1;
#line 342 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqlpfmem = (unsigned int  )0;
#line 342 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.cud = sqlcud0;
#line 342 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 342 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqlety = (unsigned short)4352;
#line 342 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.occurs = (unsigned int  )0;
#line 342 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhstv[0] = (unsigned char  *)&v_cuantos;
#line 342 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhstl[0] = (unsigned long )sizeof(int);
#line 342 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhsts[0] = (         int  )0;
#line 342 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqindv[0] = (         short *)0;
#line 342 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqinds[0] = (         int  )0;
#line 342 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqharm[0] = (unsigned long )0;
#line 342 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqadto[0] = (unsigned short )0;
#line 342 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqtdso[0] = (unsigned short )0;
#line 342 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhstv[1] = (unsigned char  *)v_banco_id;
#line 342 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhstl[1] = (unsigned long )12;
#line 342 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhsts[1] = (         int  )0;
#line 342 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqindv[1] = (         short *)0;
#line 342 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqinds[1] = (         int  )0;
#line 342 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqharm[1] = (unsigned long )0;
#line 342 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqadto[1] = (unsigned short )0;
#line 342 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqtdso[1] = (unsigned short )0;
#line 342 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqphsv = sqlstm.sqhstv;
#line 342 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqphsl = sqlstm.sqhstl;
#line 342 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqphss = sqlstm.sqhsts;
#line 342 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqpind = sqlstm.sqindv;
#line 342 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqpins = sqlstm.sqinds;
#line 342 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqparm = sqlstm.sqharm;
#line 342 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqparc = sqlstm.sqharc;
#line 342 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqpadto = sqlstm.sqadto;
#line 342 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqptdso = sqlstm.sqtdso;
#line 342 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 342 "../../gral/src_db/admfcbcocaducteb.pc"
    if (sqlca.sqlcode < 0) sql_error();
#line 342 "../../gral/src_db/admfcbcocaducteb.pc"
}

#line 346 "../../gral/src_db/admfcbcocaducteb.pc"


    if(v_cuantos > 0)
    {
      /* EXEC SQL SELECT ubic_envio INTO :v_cve_spei
               FROM ADMF_ENVIO
               WHERE banco_id = :v_banco_id
                 AND tipo_envio_id = 'SPE'
                 AND status = 'A'
                 AND ROWNUM = 1; */ 
#line 355 "../../gral/src_db/admfcbcocaducteb.pc"

{
#line 350 "../../gral/src_db/admfcbcocaducteb.pc"
      struct sqlexd sqlstm;
#line 350 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqlvsn = 13;
#line 350 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.arrsiz = 2;
#line 350 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqladtp = &sqladt;
#line 350 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqltdsp = &sqltds;
#line 350 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.stmt = "select ubic_envio into :b0  from ADMF_ENVIO where (((ba\
nco_id=:b1 and tipo_envio_id='SPE') and status='A') and ROWNUM=1)";
#line 350 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.iters = (unsigned int  )1;
#line 350 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.offset = (unsigned int  )166;
#line 350 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.selerr = (unsigned short)1;
#line 350 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqlpfmem = (unsigned int  )0;
#line 350 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.cud = sqlcud0;
#line 350 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 350 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqlety = (unsigned short)4352;
#line 350 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.occurs = (unsigned int  )0;
#line 350 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqhstv[0] = (unsigned char  *)v_cve_spei;
#line 350 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqhstl[0] = (unsigned long )11;
#line 350 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqhsts[0] = (         int  )0;
#line 350 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqindv[0] = (         short *)0;
#line 350 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqinds[0] = (         int  )0;
#line 350 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqharm[0] = (unsigned long )0;
#line 350 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqadto[0] = (unsigned short )0;
#line 350 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqtdso[0] = (unsigned short )0;
#line 350 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqhstv[1] = (unsigned char  *)v_banco_id;
#line 350 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqhstl[1] = (unsigned long )12;
#line 350 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqhsts[1] = (         int  )0;
#line 350 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqindv[1] = (         short *)0;
#line 350 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqinds[1] = (         int  )0;
#line 350 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqharm[1] = (unsigned long )0;
#line 350 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqadto[1] = (unsigned short )0;
#line 350 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqtdso[1] = (unsigned short )0;
#line 350 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqphsv = sqlstm.sqhstv;
#line 350 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqphsl = sqlstm.sqhstl;
#line 350 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqphss = sqlstm.sqhsts;
#line 350 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqpind = sqlstm.sqindv;
#line 350 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqpins = sqlstm.sqinds;
#line 350 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqparm = sqlstm.sqharm;
#line 350 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqparc = sqlstm.sqharc;
#line 350 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqpadto = sqlstm.sqadto;
#line 350 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqptdso = sqlstm.sqtdso;
#line 350 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 350 "../../gral/src_db/admfcbcocaducteb.pc"
      if (sqlca.sqlcode < 0) sql_error();
#line 350 "../../gral/src_db/admfcbcocaducteb.pc"
}

#line 355 "../../gral/src_db/admfcbcocaducteb.pc"


      v_encontrado = 2;
    }
    else
      strcpy(v_cve_spei, "XXXXX"); // SPEI Erroneo por Default
  }
  else if (v_encontrado == 0)
    strcpy(v_cve_spei, "XXXXX"); // SPEI Erroneo por Default

  if (v_inserta == 1) // Registra la corrección para uso posterior
  {
    // Ingresa en ADMF_CVE_INTERME_REC
    v_posicion = strlen(in_descrip);
    substr(in_descrip, 0, v_posicion, v_cadena);

    cout << "INSERT en ADMF_CVE_INTERME_REC desc_interme_rec >" << v_cadena << "<" << endl;
    /* EXEC SQL INSERT INTO ADMF_CVE_INTERME_REC
             (cve_interme_rec, desc_interme_rec,
              tipo_envio_id, status, fc_status)
             VALUES (:v_cve_spei, TRIM(:v_cadena), 'TRA', 'A', sysdate); */ 
#line 375 "../../gral/src_db/admfcbcocaducteb.pc"

{
#line 372 "../../gral/src_db/admfcbcocaducteb.pc"
    struct sqlexd sqlstm;
#line 372 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqlvsn = 13;
#line 372 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.arrsiz = 2;
#line 372 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqladtp = &sqladt;
#line 372 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqltdsp = &sqltds;
#line 372 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.stmt = "insert into ADMF_CVE_INTERME_REC (cve_interme_rec,desc_in\
terme_rec,tipo_envio_id,status,fc_status) values (:b0,trim(:b1),'TRA','A',sysd\
ate)";
#line 372 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.iters = (unsigned int  )1;
#line 372 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.offset = (unsigned int  )189;
#line 372 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.cud = sqlcud0;
#line 372 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 372 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqlety = (unsigned short)4352;
#line 372 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.occurs = (unsigned int  )0;
#line 372 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhstv[0] = (unsigned char  *)v_cve_spei;
#line 372 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhstl[0] = (unsigned long )11;
#line 372 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhsts[0] = (         int  )0;
#line 372 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqindv[0] = (         short *)0;
#line 372 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqinds[0] = (         int  )0;
#line 372 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqharm[0] = (unsigned long )0;
#line 372 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqadto[0] = (unsigned short )0;
#line 372 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqtdso[0] = (unsigned short )0;
#line 372 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhstv[1] = (unsigned char  *)v_cadena;
#line 372 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhstl[1] = (unsigned long )100;
#line 372 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhsts[1] = (         int  )0;
#line 372 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqindv[1] = (         short *)0;
#line 372 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqinds[1] = (         int  )0;
#line 372 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqharm[1] = (unsigned long )0;
#line 372 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqadto[1] = (unsigned short )0;
#line 372 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqtdso[1] = (unsigned short )0;
#line 372 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqphsv = sqlstm.sqhstv;
#line 372 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqphsl = sqlstm.sqhstl;
#line 372 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqphss = sqlstm.sqhsts;
#line 372 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqpind = sqlstm.sqindv;
#line 372 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqpins = sqlstm.sqinds;
#line 372 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqparm = sqlstm.sqharm;
#line 372 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqparc = sqlstm.sqharc;
#line 372 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqpadto = sqlstm.sqadto;
#line 372 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqptdso = sqlstm.sqtdso;
#line 372 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 372 "../../gral/src_db/admfcbcocaducteb.pc"
    if (sqlca.sqlcode < 0) sql_error();
#line 372 "../../gral/src_db/admfcbcocaducteb.pc"
}

#line 375 "../../gral/src_db/admfcbcocaducteb.pc"

    // Finaliza funcion, realiza commit ORACLE
    if (sqlca.sqlcode == 0)
      /* EXEC SQL COMMIT; */ 
#line 378 "../../gral/src_db/admfcbcocaducteb.pc"

{
#line 378 "../../gral/src_db/admfcbcocaducteb.pc"
      struct sqlexd sqlstm;
#line 378 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqlvsn = 13;
#line 378 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.arrsiz = 2;
#line 378 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqladtp = &sqladt;
#line 378 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqltdsp = &sqltds;
#line 378 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.iters = (unsigned int  )1;
#line 378 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.offset = (unsigned int  )212;
#line 378 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.cud = sqlcud0;
#line 378 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 378 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.sqlety = (unsigned short)4352;
#line 378 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlstm.occurs = (unsigned int  )0;
#line 378 "../../gral/src_db/admfcbcocaducteb.pc"
      sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 378 "../../gral/src_db/admfcbcocaducteb.pc"
      if (sqlca.sqlcode < 0) sql_error();
#line 378 "../../gral/src_db/admfcbcocaducteb.pc"
}

#line 378 "../../gral/src_db/admfcbcocaducteb.pc"

  }

  if (v_encontrado == 2)
  {
    Gfi_StrRTrim(v_cve_spei);
    /* EXEC SQL SELECT e.descripcion, length(trim(e.descripcion))
             INTO :v_cadena, :v_posicion
             FROM ADMF_ENVIO a, ADMF_BANCO e
             WHERE a.ubic_envio = :v_cve_spei
               AND a.tipo_envio_id = 'SPE'
               AND e.id = a.banco_id
               AND e.status = 'A'
               AND a.status = 'A'
               AND ROWNUM = 1; */ 
#line 392 "../../gral/src_db/admfcbcocaducteb.pc"

{
#line 384 "../../gral/src_db/admfcbcocaducteb.pc"
    struct sqlexd sqlstm;
#line 384 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqlvsn = 13;
#line 384 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.arrsiz = 3;
#line 384 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqladtp = &sqladt;
#line 384 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqltdsp = &sqltds;
#line 384 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.stmt = "select e.descripcion ,length(trim(e.descripcion)) into :b\
0,:b1  from ADMF_ENVIO a ,ADMF_BANCO e where (((((a.ubic_envio=:b2 and a.tipo_\
envio_id='SPE') and e.id=a.banco_id) and e.status='A') and a.status='A') and R\
OWNUM=1)";
#line 384 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.iters = (unsigned int  )1;
#line 384 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.offset = (unsigned int  )227;
#line 384 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.selerr = (unsigned short)1;
#line 384 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqlpfmem = (unsigned int  )0;
#line 384 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.cud = sqlcud0;
#line 384 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 384 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqlety = (unsigned short)4352;
#line 384 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.occurs = (unsigned int  )0;
#line 384 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhstv[0] = (unsigned char  *)v_cadena;
#line 384 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhstl[0] = (unsigned long )100;
#line 384 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhsts[0] = (         int  )0;
#line 384 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqindv[0] = (         short *)0;
#line 384 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqinds[0] = (         int  )0;
#line 384 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqharm[0] = (unsigned long )0;
#line 384 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqadto[0] = (unsigned short )0;
#line 384 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqtdso[0] = (unsigned short )0;
#line 384 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhstv[1] = (unsigned char  *)&v_posicion;
#line 384 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhstl[1] = (unsigned long )sizeof(int);
#line 384 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhsts[1] = (         int  )0;
#line 384 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqindv[1] = (         short *)0;
#line 384 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqinds[1] = (         int  )0;
#line 384 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqharm[1] = (unsigned long )0;
#line 384 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqadto[1] = (unsigned short )0;
#line 384 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqtdso[1] = (unsigned short )0;
#line 384 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhstv[2] = (unsigned char  *)v_cve_spei;
#line 384 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhstl[2] = (unsigned long )11;
#line 384 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhsts[2] = (         int  )0;
#line 384 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqindv[2] = (         short *)0;
#line 384 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqinds[2] = (         int  )0;
#line 384 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqharm[2] = (unsigned long )0;
#line 384 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqadto[2] = (unsigned short )0;
#line 384 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqtdso[2] = (unsigned short )0;
#line 384 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqphsv = sqlstm.sqhstv;
#line 384 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqphsl = sqlstm.sqhstl;
#line 384 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqphss = sqlstm.sqhsts;
#line 384 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqpind = sqlstm.sqindv;
#line 384 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqpins = sqlstm.sqinds;
#line 384 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqparm = sqlstm.sqharm;
#line 384 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqparc = sqlstm.sqharc;
#line 384 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqpadto = sqlstm.sqadto;
#line 384 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqptdso = sqlstm.sqtdso;
#line 384 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 384 "../../gral/src_db/admfcbcocaducteb.pc"
    if (sqlca.sqlcode < 0) sql_error();
#line 384 "../../gral/src_db/admfcbcocaducteb.pc"
}

#line 392 "../../gral/src_db/admfcbcocaducteb.pc"


    substr(v_cadena, 0, v_posicion, in_descrip);
  }
  strcpy(v_clave, v_cve_spei);
  return;
}

///////////////////////////////////////////////////////////////////
//                                                               //
//  Funcion : SpAdmfChecaAduana                                  //
//                                                               //
///////////////////////////////////////////////////////////////////
void SpAdmfChecaAduana(char ds_bcoord[], char cuenta_id[], char referencia[],
                       char fec_val[],   double importe,  char ds_bco_ben[],
                       char cta_benvos[],char cta_bencli[], char ds_cliben[],
                       char& regreso)
{
  /* EXEC SQL DECLARE c1 CURSOR FOR
    SELECT NO_CRITERIO, DS_BCOORD, CUENTA_ID, REFERENCIA, TO_CHAR(TO_DATE(FEC_VAL, 'dd/mm/yyyy'), 'yyyymmdd'),
           IMPORTE_DE,  IMPORTE_A, DS_BCO_BEN,CTA_BENVOS, CTA_BENCLI, DS_CLIBEN
    FROM ADMF_ADUANA_TRASPASOS; */ 
#line 413 "../../gral/src_db/admfcbcocaducteb.pc"


  /* EXEC SQL WHENEVER SQLERROR DO sql_error(); */ 
#line 415 "../../gral/src_db/admfcbcocaducteb.pc"


  regreso = 'R';

  /* EXEC SQL OPEN c1; */ 
#line 419 "../../gral/src_db/admfcbcocaducteb.pc"

{
#line 419 "../../gral/src_db/admfcbcocaducteb.pc"
  struct sqlexd sqlstm;
#line 419 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqlvsn = 13;
#line 419 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.arrsiz = 3;
#line 419 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqladtp = &sqladt;
#line 419 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqltdsp = &sqltds;
#line 419 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.stmt = sq0012;
#line 419 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.iters = (unsigned int  )1;
#line 419 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.offset = (unsigned int  )254;
#line 419 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.selerr = (unsigned short)1;
#line 419 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqlpfmem = (unsigned int  )0;
#line 419 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.cud = sqlcud0;
#line 419 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 419 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqlety = (unsigned short)4352;
#line 419 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.occurs = (unsigned int  )0;
#line 419 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqcmod = (unsigned int )0;
#line 419 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 419 "../../gral/src_db/admfcbcocaducteb.pc"
  if (sqlca.sqlcode < 0) sql_error();
#line 419 "../../gral/src_db/admfcbcocaducteb.pc"
}

#line 419 "../../gral/src_db/admfcbcocaducteb.pc"


  do
  {
    /* EXEC SQL FETCH c1 INTO
        :NO_CRITERIO INDICATOR :ind_NO_CRITERIO, :DS_BCOORD  INDICATOR :ind_DS_BCOORD, :CUENTA_ID  INDICATOR :ind_CUENTA_ID,
        :REFERENCIA  INDICATOR :ind_REFERENCIA,  :FEC_VAL    INDICATOR :ind_FEC_VAL,   :IMPORTE_DE INDICATOR :ind_IMPORTE_DE,
        :IMPORTE_A   INDICATOR :ind_IMPORTE_A,   :DS_BCO_BEN INDICATOR :ind_DS_BCO_BEN,:CTA_BENVOS INDICATOR :ind_CTA_BENVOS,
        :CTA_BENCLI  INDICATOR :ind_CTA_BENCLI,  :DS_CLIBEN  INDICATOR :ind_DS_CLIBEN; */ 
#line 427 "../../gral/src_db/admfcbcocaducteb.pc"

{
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    struct sqlexd sqlstm;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqlvsn = 13;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.arrsiz = 11;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqladtp = &sqladt;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqltdsp = &sqltds;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.iters = (unsigned int  )1;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.offset = (unsigned int  )269;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.selerr = (unsigned short)1;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqlpfmem = (unsigned int  )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.cud = sqlcud0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqlety = (unsigned short)4352;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.occurs = (unsigned int  )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqfoff = (         int )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqfmod = (unsigned int )2;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhstv[0] = (unsigned char  *)&NO_CRITERIO;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhstl[0] = (unsigned long )sizeof(int);
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhsts[0] = (         int  )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqindv[0] = (         short *)&ind_NO_CRITERIO;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqinds[0] = (         int  )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqharm[0] = (unsigned long )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqadto[0] = (unsigned short )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqtdso[0] = (unsigned short )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhstv[1] = (unsigned char  *)&DS_BCOORD;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhstl[1] = (unsigned long )38;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhsts[1] = (         int  )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqindv[1] = (         short *)&ind_DS_BCOORD;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqinds[1] = (         int  )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqharm[1] = (unsigned long )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqadto[1] = (unsigned short )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqtdso[1] = (unsigned short )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhstv[2] = (unsigned char  *)&CUENTA_ID;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhstl[2] = (unsigned long )11;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhsts[2] = (         int  )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqindv[2] = (         short *)&ind_CUENTA_ID;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqinds[2] = (         int  )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqharm[2] = (unsigned long )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqadto[2] = (unsigned short )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqtdso[2] = (unsigned short )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhstv[3] = (unsigned char  *)&REFERENCIA;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhstl[3] = (unsigned long )19;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhsts[3] = (         int  )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqindv[3] = (         short *)&ind_REFERENCIA;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqinds[3] = (         int  )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqharm[3] = (unsigned long )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqadto[3] = (unsigned short )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqtdso[3] = (unsigned short )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhstv[4] = (unsigned char  *)&FEC_VAL;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhstl[4] = (unsigned long )13;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhsts[4] = (         int  )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqindv[4] = (         short *)&ind_FEC_VAL;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqinds[4] = (         int  )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqharm[4] = (unsigned long )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqadto[4] = (unsigned short )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqtdso[4] = (unsigned short )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhstv[5] = (unsigned char  *)&IMPORTE_DE;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhstl[5] = (unsigned long )sizeof(double);
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhsts[5] = (         int  )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqindv[5] = (         short *)&ind_IMPORTE_DE;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqinds[5] = (         int  )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqharm[5] = (unsigned long )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqadto[5] = (unsigned short )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqtdso[5] = (unsigned short )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhstv[6] = (unsigned char  *)&IMPORTE_A;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhstl[6] = (unsigned long )sizeof(double);
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhsts[6] = (         int  )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqindv[6] = (         short *)&ind_IMPORTE_A;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqinds[6] = (         int  )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqharm[6] = (unsigned long )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqadto[6] = (unsigned short )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqtdso[6] = (unsigned short )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhstv[7] = (unsigned char  *)&DS_BCO_BEN;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhstl[7] = (unsigned long )38;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhsts[7] = (         int  )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqindv[7] = (         short *)&ind_DS_BCO_BEN;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqinds[7] = (         int  )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqharm[7] = (unsigned long )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqadto[7] = (unsigned short )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqtdso[7] = (unsigned short )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhstv[8] = (unsigned char  *)&CTA_BENVOS;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhstl[8] = (unsigned long )11;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhsts[8] = (         int  )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqindv[8] = (         short *)&ind_CTA_BENVOS;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqinds[8] = (         int  )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqharm[8] = (unsigned long )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqadto[8] = (unsigned short )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqtdso[8] = (unsigned short )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhstv[9] = (unsigned char  *)&CTA_BENCLI;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhstl[9] = (unsigned long )23;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhsts[9] = (         int  )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqindv[9] = (         short *)&ind_CTA_BENCLI;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqinds[9] = (         int  )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqharm[9] = (unsigned long )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqadto[9] = (unsigned short )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqtdso[9] = (unsigned short )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhstv[10] = (unsigned char  *)&DS_CLIBEN;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhstl[10] = (unsigned long )38;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqhsts[10] = (         int  )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqindv[10] = (         short *)&ind_DS_CLIBEN;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqinds[10] = (         int  )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqharm[10] = (unsigned long )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqadto[10] = (unsigned short )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqtdso[10] = (unsigned short )0;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqphsv = sqlstm.sqhstv;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqphsl = sqlstm.sqhstl;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqphss = sqlstm.sqhsts;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqpind = sqlstm.sqindv;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqpins = sqlstm.sqinds;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqparm = sqlstm.sqharm;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqparc = sqlstm.sqharc;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqpadto = sqlstm.sqadto;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqptdso = sqlstm.sqtdso;
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
    if (sqlca.sqlcode < 0) sql_error();
#line 423 "../../gral/src_db/admfcbcocaducteb.pc"
}

#line 427 "../../gral/src_db/admfcbcocaducteb.pc"


    if (sqlca.sqlcode != 0)
       break; //Verificar la condicion para salir del do while;

    if (regreso == 'R')
    {
      if (ind_DS_BCOORD IS_NOT_NULL)         /* checa 1er campo */
        if (!strcmp((const char*)DS_BCOORD.arr, ds_bcoord))
          regreso = 'A';
        else
          regreso = 'R';

      if (ind_CUENTA_ID IS_NOT_NULL)         /* checa 2o campo  */
        if (!strcmp((const char*)CUENTA_ID.arr, cuenta_id))
          regreso = 'A';
        else
          regreso = 'R';

      if (ind_REFERENCIA IS_NOT_NULL)        /* checa campo 3   */
        if (!strcmp((const char*)REFERENCIA.arr, referencia))
          regreso = 'A';
        else
          regreso = 'R';

      if (ind_FEC_VAL IS_NOT_NULL)           /* checa campo 4   */
        if (!strcmp((const char*)FEC_VAL.arr, fec_val))
          regreso = 'A';
        else
          regreso = 'R';

      if (ind_IMPORTE_DE IS_NOT_NULL)        /* checa campo 5   */
        if (IMPORTE_DE <= importe)
          regreso = 'A';
        else
          regreso = 'R';

      if (ind_IMPORTE_A IS_NOT_NULL)         /* checa campo 6   */
        if (IMPORTE_A >= importe)
          regreso = 'A';
        else
          regreso = 'R';

      if (ind_DS_BCO_BEN IS_NOT_NULL)        /* checa campo 7   */
        if (!strcmp((const char*)DS_BCO_BEN.arr, ds_bco_ben))
          regreso = 'A';
        else
          regreso = 'R';

      if (ind_CTA_BENVOS IS_NOT_NULL)        /* checa campo 8   */
        if (!strcmp((const char*)CTA_BENVOS.arr, cta_benvos))
          regreso = 'A';
        else
          regreso = 'R';

      if (ind_CTA_BENCLI IS_NOT_NULL)        /* checa campo 9   */
        if (!strcmp((const char*)CTA_BENCLI.arr, cta_bencli))
          regreso = 'A';
        else
          regreso = 'R';

      if (ind_DS_CLIBEN IS_NOT_NULL)        /* checa campo 10   */
        if (!strcmp((const char*)DS_CLIBEN.arr, ds_cliben))
          regreso = 'A';
        else
          regreso = 'R';
    }
  }while(!sqlca.sqlcode);

  /* EXEC SQL CLOSE c1; */ 
#line 496 "../../gral/src_db/admfcbcocaducteb.pc"

{
#line 496 "../../gral/src_db/admfcbcocaducteb.pc"
  struct sqlexd sqlstm;
#line 496 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqlvsn = 13;
#line 496 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.arrsiz = 11;
#line 496 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqladtp = &sqladt;
#line 496 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqltdsp = &sqltds;
#line 496 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.iters = (unsigned int  )1;
#line 496 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.offset = (unsigned int  )328;
#line 496 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.cud = sqlcud0;
#line 496 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 496 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqlety = (unsigned short)4352;
#line 496 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.occurs = (unsigned int  )0;
#line 496 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 496 "../../gral/src_db/admfcbcocaducteb.pc"
  if (sqlca.sqlcode < 0) sql_error();
#line 496 "../../gral/src_db/admfcbcocaducteb.pc"
}

#line 496 "../../gral/src_db/admfcbcocaducteb.pc"


  return;
}

///////////////////////////////////////////////////////////////////
//                                                               //
//  Funcion : SpAdmfVosCteBen                                    //
//                                                               //
///////////////////////////////////////////////////////////////////
void SpAdmfVosCteBen(char v_cmpa[], char v_cmpb[], char v_cmpd[],
                     char v_nom_bco_ben[], char v_cve_bco_ben[], int& v_existe_banco)
{
  string strTemp;
  int    posicion,
         posicion2;

  if (v_cmpa[0] == '\0')
  {
    if (v_cmpb[0] == '\0')
    {
      if (v_cmpd[0] == '\0')
          strcpy(v_cve_bco_ben, v_cmpa);
      else
      {
        strTemp = v_cmpd;
        posicion = strTemp.find("\n", 0);

        if (posicion == string::npos && strlen(v_cmpd) > 0)
          posicion = strlen(v_cmpd);

        if (posicion > 35)
          posicion = 35;

        if (v_cmpd[0] == '/')
        {
          posicion2 = strTemp.find("\n", posicion + 1); //HGT 21-07-2008

          if (posicion2 == string::npos)
            posicion2 = 34;

          strTemp = strTemp.substr(posicion + 1, posicion2 - posicion - 1); //HGT 21-07-2008
        }
        else
          strTemp = strTemp.substr(0, posicion - 1);

        strTemp = strTemp.substr(0, 35);

        strcpy(v_nom_bco_ben, strTemp.c_str());
      }

      cout << "Clave banco beneficiario 1 = " << v_cve_bco_ben  << endl; //DHJ 10-04-2008 (Correccion cmp57 o cmp58)
      cout << "Nombre banco beneficiario 1 = " << v_nom_bco_ben  << endl; //DHJ 10-04-2008 (Correccion cmp57 o cmp58)

    }
    else
    {
      /* RVV 20080308 Codigo original
      strTemp = v_cmpb;
      strTemp = strTemp.substr(strTemp.rfind("/"), 11);
      strcpy(v_cve_bco_ben, strTemp.c_str());
      v_existe_banco = 1;
      */

      strTemp = v_cmpb;

      cout << "Entra CMP57 o CMP58 = " << strTemp  << endl; //DHJ 10-04-2008 (Correccion cmp57 o cmp58)

      if(strTemp.rfind("/") != string::npos)
        strTemp = strTemp.substr(strTemp.rfind("/"), 11);
      else
        strTemp = strTemp.substr(0, 11);

      cout << "Sale CMP57 o CMP58 = " << strTemp  << endl; //DHJ 10-04-2008 (Correccion cmp57 o cmp58)

      strcpy(v_cve_bco_ben, strTemp.c_str());

      cout << "Clave banco beneficiario 2 = " << v_cve_bco_ben  << endl; //DHJ 10-04-2008 (Correccion cmp57 o cmp58)
      cout << "Nombre banco beneficiario 2 = " << v_nom_bco_ben  << endl; //DHJ 10-04-2008 (Correccion cmp57 o cmp58)

      v_existe_banco = 1;
    }
  }
  else
  {
    strTemp = v_cmpa;

    if (v_cmpa[0] == '/')
      strTemp = strTemp.substr(strTemp.find("\n", 0) + 1, 11);
    else
      strTemp = strTemp.substr(strTemp.find("/", 0) + 1, 11);

    strcpy(v_cve_bco_ben, strTemp.c_str());
    v_existe_banco = 1;

    cout << "Clave banco beneficiario 3 = " << v_cve_bco_ben  << endl; //DHJ 10-04-2008 (Correccion cmp57 o cmp58)
    cout << "Nombre banco beneficiario 3 = " << v_nom_bco_ben  << endl; //DHJ 10-04-2008 (Correccion cmp57 o cmp58)

  }

  return;
}

////////////////////////////////////////////////////////////////////////////
//                                                                        //
// Funcion para validar y separar los Campos CMP:                         //
// 50, 53, 56, 57, 58, 59, 70 y 72                                        //
// de los procesos de carga definitiva MT:                                //
// 103, 202                                                               //
//                                                                        //
// REALIZO: Grupo Tecnis(HGT Agosto-2008)                                 //
//                                                                        //
////////////////////////////////////////////////////////////////////////////
void SpAdmfValidaCMP(char v_cmp[], char v_tipocmp[], char v_clave[], char v_clave_old[],
                     char v_nombre[], int& v_correccion, int& l_hayclave, int& l_lenclave,
                     int l_ajuclave)
{
  // Variables locales
  char caracter[1],
       doblecta[1];
  int   i,
        iCve = 0,
        iNom = 0,
        iOld = 0,
        iSalto = 0,
        swEsCta = 0,
        swChrVal,
        swNewCta = 0,
        swNewNom = 0,
        sw2doBen = 0;
  // Inicia funcion
  if (l_hayclave == -1)
  {
    sw2doBen = l_hayclave;
    l_hayclave = 0;
  }
  // INICIA CICLO PARA VALIDAR EL CONTENIDO DEL CAMPO CMP RECIBIDO
  for( i=0; i<=strlen(v_cmp); i++)
  {
      // Evalua caracter por caracter del campo CMP
      swChrVal = 1;
      caracter[0] = '\0';
      sprintf(caracter, "%c", v_cmp[i]);
      // EXCEPCION: Solo si se trata de los campos 70 o 72 (Carga Completa sin evaluar Cuenta)
      if ( (strcmp(v_tipocmp, "70") == 0) || (strcmp(v_tipocmp, "72") == 0) )
      {
        // Apaga switch's para carga de la CUENTA
        swEsCta = 0;
      }
      else if ( (strcmp(caracter, "/") == 0) && (i == 0) )
      {
        // Evalua los dos primeros caracteres si se trata de / = CUENTA

        // EXCEPCION: Verifica si no existe una doble /
         doblecta[0] = '\0';
        sprintf(doblecta, "%c", v_cmp[i+1]);
        if (strcmp(doblecta, "/") == 0) // Existe doble /?
        {
          i++;
          caracter[0] = '\0';
          sprintf(caracter, "%c", v_cmp[i]);
        }
        else // EXCEPCION: Verifica si no existe una doble con D intermedia
        {
          doblecta[0] = '\0';
          sprintf(doblecta, "%c", v_cmp[i+2]);
          if (strcmp(doblecta, "/") == 0) // Existe doble /?
          {
            i++;
            caracter[0] = '\0';
            sprintf(caracter, "%c", v_cmp[i]);
          }
        }
        // Prende switch's para carga de la CUENTA y apaga el de caracter Válido
        swEsCta = 1;
        swChrVal = 0;
        // EXCEPCION: Si ya existe cuenta, no carga la CUENTA de este campo
        if (l_hayclave == 1)
          break;
        else
          l_hayclave = 1;
      }

      if ( (swChrVal == 1) || (swEsCta == 1) )
      { //1
        // Es delimitador NATURAL?
        if ( (strcmp(caracter, "\n") == 0) || (strcmp(caracter, "\r") == 0) )
        {
          if (swEsCta == 1)
          {
            // Apaga switch's para carga de la CUENTA y el de caracter Válido
            swEsCta = 0;
            swChrVal = 0;
          }
          else
          {
            iSalto++;
            // EXCEPCION: Recorte en los campos 50, 70 o 72
            if (strcmp(v_tipocmp, "50") == 0)
            {
              if (iSalto >= 3) // Máximo tres saltos
                break;
            }
            else if ( (strcmp(v_tipocmp, "70") == 0) || (strcmp(v_tipocmp, "72") == 0) ) // HGT AMX-10-01186-0A (Oct-2010) Longitud Campo 70
            {
              if (iSalto >= 4) // Máximo cuatro saltos
                break;
            }
            else if ( strcmp(v_tipocmp, "F") == 0 ) // ASLS
            {
              if (iSalto >= 3) // Máximo tres saltos
                break;
            }
            else
            {
              break; // Corta ciclo para los demás campos
            }
          }
        }
        // Es caracter válido?
        if (swChrVal == 1)
        { //2
          // Es cuenta?
          if (swEsCta == 1)
          { //3a
            // Es el primer caracter?
            if (swNewCta == 0)
            {
              // Reinicia contenido de las variables
              memset(v_clave, '\0', sizeof(v_clave));

              if (v_correccion == 0)
                memset(v_clave_old, '\0', sizeof(v_clave_old));
            }

            if (l_ajuclave == 1) // Corrige la clave
            {
              if (v_correccion == 0)
              {
                // Carga clave Original
                strcat( v_clave_old, caracter );
                iOld++;
              }

              // Evalua caracteres válidos en la Cuenta
              if (strcmp(caracter, " ") == 0) // Es un blanco?
                  swChrVal = 0;
              else if (isdigit(v_cmp[i]) == 0) // Es digito?  Nota: falla con ñ y letras acentuadas
                  swChrVal = 0;
              else if (isprint(v_cmp[i]) == 0) // Es imprimible? Nota: quita ñ y letras acentuadas
                  swChrVal = 0;
            }

            // Es caracter válido?
            if (swChrVal == 1)
            {
              if (iCve < 34) // Longitud maxima
              {
                // Carga la CUENTA debidamente depurada
                strcat( v_clave, caracter );
                swNewCta = 1;
                iCve++;
              }
            }

          } //3a
          else
          { //3b

            // EXCEPCION: Recorte en CampoD
            if (strcmp(v_tipocmp, "D") == 0)
              if (iNom > 35) // Longitud maxima
                break;

            // EXCEPCION: Recorte en CampoF // ASLS
            if (strcmp(v_tipocmp, "F") == 0)
              if (iNom > 170) // Longitud maxima
                break;

            // EXCEPCION: Recorte en Campo sin Letra
            if (strcmp(v_tipocmp, " ") == 0)
              if (iNom > 50) // Longitud maxima
                break;

            // Es el primer caracter?
            if (swNewNom == 0)
            {
              // Reinicia contenido de la variable
              memset(v_nombre, '\0', sizeof(v_nombre));
              swNewNom = 1;
            }
            // Carga BIC / BEI / Location / Name & Address
            if ( (strcmp(caracter, "\n") == 0) || (strcmp(caracter, "\r") == 0) )
            {
              // Convierte DELIMITADOR a espacio
              strcat( v_nombre, " " );
            }
            else
            {
              // Deja caracter intacto
              strcat( v_nombre, caracter );
            }
            iNom++;
          } //3b
        } //2
      } //1
  } //End For
  // TERMINA CICLO PARA VALIDAR EL CONTENIDO DEL CAMPO CMP RECIBIDO

  if (swNewCta == 1) // Hay nueva cuenta?
  {
    // Carga Longitud del Campo CLAVE
    l_lenclave = iCve;

    // Delimina las variables y las cierra
    if (iCve >= 0)
      v_clave[iCve] = '\0';

    if (iOld > 0)
      v_clave_old[iOld] = '\0';

    if (iNom > 0)
      v_nombre[iNom] = '\0';

    // Evalua si hay una correcion en la Cuenta
    if (v_correccion == 0)
      if  (strlen(v_clave_old) > 0)
      {
        if  (strcmp(v_clave_old, v_clave) != 0)
          v_correccion = 1; // Se registra la correcion
        else
          memset(v_clave_old, '\0', sizeof(v_clave_old));
      }
  }
  else
  {
    // Carga Longitud del Campo CLAVE como NULA
    l_lenclave = -1;
  }

  // EXCEPCION: 2o. Beneficiario, carga exclusiva campo A por BIC de Nombre de Banco
  if ( (strcmp(v_tipocmp, "A") == 0) && (sw2doBen == -1) )
    l_hayclave = -2;

  // Finaliza funcion
  return;
}

////////////////////////////////////////////////////////////////////////////
//                                                                        //
// Funcion para registrar EVIDENCIAS de arreglos a los mensajes MT        //
// en los procesos de carga definitiva:                                   //
// 103, 202                                                               //
//                                                                        //
// REALIZO: Grupo Tecnis(HGT Septiembre-2008)                             //
//                                                                        //
////////////////////////////////////////////////////////////////////////////
int SpAdmfRegEviMsj(char v_cmp2[],   char v_cmpf21[],  char& v_stts,    char v_referencia[],
                    char v_cuenta[], char v_nbcoord[], char v_fecval[], double v_importe,
                    char v_cbcben[], char v_nbcben[],  char v_nctben[], char v_cta_cli[],
                    char v_nomcli[], char v_ins_spe[], char v_anio[],   char v_tip_ope[],
                    char v_cmp_corregido[], char v_cnt_original[],      char v_tipoctaord[],
                    char v_tipoctaben[],    char v_cta2doben[],         char v_nom2doben[],
                    char v_conpago2doben[], char v_tipocta2doben[])
{
  // Variables locales
  int   resultado = GFI_ERRC_OK;
  // Inicia funcion, valida campos OBLIGATORIOS
  if (strlen(v_cmp2) == 0)
  {
    cout << "Campo CMP2 nulo, verifiquelo!" << endl;
    resultado = GFI_ERRC_FATAL;
    return resultado;
  }

  if (strlen(v_cmpf21) == 0)
  {
    cout << "Campo CMPF21 nulo, verifiquelo!" << endl;
    resultado = GFI_ERRC_FATAL;
    return resultado;
  }

  if (strlen(v_referencia) == 0)
  {
    cout << "Campo REFERENCIA nulo, verifiquelo!" << endl;
    resultado = GFI_ERRC_FATAL;
    return resultado;
  }

  if (strlen(v_fecval) == 0)
  {
    cout << "Campo FECHA VALOR nulo, verifiquelo!" << endl;
    resultado = GFI_ERRC_FATAL;
    return resultado;
  }
  // Reinicia variables ORACLE
  memset(v_CMP2.arr, '\0', sizeof(v_CMP2.arr));         v_CMP2.len = 0;
  memset(v_CMPF21.arr, '\0', sizeof(v_CMPF21.arr));     v_CMPF21.len = 0;
  memset(REFERENCIA.arr, '\0', sizeof(REFERENCIA.arr)); REFERENCIA.len = 0;
  memset(DS_BCOORD.arr, '\0', sizeof(DS_BCOORD.arr));   DS_BCOORD.len = 0;
  memset(FEC_VAL.arr, '\0', sizeof(FEC_VAL.arr));       FEC_VAL.len = 0;
  memset(CTA_BENCLI.arr, '\0', sizeof(CTA_BENCLI.arr)); CTA_BENCLI.len = 0;
  memset(DS_BCO_BEN.arr, '\0', sizeof(DS_BCO_BEN.arr)); DS_BCO_BEN.len = 0;
  memset(CTA_BENVOS.arr, '\0', sizeof(CTA_BENVOS.arr)); CTA_BENVOS.len = 0;
  memset(CTA_CLI.arr, '\0', sizeof(CTA_CLI.arr));       CTA_CLI.len = 0;
  memset(NOM_CLI.arr, '\0', sizeof(NOM_CLI.arr));       NOM_CLI.len = 0;
  memset(INS_SPE.arr, '\0', sizeof(INS_SPE.arr));       INS_SPE.len = 0;
  memset(C_ORIGINAL.arr, '\0', sizeof(C_ORIGINAL.arr)); C_ORIGINAL.len = 0;
  memset(CTA_2DO_BEN.arr, '\0', sizeof(CTA_2DO_BEN.arr)); CTA_2DO_BEN.len = 0;
  memset(NOM_2DO_BEN.arr, '\0', sizeof(NOM_2DO_BEN.arr)); NOM_2DO_BEN.len = 0;
  // Traspasa valores a variables ORACLE
  strcpy((char *)v_CMP2.arr, v_cmp2);             v_CMP2.len = strlen((char *)v_CMP2.arr);
  strcpy((char *)v_CMPF21.arr, v_cmpf21);         v_CMPF21.len = strlen((char *)v_CMPF21.arr);
  strcpy((char *)REFERENCIA.arr, v_referencia);   REFERENCIA.len = strlen((char *)REFERENCIA.arr);
  strcpy((char *)DS_BCOORD.arr, v_nbcoord);       DS_BCOORD.len = strlen((char *)DS_BCOORD.arr);
  strcpy((char *)FEC_VAL.arr, v_fecval);          FEC_VAL.len = strlen((char *)FEC_VAL.arr);
  strcpy((char *)CTA_BENCLI.arr, v_cbcben);       CTA_BENCLI.len = strlen((char *)CTA_BENCLI.arr);
  strcpy((char *)DS_BCO_BEN.arr, v_nbcben);       DS_BCO_BEN.len = strlen((char *)DS_BCO_BEN.arr);
  strcpy((char *)CTA_BENVOS.arr, v_nctben);       CTA_BENVOS.len = strlen((char *)CTA_BENVOS.arr);
  strcpy((char *)CTA_CLI.arr, v_cta_cli);         CTA_CLI.len = strlen((char *)CTA_CLI.arr);
  strcpy((char *)NOM_CLI.arr, v_nomcli);          NOM_CLI.len = strlen((char *)NOM_CLI.arr);
  strcpy((char *)INS_SPE.arr, v_ins_spe);         INS_SPE.len = strlen((char *)INS_SPE.arr);
  strcpy((char *)C_ORIGINAL.arr, v_cnt_original); C_ORIGINAL.len = strlen((char *)C_ORIGINAL.arr);
  strcpy((char *)CTA_2DO_BEN.arr, v_cta2doben);   CTA_2DO_BEN.len = strlen((char *)CTA_2DO_BEN.arr);
  strcpy((char *)NOM_2DO_BEN.arr, v_nom2doben);   NOM_2DO_BEN.len = strlen((char *)NOM_2DO_BEN.arr);
  // Ejecuta SQL ORACLE
  cout << "Realiza INSERT en EVIDENCIAS campo:" << v_cmp_corregido << endl;
  /* EXEC SQL INSERT INTO ADMF_VOS_TRASPASOS_EVIDENCIAS
                       (CVE_MSG,    HOR_INI,      FEC_INI,    ID_BNCO_ENV,
                        COD_BRANCH, NUM_SESION,   NUM_SEC,    FEC_FIN,
                        HOR_FIN,    PRI_MSG,      STATUS,     REFERENCIA,
                        CUENTA_ID,  DS_BCOORD,    FEC_VAL,    IMPORTE,
                        CV_BCOBEN,  DS_BCOBEN,    CTA_BENVOS, CTA_BENCLI,
                        DS_CLIBEN,  INS_SPEUA,    FE_MSG,     TIP_OPE,
                        PLAZA,      REF_TRANSFER, COD_ERROR,
                        NOM_CAMPO_CORREGIDO, CONTENIDO_ORIGINAL, TIPO, FEC_CORRECION, USUARIO_C,
                        TIPO_CTA_ORD,   TIPO_CTA_BEN,   CUENTA_2DO_BEN,
                        NOMBRE_2DO_BEN, CONCEPTO_PAGO2, TIPO_CTA2)
           VALUES(SUBSTR(:v_CMP2, 2, 3), SUBSTR(:v_CMP2, 4, 4), SUBSTR(:v_CMP2, 9, 6), SUBSTR(:v_CMP2, 15, 8),
                  SUBSTR(:v_CMP2, 24, 3), TO_NUMBER(SUBSTR(:v_CMPF21, 13, 4)), TO_NUMBER(SUBSTR(:v_CMPF21, 17, 6)), SUBSTR(:v_CMP2, 37, 6),
                  SUBSTR(:v_CMP2, 43, 4), SUBSTR(:v_CMP2, 47, 1), NVL(:v_stts,'R'), :REFERENCIA,
                  NVL(:v_cuenta,NULL), NVL(:DS_BCOORD,NULL), TO_DATE(:FEC_VAL, 'yyyymmdd'), :v_importe,
                  NVL(:CTA_BENCLI,NULL), NVL(:DS_BCO_BEN,NULL), NVL(:CTA_BENVOS,NULL), NVL(SUBSTR(:CTA_CLI,1,20),NULL),
                  NVL(:NOM_CLI,NULL), NVL(:INS_SPE,NULL), TO_DATE(:v_anio || SUBSTR(:v_CMP2, 37, 6), 'yyyymmdd'), :v_tip_ope,
                  1001, NULL, NULL,
                  NVL(:v_cmp_corregido,NULL), NVL(:C_ORIGINAL,NULL), 'R', SYSDATE, 'REP_AUT',
                  NVL(:v_tipoctaord,NULL), NVL(:v_tipoctaben,NULL), NVL(:CTA_2DO_BEN,NULL),
                  NVL(:NOM_2DO_BEN,NULL), NVL(:v_conpago2doben,NULL), NVL(:v_tipocta2doben,NULL)); */ 
#line 943 "../../gral/src_db/admfcbcocaducteb.pc"

{
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  struct sqlexd sqlstm;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqlvsn = 13;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.arrsiz = 33;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqladtp = &sqladt;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqltdsp = &sqltds;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlbuft((void **)0, 
    "insert into ADMF_VOS_TRASPASOS_EVIDENCIAS (CVE_MSG,HOR_INI,FEC_INI,ID_B\
NCO_ENV,COD_BRANCH,NUM_SESION,NUM_SEC,FEC_FIN,HOR_FIN,PRI_MSG,STATUS,REFEREN\
CIA,CUENTA_ID,DS_BCOORD,FEC_VAL,IMPORTE,CV_BCOBEN,DS_BCOBEN,CTA_BENVOS,CTA_B\
ENCLI,DS_CLIBEN,INS_SPEUA,FE_MSG,TIP_OPE,PLAZA,REF_TRANSFER,COD_ERROR,NOM_CA\
MPO_CORREGIDO,CONTENIDO_ORIGINAL,TIPO,FEC_CORRECION,USUARIO_C,TIPO_CTA_ORD,T\
IPO_CTA_BEN,CUENTA_2DO_BEN,NOMBRE_2DO_BEN,CONCEPTO_PAGO2,TIPO_CTA2) values (\
SUBSTR(:b0,2,3),SUBSTR(:b0,4,4),SUBSTR(:b0,9,6),SUBSTR(:b0,15,8),SUBSTR(:b0,\
24,3),TO_NUMBER(SUBSTR(:b5,13,4)),TO_NUMBER(SUBSTR(:b5,17,6)),SUBSTR(:b0,37,\
6),SUBSTR(:b0,43,4),SUBSTR(:b0,47,1),NVL(:b10,'R'),:b11,NVL(:b12,null ),NVL(\
:b13,null ),TO_DATE(:b14,'yyyymmdd'),:b15,NVL(:b16,null ),NVL(:b17,null ),NV\
L(:b18,null ),NVL(SUBSTR(:b19,1,20),null ),NVL(:b20,null ),NVL(:b21,null ),T\
O_DATE((:b22||SUBSTR(:b0,37,6)),'yyyymmdd'),:b24,1001,null ,null ,NVL(:b25,n\
ull ),NVL(:b26,null ),'R',SYSDATE,'REP_AUT',NVL(:b27,null ),NVL(:b28,null ),\
NVL(:b29,null ),NVL(:b30,null ),NVL(:b31,");
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.stmt = "null ),NVL(:b32,null ))";
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.iters = (unsigned int  )1;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.offset = (unsigned int  )343;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.cud = sqlcud0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqlety = (unsigned short)4352;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.occurs = (unsigned int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstv[0] = (unsigned char  *)&v_CMP2;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstl[0] = (unsigned long )50;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhsts[0] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqindv[0] = (         short *)0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqinds[0] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqharm[0] = (unsigned long )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqadto[0] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqtdso[0] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstv[1] = (unsigned char  *)&v_CMP2;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstl[1] = (unsigned long )50;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhsts[1] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqindv[1] = (         short *)0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqinds[1] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqharm[1] = (unsigned long )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqadto[1] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqtdso[1] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstv[2] = (unsigned char  *)&v_CMP2;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstl[2] = (unsigned long )50;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhsts[2] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqindv[2] = (         short *)0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqinds[2] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqharm[2] = (unsigned long )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqadto[2] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqtdso[2] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstv[3] = (unsigned char  *)&v_CMP2;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstl[3] = (unsigned long )50;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhsts[3] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqindv[3] = (         short *)0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqinds[3] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqharm[3] = (unsigned long )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqadto[3] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqtdso[3] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstv[4] = (unsigned char  *)&v_CMP2;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstl[4] = (unsigned long )50;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhsts[4] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqindv[4] = (         short *)0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqinds[4] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqharm[4] = (unsigned long )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqadto[4] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqtdso[4] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstv[5] = (unsigned char  *)&v_CMPF21;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstl[5] = (unsigned long )25;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhsts[5] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqindv[5] = (         short *)0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqinds[5] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqharm[5] = (unsigned long )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqadto[5] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqtdso[5] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstv[6] = (unsigned char  *)&v_CMPF21;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstl[6] = (unsigned long )25;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhsts[6] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqindv[6] = (         short *)0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqinds[6] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqharm[6] = (unsigned long )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqadto[6] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqtdso[6] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstv[7] = (unsigned char  *)&v_CMP2;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstl[7] = (unsigned long )50;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhsts[7] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqindv[7] = (         short *)0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqinds[7] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqharm[7] = (unsigned long )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqadto[7] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqtdso[7] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstv[8] = (unsigned char  *)&v_CMP2;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstl[8] = (unsigned long )50;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhsts[8] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqindv[8] = (         short *)0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqinds[8] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqharm[8] = (unsigned long )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqadto[8] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqtdso[8] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstv[9] = (unsigned char  *)&v_CMP2;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstl[9] = (unsigned long )50;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhsts[9] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqindv[9] = (         short *)0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqinds[9] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqharm[9] = (unsigned long )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqadto[9] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqtdso[9] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstv[10] = (unsigned char  *)&v_stts;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstl[10] = (unsigned long )1;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhsts[10] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqindv[10] = (         short *)0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqinds[10] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqharm[10] = (unsigned long )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqadto[10] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqtdso[10] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstv[11] = (unsigned char  *)&REFERENCIA;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstl[11] = (unsigned long )19;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhsts[11] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqindv[11] = (         short *)0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqinds[11] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqharm[11] = (unsigned long )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqadto[11] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqtdso[11] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstv[12] = (unsigned char  *)v_cuenta;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstl[12] = (unsigned long )9;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhsts[12] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqindv[12] = (         short *)0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqinds[12] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqharm[12] = (unsigned long )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqadto[12] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqtdso[12] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstv[13] = (unsigned char  *)&DS_BCOORD;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstl[13] = (unsigned long )38;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhsts[13] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqindv[13] = (         short *)0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqinds[13] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqharm[13] = (unsigned long )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqadto[13] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqtdso[13] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstv[14] = (unsigned char  *)&FEC_VAL;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstl[14] = (unsigned long )13;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhsts[14] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqindv[14] = (         short *)0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqinds[14] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqharm[14] = (unsigned long )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqadto[14] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqtdso[14] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstv[15] = (unsigned char  *)&v_importe;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstl[15] = (unsigned long )sizeof(double);
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhsts[15] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqindv[15] = (         short *)0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqinds[15] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqharm[15] = (unsigned long )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqadto[15] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqtdso[15] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstv[16] = (unsigned char  *)&CTA_BENCLI;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstl[16] = (unsigned long )23;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhsts[16] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqindv[16] = (         short *)0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqinds[16] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqharm[16] = (unsigned long )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqadto[16] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqtdso[16] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstv[17] = (unsigned char  *)&DS_BCO_BEN;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstl[17] = (unsigned long )38;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhsts[17] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqindv[17] = (         short *)0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqinds[17] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqharm[17] = (unsigned long )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqadto[17] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqtdso[17] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstv[18] = (unsigned char  *)&CTA_BENVOS;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstl[18] = (unsigned long )11;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhsts[18] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqindv[18] = (         short *)0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqinds[18] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqharm[18] = (unsigned long )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqadto[18] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqtdso[18] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstv[19] = (unsigned char  *)&CTA_CLI;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstl[19] = (unsigned long )37;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhsts[19] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqindv[19] = (         short *)0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqinds[19] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqharm[19] = (unsigned long )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqadto[19] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqtdso[19] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstv[20] = (unsigned char  *)&NOM_CLI;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstl[20] = (unsigned long )178;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhsts[20] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqindv[20] = (         short *)0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqinds[20] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqharm[20] = (unsigned long )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqadto[20] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqtdso[20] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstv[21] = (unsigned char  *)&INS_SPE;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstl[21] = (unsigned long )213;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhsts[21] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqindv[21] = (         short *)0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqinds[21] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqharm[21] = (unsigned long )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqadto[21] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqtdso[21] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstv[22] = (unsigned char  *)v_anio;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstl[22] = (unsigned long )3;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhsts[22] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqindv[22] = (         short *)0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqinds[22] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqharm[22] = (unsigned long )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqadto[22] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqtdso[22] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstv[23] = (unsigned char  *)&v_CMP2;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstl[23] = (unsigned long )50;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhsts[23] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqindv[23] = (         short *)0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqinds[23] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqharm[23] = (unsigned long )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqadto[23] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqtdso[23] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstv[24] = (unsigned char  *)v_tip_ope;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstl[24] = (unsigned long )9;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhsts[24] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqindv[24] = (         short *)0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqinds[24] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqharm[24] = (unsigned long )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqadto[24] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqtdso[24] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstv[25] = (unsigned char  *)v_cmp_corregido;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstl[25] = (unsigned long )13;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhsts[25] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqindv[25] = (         short *)0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqinds[25] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqharm[25] = (unsigned long )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqadto[25] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqtdso[25] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstv[26] = (unsigned char  *)&C_ORIGINAL;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstl[26] = (unsigned long )213;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhsts[26] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqindv[26] = (         short *)0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqinds[26] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqharm[26] = (unsigned long )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqadto[26] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqtdso[26] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstv[27] = (unsigned char  *)v_tipoctaord;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstl[27] = (unsigned long )3;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhsts[27] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqindv[27] = (         short *)0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqinds[27] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqharm[27] = (unsigned long )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqadto[27] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqtdso[27] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstv[28] = (unsigned char  *)v_tipoctaben;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstl[28] = (unsigned long )3;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhsts[28] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqindv[28] = (         short *)0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqinds[28] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqharm[28] = (unsigned long )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqadto[28] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqtdso[28] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstv[29] = (unsigned char  *)&CTA_2DO_BEN;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstl[29] = (unsigned long )36;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhsts[29] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqindv[29] = (         short *)0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqinds[29] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqharm[29] = (unsigned long )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqadto[29] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqtdso[29] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstv[30] = (unsigned char  *)&NOM_2DO_BEN;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstl[30] = (unsigned long )175;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhsts[30] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqindv[30] = (         short *)0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqinds[30] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqharm[30] = (unsigned long )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqadto[30] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqtdso[30] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstv[31] = (unsigned char  *)v_conpago2doben;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstl[31] = (unsigned long )41;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhsts[31] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqindv[31] = (         short *)0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqinds[31] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqharm[31] = (unsigned long )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqadto[31] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqtdso[31] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstv[32] = (unsigned char  *)v_tipocta2doben;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhstl[32] = (unsigned long )3;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqhsts[32] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqindv[32] = (         short *)0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqinds[32] = (         int  )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqharm[32] = (unsigned long )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqadto[32] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqtdso[32] = (unsigned short )0;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqphsv = sqlstm.sqhstv;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqphsl = sqlstm.sqhstl;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqphss = sqlstm.sqhsts;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqpind = sqlstm.sqindv;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqpins = sqlstm.sqinds;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqparm = sqlstm.sqharm;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqparc = sqlstm.sqharc;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqpadto = sqlstm.sqadto;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlstm.sqptdso = sqlstm.sqtdso;
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
  if (sqlca.sqlcode < 0) sql_error();
#line 923 "../../gral/src_db/admfcbcocaducteb.pc"
}

#line 943 "../../gral/src_db/admfcbcocaducteb.pc"

  // Finaliza funcion, realiza commit ORACLE
  if (sqlca.sqlcode == 0)
    /* EXEC SQL COMMIT; */ 
#line 946 "../../gral/src_db/admfcbcocaducteb.pc"

{
#line 946 "../../gral/src_db/admfcbcocaducteb.pc"
    struct sqlexd sqlstm;
#line 946 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqlvsn = 13;
#line 946 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.arrsiz = 33;
#line 946 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqladtp = &sqladt;
#line 946 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqltdsp = &sqltds;
#line 946 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.iters = (unsigned int  )1;
#line 946 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.offset = (unsigned int  )490;
#line 946 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.cud = sqlcud0;
#line 946 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 946 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.sqlety = (unsigned short)4352;
#line 946 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlstm.occurs = (unsigned int  )0;
#line 946 "../../gral/src_db/admfcbcocaducteb.pc"
    sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 946 "../../gral/src_db/admfcbcocaducteb.pc"
    if (sqlca.sqlcode < 0) sql_error();
#line 946 "../../gral/src_db/admfcbcocaducteb.pc"
}

#line 946 "../../gral/src_db/admfcbcocaducteb.pc"

  else
    resultado = GFI_ERRC_FATAL;

  return resultado;
}