
/* Result Sets Interface */
#ifndef SQL_CRSR
#  define SQL_CRSR
  struct sql_cursor
  {
    unsigned int curocn;
    void *ptr1;
    void *ptr2;
    unsigned int magic;
  };
  typedef struct sql_cursor sql_cursor;
  typedef struct sql_cursor SQL_CURSOR;
#endif /* SQL_CRSR */

/* Thread Safety */
typedef void * sql_context;
typedef void * SQL_CONTEXT;

/* Object support */
struct sqltvn
{
  unsigned char *tvnvsn; 
  unsigned short tvnvsnl; 
  unsigned char *tvnnm;
  unsigned short tvnnml; 
  unsigned char *tvnsnm;
  unsigned short tvnsnml;
};
typedef struct sqltvn sqltvn;

struct sqladts
{
  unsigned int adtvsn; 
  unsigned short adtmode; 
  unsigned short adtnum;  
  sqltvn adttvn[1];       
};
typedef struct sqladts sqladts;

static struct sqladts sqladt = {
  1,1,0,
};

/* Binding to PL/SQL Records */
struct sqltdss
{
  unsigned int tdsvsn; 
  unsigned short tdsnum; 
  unsigned char *tdsval[1]; 
};
typedef struct sqltdss sqltdss;
static struct sqltdss sqltds =
{
  1,
  0,
};

/* File name & Package Name */
struct sqlcxp
{
  unsigned short fillen;
           char  filnam[39];
};
static const struct sqlcxp sqlfpn =
{
    38,
    "../../gral/src_db/CTransaccionalCdl.pc"
};


static unsigned int sqlctx = 615941093;


static struct sqlexd {
   unsigned long  sqlvsn;
   unsigned int   arrsiz;
   unsigned int   iters;
   unsigned int   offset;
   unsigned short selerr;
   unsigned short sqlety;
   unsigned int   occurs;
      const short *cud;
   unsigned char  *sqlest;
      const char  *stmt;
   sqladts *sqladtp;
   sqltdss *sqltdsp;
   unsigned char  **sqphsv;
   unsigned long  *sqphsl;
            int   *sqphss;
            short **sqpind;
            int   *sqpins;
   unsigned long  *sqparm;
   unsigned long  **sqparc;
   unsigned short  *sqpadto;
   unsigned short  *sqptdso;
   unsigned int   sqlcmax;
   unsigned int   sqlcmin;
   unsigned int   sqlcincr;
   unsigned int   sqlctimeout;
   unsigned int   sqlcnowait;
            int   sqfoff;
   unsigned int   sqcmod;
   unsigned int   sqfmod;
   unsigned int   sqlpfmem;
   unsigned char  *sqhstv[26];
   unsigned long  sqhstl[26];
            int   sqhsts[26];
            short *sqindv[26];
            int   sqinds[26];
   unsigned long  sqharm[26];
   unsigned long  *sqharc[26];
   unsigned short  sqadto[26];
   unsigned short  sqtdso[26];
} sqlstm = {13,26};

// Prototypes
extern "C" {
  void sqlcxt (void **, unsigned int *,
               struct sqlexd *, const struct sqlcxp *);
  void sqlcx2t(void **, unsigned int *,
               struct sqlexd *, const struct sqlcxp *);
  void sqlbuft(void **, char *);
  void sqlgs2t(void **, char *);
  void sqlorat(void **, unsigned int *, void *);
}

// Forms Interface
static const int IAPSUCC = 0;
static const int IAPFAIL = 1403;
static const int IAPFTL  = 535;
extern "C" { void sqliem(unsigned char *, signed int *); }

 static const char *sq0003 = 
"select trim(VALOR)  from ALLPARAMETER where ((CLAVE_INST=:b0 and CLAVE_SIS=:\
b1) and NOMBRE=:b2) order by NO_PARAMETRO            ";

 static const char *sq0002 = 
"select trim(VALOR)  from ALLPARAMETER where ((CLAVE_INST=:b0 and CLAVE_SIS=:\
b1) and NOMBRE=:b2)           ";

typedef struct { unsigned short len; unsigned char arr[1]; } VARCHAR;
typedef struct { unsigned short len; unsigned char arr[1]; } varchar;

/* cud (compilation unit data) array */
static const short sqlcud0[] =
{13,4146,1,0,0,
5,0,0,1,109,0,4,73,0,0,1,0,0,1,0,2,97,0,0,
24,0,0,4,187,0,4,188,0,0,7,6,0,1,0,2,3,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,
1,97,0,0,1,97,0,0,
67,0,0,5,212,0,5,203,0,0,7,7,0,1,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,
0,1,97,0,0,1,97,0,0,
110,0,0,6,214,0,4,231,0,0,7,6,0,1,0,2,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,
0,1,97,0,0,1,97,0,0,
153,0,0,7,244,0,5,248,0,0,7,7,0,1,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,
0,1,97,0,0,1,97,0,0,
196,0,0,8,255,0,4,447,0,0,9,6,0,1,0,2,97,0,0,2,97,0,0,2,97,0,0,1,97,0,0,1,97,0,
0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,
247,0,0,9,267,0,5,468,0,0,9,9,0,1,0,1,97,0,0,1,4,0,0,1,97,0,0,1,97,0,0,1,97,0,
0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,
298,0,0,10,626,0,3,507,0,0,26,26,0,1,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,
97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,
1,97,0,0,1,97,0,0,1,97,0,0,1,4,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,
0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,
417,0,0,3,129,0,9,648,0,0,3,3,0,1,0,1,97,0,0,1,97,0,0,1,97,0,0,
444,0,0,3,0,0,13,651,0,0,1,0,0,1,0,2,97,0,0,
463,0,0,11,213,0,4,692,0,0,8,6,0,1,0,2,97,0,0,2,97,0,0,1,97,0,0,1,97,0,0,1,97,
0,0,1,97,0,0,1,97,0,0,1,97,0,0,
510,0,0,12,228,0,5,715,0,0,8,8,0,1,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,
0,0,1,97,0,0,1,97,0,0,1,97,0,0,
557,0,0,13,631,0,3,742,0,0,26,26,0,1,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,
97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,
1,97,0,0,1,97,0,0,1,97,0,0,1,4,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,
0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,
676,0,0,14,233,0,4,782,0,0,8,6,0,1,0,2,97,0,0,2,97,0,0,1,97,0,0,1,97,0,0,1,97,
0,0,1,97,0,0,1,97,0,0,1,97,0,0,
723,0,0,15,270,0,5,800,0,0,9,9,0,1,0,1,97,0,0,1,97,0,0,1,4,0,0,1,97,0,0,1,97,0,
0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,
774,0,0,16,598,0,3,829,0,0,25,25,0,1,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,
97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,
1,97,0,0,1,97,0,0,1,97,0,0,1,4,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,
0,1,97,0,0,1,97,0,0,1,97,0,0,
889,0,0,2,106,0,9,876,0,0,3,3,0,1,0,1,97,0,0,1,97,0,0,1,97,0,0,
916,0,0,2,0,0,13,879,0,0,1,0,0,1,0,2,97,0,0,
935,0,0,2,0,0,15,891,0,0,0,0,0,1,0,
950,0,0,17,213,0,4,903,0,0,8,6,0,1,0,2,97,0,0,2,97,0,0,1,97,0,0,1,97,0,0,1,97,
0,0,1,97,0,0,1,97,0,0,1,97,0,0,
997,0,0,18,228,0,5,924,0,0,8,8,0,1,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,
0,0,1,97,0,0,1,97,0,0,1,97,0,0,
1044,0,0,19,631,0,3,952,0,0,26,26,0,1,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,
97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,
1,97,0,0,1,97,0,0,1,97,0,0,1,4,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,
0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,
1163,0,0,20,233,0,4,992,0,0,8,6,0,1,0,2,97,0,0,2,97,0,0,1,97,0,0,1,97,0,0,1,97,
0,0,1,97,0,0,1,97,0,0,1,97,0,0,
1210,0,0,21,270,0,5,1010,0,0,9,9,0,1,0,1,97,0,0,1,97,0,0,1,4,0,0,1,97,0,0,1,97,
0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,
1261,0,0,22,598,0,3,1039,0,0,25,25,0,1,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,
97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,
1,97,0,0,1,97,0,0,1,97,0,0,1,4,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,
0,1,97,0,0,1,97,0,0,1,97,0,0,
1376,0,0,23,245,0,4,1115,0,0,8,7,0,1,0,2,3,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,
0,0,1,97,0,0,1,97,0,0,1,97,0,0,
1423,0,0,24,342,0,4,1129,0,0,10,7,0,1,0,2,97,0,0,2,97,0,0,2,4,0,0,1,97,0,0,1,
97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,
1478,0,0,25,286,0,5,1158,0,0,9,9,0,1,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,
97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,
1529,0,0,26,241,0,4,1198,0,0,7,6,0,1,0,2,3,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,
0,0,1,97,0,0,1,97,0,0,
1572,0,0,27,267,0,4,1212,0,0,8,6,0,1,0,2,97,0,0,2,97,0,0,1,97,0,0,1,97,0,0,1,
97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,
1619,0,0,28,304,0,5,1235,0,0,9,9,0,1,0,1,97,0,0,1,97,0,0,1,4,0,0,1,97,0,0,1,97,
0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,
1670,0,0,29,598,0,3,1267,0,0,25,25,0,1,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,
97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,
1,97,0,0,1,97,0,0,1,97,0,0,1,4,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,
0,1,97,0,0,1,97,0,0,1,97,0,0,
1785,0,0,2,106,0,9,1324,0,0,3,3,0,1,0,1,97,0,0,1,97,0,0,1,97,0,0,
1812,0,0,2,0,0,13,1327,0,0,1,0,0,1,0,2,97,0,0,
1831,0,0,2,0,0,15,1337,0,0,0,0,0,1,0,
1846,0,0,30,250,0,4,1350,0,0,8,5,0,1,0,2,97,0,0,2,97,0,0,2,4,0,0,1,97,0,0,1,97,
0,0,1,97,0,0,1,97,0,0,1,97,0,0,
1893,0,0,31,228,0,4,1375,0,0,8,7,0,1,0,2,4,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,
0,0,1,97,0,0,1,97,0,0,1,97,0,0,
1940,0,0,32,255,0,4,1442,0,0,9,6,0,1,0,2,97,0,0,2,97,0,0,2,97,0,0,1,97,0,0,1,
97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,
1991,0,0,33,300,0,5,1464,0,0,11,11,0,1,0,1,97,0,0,1,97,0,0,1,4,0,0,1,97,0,0,1,
97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,
2050,0,0,34,268,0,5,1492,0,0,9,9,0,1,0,1,97,0,0,1,97,0,0,1,97,0,0,1,4,0,0,1,97,
0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,
2101,0,0,35,598,0,3,1522,0,0,25,25,0,1,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,
97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,
1,97,0,0,1,97,0,0,1,97,0,0,1,4,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,
0,1,97,0,0,1,97,0,0,1,97,0,0,
2216,0,0,36,268,0,5,1554,0,0,9,9,0,1,0,1,97,0,0,1,97,0,0,1,97,0,0,1,4,0,0,1,97,
0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,
2267,0,0,2,106,0,9,1592,0,0,3,3,0,1,0,1,97,0,0,1,97,0,0,1,97,0,0,
2294,0,0,2,0,0,13,1595,0,0,1,0,0,1,0,2,97,0,0,
2313,0,0,2,0,0,15,1605,0,0,0,0,0,1,0,
2328,0,0,37,134,0,5,1609,0,0,3,3,0,1,0,1,97,0,0,1,97,0,0,1,97,0,0,
2355,0,0,2,106,0,9,1638,0,0,3,3,0,1,0,1,97,0,0,1,97,0,0,1,97,0,0,
2382,0,0,2,0,0,13,1641,0,0,1,0,0,1,0,2,97,0,0,
2401,0,0,2,0,0,15,1651,0,0,0,0,0,1,0,
2416,0,0,38,356,0,4,1660,0,0,13,10,0,1,0,2,97,0,0,2,97,0,0,2,97,0,0,1,97,0,0,1,
97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,4,0,0,1,4,0,0,1,4,0,0,1,4,0,0,
2483,0,0,39,255,0,4,1714,0,0,9,6,0,1,0,2,97,0,0,2,97,0,0,2,97,0,0,1,97,0,0,1,
97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,
2534,0,0,40,287,0,5,1738,0,0,11,11,0,1,0,1,97,0,0,1,97,0,0,1,97,0,0,1,4,0,0,1,
97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,
2593,0,0,41,313,0,5,1768,0,0,12,12,0,1,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,
97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,4,0,0,1,4,0,0,1,4,0,0,1,4,0,0,
2656,0,0,42,598,0,3,1802,0,0,26,26,0,1,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,
97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,
1,97,0,0,1,97,0,0,1,97,0,0,1,4,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,
0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,
2775,0,0,43,313,0,5,1833,0,0,12,12,0,1,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,
97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,4,0,0,1,4,0,0,1,4,0,0,1,4,0,0,
2838,0,0,2,106,0,9,1872,0,0,3,3,0,1,0,1,97,0,0,1,97,0,0,1,97,0,0,
2865,0,0,2,0,0,13,1875,0,0,1,0,0,1,0,2,97,0,0,
2884,0,0,2,0,0,15,1885,0,0,0,0,0,1,0,
2899,0,0,44,134,0,5,1889,0,0,3,3,0,1,0,1,97,0,0,1,97,0,0,1,97,0,0,
2926,0,0,3,0,0,15,1912,0,0,0,0,0,1,0,
2941,0,0,45,276,0,4,1924,0,0,9,7,0,1,0,2,97,0,0,2,97,0,0,1,97,0,0,1,97,0,0,1,
97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,4,0,0,
2992,0,0,46,306,0,5,1958,0,0,9,9,0,1,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,
97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,4,0,0,
3043,0,0,47,255,0,4,1996,0,0,9,6,0,1,0,2,97,0,0,2,97,0,0,2,97,0,0,1,97,0,0,1,
97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,
3094,0,0,48,282,0,5,2017,0,0,11,11,0,1,0,1,97,0,0,1,97,0,0,1,97,0,0,1,4,0,0,1,
97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,
3153,0,0,49,598,0,3,2046,0,0,26,26,0,1,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,
97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,
1,97,0,0,1,97,0,0,1,97,0,0,1,4,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,
0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,
3272,0,0,50,187,0,4,2190,0,0,7,6,0,1,0,2,3,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,
0,0,1,97,0,0,1,97,0,0,
3315,0,0,51,220,0,4,2204,0,0,7,6,0,1,0,2,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,
97,0,0,1,97,0,0,1,97,0,0,
3358,0,0,52,475,0,5,2233,0,0,22,22,0,1,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,
97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,4,0,0,1,97,0,0,1,97,0,0,
1,97,0,0,1,4,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,
0,
3461,0,0,53,485,0,3,2367,0,0,26,26,0,1,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,
97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,
1,97,0,0,1,97,0,0,1,97,0,0,1,4,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,
0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,
3580,0,0,54,187,0,4,2442,0,0,7,6,0,1,0,2,3,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,
0,0,1,97,0,0,1,97,0,0,
3623,0,0,55,194,0,4,2457,0,0,7,6,0,1,0,2,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,
97,0,0,1,97,0,0,1,97,0,0,
3666,0,0,56,212,0,5,2474,0,0,7,7,0,1,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,
97,0,0,1,97,0,0,1,97,0,0,
3709,0,0,57,181,0,4,2542,0,0,7,6,0,1,0,2,3,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,
0,0,1,97,0,0,1,97,0,0,
3752,0,0,58,164,0,2,2554,0,0,6,6,0,1,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,
97,0,0,1,97,0,0,
3791,0,0,59,187,0,4,2592,0,0,7,6,0,1,0,2,3,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,
0,0,1,97,0,0,1,97,0,0,
3834,0,0,60,170,0,2,2607,0,0,6,6,0,1,0,1,97,0,0,1,97,0,0,1,97,0,0,1,97,0,0,1,
97,0,0,1,97,0,0,
3873,0,0,3,129,0,9,2652,0,0,3,3,0,1,0,1,97,0,0,1,97,0,0,1,97,0,0,
3900,0,0,3,0,0,13,2655,0,0,1,0,0,1,0,2,97,0,0,
3919,0,0,3,0,0,15,2666,0,0,0,0,0,1,0,
};


#line 1 "../../gral/src_db/CTransaccionalCdl.pc"
///////////////////////////////////////////////////////////////////
//                                                               //
//  Programa auxiliar para ejecutar la CONCILIACION CDL          //
//                                                               //
//  CTRANSACCIONALCDL.PC                                         //
//                          Heriberto Guapo (G. Tecnis) Jul-2010 //
//                                                               //
///////////////////////////////////////////////////////////////////

#define SQLCA_STORAGE_CLASS extern
#define ORACA_STORAGE_CLASS extern

#define SEPARADOR            '|'

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <iostream>

#include <ctype.h>
#include <time.h>
#include <math.h>
#include <gfi-err.h>
#include "../../gral/include/hora.h"
#include "../../gral/include_db/gralsql_oracle.h"
#include "../../gral/include/Mensaje.h"
#include "../../gral/include_db/CTransaccionalCdl.h" // CONTROL DE TRANSACCIONES CDL

using namespace std;

#if defined (__cplusplus) || defined (c_plusplus)
extern "C"
{
#endif

#include <sqlda.h>
#include <sqlcpr.h>

#if defined (__cplusplus) || defined (c_plusplus)
}
#endif

using namespace std;

extern int  NIVEL_TRACE;
extern char err_desc[ 10 ];
extern int  sql_error(void); // HGT 11-May-2010

// Globales internas
char contrapartes[ 200 ]; // HGT 26-May-2010 Ini
char * found;
char llave_c[ 10 ]; // HGT 26-May-2010 Fin


/* EXEC SQL BEGIN DECLARE SECTION; */ 
#line 55 "../../gral/src_db/CTransaccionalCdl.pc"

   char sql_insth[20];
   char sql_sish[20];
   char sql_nmph[20];
   char ref_concilG[20];
   char sql_valor[100];
/* EXEC SQL END DECLARE SECTION; */ 
#line 61 "../../gral/src_db/CTransaccionalCdl.pc"


///////////////////////////////////////////////////////////////////
//  Funcion: Obtiene_Ref_Concil                                  //
///////////////////////////////////////////////////////////////////
int Obtiene_Ref_Concil()
{
  int  resultado = GFI_ERRC_OK;
//  long rowcount;
  ref_concilG[0] = '\0';  //NULL

  // Modificado HGT 15-Jun-2010
  /* EXEC SQL SELECT TRIM(TO_CHAR(SYSDATE, 'SSSSS'))||TRIM(TO_CHAR(dbms_random.value(1,1000),'9999'))
           INTO   :ref_concilG
           FROM   DUAL; */ 
#line 75 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 73 "../../gral/src_db/CTransaccionalCdl.pc"
  struct sqlexd sqlstm;
#line 73 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqlvsn = 13;
#line 73 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.arrsiz = 1;
#line 73 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqladtp = &sqladt;
#line 73 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqltdsp = &sqltds;
#line 73 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.stmt = "select (trim(TO_CHAR(SYSDATE,'SSSSS'))||trim(TO_CHAR(dbms_r\
andom.value(1,1000),'9999'))) into :b0  from DUAL ";
#line 73 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.iters = (unsigned int  )1;
#line 73 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.offset = (unsigned int  )5;
#line 73 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.selerr = (unsigned short)1;
#line 73 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqlpfmem = (unsigned int  )0;
#line 73 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.cud = sqlcud0;
#line 73 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 73 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqlety = (unsigned short)4352;
#line 73 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.occurs = (unsigned int  )0;
#line 73 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstv[0] = (unsigned char  *)ref_concilG;
#line 73 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstl[0] = (unsigned long )20;
#line 73 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhsts[0] = (         int  )0;
#line 73 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqindv[0] = (         short *)0;
#line 73 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqinds[0] = (         int  )0;
#line 73 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqharm[0] = (unsigned long )0;
#line 73 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqadto[0] = (unsigned short )0;
#line 73 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqtdso[0] = (unsigned short )0;
#line 73 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqphsv = sqlstm.sqhstv;
#line 73 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqphsl = sqlstm.sqhstl;
#line 73 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqphss = sqlstm.sqhsts;
#line 73 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqpind = sqlstm.sqindv;
#line 73 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqpins = sqlstm.sqinds;
#line 73 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqparm = sqlstm.sqharm;
#line 73 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqparc = sqlstm.sqharc;
#line 73 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqpadto = sqlstm.sqadto;
#line 73 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqptdso = sqlstm.sqtdso;
#line 73 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 73 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 75 "../../gral/src_db/CTransaccionalCdl.pc"


  Gfi_StrRTrim(ref_concilG);

  //cout << "NIVEL_TRACE:" << NIVEL_TRACE << endl;
  if (resultado != GFI_ERRC_OK)
  {
    if (NIVEL_TRACE > 5)
      cout << "ERROR AL OBTENER LA REFERENCIA DE CONCILIACION!" << endl;

    strcpy(err_desc,"G0001");
  }
  else
  {
    if (NIVEL_TRACE > 9)
      cout << "***** Referencia Conciliacion >" << ref_concilG << "< *****" << endl;
  }

  return (resultado);
}

/////////////////////////////////////////////////////
// DeclaraCursorsH  (Declaraciones de cursores)    //
/////////////////////////////////////////////////////
int DeclaraCursorsH(void)
{
   int resultado = GFI_ERRC_OK;

   // cursor para obtener los PARAMETROS para CONCILIAR
   /* EXEC SQL DECLARE OBTENPARAM CURSOR FOR
   SELECT TRIM(VALOR)
   FROM   ALLPARAMETER
   WHERE  CLAVE_INST = :sql_insth
     AND  CLAVE_SIS  = :sql_sish
     AND  NOMBRE     = :sql_nmph; */ 
#line 109 "../../gral/src_db/CTransaccionalCdl.pc"


   // cursor para obtener los TIPOS Y PRIORIDADES para CONCILIAR
   /* EXEC SQL DECLARE OBTENPTCONC CURSOR FOR
   SELECT TRIM(VALOR)
   FROM   ALLPARAMETER
   WHERE  CLAVE_INST = :sql_insth
     AND  CLAVE_SIS  = :sql_sish
     AND  NOMBRE     = :sql_nmph
   ORDER BY NO_PARAMETRO; */ 
#line 118 "../../gral/src_db/CTransaccionalCdl.pc"


   return (resultado);
}

/////////////////////////////////////////////////////////
//  Funcion:  ProcesaBaja                              //
/////////////////////////////////////////////////////////
int  ProcesaBaja( char *clave_inst,  char *clave_sis, char *clave_rast, char *clave_tran,
                  char *fecha_recep, char *tipo,      char *status )
{ // Ini
  /* EXEC SQL BEGIN DECLARE SECTION; */ 
#line 129 "../../gral/src_db/CTransaccionalCdl.pc"

  char sql_clave_inst[10];
  char sql_clave_sis[10];
  char sql_clave_rast[31];
  char sql_clave_tran[10];
  char sql_fecha_recep[10];
  char sql_tipo[2];
  char sql_status[5];
  char loc_status[5];

  //int  contpar;
  int  contpro;
  /* EXEC SQL END DECLARE SECTION; */ 
#line 141 "../../gral/src_db/CTransaccionalCdl.pc"


/*   EXEC SQL INCLUDE SQLCA;
 */ 
#line 1 "/oracle/12.1.0/precomp/public/SQLCA.H"
/*
 * $Header: sqlca.h 24-apr-2003.12:50:58 mkandarp Exp $ sqlca.h 
 */

/* Copyright (c) 1985, 2003, Oracle Corporation.  All rights reserved.  */
 
/*
NAME
  SQLCA : SQL Communications Area.
FUNCTION
  Contains no code. Oracle fills in the SQLCA with status info
  during the execution of a SQL stmt.
NOTES
  **************************************************************
  ***                                                        ***
  *** This file is SOSD.  Porters must change the data types ***
  *** appropriately on their platform.  See notes/pcport.doc ***
  *** for more information.                                  ***
  ***                                                        ***
  **************************************************************

  If the symbol SQLCA_STORAGE_CLASS is defined, then the SQLCA
  will be defined to have this storage class. For example:
 
    #define SQLCA_STORAGE_CLASS extern
 
  will define the SQLCA as an extern.
 
  If the symbol SQLCA_INIT is defined, then the SQLCA will be
  statically initialized. Although this is not necessary in order
  to use the SQLCA, it is a good pgming practice not to have
  unitialized variables. However, some C compilers/OS's don't
  allow automatic variables to be init'd in this manner. Therefore,
  if you are INCLUDE'ing the SQLCA in a place where it would be
  an automatic AND your C compiler/OS doesn't allow this style
  of initialization, then SQLCA_INIT should be left undefined --
  all others can define SQLCA_INIT if they wish.

  If the symbol SQLCA_NONE is defined, then the SQLCA variable will
  not be defined at all.  The symbol SQLCA_NONE should not be defined
  in source modules that have embedded SQL.  However, source modules
  that have no embedded SQL, but need to manipulate a sqlca struct
  passed in as a parameter, can set the SQLCA_NONE symbol to avoid
  creation of an extraneous sqlca variable.
 
MODIFIED
    lvbcheng   07/31/98 -  long to int
    jbasu      12/12/94 -  Bug 217878: note this is an SOSD file
    losborne   08/11/92 -  No sqlca var if SQLCA_NONE macro set 
  Clare      12/06/84 - Ch SQLCA to not be an extern.
  Clare      10/21/85 - Add initialization.
  Bradbury   01/05/86 - Only initialize when SQLCA_INIT set
  Clare      06/12/86 - Add SQLCA_STORAGE_CLASS option.
*/
 
#ifndef SQLCA
#define SQLCA 1
 
struct   sqlca
         {
         /* ub1 */ char    sqlcaid[8];
         /* b4  */ int     sqlabc;
         /* b4  */ int     sqlcode;
         struct
           {
           /* ub2 */ unsigned short sqlerrml;
           /* ub1 */ char           sqlerrmc[70];
           } sqlerrm;
         /* ub1 */ char    sqlerrp[8];
         /* b4  */ int     sqlerrd[6];
         /* ub1 */ char    sqlwarn[8];
         /* ub1 */ char    sqlext[8];
         };

#ifndef SQLCA_NONE 
#ifdef   SQLCA_STORAGE_CLASS
SQLCA_STORAGE_CLASS struct sqlca sqlca
#else
         struct sqlca sqlca
#endif
 
#ifdef  SQLCA_INIT
         = {
         {'S', 'Q', 'L', 'C', 'A', ' ', ' ', ' '},
         sizeof(struct sqlca),
         0,
         { 0, {0}},
         {'N', 'O', 'T', ' ', 'S', 'E', 'T', ' '},
         {0, 0, 0, 0, 0, 0},
         {0, 0, 0, 0, 0, 0, 0, 0},
         {0, 0, 0, 0, 0, 0, 0, 0}
         }
#endif
         ;
#endif
 
#endif
 
/* end SQLCA */

#line 144 "../../gral/src_db/CTransaccionalCdl.pc"

/////////////////////////////////////////////////////////
// Declaracion de datos para uso dentro de la funcion  //
/////////////////////////////////////////////////////////
  int resultado = GFI_ERRC_OK;
  long rowcount;

/////////////////////////////////////////////////////////
//  Inicio de codigo                                   //
/////////////////////////////////////////////////////////
  strcpy( sql_clave_inst,  clave_inst);
  strcpy( sql_clave_sis,   clave_sis);
  strcpy( sql_clave_rast,  clave_rast);
  strcpy( sql_clave_tran,  clave_tran);
  strcpy( sql_fecha_recep, fecha_recep);
  strcpy( sql_tipo,        tipo);
  strcpy( sql_status,      status);

  if (NIVEL_TRACE > 5)
    printf("Inicia BAJA con Clave de Rastreo: >%s<\n", sql_clave_rast);

  // Busca la contraparte
  /* HGT 26-May-2010 - Inicio cambio Propuesto por H.Martinez 19-May-2010
  contpar = 0;

  EXEC SQL SELECT COUNT(1)
           INTO   :contpar
           FROM   ALLPARAMETER
           WHERE  CLAVE_INST = :sql_clave_inst
             AND  CLAVE_SIS  = :sql_clave_sis
             AND  NOMBRE     = 'CONTRAPARTE'
             AND  VALOR      = :sql_clave_tran;*/
  sprintf(llave_c,"|%s|",sql_clave_tran);

  if (strstr(contrapartes,llave_c) != 0)
  //if (contpar > 0) HGT 26-May-2010 Fin
  { // 1baja
    if (NIVEL_TRACE > 9)
      cout << "VERIFICA SI EXISTE LA CONTRAPARTE EN TRANS_PROGRAMADO" << endl;

    contpro = 0;

    // Index: CLAVE_INST, CLAVE_SIS, CLAVE_RAST, CLAVE_TRAN, FECHA_RECEP, TIPO

    /* EXEC SQL SELECT COUNT(1)
             INTO   :contpro
             FROM   TRANS_PROGRAMADO
             WHERE  CLAVE_INST  = :sql_clave_inst
               AND  CLAVE_SIS   = :sql_clave_sis
               AND  CLAVE_RAST  = :sql_clave_rast
               AND  CLAVE_TRAN  = :sql_clave_tran
               AND  FECHA_RECEP = TO_DATE(:sql_fecha_recep, 'YYYYMMDD')
               AND  TIPO        = :sql_tipo; */ 
#line 196 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    struct sqlexd sqlstm;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqlvsn = 13;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.arrsiz = 7;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqladtp = &sqladt;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqltdsp = &sqltds;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.stmt = "select count(1) into :b0  from TRANS_PROGRAMADO where (((\
((CLAVE_INST=:b1 and CLAVE_SIS=:b2) and CLAVE_RAST=:b3) and CLAVE_TRAN=:b4) an\
d FECHA_RECEP=TO_DATE(:b5,'YYYYMMDD')) and TIPO=:b6)";
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.iters = (unsigned int  )1;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.offset = (unsigned int  )24;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.selerr = (unsigned short)1;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqlpfmem = (unsigned int  )0;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.cud = sqlcud0;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqlety = (unsigned short)4352;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.occurs = (unsigned int  )0;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[0] = (unsigned char  *)&contpro;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[0] = (unsigned long )sizeof(int);
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[0] = (         int  )0;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[0] = (         short *)0;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[0] = (         int  )0;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[0] = (unsigned long )0;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[0] = (unsigned short )0;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[0] = (unsigned short )0;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[1] = (unsigned char  *)sql_clave_inst;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[1] = (unsigned long )10;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[1] = (         int  )0;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[1] = (         short *)0;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[1] = (         int  )0;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[1] = (unsigned long )0;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[1] = (unsigned short )0;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[1] = (unsigned short )0;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[2] = (unsigned char  *)sql_clave_sis;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[2] = (unsigned long )10;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[2] = (         int  )0;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[2] = (         short *)0;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[2] = (         int  )0;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[2] = (unsigned long )0;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[2] = (unsigned short )0;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[2] = (unsigned short )0;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[3] = (unsigned char  *)sql_clave_rast;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[3] = (unsigned long )31;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[3] = (         int  )0;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[3] = (         short *)0;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[3] = (         int  )0;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[3] = (unsigned long )0;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[3] = (unsigned short )0;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[3] = (unsigned short )0;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[4] = (unsigned char  *)sql_clave_tran;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[4] = (unsigned long )10;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[4] = (         int  )0;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[4] = (         short *)0;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[4] = (         int  )0;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[4] = (unsigned long )0;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[4] = (unsigned short )0;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[4] = (unsigned short )0;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[5] = (unsigned char  *)sql_fecha_recep;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[5] = (unsigned long )10;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[5] = (         int  )0;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[5] = (         short *)0;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[5] = (         int  )0;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[5] = (unsigned long )0;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[5] = (unsigned short )0;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[5] = (unsigned short )0;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[6] = (unsigned char  *)sql_tipo;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[6] = (unsigned long )2;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[6] = (         int  )0;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[6] = (         short *)0;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[6] = (         int  )0;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[6] = (unsigned long )0;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[6] = (unsigned short )0;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[6] = (unsigned short )0;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqphsv = sqlstm.sqhstv;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqphsl = sqlstm.sqhstl;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqphss = sqlstm.sqhsts;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqpind = sqlstm.sqindv;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqpins = sqlstm.sqinds;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqparm = sqlstm.sqharm;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqparc = sqlstm.sqharc;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqpadto = sqlstm.sqadto;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqptdso = sqlstm.sqtdso;
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 188 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 196 "../../gral/src_db/CTransaccionalCdl.pc"


    if (contpro > 0)
    {
      if (NIVEL_TRACE > 9)
        cout << "DANDO DE BAJA EL PROGRAMADO" << endl;

      /* EXEC SQL UPDATE TRANS_PROGRAMADO
               SET    STATUS = 'P' || :sql_tipo || 'B',
                      FECHA_BAJA = SYSDATE
               WHERE  CLAVE_INST  = :sql_clave_inst
                 AND  CLAVE_SIS   = :sql_clave_sis
                 AND  CLAVE_RAST  = :sql_clave_rast
                 AND  CLAVE_TRAN  = :sql_clave_tran
                 AND  FECHA_RECEP = TO_DATE(:sql_fecha_recep, 'YYYYMMDD')
                 AND  TIPO        = :sql_tipo; */ 
#line 211 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      struct sqlexd sqlstm;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqlvsn = 13;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.arrsiz = 7;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqladtp = &sqladt;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqltdsp = &sqltds;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.stmt = "update TRANS_PROGRAMADO  set STATUS=(('P'||:b0)||'B'),F\
ECHA_BAJA=SYSDATE where (((((CLAVE_INST=:b1 and CLAVE_SIS=:b2) and CLAVE_RAST=\
:b3) and CLAVE_TRAN=:b4) and FECHA_RECEP=TO_DATE(:b5,'YYYYMMDD')) and TIPO=:b0\
)";
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.iters = (unsigned int  )1;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.offset = (unsigned int  )67;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.cud = sqlcud0;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqlety = (unsigned short)4352;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.occurs = (unsigned int  )0;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[0] = (unsigned char  *)sql_tipo;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[0] = (unsigned long )2;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[0] = (         int  )0;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[0] = (         short *)0;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[0] = (         int  )0;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[0] = (unsigned long )0;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[0] = (unsigned short )0;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[0] = (unsigned short )0;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[1] = (unsigned char  *)sql_clave_inst;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[1] = (unsigned long )10;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[1] = (         int  )0;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[1] = (         short *)0;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[1] = (         int  )0;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[1] = (unsigned long )0;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[1] = (unsigned short )0;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[1] = (unsigned short )0;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[2] = (unsigned char  *)sql_clave_sis;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[2] = (unsigned long )10;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[2] = (         int  )0;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[2] = (         short *)0;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[2] = (         int  )0;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[2] = (unsigned long )0;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[2] = (unsigned short )0;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[2] = (unsigned short )0;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[3] = (unsigned char  *)sql_clave_rast;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[3] = (unsigned long )31;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[3] = (         int  )0;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[3] = (         short *)0;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[3] = (         int  )0;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[3] = (unsigned long )0;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[3] = (unsigned short )0;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[3] = (unsigned short )0;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[4] = (unsigned char  *)sql_clave_tran;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[4] = (unsigned long )10;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[4] = (         int  )0;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[4] = (         short *)0;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[4] = (         int  )0;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[4] = (unsigned long )0;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[4] = (unsigned short )0;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[4] = (unsigned short )0;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[5] = (unsigned char  *)sql_fecha_recep;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[5] = (unsigned long )10;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[5] = (         int  )0;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[5] = (         short *)0;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[5] = (         int  )0;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[5] = (unsigned long )0;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[5] = (unsigned short )0;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[5] = (unsigned short )0;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[6] = (unsigned char  *)sql_tipo;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[6] = (unsigned long )2;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[6] = (         int  )0;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[6] = (         short *)0;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[6] = (         int  )0;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[6] = (unsigned long )0;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[6] = (unsigned short )0;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[6] = (unsigned short )0;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqphsv = sqlstm.sqhstv;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqphsl = sqlstm.sqhstl;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqphss = sqlstm.sqhsts;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqpind = sqlstm.sqindv;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqpins = sqlstm.sqinds;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqparm = sqlstm.sqharm;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqparc = sqlstm.sqharc;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqpadto = sqlstm.sqadto;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqptdso = sqlstm.sqtdso;
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 203 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 211 "../../gral/src_db/CTransaccionalCdl.pc"


      resultado = ChecaErrorOracle( 0, 1, &rowcount);
      if (resultado != GFI_ERRC_OK)
      {
        if (NIVEL_TRACE > 5)
          cout << "ERROR AL DAR DE BAJA EL PROGRAMADO!" << endl;

        strcpy(err_desc,"B0001");
      }
    }
  } // 1baja
  //else
  //  cout << "llave_c >" << llave_c << "< not found!" << endl;

  //Busca la Operacion Real
  loc_status[0] = '\0'; //NULL

  // Index: CLAVE_INST, CLAVE_SIS, CLAVE_RAST, CLAVE_TRAN, FECHA_RECEP, TIPO

  /* EXEC SQL SELECT NVL(STATUS,' ')
           INTO   :loc_status
           FROM   TRANS_PAGO
           WHERE  CLAVE_INST  = :sql_clave_inst
             AND  CLAVE_SIS   = :sql_clave_sis
             AND  CLAVE_RAST  = :sql_clave_rast
             AND  CLAVE_TRAN  = :sql_clave_tran
             AND  FECHA_RECEP = TO_DATE(:sql_fecha_recep, 'YYYYMMDD')
             AND  TIPO        = :sql_tipo
             AND  FECHA_BAJA IS NULL; */ 
#line 240 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  struct sqlexd sqlstm;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqlvsn = 13;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.arrsiz = 7;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqladtp = &sqladt;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqltdsp = &sqltds;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.stmt = "select NVL(STATUS,' ') into :b0  from TRANS_PAGO where ((((\
((CLAVE_INST=:b1 and CLAVE_SIS=:b2) and CLAVE_RAST=:b3) and CLAVE_TRAN=:b4) an\
d FECHA_RECEP=TO_DATE(:b5,'YYYYMMDD')) and TIPO=:b6) and FECHA_BAJA is null )";
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.iters = (unsigned int  )1;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.offset = (unsigned int  )110;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.selerr = (unsigned short)1;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqlpfmem = (unsigned int  )0;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.cud = sqlcud0;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqlety = (unsigned short)4352;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.occurs = (unsigned int  )0;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstv[0] = (unsigned char  *)loc_status;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstl[0] = (unsigned long )5;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhsts[0] = (         int  )0;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqindv[0] = (         short *)0;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqinds[0] = (         int  )0;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqharm[0] = (unsigned long )0;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqadto[0] = (unsigned short )0;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqtdso[0] = (unsigned short )0;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstv[1] = (unsigned char  *)sql_clave_inst;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstl[1] = (unsigned long )10;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhsts[1] = (         int  )0;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqindv[1] = (         short *)0;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqinds[1] = (         int  )0;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqharm[1] = (unsigned long )0;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqadto[1] = (unsigned short )0;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqtdso[1] = (unsigned short )0;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstv[2] = (unsigned char  *)sql_clave_sis;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstl[2] = (unsigned long )10;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhsts[2] = (         int  )0;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqindv[2] = (         short *)0;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqinds[2] = (         int  )0;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqharm[2] = (unsigned long )0;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqadto[2] = (unsigned short )0;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqtdso[2] = (unsigned short )0;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstv[3] = (unsigned char  *)sql_clave_rast;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstl[3] = (unsigned long )31;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhsts[3] = (         int  )0;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqindv[3] = (         short *)0;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqinds[3] = (         int  )0;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqharm[3] = (unsigned long )0;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqadto[3] = (unsigned short )0;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqtdso[3] = (unsigned short )0;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstv[4] = (unsigned char  *)sql_clave_tran;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstl[4] = (unsigned long )10;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhsts[4] = (         int  )0;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqindv[4] = (         short *)0;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqinds[4] = (         int  )0;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqharm[4] = (unsigned long )0;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqadto[4] = (unsigned short )0;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqtdso[4] = (unsigned short )0;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstv[5] = (unsigned char  *)sql_fecha_recep;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstl[5] = (unsigned long )10;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhsts[5] = (         int  )0;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqindv[5] = (         short *)0;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqinds[5] = (         int  )0;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqharm[5] = (unsigned long )0;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqadto[5] = (unsigned short )0;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqtdso[5] = (unsigned short )0;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstv[6] = (unsigned char  *)sql_tipo;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstl[6] = (unsigned long )2;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhsts[6] = (         int  )0;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqindv[6] = (         short *)0;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqinds[6] = (         int  )0;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqharm[6] = (unsigned long )0;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqadto[6] = (unsigned short )0;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqtdso[6] = (unsigned short )0;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqphsv = sqlstm.sqhstv;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqphsl = sqlstm.sqhstl;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqphss = sqlstm.sqhsts;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqpind = sqlstm.sqindv;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqpins = sqlstm.sqinds;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqparm = sqlstm.sqharm;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqparc = sqlstm.sqharc;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqpadto = sqlstm.sqadto;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqptdso = sqlstm.sqtdso;
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 231 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 240 "../../gral/src_db/CTransaccionalCdl.pc"

             //AND  ROWNUM = 1; Propuesto por H.Martinez 19-May-2010

  if (strlen(loc_status) > 0)
  {
    if (NIVEL_TRACE > 9)
      cout << "DANDO DE BAJA EL REAL" << endl;

    /* EXEC SQL UPDATE TRANS_PAGO
             SET    STATUS = 'R' || SUBSTR(:loc_status,2,2) || 'B',
                    FECHA_BAJA = SYSDATE
             WHERE  CLAVE_INST  = :sql_clave_inst
               AND  CLAVE_SIS   = :sql_clave_sis
               AND  CLAVE_RAST  = :sql_clave_rast
               AND  CLAVE_TRAN  = :sql_clave_tran
               AND  FECHA_RECEP = TO_DATE(:sql_fecha_recep, 'YYYYMMDD')
               AND  TIPO        = :sql_tipo
               AND  FECHA_BAJA IS NULL; */ 
#line 257 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    struct sqlexd sqlstm;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqlvsn = 13;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.arrsiz = 7;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqladtp = &sqladt;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqltdsp = &sqltds;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.stmt = "update TRANS_PAGO  set STATUS=(('R'||SUBSTR(:b0,2,2))||'B\
'),FECHA_BAJA=SYSDATE where ((((((CLAVE_INST=:b1 and CLAVE_SIS=:b2) and CLAVE_\
RAST=:b3) and CLAVE_TRAN=:b4) and FECHA_RECEP=TO_DATE(:b5,'YYYYMMDD')) and TIP\
O=:b6) and FECHA_BAJA is null )";
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.iters = (unsigned int  )1;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.offset = (unsigned int  )153;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.cud = sqlcud0;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqlety = (unsigned short)4352;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.occurs = (unsigned int  )0;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[0] = (unsigned char  *)loc_status;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[0] = (unsigned long )5;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[0] = (         int  )0;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[0] = (         short *)0;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[0] = (         int  )0;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[0] = (unsigned long )0;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[0] = (unsigned short )0;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[0] = (unsigned short )0;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[1] = (unsigned char  *)sql_clave_inst;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[1] = (unsigned long )10;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[1] = (         int  )0;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[1] = (         short *)0;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[1] = (         int  )0;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[1] = (unsigned long )0;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[1] = (unsigned short )0;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[1] = (unsigned short )0;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[2] = (unsigned char  *)sql_clave_sis;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[2] = (unsigned long )10;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[2] = (         int  )0;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[2] = (         short *)0;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[2] = (         int  )0;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[2] = (unsigned long )0;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[2] = (unsigned short )0;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[2] = (unsigned short )0;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[3] = (unsigned char  *)sql_clave_rast;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[3] = (unsigned long )31;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[3] = (         int  )0;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[3] = (         short *)0;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[3] = (         int  )0;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[3] = (unsigned long )0;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[3] = (unsigned short )0;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[3] = (unsigned short )0;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[4] = (unsigned char  *)sql_clave_tran;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[4] = (unsigned long )10;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[4] = (         int  )0;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[4] = (         short *)0;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[4] = (         int  )0;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[4] = (unsigned long )0;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[4] = (unsigned short )0;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[4] = (unsigned short )0;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[5] = (unsigned char  *)sql_fecha_recep;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[5] = (unsigned long )10;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[5] = (         int  )0;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[5] = (         short *)0;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[5] = (         int  )0;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[5] = (unsigned long )0;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[5] = (unsigned short )0;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[5] = (unsigned short )0;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[6] = (unsigned char  *)sql_tipo;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[6] = (unsigned long )2;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[6] = (         int  )0;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[6] = (         short *)0;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[6] = (         int  )0;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[6] = (unsigned long )0;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[6] = (unsigned short )0;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[6] = (unsigned short )0;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqphsv = sqlstm.sqhstv;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqphsl = sqlstm.sqhstl;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqphss = sqlstm.sqhsts;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqpind = sqlstm.sqindv;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqpins = sqlstm.sqinds;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqparm = sqlstm.sqharm;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqparc = sqlstm.sqharc;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqpadto = sqlstm.sqadto;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqptdso = sqlstm.sqtdso;
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 248 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 257 "../../gral/src_db/CTransaccionalCdl.pc"


    resultado = ChecaErrorOracle( 0, 1, &rowcount);
    if (resultado != GFI_ERRC_OK)
    {
      if (NIVEL_TRACE > 5)
        cout << "ERROR AL DAR DE BAJA EL PAGO!" << endl;

      strcpy(err_desc,"B0002");
    }
  }
  else
  {
    resultado = GFI_ERRC_FATAL;

    if (NIVEL_TRACE > 5)
      cout << "NO ENCONTRADA PARA APLICAR BAJA!" << endl;

    strcpy(err_desc,"B0003");
  }

  if (NIVEL_TRACE > 5)
    cout << "Termina BAJA" << endl;

  return( resultado);
} // End

/////////////////////////////////////////////////////////
//  Funcion:  ProcesaSala                              //
/////////////////////////////////////////////////////////
int  ProcesaSala( char *clave_inst,  char *clave_sis,     char *clave_rast,   char *clave_tran,
                  char *fecha_recep, char *tipo,          char *folio_origen, char *tipo_oper,
                  char *banco_ord,   char *cve_banco_ord, char *nombre_ord,   char *cuenta_ord,
                  char *banco_ben,   char *cve_banco_ben, char *nombre_ben,   char *cuenta_ben,
                  double importe,    char *observacion,   char *ref_concil,   char *ref_med,
                  char *fecha_cap,   char *fecha_venc,    char *fecha_flujo,  char *fecha_reg,
                  char *fecha_baja,  char *status )
{ // Ini
  /* EXEC SQL BEGIN DECLARE SECTION; */ 
#line 295 "../../gral/src_db/CTransaccionalCdl.pc"

    char sql_clave_inst[6];
    char sql_clave_sis[6];
    char sql_clave_rast[31];
    char sql_clave_tran[8];
    char sql_fecha_recep[40];
    char sql_tipo[2];
    char sql_folio_origen[10];
    char sql_tipo_oper[5];
    char sql_banco_ord[7];
    char sql_cve_banco_ord[16];
    char sql_nombre_ord[52];
    char sql_cuenta_ord[21];
    char sql_banco_ben[7];
    char sql_cve_banco_ben[16];
    char sql_nombre_ben[52];
    char sql_cuenta_ben[21];
    char sql_observacion[50];
    char sql_ref_concil[16];
    char sql_ref_med[18];
    char sql_fecha_cap[40];
    char sql_fecha_venc[18];
    char sql_fecha_flujo[18];
    char sql_fecha_reg[18];
    char sql_fecha_baja[18];
    char sql_status[5];
    char loc_status[5];
    char loc_tipo[2];
    char loc_ref_med[13];
    double sql_importe;
    //int  contpar;
//    int  contpro;
  /* EXEC SQL END DECLARE SECTION; */ 
#line 327 "../../gral/src_db/CTransaccionalCdl.pc"


/*   EXEC SQL INCLUDE SQLCA;
 */ 
#line 1 "/oracle/12.1.0/precomp/public/SQLCA.H"
/*
 * $Header: sqlca.h 24-apr-2003.12:50:58 mkandarp Exp $ sqlca.h 
 */

/* Copyright (c) 1985, 2003, Oracle Corporation.  All rights reserved.  */
 
/*
NAME
  SQLCA : SQL Communications Area.
FUNCTION
  Contains no code. Oracle fills in the SQLCA with status info
  during the execution of a SQL stmt.
NOTES
  **************************************************************
  ***                                                        ***
  *** This file is SOSD.  Porters must change the data types ***
  *** appropriately on their platform.  See notes/pcport.doc ***
  *** for more information.                                  ***
  ***                                                        ***
  **************************************************************

  If the symbol SQLCA_STORAGE_CLASS is defined, then the SQLCA
  will be defined to have this storage class. For example:
 
    #define SQLCA_STORAGE_CLASS extern
 
  will define the SQLCA as an extern.
 
  If the symbol SQLCA_INIT is defined, then the SQLCA will be
  statically initialized. Although this is not necessary in order
  to use the SQLCA, it is a good pgming practice not to have
  unitialized variables. However, some C compilers/OS's don't
  allow automatic variables to be init'd in this manner. Therefore,
  if you are INCLUDE'ing the SQLCA in a place where it would be
  an automatic AND your C compiler/OS doesn't allow this style
  of initialization, then SQLCA_INIT should be left undefined --
  all others can define SQLCA_INIT if they wish.

  If the symbol SQLCA_NONE is defined, then the SQLCA variable will
  not be defined at all.  The symbol SQLCA_NONE should not be defined
  in source modules that have embedded SQL.  However, source modules
  that have no embedded SQL, but need to manipulate a sqlca struct
  passed in as a parameter, can set the SQLCA_NONE symbol to avoid
  creation of an extraneous sqlca variable.
 
MODIFIED
    lvbcheng   07/31/98 -  long to int
    jbasu      12/12/94 -  Bug 217878: note this is an SOSD file
    losborne   08/11/92 -  No sqlca var if SQLCA_NONE macro set 
  Clare      12/06/84 - Ch SQLCA to not be an extern.
  Clare      10/21/85 - Add initialization.
  Bradbury   01/05/86 - Only initialize when SQLCA_INIT set
  Clare      06/12/86 - Add SQLCA_STORAGE_CLASS option.
*/
 
#ifndef SQLCA
#define SQLCA 1
 
struct   sqlca
         {
         /* ub1 */ char    sqlcaid[8];
         /* b4  */ int     sqlabc;
         /* b4  */ int     sqlcode;
         struct
           {
           /* ub2 */ unsigned short sqlerrml;
           /* ub1 */ char           sqlerrmc[70];
           } sqlerrm;
         /* ub1 */ char    sqlerrp[8];
         /* b4  */ int     sqlerrd[6];
         /* ub1 */ char    sqlwarn[8];
         /* ub1 */ char    sqlext[8];
         };

#ifndef SQLCA_NONE 
#ifdef   SQLCA_STORAGE_CLASS
SQLCA_STORAGE_CLASS struct sqlca sqlca
#else
         struct sqlca sqlca
#endif
 
#ifdef  SQLCA_INIT
         = {
         {'S', 'Q', 'L', 'C', 'A', ' ', ' ', ' '},
         sizeof(struct sqlca),
         0,
         { 0, {0}},
         {'N', 'O', 'T', ' ', 'S', 'E', 'T', ' '},
         {0, 0, 0, 0, 0, 0},
         {0, 0, 0, 0, 0, 0, 0, 0},
         {0, 0, 0, 0, 0, 0, 0, 0}
         }
#endif
         ;
#endif
 
#endif
 
/* end SQLCA */

#line 330 "../../gral/src_db/CTransaccionalCdl.pc"

/////////////////////////////////////////////////////////
// Declaracion de datos para uso dentro de la funcion  //
/////////////////////////////////////////////////////////
  int resultado = GFI_ERRC_OK;
  long rowcount;
  char loc_status1[9];
  char loc_status2[9];

/////////////////////////////////////////////////////////
//  Inicio de codigo                                   //
/////////////////////////////////////////////////////////
  strcpy( sql_clave_inst,  clave_inst);
  strcpy( sql_clave_sis,  clave_sis);
  strcpy( sql_clave_rast,  clave_rast);
  strcpy( sql_clave_tran,  clave_tran);
  strcpy( sql_fecha_recep,  fecha_recep);
  strcpy( sql_tipo,  tipo);
  strcpy( sql_folio_origen,  folio_origen);
  strcpy( sql_tipo_oper,  tipo_oper);
  strcpy( sql_banco_ord,  banco_ord);
  strcpy( sql_cve_banco_ord,  cve_banco_ord);
  strcpy( sql_nombre_ord,  nombre_ord);
  strcpy( sql_cuenta_ord,  cuenta_ord);
  strcpy( sql_banco_ben,  banco_ben);
  strcpy( sql_cve_banco_ben,  cve_banco_ben);
  strcpy( sql_nombre_ben,  nombre_ben);
  strcpy( sql_cuenta_ben,  cuenta_ben);
  strcpy( sql_observacion,  observacion);
  strcpy( sql_ref_concil,  ref_concil);
  strcpy( sql_ref_med,  ref_med);
  strcpy( sql_fecha_cap,  fecha_cap);
  strcpy( sql_fecha_venc,  fecha_venc);
  strcpy( sql_fecha_flujo,  fecha_flujo);
  strcpy( sql_fecha_reg,  fecha_reg);
  strcpy( sql_fecha_baja,  fecha_baja);
  strcpy( sql_status,  status);
  sql_importe = importe;

  if (NIVEL_TRACE > 5)
    printf("Inicia SALA DE ESPERA con Clave de Rastreo: >%s<\n", sql_clave_rast);

  /* Propuesto por H.Martinez 19-May-2010
  // Busca la contraparte
  contpar = 0;

  EXEC SQL SELECT COUNT(1)
           INTO   :contpar
           FROM   ALLPARAMETER
           WHERE  CLAVE_INST = :sql_clave_inst
             AND  CLAVE_SIS  = :sql_clave_sis
             AND  NOMBRE     = 'CONTRAPARTE'
             AND  VALOR      = :sql_clave_tran;

  if (contpar > 0)
  { // 1sala
    if (NIVEL_TRACE > 9)
      cout << "GENERANDO CONTRAPARTE" << endl;

    contpro = 0;

    // Index: CLAVE_INST, CLAVE_SIS, CLAVE_RAST, CLAVE_TRAN, FECHA_RECEP, TIPO

    EXEC SQL SELECT COUNT(1)
             INTO   :contpro
             FROM   TRANS_PROGRAMADO
             WHERE  CLAVE_INST  = :sql_clave_inst
               AND  CLAVE_SIS   = :sql_clave_sis
               AND  CLAVE_RAST  = :sql_clave_rast
               AND  CLAVE_TRAN  = :sql_clave_tran
               AND  FECHA_RECEP = TO_DATE(:sql_fecha_recep, 'YYYYMMDD')
               AND  TIPO        = :sql_tipo;

    if (contpro == 0)
    {
      if (NIVEL_TRACE > 9)
        cout << "INSERTANDO EL PROGRAMADO" << endl;

      EXEC SQL INSERT INTO TRANS_PROGRAMADO ( CLAVE_INST , CLAVE_SIS , CLAVE_RAST , CLAVE_TRAN ,
                                              FECHA_RECEP , TIPO , FOLIO_ORIGEN , TIPO_OPER ,
                                              BANCO_ORD , CVE_BANCO_ORD , NOMBRE_ORD , CUENTA_ORD ,
                                              BANCO_BEN , CVE_BANCO_BEN , NOMBRE_BEN , CUENTA_BEN ,
                                              IMPORTE , STATUS , OBSERVACION , REF_CONCIL ,
                                              REF_MED , FECHA_CAP , FECHA_VENC , FECHA_FLUJO ,
                                              FECHA_REG , FECHA_BAJA )
                           VALUES( :sql_clave_inst , :sql_clave_sis , :sql_clave_rast , :sql_clave_tran ,
                                  TO_DATE(:sql_fecha_recep, 'YYYYMMDD') , :sql_tipo , :sql_folio_origen ,
                                  :sql_tipo_oper , :sql_banco_ord , :sql_cve_banco_ord , :sql_nombre_ord ,
                                  :sql_cuenta_ord , :sql_banco_ben , :sql_cve_banco_ben , :sql_nombre_ben ,
                                  :sql_cuenta_ben , ROUND(:sql_importe,2) , 'P' || :sql_tipo , :sql_observacion ,
                                  :ref_concilG , :sql_ref_med , TO_DATE(:sql_fecha_cap, 'YYYYMMDD HH24:MI:SS') ,
                                  TO_DATE(:sql_fecha_venc, 'YYYYMMDD HH24:MI:SS')  ,
                                  TO_DATE(:sql_fecha_flujo, 'YYYYMMDD HH24:MI:SS') ,
                                  TO_DATE(:sql_fecha_reg, 'YYYYMMDD HH24:MI:SS') ,
                                  TO_DATE(:sql_fecha_baja, 'YYYYMMDD HH24:MI:SS') );

      resultado = ChecaErrorOracle( 0, 1, &rowcount);
      if (resultado != GFI_ERRC_OK)
      {
        if (NIVEL_TRACE > 5)
          cout << "ERROR AL INSERTAR EL PROGRAMADO!" << endl;

        strcpy(err_desc,"S0001");
      }
    }
  } // 1sala */

  if (strlen(err_desc) == 0)
  { // 2sala

    if (NIVEL_TRACE > 9)
      cout << "BUSCANDO LA SALA DE ESPERA" << endl;

    loc_status[0] = '\0'; //NULL

    // Index: CLAVE_INST, CLAVE_SIS, CLAVE_RAST, CLAVE_TRAN, FECHA_RECEP, TIPO

    /* EXEC SQL SELECT NVL(STATUS,' '), NVL(TIPO,' '), NVL(REF_MED,' ')
             INTO   :loc_status, :loc_tipo, :loc_ref_med
             FROM   TRANS_PAGO
             WHERE  CLAVE_INST  = :sql_clave_inst
               AND  CLAVE_SIS   = :sql_clave_sis
               AND  CLAVE_RAST  = :sql_clave_rast
               AND  CLAVE_TRAN  = :sql_clave_tran
               AND  FECHA_RECEP = TO_DATE(:sql_fecha_recep, 'YYYYMMDD')
               AND  TIPO        = :sql_tipo
               AND  FECHA_BAJA IS NULL; */ 
#line 456 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    struct sqlexd sqlstm;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqlvsn = 13;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.arrsiz = 9;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqladtp = &sqladt;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqltdsp = &sqltds;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.stmt = "select NVL(STATUS,' ') ,NVL(TIPO,' ') ,NVL(REF_MED,' ') i\
nto :b0,:b1,:b2  from TRANS_PAGO where ((((((CLAVE_INST=:b3 and CLAVE_SIS=:b4)\
 and CLAVE_RAST=:b5) and CLAVE_TRAN=:b6) and FECHA_RECEP=TO_DATE(:b7,'YYYYMMDD\
')) and TIPO=:b8) and FECHA_BAJA is null )";
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.iters = (unsigned int  )1;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.offset = (unsigned int  )196;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.selerr = (unsigned short)1;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqlpfmem = (unsigned int  )0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.cud = sqlcud0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqlety = (unsigned short)4352;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.occurs = (unsigned int  )0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[0] = (unsigned char  *)loc_status;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[0] = (unsigned long )5;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[0] = (         int  )0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[0] = (         short *)0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[0] = (         int  )0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[0] = (unsigned long )0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[0] = (unsigned short )0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[0] = (unsigned short )0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[1] = (unsigned char  *)loc_tipo;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[1] = (unsigned long )2;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[1] = (         int  )0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[1] = (         short *)0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[1] = (         int  )0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[1] = (unsigned long )0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[1] = (unsigned short )0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[1] = (unsigned short )0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[2] = (unsigned char  *)loc_ref_med;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[2] = (unsigned long )13;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[2] = (         int  )0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[2] = (         short *)0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[2] = (         int  )0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[2] = (unsigned long )0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[2] = (unsigned short )0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[2] = (unsigned short )0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[3] = (unsigned char  *)sql_clave_inst;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[3] = (unsigned long )6;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[3] = (         int  )0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[3] = (         short *)0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[3] = (         int  )0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[3] = (unsigned long )0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[3] = (unsigned short )0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[3] = (unsigned short )0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[4] = (unsigned char  *)sql_clave_sis;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[4] = (unsigned long )6;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[4] = (         int  )0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[4] = (         short *)0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[4] = (         int  )0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[4] = (unsigned long )0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[4] = (unsigned short )0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[4] = (unsigned short )0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[5] = (unsigned char  *)sql_clave_rast;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[5] = (unsigned long )31;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[5] = (         int  )0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[5] = (         short *)0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[5] = (         int  )0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[5] = (unsigned long )0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[5] = (unsigned short )0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[5] = (unsigned short )0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[6] = (unsigned char  *)sql_clave_tran;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[6] = (unsigned long )8;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[6] = (         int  )0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[6] = (         short *)0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[6] = (         int  )0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[6] = (unsigned long )0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[6] = (unsigned short )0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[6] = (unsigned short )0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[7] = (unsigned char  *)sql_fecha_recep;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[7] = (unsigned long )40;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[7] = (         int  )0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[7] = (         short *)0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[7] = (         int  )0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[7] = (unsigned long )0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[7] = (unsigned short )0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[7] = (unsigned short )0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[8] = (unsigned char  *)sql_tipo;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[8] = (unsigned long )2;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[8] = (         int  )0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[8] = (         short *)0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[8] = (         int  )0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[8] = (unsigned long )0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[8] = (unsigned short )0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[8] = (unsigned short )0;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqphsv = sqlstm.sqhstv;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqphsl = sqlstm.sqhstl;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqphss = sqlstm.sqhsts;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqpind = sqlstm.sqindv;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqpins = sqlstm.sqinds;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqparm = sqlstm.sqharm;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqparc = sqlstm.sqharc;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqpadto = sqlstm.sqadto;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqptdso = sqlstm.sqtdso;
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 447 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 456 "../../gral/src_db/CTransaccionalCdl.pc"

               //AND  ROWNUM = 1; Propuesto por H.Martinez 19-May-2010

    if (strlen(loc_status) > 0)
    { // 3sala
      sprintf(loc_status1, "R%sPC",sql_tipo);
      sprintf(loc_status2, "R%sNP",sql_tipo);
      if ((strcmp(loc_status,loc_status1)) && (strcmp(loc_status,loc_status2)) && (strlen(sql_fecha_baja)==0))
      {
        if (NIVEL_TRACE > 9)
          cout << "ACTUALIZANDO SALA DE ESPERA" << endl;

        /* EXEC SQL UPDATE TRANS_PAGO
                 SET    STATUS = 'R' || :loc_tipo || 'SE',
                        FECHA_FLUJO = SYSDATE,
                        IMPORTE     = ROUND(:sql_importe,2),
                        REF_MED     = :loc_ref_med
                 WHERE  CLAVE_INST  = :sql_clave_inst
                   AND  CLAVE_SIS   = :sql_clave_sis
                   AND  CLAVE_RAST  = :sql_clave_rast
                   AND  CLAVE_TRAN  = :sql_clave_tran
                   AND  FECHA_RECEP = TO_DATE(:sql_fecha_recep, 'YYYYMMDD')
                   AND  TIPO        = :sql_tipo
                   AND  FECHA_BAJA IS NULL; */ 
#line 479 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        struct sqlexd sqlstm;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqlvsn = 13;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.arrsiz = 9;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqladtp = &sqladt;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqltdsp = &sqltds;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.stmt = "update TRANS_PAGO  set STATUS=(('R'||:b0)||'SE'),FECH\
A_FLUJO=SYSDATE,IMPORTE=ROUND(:b1,2),REF_MED=:b2 where ((((((CLAVE_INST=:b3 an\
d CLAVE_SIS=:b4) and CLAVE_RAST=:b5) and CLAVE_TRAN=:b6) and FECHA_RECEP=TO_DA\
TE(:b7,'YYYYMMDD')) and TIPO=:b8) and FECHA_BAJA is null )";
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.iters = (unsigned int  )1;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.offset = (unsigned int  )247;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.cud = sqlcud0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqlety = (unsigned short)4352;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.occurs = (unsigned int  )0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[0] = (unsigned char  *)loc_tipo;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[0] = (unsigned long )2;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[0] = (         int  )0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[0] = (         short *)0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[0] = (         int  )0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[0] = (unsigned long )0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[0] = (unsigned short )0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[0] = (unsigned short )0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[1] = (unsigned char  *)&sql_importe;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[1] = (unsigned long )sizeof(double);
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[1] = (         int  )0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[1] = (         short *)0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[1] = (         int  )0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[1] = (unsigned long )0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[1] = (unsigned short )0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[1] = (unsigned short )0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[2] = (unsigned char  *)loc_ref_med;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[2] = (unsigned long )13;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[2] = (         int  )0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[2] = (         short *)0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[2] = (         int  )0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[2] = (unsigned long )0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[2] = (unsigned short )0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[2] = (unsigned short )0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[3] = (unsigned char  *)sql_clave_inst;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[3] = (unsigned long )6;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[3] = (         int  )0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[3] = (         short *)0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[3] = (         int  )0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[3] = (unsigned long )0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[3] = (unsigned short )0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[3] = (unsigned short )0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[4] = (unsigned char  *)sql_clave_sis;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[4] = (unsigned long )6;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[4] = (         int  )0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[4] = (         short *)0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[4] = (         int  )0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[4] = (unsigned long )0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[4] = (unsigned short )0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[4] = (unsigned short )0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[5] = (unsigned char  *)sql_clave_rast;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[5] = (unsigned long )31;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[5] = (         int  )0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[5] = (         short *)0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[5] = (         int  )0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[5] = (unsigned long )0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[5] = (unsigned short )0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[5] = (unsigned short )0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[6] = (unsigned char  *)sql_clave_tran;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[6] = (unsigned long )8;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[6] = (         int  )0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[6] = (         short *)0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[6] = (         int  )0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[6] = (unsigned long )0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[6] = (unsigned short )0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[6] = (unsigned short )0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[7] = (unsigned char  *)sql_fecha_recep;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[7] = (unsigned long )40;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[7] = (         int  )0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[7] = (         short *)0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[7] = (         int  )0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[7] = (unsigned long )0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[7] = (unsigned short )0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[7] = (unsigned short )0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[8] = (unsigned char  *)sql_tipo;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[8] = (unsigned long )2;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[8] = (         int  )0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[8] = (         short *)0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[8] = (         int  )0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[8] = (unsigned long )0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[8] = (unsigned short )0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[8] = (unsigned short )0;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqphsv = sqlstm.sqhstv;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqphsl = sqlstm.sqhstl;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqphss = sqlstm.sqhsts;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqpind = sqlstm.sqindv;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqpins = sqlstm.sqinds;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqparm = sqlstm.sqharm;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqparc = sqlstm.sqharc;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqpadto = sqlstm.sqadto;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqptdso = sqlstm.sqtdso;
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 468 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 479 "../../gral/src_db/CTransaccionalCdl.pc"


        resultado = ChecaErrorOracle( 0, 1, &rowcount);
        if (resultado != GFI_ERRC_OK)
        {
          if (NIVEL_TRACE > 5)
            cout << "ERROR AL ACTUALIZAR SALA DE ESPERA!" << endl;

          strcpy(err_desc,"S0002");
        }
      }
      else
      {
        if (NIVEL_TRACE > 9)
        {
          cout << "clave_inst >" << sql_clave_inst << "<" << endl;
          cout << "clave_rast >" << sql_clave_rast << "<" << endl;
          cout << "status     >" << loc_status << "<" << endl;
          cout << "tipo       >" << loc_tipo << "<" << endl;
          cout << "fecha_baja >" << sql_fecha_baja << "<" << endl;
        }
      }
    } // 3sala
    else
    {
      if (NIVEL_TRACE > 9)
        cout << "AGREGANDO SALA DE ESPERA" << endl;

      /* EXEC SQL INSERT INTO TRANS_PAGO ( CLAVE_INST , CLAVE_SIS , CLAVE_RAST , CLAVE_TRAN ,
                                        FECHA_RECEP , TIPO , FOLIO_ORIGEN , TIPO_OPER ,
                                        BANCO_ORD , CVE_BANCO_ORD , NOMBRE_ORD , CUENTA_ORD ,
                                        BANCO_BEN , CVE_BANCO_BEN , NOMBRE_BEN , CUENTA_BEN ,
                                        IMPORTE , STATUS , OBSERVACION , REF_CONCIL ,
                                        REF_MED , FECHA_CAP , FECHA_VENC , FECHA_FLUJO ,
                                        FECHA_REG , FECHA_BAJA )
                           VALUES( :sql_clave_inst , :sql_clave_sis , :sql_clave_rast , :sql_clave_tran ,
                                   TO_DATE(:sql_fecha_recep, 'YYYYMMDD') , :sql_tipo , :sql_folio_origen ,
                                   :sql_tipo_oper , :sql_banco_ord , :sql_cve_banco_ord , :sql_nombre_ord ,
                                   :sql_cuenta_ord , :sql_banco_ben , :sql_cve_banco_ben , :sql_nombre_ben ,
                                   :sql_cuenta_ben , ROUND(:sql_importe,2) , 'R' || :sql_tipo || 'SE' ,
                                   :sql_observacion , :ref_concilG , :sql_ref_med ,
                                   TO_DATE(:sql_fecha_cap, 'YYYYMMDD HH24:MI:SS') ,
                                   TO_DATE(:sql_fecha_venc, 'YYYYMMDD HH24:MI:SS') ,
                                   TO_DATE(:sql_fecha_flujo, 'YYYYMMDD HH24:MI:SS') ,
                                   TO_DATE(:sql_fecha_reg, 'YYYYMMDD HH24:MI:SS') ,
                                   TO_DATE(:sql_fecha_baja, 'YYYYMMDD HH24:MI:SS') ); */ 
#line 524 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      struct sqlexd sqlstm;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqlvsn = 13;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.arrsiz = 26;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqladtp = &sqladt;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqltdsp = &sqltds;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.stmt = "insert into TRANS_PAGO (CLAVE_INST,CLAVE_SIS,CLAVE_RAST\
,CLAVE_TRAN,FECHA_RECEP,TIPO,FOLIO_ORIGEN,TIPO_OPER,BANCO_ORD,CVE_BANCO_ORD,NO\
MBRE_ORD,CUENTA_ORD,BANCO_BEN,CVE_BANCO_BEN,NOMBRE_BEN,CUENTA_BEN,IMPORTE,STAT\
US,OBSERVACION,REF_CONCIL,REF_MED,FECHA_CAP,FECHA_VENC,FECHA_FLUJO,FECHA_REG,F\
ECHA_BAJA) values (:b0,:b1,:b2,:b3,TO_DATE(:b4,'YYYYMMDD'),:b5,:b6,:b7,:b8,:b9\
,:b10,:b11,:b12,:b13,:b14,:b15,ROUND(:b16,2),(('R'||:b5)||'SE'),:b18,:b19,:b20\
,TO_DATE(:b21,'YYYYMMDD HH24:MI:SS'),TO_DATE(:b22,'YYYYMMDD HH24:MI:SS'),TO_DA\
TE(:b23,'YYYYMMDD HH24:MI:SS'),TO_DATE(:b24,'YYYYMMDD HH24:MI:SS'),TO_DATE(:b2\
5,'YYYYMMDD HH24:MI:SS'))";
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.iters = (unsigned int  )1;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.offset = (unsigned int  )298;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.cud = sqlcud0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqlety = (unsigned short)4352;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.occurs = (unsigned int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[0] = (unsigned char  *)sql_clave_inst;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[0] = (unsigned long )6;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[0] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[0] = (         short *)0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[0] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[0] = (unsigned long )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[0] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[0] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[1] = (unsigned char  *)sql_clave_sis;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[1] = (unsigned long )6;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[1] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[1] = (         short *)0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[1] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[1] = (unsigned long )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[1] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[1] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[2] = (unsigned char  *)sql_clave_rast;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[2] = (unsigned long )31;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[2] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[2] = (         short *)0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[2] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[2] = (unsigned long )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[2] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[2] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[3] = (unsigned char  *)sql_clave_tran;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[3] = (unsigned long )8;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[3] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[3] = (         short *)0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[3] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[3] = (unsigned long )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[3] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[3] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[4] = (unsigned char  *)sql_fecha_recep;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[4] = (unsigned long )40;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[4] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[4] = (         short *)0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[4] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[4] = (unsigned long )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[4] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[4] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[5] = (unsigned char  *)sql_tipo;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[5] = (unsigned long )2;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[5] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[5] = (         short *)0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[5] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[5] = (unsigned long )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[5] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[5] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[6] = (unsigned char  *)sql_folio_origen;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[6] = (unsigned long )10;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[6] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[6] = (         short *)0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[6] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[6] = (unsigned long )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[6] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[6] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[7] = (unsigned char  *)sql_tipo_oper;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[7] = (unsigned long )5;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[7] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[7] = (         short *)0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[7] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[7] = (unsigned long )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[7] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[7] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[8] = (unsigned char  *)sql_banco_ord;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[8] = (unsigned long )7;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[8] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[8] = (         short *)0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[8] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[8] = (unsigned long )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[8] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[8] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[9] = (unsigned char  *)sql_cve_banco_ord;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[9] = (unsigned long )16;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[9] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[9] = (         short *)0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[9] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[9] = (unsigned long )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[9] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[9] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[10] = (unsigned char  *)sql_nombre_ord;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[10] = (unsigned long )52;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[10] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[10] = (         short *)0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[10] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[10] = (unsigned long )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[10] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[10] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[11] = (unsigned char  *)sql_cuenta_ord;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[11] = (unsigned long )21;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[11] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[11] = (         short *)0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[11] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[11] = (unsigned long )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[11] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[11] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[12] = (unsigned char  *)sql_banco_ben;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[12] = (unsigned long )7;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[12] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[12] = (         short *)0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[12] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[12] = (unsigned long )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[12] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[12] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[13] = (unsigned char  *)sql_cve_banco_ben;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[13] = (unsigned long )16;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[13] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[13] = (         short *)0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[13] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[13] = (unsigned long )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[13] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[13] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[14] = (unsigned char  *)sql_nombre_ben;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[14] = (unsigned long )52;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[14] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[14] = (         short *)0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[14] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[14] = (unsigned long )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[14] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[14] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[15] = (unsigned char  *)sql_cuenta_ben;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[15] = (unsigned long )21;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[15] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[15] = (         short *)0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[15] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[15] = (unsigned long )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[15] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[15] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[16] = (unsigned char  *)&sql_importe;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[16] = (unsigned long )sizeof(double);
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[16] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[16] = (         short *)0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[16] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[16] = (unsigned long )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[16] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[16] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[17] = (unsigned char  *)sql_tipo;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[17] = (unsigned long )2;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[17] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[17] = (         short *)0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[17] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[17] = (unsigned long )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[17] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[17] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[18] = (unsigned char  *)sql_observacion;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[18] = (unsigned long )50;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[18] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[18] = (         short *)0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[18] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[18] = (unsigned long )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[18] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[18] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[19] = (unsigned char  *)ref_concilG;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[19] = (unsigned long )20;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[19] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[19] = (         short *)0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[19] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[19] = (unsigned long )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[19] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[19] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[20] = (unsigned char  *)sql_ref_med;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[20] = (unsigned long )18;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[20] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[20] = (         short *)0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[20] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[20] = (unsigned long )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[20] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[20] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[21] = (unsigned char  *)sql_fecha_cap;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[21] = (unsigned long )40;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[21] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[21] = (         short *)0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[21] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[21] = (unsigned long )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[21] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[21] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[22] = (unsigned char  *)sql_fecha_venc;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[22] = (unsigned long )18;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[22] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[22] = (         short *)0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[22] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[22] = (unsigned long )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[22] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[22] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[23] = (unsigned char  *)sql_fecha_flujo;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[23] = (unsigned long )18;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[23] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[23] = (         short *)0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[23] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[23] = (unsigned long )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[23] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[23] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[24] = (unsigned char  *)sql_fecha_reg;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[24] = (unsigned long )18;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[24] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[24] = (         short *)0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[24] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[24] = (unsigned long )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[24] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[24] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[25] = (unsigned char  *)sql_fecha_baja;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[25] = (unsigned long )18;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[25] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[25] = (         short *)0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[25] = (         int  )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[25] = (unsigned long )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[25] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[25] = (unsigned short )0;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqphsv = sqlstm.sqhstv;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqphsl = sqlstm.sqhstl;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqphss = sqlstm.sqhsts;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqpind = sqlstm.sqindv;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqpins = sqlstm.sqinds;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqparm = sqlstm.sqharm;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqparc = sqlstm.sqharc;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqpadto = sqlstm.sqadto;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqptdso = sqlstm.sqtdso;
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 507 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 524 "../../gral/src_db/CTransaccionalCdl.pc"


      resultado = ChecaErrorOracle (0, 1, &rowcount);
      if (resultado != GFI_ERRC_OK)
      {
        if (NIVEL_TRACE > 5)
          cout << "ERROR AL INSERTAR SALA DE ESPERA" << endl;

        strcpy(err_desc,"S0003");
      }
    }
  } // 2sala

  if (NIVEL_TRACE > 5)
    cout << "Termina SALA DE ESPERA" << endl;

  return( resultado);
} // End

/////////////////////////////////////////////////////////
//  Funcion:  ProcesaReal                              //
/////////////////////////////////////////////////////////
int  ProcesaReal( char *clave_inst,  char *clave_sis,     char *clave_rast,   char *clave_tran,
                  char *fecha_recep, char *tipo,          char *folio_origen, char *tipo_oper,
                  char *banco_ord,   char *cve_banco_ord, char *nombre_ord,   char *cuenta_ord,
                  char *banco_ben,   char *cve_banco_ben, char *nombre_ben,   char *cuenta_ben,
                  double importe,    char *observacion,   char *ref_concil,   char *ref_med,
                  char *fecha_cap,   char *fecha_venc,    char *fecha_flujo,  char *fecha_reg,
                  char *fecha_baja,  char *status )
{ // Ini
  /* EXEC SQL BEGIN DECLARE SECTION; */ 
#line 554 "../../gral/src_db/CTransaccionalCdl.pc"

    char sql_clave_inst[6];
    char sql_clave_sis[6];
    char sql_clave_rast[31];
    char sql_clave_tran[8];
    char sql_fecha_recep[40];
    char sql_tipo[2];
    char sql_folio_origen[10];
    char sql_tipo_oper[5];
    char sql_banco_ord[7];
    char sql_cve_banco_ord[16];
    char sql_nombre_ord[52];
    char sql_cuenta_ord[21];
    char sql_banco_ben[7];
    char sql_cve_banco_ben[16];
    char sql_nombre_ben[52];
    char sql_cuenta_ben[21];
    char sql_observacion[50];
    char sql_ref_concil[16];
    char sql_ref_med[18];
    char sql_fecha_cap[40];
    char sql_fecha_venc[18];
    char sql_fecha_flujo[18];
    char sql_fecha_reg[18];
    char sql_fecha_baja[18];
    char sql_status[5];
    char loc_ref_med[13];
    char loc_clave_tran[8];
    char loc_banco_ben[7];
    char loc_idcd[10];
    char loc_status[5];
    char loc_tipo[2];
    char loc_status2[9];
    char loc_br[10];
    char loc_fecha_baja[18];
    double loc_limite;
    double loc_remanente;
    double loc_importe;
    double loc_simporte;
    double sql_importe;
    //int contpar;
    int contpag, contpro;
  /* EXEC SQL END DECLARE SECTION; */ 
#line 596 "../../gral/src_db/CTransaccionalCdl.pc"


/*   EXEC SQL INCLUDE SQLCA;
 */ 
#line 1 "/oracle/12.1.0/precomp/public/SQLCA.H"
/*
 * $Header: sqlca.h 24-apr-2003.12:50:58 mkandarp Exp $ sqlca.h 
 */

/* Copyright (c) 1985, 2003, Oracle Corporation.  All rights reserved.  */
 
/*
NAME
  SQLCA : SQL Communications Area.
FUNCTION
  Contains no code. Oracle fills in the SQLCA with status info
  during the execution of a SQL stmt.
NOTES
  **************************************************************
  ***                                                        ***
  *** This file is SOSD.  Porters must change the data types ***
  *** appropriately on their platform.  See notes/pcport.doc ***
  *** for more information.                                  ***
  ***                                                        ***
  **************************************************************

  If the symbol SQLCA_STORAGE_CLASS is defined, then the SQLCA
  will be defined to have this storage class. For example:
 
    #define SQLCA_STORAGE_CLASS extern
 
  will define the SQLCA as an extern.
 
  If the symbol SQLCA_INIT is defined, then the SQLCA will be
  statically initialized. Although this is not necessary in order
  to use the SQLCA, it is a good pgming practice not to have
  unitialized variables. However, some C compilers/OS's don't
  allow automatic variables to be init'd in this manner. Therefore,
  if you are INCLUDE'ing the SQLCA in a place where it would be
  an automatic AND your C compiler/OS doesn't allow this style
  of initialization, then SQLCA_INIT should be left undefined --
  all others can define SQLCA_INIT if they wish.

  If the symbol SQLCA_NONE is defined, then the SQLCA variable will
  not be defined at all.  The symbol SQLCA_NONE should not be defined
  in source modules that have embedded SQL.  However, source modules
  that have no embedded SQL, but need to manipulate a sqlca struct
  passed in as a parameter, can set the SQLCA_NONE symbol to avoid
  creation of an extraneous sqlca variable.
 
MODIFIED
    lvbcheng   07/31/98 -  long to int
    jbasu      12/12/94 -  Bug 217878: note this is an SOSD file
    losborne   08/11/92 -  No sqlca var if SQLCA_NONE macro set 
  Clare      12/06/84 - Ch SQLCA to not be an extern.
  Clare      10/21/85 - Add initialization.
  Bradbury   01/05/86 - Only initialize when SQLCA_INIT set
  Clare      06/12/86 - Add SQLCA_STORAGE_CLASS option.
*/
 
#ifndef SQLCA
#define SQLCA 1
 
struct   sqlca
         {
         /* ub1 */ char    sqlcaid[8];
         /* b4  */ int     sqlabc;
         /* b4  */ int     sqlcode;
         struct
           {
           /* ub2 */ unsigned short sqlerrml;
           /* ub1 */ char           sqlerrmc[70];
           } sqlerrm;
         /* ub1 */ char    sqlerrp[8];
         /* b4  */ int     sqlerrd[6];
         /* ub1 */ char    sqlwarn[8];
         /* ub1 */ char    sqlext[8];
         };

#ifndef SQLCA_NONE 
#ifdef   SQLCA_STORAGE_CLASS
SQLCA_STORAGE_CLASS struct sqlca sqlca
#else
         struct sqlca sqlca
#endif
 
#ifdef  SQLCA_INIT
         = {
         {'S', 'Q', 'L', 'C', 'A', ' ', ' ', ' '},
         sizeof(struct sqlca),
         0,
         { 0, {0}},
         {'N', 'O', 'T', ' ', 'S', 'E', 'T', ' '},
         {0, 0, 0, 0, 0, 0},
         {0, 0, 0, 0, 0, 0, 0, 0},
         {0, 0, 0, 0, 0, 0, 0, 0}
         }
#endif
         ;
#endif
 
#endif
 
/* end SQLCA */

#line 599 "../../gral/src_db/CTransaccionalCdl.pc"

/////////////////////////////////////////////////////////
// Declaracion de datos para uso dentro de la funcion  //
/////////////////////////////////////////////////////////
  int resultado = GFI_ERRC_OK;
  long rowcount;
  char loc_status1[9];
  int sw = 0;
  int loc_tpconc, swConcOk; // HGT 18-May-2010

/////////////////////////////////////////////////////////
//  Inicio de codigo                                   //
/////////////////////////////////////////////////////////
  strcpy( sql_clave_inst,  clave_inst);
  strcpy( sql_clave_sis,  clave_sis);
  strcpy( sql_clave_rast,  clave_rast);
  strcpy( sql_clave_tran,  clave_tran);
  strcpy( sql_fecha_recep,  fecha_recep);
  strcpy( sql_tipo,  tipo);
  strcpy( sql_folio_origen,  folio_origen);
  strcpy( sql_tipo_oper,  tipo_oper);
  strcpy( sql_banco_ord,  banco_ord);
  strcpy( sql_cve_banco_ord,  cve_banco_ord);
  strcpy( sql_nombre_ord,  nombre_ord);
  strcpy( sql_cuenta_ord,  cuenta_ord);
  strcpy( sql_banco_ben,  banco_ben);
  strcpy( sql_cve_banco_ben,  cve_banco_ben);
  strcpy( sql_nombre_ben,  nombre_ben);
  strcpy( sql_cuenta_ben,  cuenta_ben);
  strcpy( sql_observacion,  observacion);
  strcpy( sql_ref_concil,  ref_concil);
  strcpy( sql_ref_med,  ref_med);
  strcpy( sql_fecha_cap,  fecha_cap);
  strcpy( sql_fecha_venc,  fecha_venc);
  strcpy( sql_fecha_flujo,  fecha_flujo);
  strcpy( sql_fecha_reg,  fecha_reg);
  strcpy( sql_fecha_baja,  fecha_baja);
  strcpy( sql_status,  status);
  sql_importe = importe;

  if (NIVEL_TRACE > 5)
    printf("Inicio REAL con Clave de Rastreo: >%s<\n", sql_clave_rast);

  // Ciclo por tipo y prioridad de Conciliacion - HGT 18-May-2010
  if (NIVEL_TRACE > 9)
    cout << "CARGA LOS TIPOS Y PRIORIDADES DE CONCILIACION" << endl;

  strcpy(sql_insth,sql_clave_inst); strcpy(sql_sish,sql_clave_sis); strcpy(sql_nmph,"CONCILIA");
  swConcOk = 0;
  /* EXEC SQL OPEN OBTENPTCONC; */ 
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"
  struct sqlexd sqlstm;
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqlvsn = 13;
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.arrsiz = 26;
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqladtp = &sqladt;
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqltdsp = &sqltds;
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.stmt = sq0003;
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.iters = (unsigned int  )1;
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.offset = (unsigned int  )417;
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.selerr = (unsigned short)1;
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqlpfmem = (unsigned int  )0;
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.cud = sqlcud0;
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqlety = (unsigned short)4352;
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.occurs = (unsigned int  )0;
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqcmod = (unsigned int )0;
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstv[0] = (unsigned char  *)sql_insth;
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstl[0] = (unsigned long )20;
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhsts[0] = (         int  )0;
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqindv[0] = (         short *)0;
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqinds[0] = (         int  )0;
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqharm[0] = (unsigned long )0;
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqadto[0] = (unsigned short )0;
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqtdso[0] = (unsigned short )0;
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstv[1] = (unsigned char  *)sql_sish;
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstl[1] = (unsigned long )20;
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhsts[1] = (         int  )0;
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqindv[1] = (         short *)0;
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqinds[1] = (         int  )0;
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqharm[1] = (unsigned long )0;
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqadto[1] = (unsigned short )0;
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqtdso[1] = (unsigned short )0;
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstv[2] = (unsigned char  *)sql_nmph;
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstl[2] = (unsigned long )20;
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhsts[2] = (         int  )0;
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqindv[2] = (         short *)0;
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqinds[2] = (         int  )0;
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqharm[2] = (unsigned long )0;
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqadto[2] = (unsigned short )0;
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqtdso[2] = (unsigned short )0;
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqphsv = sqlstm.sqhstv;
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqphsl = sqlstm.sqhstl;
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqphss = sqlstm.sqhsts;
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqpind = sqlstm.sqindv;
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqpins = sqlstm.sqinds;
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqparm = sqlstm.sqharm;
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqparc = sqlstm.sqharc;
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqpadto = sqlstm.sqadto;
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqptdso = sqlstm.sqtdso;
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 648 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 648 "../../gral/src_db/CTransaccionalCdl.pc"

  while(1)
  { // 1
    /* EXEC SQL FETCH OBTENPTCONC INTO :sql_valor; */ 
#line 651 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 651 "../../gral/src_db/CTransaccionalCdl.pc"
    struct sqlexd sqlstm;
#line 651 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqlvsn = 13;
#line 651 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.arrsiz = 26;
#line 651 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqladtp = &sqladt;
#line 651 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqltdsp = &sqltds;
#line 651 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.iters = (unsigned int  )1;
#line 651 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.offset = (unsigned int  )444;
#line 651 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.selerr = (unsigned short)1;
#line 651 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqlpfmem = (unsigned int  )0;
#line 651 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.cud = sqlcud0;
#line 651 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 651 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqlety = (unsigned short)4352;
#line 651 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.occurs = (unsigned int  )0;
#line 651 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqfoff = (         int )0;
#line 651 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqfmod = (unsigned int )2;
#line 651 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[0] = (unsigned char  *)sql_valor;
#line 651 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[0] = (unsigned long )100;
#line 651 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[0] = (         int  )0;
#line 651 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[0] = (         short *)0;
#line 651 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[0] = (         int  )0;
#line 651 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[0] = (unsigned long )0;
#line 651 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[0] = (unsigned short )0;
#line 651 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[0] = (unsigned short )0;
#line 651 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqphsv = sqlstm.sqhstv;
#line 651 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqphsl = sqlstm.sqhstl;
#line 651 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqphss = sqlstm.sqhsts;
#line 651 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqpind = sqlstm.sqindv;
#line 651 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqpins = sqlstm.sqinds;
#line 651 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqparm = sqlstm.sqharm;
#line 651 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqparc = sqlstm.sqharc;
#line 651 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqpadto = sqlstm.sqadto;
#line 651 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqptdso = sqlstm.sqtdso;
#line 651 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 651 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 651 "../../gral/src_db/CTransaccionalCdl.pc"


    resultado = ChecaErrorOracle (0, 1, &rowcount);
    if (resultado != GFI_ERRC_OK)
      break;

    loc_tpconc = 0;
    Gfi_StrRTrim(sql_valor);
    loc_tpconc = atoi(sql_valor);
    err_desc[0] = '\0'; //NULL

    cout << "TIPO DE CONCILIACION >" << loc_tpconc << "<" << endl;

    if (loc_tpconc == 2)
    { // 2
      // Busca la contraparte
      if (NIVEL_TRACE > 9)
        cout << "VERIFICA SI SE DEBE GENERAR CONTRAPARTE EN TRANS_PROGRAMADO" << endl;

        /* HGT 26-May-2010 - Inicio cambio Propuesto por H.Martinez 19-May-2010
      contpar = 0;

      EXEC SQL SELECT COUNT(1)
               INTO   :contpar
               FROM   ALLPARAMETER
               WHERE  CLAVE_INST = :sql_clave_inst
                 AND  CLAVE_SIS  = :sql_clave_sis
                 AND  NOMBRE     = 'CONTRAPARTE'
                 AND  VALOR      = :sql_clave_tran;*/
      sprintf(llave_c,"|%s|",sql_clave_tran);

      if (strstr(contrapartes,llave_c) != 0) //if (contpar > 0) HGT 26-May-2010 Fin
      { // 2-1
        if (NIVEL_TRACE > 5)
          cout << "CONCILIACION POR DEVOLUCIONES O CLIENTES" << endl;

        loc_status[0] = '\0'; //NULL
        loc_tipo[0] = '\0'; //NULL

        // Index: CLAVE_INST, CLAVE_SIS, CLAVE_RAST, CLAVE_TRAN, FECHA_RECEP, TIPO

        /* EXEC SQL SELECT NVL(STATUS,' '), NVL(TIPO,' ')
                 INTO   :loc_status, :loc_tipo
                 FROM   TRANS_PROGRAMADO
                 WHERE  CLAVE_INST  = :sql_clave_inst
                   AND  CLAVE_SIS   = :sql_clave_sis
                   AND  CLAVE_RAST  = :sql_clave_rast
                   AND  CLAVE_TRAN  = :sql_clave_tran
                   AND  FECHA_RECEP = TO_DATE(:sql_fecha_recep, 'YYYYMMDD')
                   AND  TIPO        = :sql_tipo; */ 
#line 700 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        struct sqlexd sqlstm;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqlvsn = 13;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.arrsiz = 26;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqladtp = &sqladt;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqltdsp = &sqltds;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.stmt = "select NVL(STATUS,' ') ,NVL(TIPO,' ') into :b0,:b1  f\
rom TRANS_PROGRAMADO where (((((CLAVE_INST=:b2 and CLAVE_SIS=:b3) and CLAVE_RA\
ST=:b4) and CLAVE_TRAN=:b5) and FECHA_RECEP=TO_DATE(:b6,'YYYYMMDD')) and TIPO=\
:b7)";
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.iters = (unsigned int  )1;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.offset = (unsigned int  )463;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.selerr = (unsigned short)1;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqlpfmem = (unsigned int  )0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.cud = sqlcud0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqlety = (unsigned short)4352;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.occurs = (unsigned int  )0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[0] = (unsigned char  *)loc_status;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[0] = (unsigned long )5;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[0] = (         int  )0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[0] = (         short *)0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[0] = (         int  )0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[0] = (unsigned long )0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[0] = (unsigned short )0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[0] = (unsigned short )0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[1] = (unsigned char  *)loc_tipo;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[1] = (unsigned long )2;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[1] = (         int  )0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[1] = (         short *)0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[1] = (         int  )0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[1] = (unsigned long )0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[1] = (unsigned short )0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[1] = (unsigned short )0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[2] = (unsigned char  *)sql_clave_inst;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[2] = (unsigned long )6;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[2] = (         int  )0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[2] = (         short *)0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[2] = (         int  )0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[2] = (unsigned long )0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[2] = (unsigned short )0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[2] = (unsigned short )0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[3] = (unsigned char  *)sql_clave_sis;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[3] = (unsigned long )6;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[3] = (         int  )0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[3] = (         short *)0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[3] = (         int  )0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[3] = (unsigned long )0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[3] = (unsigned short )0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[3] = (unsigned short )0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[4] = (unsigned char  *)sql_clave_rast;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[4] = (unsigned long )31;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[4] = (         int  )0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[4] = (         short *)0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[4] = (         int  )0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[4] = (unsigned long )0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[4] = (unsigned short )0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[4] = (unsigned short )0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[5] = (unsigned char  *)sql_clave_tran;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[5] = (unsigned long )8;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[5] = (         int  )0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[5] = (         short *)0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[5] = (         int  )0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[5] = (unsigned long )0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[5] = (unsigned short )0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[5] = (unsigned short )0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[6] = (unsigned char  *)sql_fecha_recep;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[6] = (unsigned long )40;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[6] = (         int  )0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[6] = (         short *)0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[6] = (         int  )0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[6] = (unsigned long )0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[6] = (unsigned short )0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[6] = (unsigned short )0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[7] = (unsigned char  *)sql_tipo;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[7] = (unsigned long )2;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[7] = (         int  )0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[7] = (         short *)0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[7] = (         int  )0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[7] = (unsigned long )0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[7] = (unsigned short )0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[7] = (unsigned short )0;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqphsv = sqlstm.sqhstv;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqphsl = sqlstm.sqhstl;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqphss = sqlstm.sqhsts;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqpind = sqlstm.sqindv;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqpins = sqlstm.sqinds;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqparm = sqlstm.sqharm;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqparc = sqlstm.sqharc;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqpadto = sqlstm.sqadto;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqptdso = sqlstm.sqtdso;
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 692 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 700 "../../gral/src_db/CTransaccionalCdl.pc"

                   //AND  ROWNUM = 1; Propuesto por H.Martinez 19-May-2010

        resultado = Obtiene_Ref_Concil();

        if (resultado != GFI_ERRC_OK)
          return (resultado);
        else
        { // 2-2
          Gfi_StrRTrim(loc_status);
          if (strlen(loc_status) > 0)
          {
            if (NIVEL_TRACE > 9)
              cout << "ACTUALIZANDO EL PROGRAMADO A CONCILIADO" << endl;

            /* EXEC SQL UPDATE TRANS_PROGRAMADO
                     SET    REF_CONCIL  = :ref_concilG,
                            STATUS      = 'P' || :loc_tipo || 'C',
                            FECHA_FLUJO = SYSDATE
                     WHERE  CLAVE_INST  = :sql_clave_inst
                       AND  CLAVE_SIS   = :sql_clave_sis
                       AND  CLAVE_RAST  = :sql_clave_rast
                       AND  CLAVE_TRAN  = :sql_clave_tran
                       AND  FECHA_RECEP = TO_DATE(:sql_fecha_recep, 'YYYYMMDD')
                       AND  TIPO        = :sql_tipo; */ 
#line 724 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            struct sqlexd sqlstm;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlvsn = 13;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.arrsiz = 26;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqladtp = &sqladt;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqltdsp = &sqltds;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.stmt = "update TRANS_PROGRAMADO  set REF_CONCIL=:b0,STATU\
S=(('P'||:b1)||'C'),FECHA_FLUJO=SYSDATE where (((((CLAVE_INST=:b2 and CLAVE_SI\
S=:b3) and CLAVE_RAST=:b4) and CLAVE_TRAN=:b5) and FECHA_RECEP=TO_DATE(:b6,'YY\
YYMMDD')) and TIPO=:b7)";
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.iters = (unsigned int  )1;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.offset = (unsigned int  )510;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.cud = sqlcud0;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlety = (unsigned short)4352;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.occurs = (unsigned int  )0;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[0] = (unsigned char  *)ref_concilG;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[0] = (unsigned long )20;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[0] = (         int  )0;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[0] = (         short *)0;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[0] = (         int  )0;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[0] = (unsigned long )0;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[0] = (unsigned short )0;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[0] = (unsigned short )0;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[1] = (unsigned char  *)loc_tipo;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[1] = (unsigned long )2;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[1] = (         int  )0;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[1] = (         short *)0;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[1] = (         int  )0;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[1] = (unsigned long )0;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[1] = (unsigned short )0;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[1] = (unsigned short )0;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[2] = (unsigned char  *)sql_clave_inst;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[2] = (unsigned long )6;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[2] = (         int  )0;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[2] = (         short *)0;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[2] = (         int  )0;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[2] = (unsigned long )0;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[2] = (unsigned short )0;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[2] = (unsigned short )0;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[3] = (unsigned char  *)sql_clave_sis;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[3] = (unsigned long )6;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[3] = (         int  )0;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[3] = (         short *)0;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[3] = (         int  )0;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[3] = (unsigned long )0;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[3] = (unsigned short )0;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[3] = (unsigned short )0;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[4] = (unsigned char  *)sql_clave_rast;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[4] = (unsigned long )31;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[4] = (         int  )0;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[4] = (         short *)0;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[4] = (         int  )0;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[4] = (unsigned long )0;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[4] = (unsigned short )0;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[4] = (unsigned short )0;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[5] = (unsigned char  *)sql_clave_tran;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[5] = (unsigned long )8;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[5] = (         int  )0;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[5] = (         short *)0;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[5] = (         int  )0;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[5] = (unsigned long )0;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[5] = (unsigned short )0;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[5] = (unsigned short )0;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[6] = (unsigned char  *)sql_fecha_recep;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[6] = (unsigned long )40;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[6] = (         int  )0;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[6] = (         short *)0;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[6] = (         int  )0;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[6] = (unsigned long )0;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[6] = (unsigned short )0;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[6] = (unsigned short )0;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[7] = (unsigned char  *)sql_tipo;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[7] = (unsigned long )2;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[7] = (         int  )0;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[7] = (         short *)0;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[7] = (         int  )0;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[7] = (unsigned long )0;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[7] = (unsigned short )0;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[7] = (unsigned short )0;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqphsv = sqlstm.sqhstv;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqphsl = sqlstm.sqhstl;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqphss = sqlstm.sqhsts;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqpind = sqlstm.sqindv;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqpins = sqlstm.sqinds;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqparm = sqlstm.sqharm;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqparc = sqlstm.sqharc;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqpadto = sqlstm.sqadto;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqptdso = sqlstm.sqtdso;
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 715 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 724 "../../gral/src_db/CTransaccionalCdl.pc"


            resultado = ChecaErrorOracle( 0, 1, &rowcount);
            if (resultado != GFI_ERRC_OK)
            {
              if (NIVEL_TRACE > 5)
                cout << "ERROR AL ACTUALIZAR EN TRANS_PROGRAMADO!" << endl;

              strcpy(err_desc,"R0001");
            }
            else
              swConcOk = 1; // HGT 18-May-2010
          }
          else
          {
            if (NIVEL_TRACE > 9)
              cout << "NO EXISTE PROGRAMADO, INSERTANDOLO..." << endl;

            /* EXEC SQL INSERT INTO TRANS_PROGRAMADO ( CLAVE_INST , CLAVE_SIS , CLAVE_RAST , CLAVE_TRAN ,
                                                    FECHA_RECEP , TIPO , FOLIO_ORIGEN , TIPO_OPER ,
                                                    BANCO_ORD , CVE_BANCO_ORD , NOMBRE_ORD , CUENTA_ORD ,
                                                    BANCO_BEN , CVE_BANCO_BEN , NOMBRE_BEN , CUENTA_BEN ,
                                                    IMPORTE , STATUS , OBSERVACION , REF_CONCIL ,
                                                    REF_MED , FECHA_CAP , FECHA_VENC , FECHA_FLUJO ,
                                                    FECHA_REG , FECHA_BAJA )
                                 VALUES( :sql_clave_inst , :sql_clave_sis , :sql_clave_rast , :sql_clave_tran ,
                                         TO_DATE(:sql_fecha_recep, 'YYYYMMDD') , :sql_tipo , :sql_folio_origen ,
                                         :sql_tipo_oper , :sql_banco_ord , :sql_cve_banco_ord , :sql_nombre_ord ,
                                         :sql_cuenta_ord , :sql_banco_ben , :sql_cve_banco_ben , :sql_nombre_ben ,
                                         :sql_cuenta_ben , ROUND(:sql_importe,2) , 'P' || :sql_tipo || 'C' , :sql_observacion ,
                                         :ref_concilG , :sql_ref_med , TO_DATE(:sql_fecha_cap, 'YYYYMMDD HH24:MI:SS') ,
                                         TO_DATE(:sql_fecha_venc, 'YYYYMMDD HH24:MI:SS') ,
                                         TO_DATE(:sql_fecha_flujo, 'YYYYMMDD HH24:MI:SS') ,
                                         TO_DATE(:sql_fecha_reg, 'YYYYMMDD HH24:MI:SS') ,
                                         TO_DATE(:sql_fecha_baja, 'YYYYMMDD HH24:MI:SS') ); */ 
#line 758 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            struct sqlexd sqlstm;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlvsn = 13;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.arrsiz = 26;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqladtp = &sqladt;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqltdsp = &sqltds;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.stmt = "insert into TRANS_PROGRAMADO (CLAVE_INST,CLAVE_SI\
S,CLAVE_RAST,CLAVE_TRAN,FECHA_RECEP,TIPO,FOLIO_ORIGEN,TIPO_OPER,BANCO_ORD,CVE_\
BANCO_ORD,NOMBRE_ORD,CUENTA_ORD,BANCO_BEN,CVE_BANCO_BEN,NOMBRE_BEN,CUENTA_BEN,\
IMPORTE,STATUS,OBSERVACION,REF_CONCIL,REF_MED,FECHA_CAP,FECHA_VENC,FECHA_FLUJO\
,FECHA_REG,FECHA_BAJA) values (:b0,:b1,:b2,:b3,TO_DATE(:b4,'YYYYMMDD'),:b5,:b6\
,:b7,:b8,:b9,:b10,:b11,:b12,:b13,:b14,:b15,ROUND(:b16,2),(('P'||:b5)||'C'),:b1\
8,:b19,:b20,TO_DATE(:b21,'YYYYMMDD HH24:MI:SS'),TO_DATE(:b22,'YYYYMMDD HH24:MI\
:SS'),TO_DATE(:b23,'YYYYMMDD HH24:MI:SS'),TO_DATE(:b24,'YYYYMMDD HH24:MI:SS'),\
TO_DATE(:b25,'YYYYMMDD HH24:MI:SS'))";
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.iters = (unsigned int  )1;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.offset = (unsigned int  )557;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.cud = sqlcud0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlety = (unsigned short)4352;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.occurs = (unsigned int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[0] = (unsigned char  *)sql_clave_inst;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[0] = (unsigned long )6;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[0] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[0] = (         short *)0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[0] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[0] = (unsigned long )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[0] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[0] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[1] = (unsigned char  *)sql_clave_sis;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[1] = (unsigned long )6;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[1] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[1] = (         short *)0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[1] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[1] = (unsigned long )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[1] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[1] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[2] = (unsigned char  *)sql_clave_rast;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[2] = (unsigned long )31;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[2] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[2] = (         short *)0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[2] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[2] = (unsigned long )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[2] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[2] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[3] = (unsigned char  *)sql_clave_tran;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[3] = (unsigned long )8;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[3] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[3] = (         short *)0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[3] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[3] = (unsigned long )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[3] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[3] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[4] = (unsigned char  *)sql_fecha_recep;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[4] = (unsigned long )40;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[4] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[4] = (         short *)0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[4] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[4] = (unsigned long )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[4] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[4] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[5] = (unsigned char  *)sql_tipo;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[5] = (unsigned long )2;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[5] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[5] = (         short *)0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[5] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[5] = (unsigned long )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[5] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[5] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[6] = (unsigned char  *)sql_folio_origen;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[6] = (unsigned long )10;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[6] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[6] = (         short *)0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[6] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[6] = (unsigned long )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[6] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[6] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[7] = (unsigned char  *)sql_tipo_oper;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[7] = (unsigned long )5;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[7] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[7] = (         short *)0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[7] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[7] = (unsigned long )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[7] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[7] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[8] = (unsigned char  *)sql_banco_ord;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[8] = (unsigned long )7;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[8] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[8] = (         short *)0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[8] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[8] = (unsigned long )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[8] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[8] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[9] = (unsigned char  *)sql_cve_banco_ord;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[9] = (unsigned long )16;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[9] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[9] = (         short *)0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[9] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[9] = (unsigned long )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[9] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[9] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[10] = (unsigned char  *)sql_nombre_ord;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[10] = (unsigned long )52;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[10] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[10] = (         short *)0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[10] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[10] = (unsigned long )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[10] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[10] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[11] = (unsigned char  *)sql_cuenta_ord;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[11] = (unsigned long )21;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[11] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[11] = (         short *)0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[11] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[11] = (unsigned long )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[11] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[11] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[12] = (unsigned char  *)sql_banco_ben;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[12] = (unsigned long )7;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[12] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[12] = (         short *)0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[12] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[12] = (unsigned long )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[12] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[12] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[13] = (unsigned char  *)sql_cve_banco_ben;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[13] = (unsigned long )16;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[13] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[13] = (         short *)0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[13] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[13] = (unsigned long )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[13] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[13] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[14] = (unsigned char  *)sql_nombre_ben;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[14] = (unsigned long )52;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[14] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[14] = (         short *)0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[14] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[14] = (unsigned long )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[14] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[14] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[15] = (unsigned char  *)sql_cuenta_ben;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[15] = (unsigned long )21;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[15] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[15] = (         short *)0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[15] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[15] = (unsigned long )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[15] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[15] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[16] = (unsigned char  *)&sql_importe;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[16] = (unsigned long )sizeof(double);
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[16] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[16] = (         short *)0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[16] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[16] = (unsigned long )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[16] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[16] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[17] = (unsigned char  *)sql_tipo;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[17] = (unsigned long )2;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[17] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[17] = (         short *)0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[17] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[17] = (unsigned long )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[17] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[17] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[18] = (unsigned char  *)sql_observacion;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[18] = (unsigned long )50;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[18] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[18] = (         short *)0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[18] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[18] = (unsigned long )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[18] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[18] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[19] = (unsigned char  *)ref_concilG;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[19] = (unsigned long )20;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[19] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[19] = (         short *)0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[19] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[19] = (unsigned long )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[19] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[19] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[20] = (unsigned char  *)sql_ref_med;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[20] = (unsigned long )18;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[20] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[20] = (         short *)0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[20] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[20] = (unsigned long )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[20] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[20] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[21] = (unsigned char  *)sql_fecha_cap;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[21] = (unsigned long )40;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[21] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[21] = (         short *)0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[21] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[21] = (unsigned long )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[21] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[21] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[22] = (unsigned char  *)sql_fecha_venc;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[22] = (unsigned long )18;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[22] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[22] = (         short *)0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[22] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[22] = (unsigned long )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[22] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[22] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[23] = (unsigned char  *)sql_fecha_flujo;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[23] = (unsigned long )18;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[23] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[23] = (         short *)0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[23] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[23] = (unsigned long )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[23] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[23] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[24] = (unsigned char  *)sql_fecha_reg;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[24] = (unsigned long )18;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[24] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[24] = (         short *)0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[24] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[24] = (unsigned long )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[24] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[24] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[25] = (unsigned char  *)sql_fecha_baja;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[25] = (unsigned long )18;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[25] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[25] = (         short *)0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[25] = (         int  )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[25] = (unsigned long )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[25] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[25] = (unsigned short )0;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqphsv = sqlstm.sqhstv;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqphsl = sqlstm.sqhstl;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqphss = sqlstm.sqhsts;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqpind = sqlstm.sqindv;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqpins = sqlstm.sqinds;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqparm = sqlstm.sqharm;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqparc = sqlstm.sqharc;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqpadto = sqlstm.sqadto;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqptdso = sqlstm.sqtdso;
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 742 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 758 "../../gral/src_db/CTransaccionalCdl.pc"


            resultado = ChecaErrorOracle( 0, 1, &rowcount);
            if (resultado != GFI_ERRC_OK)
            {
              if (NIVEL_TRACE > 5)
                cout << "ERROR AL INSERTAR EN TRANS_PROGRAMADO!" << endl;

              strcpy(err_desc,"R0002");
            }
            else
              swConcOk = 1; // HGT 18-May-2010
          }

          if (strlen(err_desc) == 0)
          { // 2-3
            if (NIVEL_TRACE > 9)
              cout << "BUSCANDO EL REAL COMO SALA DE ESPERA" << endl;

            loc_status[0] = '\0'; //NULL
            loc_tipo[0] = '\0'; //NULL

            // Index: CLAVE_INST, CLAVE_SIS, CLAVE_RAST, CLAVE_TRAN, FECHA_RECEP, TIPO

            /* EXEC SQL SELECT NVL(STATUS,' '), NVL(TIPO,' ')
                     INTO   :loc_status, :loc_tipo
                     FROM   TRANS_PAGO
                     WHERE  CLAVE_INST  = :sql_clave_inst
                       AND  CLAVE_SIS   = :sql_clave_sis
                       AND  CLAVE_RAST  = :sql_clave_rast
                       AND  CLAVE_TRAN  = :sql_clave_tran
                       AND  FECHA_RECEP = TO_DATE(:sql_fecha_recep, 'YYYYMMDD')
                       AND  TIPO        = :sql_tipo
                       AND  FECHA_BAJA IS NULL; */ 
#line 791 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            struct sqlexd sqlstm;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlvsn = 13;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.arrsiz = 26;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqladtp = &sqladt;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqltdsp = &sqltds;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.stmt = "select NVL(STATUS,' ') ,NVL(TIPO,' ') into :b0,:b\
1  from TRANS_PAGO where ((((((CLAVE_INST=:b2 and CLAVE_SIS=:b3) and CLAVE_RAS\
T=:b4) and CLAVE_TRAN=:b5) and FECHA_RECEP=TO_DATE(:b6,'YYYYMMDD')) and TIPO=:\
b7) and FECHA_BAJA is null )";
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.iters = (unsigned int  )1;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.offset = (unsigned int  )676;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.selerr = (unsigned short)1;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlpfmem = (unsigned int  )0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.cud = sqlcud0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlety = (unsigned short)4352;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.occurs = (unsigned int  )0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[0] = (unsigned char  *)loc_status;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[0] = (unsigned long )5;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[0] = (         int  )0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[0] = (         short *)0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[0] = (         int  )0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[0] = (unsigned long )0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[0] = (unsigned short )0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[0] = (unsigned short )0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[1] = (unsigned char  *)loc_tipo;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[1] = (unsigned long )2;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[1] = (         int  )0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[1] = (         short *)0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[1] = (         int  )0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[1] = (unsigned long )0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[1] = (unsigned short )0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[1] = (unsigned short )0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[2] = (unsigned char  *)sql_clave_inst;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[2] = (unsigned long )6;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[2] = (         int  )0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[2] = (         short *)0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[2] = (         int  )0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[2] = (unsigned long )0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[2] = (unsigned short )0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[2] = (unsigned short )0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[3] = (unsigned char  *)sql_clave_sis;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[3] = (unsigned long )6;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[3] = (         int  )0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[3] = (         short *)0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[3] = (         int  )0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[3] = (unsigned long )0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[3] = (unsigned short )0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[3] = (unsigned short )0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[4] = (unsigned char  *)sql_clave_rast;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[4] = (unsigned long )31;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[4] = (         int  )0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[4] = (         short *)0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[4] = (         int  )0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[4] = (unsigned long )0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[4] = (unsigned short )0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[4] = (unsigned short )0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[5] = (unsigned char  *)sql_clave_tran;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[5] = (unsigned long )8;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[5] = (         int  )0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[5] = (         short *)0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[5] = (         int  )0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[5] = (unsigned long )0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[5] = (unsigned short )0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[5] = (unsigned short )0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[6] = (unsigned char  *)sql_fecha_recep;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[6] = (unsigned long )40;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[6] = (         int  )0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[6] = (         short *)0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[6] = (         int  )0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[6] = (unsigned long )0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[6] = (unsigned short )0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[6] = (unsigned short )0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[7] = (unsigned char  *)sql_tipo;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[7] = (unsigned long )2;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[7] = (         int  )0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[7] = (         short *)0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[7] = (         int  )0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[7] = (unsigned long )0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[7] = (unsigned short )0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[7] = (unsigned short )0;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqphsv = sqlstm.sqhstv;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqphsl = sqlstm.sqhstl;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqphss = sqlstm.sqhsts;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqpind = sqlstm.sqindv;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqpins = sqlstm.sqinds;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqparm = sqlstm.sqharm;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqparc = sqlstm.sqharc;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqpadto = sqlstm.sqadto;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqptdso = sqlstm.sqtdso;
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 782 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 791 "../../gral/src_db/CTransaccionalCdl.pc"

                       //AND  ROWNUM = 1;   Propuesto por H.Martinez 19-May-2010

            Gfi_StrRTrim(loc_status);
            if (strlen(loc_status) > 0)
            {
              if (NIVEL_TRACE > 9)
                cout << "ACTUALIZANDO EL PAGO A CONCILIADO" << endl;

              /* EXEC SQL UPDATE TRANS_PAGO
                       SET    REF_CONCIL  = :ref_concilG,
                              STATUS      = 'R' || :loc_tipo || 'PC',
                              IMPORTE     = ROUND(:sql_importe,2),
                              FECHA_FLUJO = SYSDATE
                       WHERE  CLAVE_INST  = :sql_clave_inst
                         AND  CLAVE_SIS   = :sql_clave_sis
                         AND  CLAVE_RAST  = :sql_clave_rast
                         AND  CLAVE_TRAN  = :sql_clave_tran
                         AND  FECHA_RECEP = TO_DATE(:sql_fecha_recep, 'YYYYMMDD')
                         AND  TIPO        = :sql_tipo
                         AND  FECHA_BAJA IS NULL; */ 
#line 811 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              struct sqlexd sqlstm;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqlvsn = 13;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.arrsiz = 26;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqladtp = &sqladt;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqltdsp = &sqltds;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.stmt = "update TRANS_PAGO  set REF_CONCIL=:b0,STATUS=((\
'R'||:b1)||'PC'),IMPORTE=ROUND(:b2,2),FECHA_FLUJO=SYSDATE where ((((((CLAVE_IN\
ST=:b3 and CLAVE_SIS=:b4) and CLAVE_RAST=:b5) and CLAVE_TRAN=:b6) and FECHA_RE\
CEP=TO_DATE(:b7,'YYYYMMDD')) and TIPO=:b8) and FECHA_BAJA is null )";
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.iters = (unsigned int  )1;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.offset = (unsigned int  )723;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.cud = sqlcud0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqlety = (unsigned short)4352;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.occurs = (unsigned int  )0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[0] = (unsigned char  *)ref_concilG;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[0] = (unsigned long )20;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[0] = (         int  )0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[0] = (         short *)0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[0] = (         int  )0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[0] = (unsigned long )0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[0] = (unsigned short )0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[0] = (unsigned short )0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[1] = (unsigned char  *)loc_tipo;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[1] = (unsigned long )2;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[1] = (         int  )0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[1] = (         short *)0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[1] = (         int  )0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[1] = (unsigned long )0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[1] = (unsigned short )0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[1] = (unsigned short )0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[2] = (unsigned char  *)&sql_importe;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[2] = (unsigned long )sizeof(double);
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[2] = (         int  )0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[2] = (         short *)0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[2] = (         int  )0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[2] = (unsigned long )0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[2] = (unsigned short )0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[2] = (unsigned short )0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[3] = (unsigned char  *)sql_clave_inst;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[3] = (unsigned long )6;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[3] = (         int  )0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[3] = (         short *)0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[3] = (         int  )0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[3] = (unsigned long )0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[3] = (unsigned short )0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[3] = (unsigned short )0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[4] = (unsigned char  *)sql_clave_sis;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[4] = (unsigned long )6;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[4] = (         int  )0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[4] = (         short *)0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[4] = (         int  )0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[4] = (unsigned long )0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[4] = (unsigned short )0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[4] = (unsigned short )0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[5] = (unsigned char  *)sql_clave_rast;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[5] = (unsigned long )31;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[5] = (         int  )0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[5] = (         short *)0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[5] = (         int  )0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[5] = (unsigned long )0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[5] = (unsigned short )0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[5] = (unsigned short )0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[6] = (unsigned char  *)sql_clave_tran;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[6] = (unsigned long )8;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[6] = (         int  )0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[6] = (         short *)0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[6] = (         int  )0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[6] = (unsigned long )0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[6] = (unsigned short )0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[6] = (unsigned short )0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[7] = (unsigned char  *)sql_fecha_recep;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[7] = (unsigned long )40;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[7] = (         int  )0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[7] = (         short *)0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[7] = (         int  )0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[7] = (unsigned long )0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[7] = (unsigned short )0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[7] = (unsigned short )0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[8] = (unsigned char  *)sql_tipo;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[8] = (unsigned long )2;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[8] = (         int  )0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[8] = (         short *)0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[8] = (         int  )0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[8] = (unsigned long )0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[8] = (unsigned short )0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[8] = (unsigned short )0;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqphsv = sqlstm.sqhstv;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqphsl = sqlstm.sqhstl;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqphss = sqlstm.sqhsts;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqpind = sqlstm.sqindv;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqpins = sqlstm.sqinds;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqparm = sqlstm.sqharm;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqparc = sqlstm.sqharc;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqpadto = sqlstm.sqadto;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqptdso = sqlstm.sqtdso;
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 800 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 811 "../../gral/src_db/CTransaccionalCdl.pc"


              resultado = ChecaErrorOracle( 0, 1, &rowcount);
              if (resultado != GFI_ERRC_OK)
              {
                if (NIVEL_TRACE > 5)
                  cout << "ERROR AL ACTUALIZAR EN TRANS_PAGO!" << endl;

                strcpy(err_desc,"R0003");
              }
              else
                swConcOk = 1; // HGT 18-May-2010
            }
            else
            {
              if (NIVEL_TRACE > 9)
                cout << "NO EXISTE EL REAL, INSERTANDOLO..." << endl;

              /* EXEC SQL INSERT INTO TRANS_PAGO ( CLAVE_INST , CLAVE_SIS , CLAVE_RAST , CLAVE_TRAN ,
                                                FECHA_RECEP , TIPO , FOLIO_ORIGEN , TIPO_OPER ,
                                                BANCO_ORD , CVE_BANCO_ORD , NOMBRE_ORD , CUENTA_ORD ,
                                                BANCO_BEN , CVE_BANCO_BEN , NOMBRE_BEN , CUENTA_BEN ,
                                                IMPORTE , STATUS , OBSERVACION , REF_CONCIL ,
                                                REF_MED , FECHA_CAP , FECHA_VENC , FECHA_FLUJO ,
                                                FECHA_REG , FECHA_BAJA )
                                   VALUES( :sql_clave_inst , :sql_clave_sis , :sql_clave_rast , :sql_clave_tran ,
                                           TO_DATE(:sql_fecha_recep, 'YYYYMMDD') , :sql_tipo , :sql_folio_origen ,
                                           :sql_tipo_oper , :sql_banco_ord , :sql_cve_banco_ord , :sql_nombre_ord ,
                                           :sql_cuenta_ord , :sql_banco_ben , :sql_cve_banco_ben , :sql_nombre_ben ,
                                           :sql_cuenta_ben , ROUND(:sql_importe,2) , 'R' || :sql_tipo || 'PC' ,
                                           :sql_observacion , :ref_concilG , :sql_ref_med ,
                                           TO_DATE(:sql_fecha_cap, 'YYYYMMDD HH24:MI:SS') ,
                                           TO_DATE(:sql_fecha_venc, 'YYYYMMDD HH24:MI:SS') ,
                                           SYSDATE , TO_DATE(:sql_fecha_reg, 'YYYYMMDD HH24:MI:SS') ,
                                           TO_DATE(:sql_fecha_baja, 'YYYYMMDD HH24:MI:SS') ); */ 
#line 845 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              struct sqlexd sqlstm;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqlvsn = 13;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.arrsiz = 26;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqladtp = &sqladt;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqltdsp = &sqltds;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.stmt = "insert into TRANS_PAGO (CLAVE_INST,CLAVE_SIS,CL\
AVE_RAST,CLAVE_TRAN,FECHA_RECEP,TIPO,FOLIO_ORIGEN,TIPO_OPER,BANCO_ORD,CVE_BANC\
O_ORD,NOMBRE_ORD,CUENTA_ORD,BANCO_BEN,CVE_BANCO_BEN,NOMBRE_BEN,CUENTA_BEN,IMPO\
RTE,STATUS,OBSERVACION,REF_CONCIL,REF_MED,FECHA_CAP,FECHA_VENC,FECHA_FLUJO,FEC\
HA_REG,FECHA_BAJA) values (:b0,:b1,:b2,:b3,TO_DATE(:b4,'YYYYMMDD'),:b5,:b6,:b7\
,:b8,:b9,:b10,:b11,:b12,:b13,:b14,:b15,ROUND(:b16,2),(('R'||:b5)||'PC'),:b18,:\
b19,:b20,TO_DATE(:b21,'YYYYMMDD HH24:MI:SS'),TO_DATE(:b22,'YYYYMMDD HH24:MI:SS\
'),SYSDATE,TO_DATE(:b23,'YYYYMMDD HH24:MI:SS'),TO_DATE(:b24,'YYYYMMDD HH24:MI:\
SS'))";
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.iters = (unsigned int  )1;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.offset = (unsigned int  )774;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.cud = sqlcud0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqlety = (unsigned short)4352;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.occurs = (unsigned int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[0] = (unsigned char  *)sql_clave_inst;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[0] = (unsigned long )6;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[0] = (         int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[0] = (         short *)0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[0] = (         int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[0] = (unsigned long )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[0] = (unsigned short )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[0] = (unsigned short )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[1] = (unsigned char  *)sql_clave_sis;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[1] = (unsigned long )6;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[1] = (         int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[1] = (         short *)0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[1] = (         int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[1] = (unsigned long )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[1] = (unsigned short )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[1] = (unsigned short )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[2] = (unsigned char  *)sql_clave_rast;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[2] = (unsigned long )31;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[2] = (         int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[2] = (         short *)0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[2] = (         int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[2] = (unsigned long )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[2] = (unsigned short )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[2] = (unsigned short )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[3] = (unsigned char  *)sql_clave_tran;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[3] = (unsigned long )8;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[3] = (         int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[3] = (         short *)0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[3] = (         int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[3] = (unsigned long )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[3] = (unsigned short )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[3] = (unsigned short )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[4] = (unsigned char  *)sql_fecha_recep;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[4] = (unsigned long )40;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[4] = (         int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[4] = (         short *)0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[4] = (         int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[4] = (unsigned long )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[4] = (unsigned short )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[4] = (unsigned short )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[5] = (unsigned char  *)sql_tipo;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[5] = (unsigned long )2;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[5] = (         int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[5] = (         short *)0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[5] = (         int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[5] = (unsigned long )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[5] = (unsigned short )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[5] = (unsigned short )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[6] = (unsigned char  *)sql_folio_origen;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[6] = (unsigned long )10;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[6] = (         int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[6] = (         short *)0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[6] = (         int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[6] = (unsigned long )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[6] = (unsigned short )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[6] = (unsigned short )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[7] = (unsigned char  *)sql_tipo_oper;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[7] = (unsigned long )5;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[7] = (         int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[7] = (         short *)0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[7] = (         int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[7] = (unsigned long )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[7] = (unsigned short )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[7] = (unsigned short )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[8] = (unsigned char  *)sql_banco_ord;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[8] = (unsigned long )7;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[8] = (         int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[8] = (         short *)0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[8] = (         int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[8] = (unsigned long )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[8] = (unsigned short )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[8] = (unsigned short )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[9] = (unsigned char  *)sql_cve_banco_ord;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[9] = (unsigned long )16;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[9] = (         int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[9] = (         short *)0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[9] = (         int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[9] = (unsigned long )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[9] = (unsigned short )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[9] = (unsigned short )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[10] = (unsigned char  *)sql_nombre_ord;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[10] = (unsigned long )52;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[10] = (         int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[10] = (         short *)0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[10] = (         int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[10] = (unsigned long )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[10] = (unsigned short )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[10] = (unsigned short )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[11] = (unsigned char  *)sql_cuenta_ord;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[11] = (unsigned long )21;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[11] = (         int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[11] = (         short *)0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[11] = (         int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[11] = (unsigned long )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[11] = (unsigned short )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[11] = (unsigned short )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[12] = (unsigned char  *)sql_banco_ben;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[12] = (unsigned long )7;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[12] = (         int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[12] = (         short *)0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[12] = (         int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[12] = (unsigned long )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[12] = (unsigned short )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[12] = (unsigned short )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[13] = (unsigned char  *)sql_cve_banco_ben;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[13] = (unsigned long )16;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[13] = (         int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[13] = (         short *)0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[13] = (         int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[13] = (unsigned long )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[13] = (unsigned short )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[13] = (unsigned short )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[14] = (unsigned char  *)sql_nombre_ben;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[14] = (unsigned long )52;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[14] = (         int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[14] = (         short *)0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[14] = (         int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[14] = (unsigned long )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[14] = (unsigned short )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[14] = (unsigned short )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[15] = (unsigned char  *)sql_cuenta_ben;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[15] = (unsigned long )21;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[15] = (         int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[15] = (         short *)0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[15] = (         int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[15] = (unsigned long )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[15] = (unsigned short )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[15] = (unsigned short )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[16] = (unsigned char  *)&sql_importe;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[16] = (unsigned long )sizeof(double);
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[16] = (         int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[16] = (         short *)0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[16] = (         int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[16] = (unsigned long )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[16] = (unsigned short )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[16] = (unsigned short )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[17] = (unsigned char  *)sql_tipo;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[17] = (unsigned long )2;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[17] = (         int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[17] = (         short *)0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[17] = (         int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[17] = (unsigned long )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[17] = (unsigned short )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[17] = (unsigned short )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[18] = (unsigned char  *)sql_observacion;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[18] = (unsigned long )50;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[18] = (         int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[18] = (         short *)0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[18] = (         int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[18] = (unsigned long )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[18] = (unsigned short )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[18] = (unsigned short )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[19] = (unsigned char  *)ref_concilG;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[19] = (unsigned long )20;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[19] = (         int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[19] = (         short *)0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[19] = (         int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[19] = (unsigned long )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[19] = (unsigned short )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[19] = (unsigned short )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[20] = (unsigned char  *)sql_ref_med;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[20] = (unsigned long )18;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[20] = (         int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[20] = (         short *)0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[20] = (         int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[20] = (unsigned long )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[20] = (unsigned short )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[20] = (unsigned short )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[21] = (unsigned char  *)sql_fecha_cap;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[21] = (unsigned long )40;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[21] = (         int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[21] = (         short *)0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[21] = (         int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[21] = (unsigned long )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[21] = (unsigned short )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[21] = (unsigned short )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[22] = (unsigned char  *)sql_fecha_venc;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[22] = (unsigned long )18;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[22] = (         int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[22] = (         short *)0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[22] = (         int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[22] = (unsigned long )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[22] = (unsigned short )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[22] = (unsigned short )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[23] = (unsigned char  *)sql_fecha_reg;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[23] = (unsigned long )18;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[23] = (         int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[23] = (         short *)0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[23] = (         int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[23] = (unsigned long )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[23] = (unsigned short )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[23] = (unsigned short )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[24] = (unsigned char  *)sql_fecha_baja;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[24] = (unsigned long )18;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[24] = (         int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[24] = (         short *)0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[24] = (         int  )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[24] = (unsigned long )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[24] = (unsigned short )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[24] = (unsigned short )0;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqphsv = sqlstm.sqhstv;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqphsl = sqlstm.sqhstl;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqphss = sqlstm.sqhsts;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqpind = sqlstm.sqindv;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqpins = sqlstm.sqinds;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqparm = sqlstm.sqharm;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqparc = sqlstm.sqharc;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqpadto = sqlstm.sqadto;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqptdso = sqlstm.sqtdso;
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 829 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 845 "../../gral/src_db/CTransaccionalCdl.pc"


              resultado = ChecaErrorOracle (0, 1, &rowcount);
              if (resultado != GFI_ERRC_OK)
              {
                if (NIVEL_TRACE > 5)
                  cout << "ERROR AL INSERTAR EN TRANS_PAGO!" << endl;

                strcpy(err_desc,"R0004");
              }
              else
                swConcOk = 1; // HGT 18-May-2010
            }
          } // 2-3
        } // 2-2

        if (NIVEL_TRACE > 9)
          cout << "TERMINO CONCILIACION POR DEVOLUCIONES O CLIENTES" << endl;

      } // 2-1
      //else
      //  cout << "llave_c >" << llave_c << "< not found!" << endl;

    } // 2

    if ((loc_tpconc == 4) && (swConcOk == 0)) // HGT 18-May-2010
    { // 4
      if (NIVEL_TRACE > 5)
        cout << "BUSCANDO COLOCACION PRIMARIA" << endl;

      strcpy(sql_insth,sql_clave_inst); strcpy(sql_sish,sql_clave_sis); strcpy(sql_nmph,"COLPRIM");
      /* EXEC SQL OPEN OBTENPARAM; */ 
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"
      struct sqlexd sqlstm;
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqlvsn = 13;
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.arrsiz = 26;
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqladtp = &sqladt;
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqltdsp = &sqltds;
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.stmt = sq0002;
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.iters = (unsigned int  )1;
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.offset = (unsigned int  )889;
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.selerr = (unsigned short)1;
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqlpfmem = (unsigned int  )0;
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.cud = sqlcud0;
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqlety = (unsigned short)4352;
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.occurs = (unsigned int  )0;
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqcmod = (unsigned int )0;
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[0] = (unsigned char  *)sql_insth;
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[0] = (unsigned long )20;
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[0] = (         int  )0;
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[0] = (         short *)0;
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[0] = (         int  )0;
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[0] = (unsigned long )0;
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[0] = (unsigned short )0;
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[0] = (unsigned short )0;
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[1] = (unsigned char  *)sql_sish;
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[1] = (unsigned long )20;
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[1] = (         int  )0;
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[1] = (         short *)0;
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[1] = (         int  )0;
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[1] = (unsigned long )0;
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[1] = (unsigned short )0;
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[1] = (unsigned short )0;
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[2] = (unsigned char  *)sql_nmph;
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[2] = (unsigned long )20;
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[2] = (         int  )0;
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[2] = (         short *)0;
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[2] = (         int  )0;
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[2] = (unsigned long )0;
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[2] = (unsigned short )0;
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[2] = (unsigned short )0;
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqphsv = sqlstm.sqhstv;
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqphsl = sqlstm.sqhstl;
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqphss = sqlstm.sqhsts;
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqpind = sqlstm.sqindv;
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqpins = sqlstm.sqinds;
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqparm = sqlstm.sqharm;
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqparc = sqlstm.sqharc;
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqpadto = sqlstm.sqadto;
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqptdso = sqlstm.sqtdso;
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 876 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 876 "../../gral/src_db/CTransaccionalCdl.pc"

      while(1)
      {
          /* EXEC SQL FETCH OBTENPARAM INTO :sql_valor; */ 
#line 879 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 879 "../../gral/src_db/CTransaccionalCdl.pc"
          struct sqlexd sqlstm;
#line 879 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqlvsn = 13;
#line 879 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.arrsiz = 26;
#line 879 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqladtp = &sqladt;
#line 879 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqltdsp = &sqltds;
#line 879 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.iters = (unsigned int  )1;
#line 879 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.offset = (unsigned int  )916;
#line 879 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.selerr = (unsigned short)1;
#line 879 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqlpfmem = (unsigned int  )0;
#line 879 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.cud = sqlcud0;
#line 879 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 879 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqlety = (unsigned short)4352;
#line 879 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.occurs = (unsigned int  )0;
#line 879 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqfoff = (         int )0;
#line 879 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqfmod = (unsigned int )2;
#line 879 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstv[0] = (unsigned char  *)sql_valor;
#line 879 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstl[0] = (unsigned long )100;
#line 879 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhsts[0] = (         int  )0;
#line 879 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqindv[0] = (         short *)0;
#line 879 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqinds[0] = (         int  )0;
#line 879 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqharm[0] = (unsigned long )0;
#line 879 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqadto[0] = (unsigned short )0;
#line 879 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqtdso[0] = (unsigned short )0;
#line 879 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqphsv = sqlstm.sqhstv;
#line 879 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqphsl = sqlstm.sqhstl;
#line 879 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqphss = sqlstm.sqhsts;
#line 879 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqpind = sqlstm.sqindv;
#line 879 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqpins = sqlstm.sqinds;
#line 879 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqparm = sqlstm.sqharm;
#line 879 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqparc = sqlstm.sqharc;
#line 879 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqpadto = sqlstm.sqadto;
#line 879 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqptdso = sqlstm.sqtdso;
#line 879 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 879 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 879 "../../gral/src_db/CTransaccionalCdl.pc"

          resultado = ChecaErrorOracle (0, 1, &rowcount);
          if (resultado != GFI_ERRC_OK)
            break;
          Gfi_StrRTrim(sql_valor);
          if (rowcount == 1)
            strcpy(loc_clave_tran,sql_valor);
          else if (rowcount == 2)
            strcpy(loc_banco_ben,sql_valor);
          else
            break;
      }
      /* EXEC SQL CLOSE OBTENPARAM; */ 
#line 891 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 891 "../../gral/src_db/CTransaccionalCdl.pc"
      struct sqlexd sqlstm;
#line 891 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqlvsn = 13;
#line 891 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.arrsiz = 26;
#line 891 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqladtp = &sqladt;
#line 891 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqltdsp = &sqltds;
#line 891 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.iters = (unsigned int  )1;
#line 891 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.offset = (unsigned int  )935;
#line 891 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.cud = sqlcud0;
#line 891 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 891 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqlety = (unsigned short)4352;
#line 891 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.occurs = (unsigned int  )0;
#line 891 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 891 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 891 "../../gral/src_db/CTransaccionalCdl.pc"


      if ((!strcmp(sql_clave_tran,loc_clave_tran)) && (!strcmp(sql_banco_ben,loc_banco_ben)))
      { // 4-1
        if (NIVEL_TRACE > 9)
          cout << "CONCILIACION POR COLOCACION PRIMARIA" << endl;

        loc_status[0] = '\0'; //NULL
        loc_tipo[0] = '\0'; //NULL

        // Index: CLAVE_INST, CLAVE_SIS, CLAVE_RAST, CLAVE_TRAN, FECHA_RECEP, TIPO

        /* EXEC SQL SELECT NVL(STATUS,' '), NVL(TIPO,' ')
                 INTO   :loc_status, :loc_tipo
                 FROM   TRANS_PROGRAMADO
                 WHERE  CLAVE_INST  = :sql_clave_inst
                   AND  CLAVE_SIS   = :sql_clave_sis
                   AND  CLAVE_RAST  = :sql_clave_rast
                   AND  CLAVE_TRAN  = :sql_clave_tran
                   AND  FECHA_RECEP = TO_DATE(:sql_fecha_recep, 'YYYYMMDD')
                   AND  TIPO        = :sql_tipo; */ 
#line 911 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        struct sqlexd sqlstm;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqlvsn = 13;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.arrsiz = 26;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqladtp = &sqladt;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqltdsp = &sqltds;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.stmt = "select NVL(STATUS,' ') ,NVL(TIPO,' ') into :b0,:b1  f\
rom TRANS_PROGRAMADO where (((((CLAVE_INST=:b2 and CLAVE_SIS=:b3) and CLAVE_RA\
ST=:b4) and CLAVE_TRAN=:b5) and FECHA_RECEP=TO_DATE(:b6,'YYYYMMDD')) and TIPO=\
:b7)";
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.iters = (unsigned int  )1;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.offset = (unsigned int  )950;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.selerr = (unsigned short)1;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqlpfmem = (unsigned int  )0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.cud = sqlcud0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqlety = (unsigned short)4352;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.occurs = (unsigned int  )0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[0] = (unsigned char  *)loc_status;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[0] = (unsigned long )5;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[0] = (         int  )0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[0] = (         short *)0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[0] = (         int  )0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[0] = (unsigned long )0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[0] = (unsigned short )0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[0] = (unsigned short )0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[1] = (unsigned char  *)loc_tipo;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[1] = (unsigned long )2;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[1] = (         int  )0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[1] = (         short *)0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[1] = (         int  )0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[1] = (unsigned long )0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[1] = (unsigned short )0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[1] = (unsigned short )0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[2] = (unsigned char  *)sql_clave_inst;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[2] = (unsigned long )6;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[2] = (         int  )0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[2] = (         short *)0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[2] = (         int  )0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[2] = (unsigned long )0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[2] = (unsigned short )0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[2] = (unsigned short )0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[3] = (unsigned char  *)sql_clave_sis;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[3] = (unsigned long )6;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[3] = (         int  )0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[3] = (         short *)0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[3] = (         int  )0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[3] = (unsigned long )0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[3] = (unsigned short )0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[3] = (unsigned short )0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[4] = (unsigned char  *)sql_clave_rast;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[4] = (unsigned long )31;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[4] = (         int  )0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[4] = (         short *)0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[4] = (         int  )0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[4] = (unsigned long )0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[4] = (unsigned short )0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[4] = (unsigned short )0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[5] = (unsigned char  *)sql_clave_tran;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[5] = (unsigned long )8;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[5] = (         int  )0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[5] = (         short *)0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[5] = (         int  )0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[5] = (unsigned long )0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[5] = (unsigned short )0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[5] = (unsigned short )0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[6] = (unsigned char  *)sql_fecha_recep;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[6] = (unsigned long )40;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[6] = (         int  )0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[6] = (         short *)0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[6] = (         int  )0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[6] = (unsigned long )0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[6] = (unsigned short )0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[6] = (unsigned short )0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[7] = (unsigned char  *)sql_tipo;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[7] = (unsigned long )2;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[7] = (         int  )0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[7] = (         short *)0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[7] = (         int  )0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[7] = (unsigned long )0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[7] = (unsigned short )0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[7] = (unsigned short )0;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqphsv = sqlstm.sqhstv;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqphsl = sqlstm.sqhstl;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqphss = sqlstm.sqhsts;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqpind = sqlstm.sqindv;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqpins = sqlstm.sqinds;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqparm = sqlstm.sqharm;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqparc = sqlstm.sqharc;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqpadto = sqlstm.sqadto;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqptdso = sqlstm.sqtdso;
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 903 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 911 "../../gral/src_db/CTransaccionalCdl.pc"

                   // AND  ROWNUM = 1;  Propuesto por H.Martinez 19-May-2010

        resultado = Obtiene_Ref_Concil();

        if (resultado == GFI_ERRC_OK)
        { // 4-2
          Gfi_StrRTrim(loc_status);
          if (strlen(loc_status) > 0)
          {
            if (NIVEL_TRACE > 9)
              cout << "ACTUALIZANDO EL PROGRAMADO A CONCILIADO" << endl;

            /* EXEC SQL UPDATE TRANS_PROGRAMADO
                     SET    REF_CONCIL  = :ref_concilG,
                            STATUS      = 'P' || :loc_tipo || 'C',
                            //IMPORTE     = ROUND(:sql_importe,2), Propuesto por H.Martinez 19-May-2010
                            FECHA_FLUJO = SYSDATE
                     WHERE  CLAVE_INST  = :sql_clave_inst
                       AND  CLAVE_SIS   = :sql_clave_sis
                       AND  CLAVE_RAST  = :sql_clave_rast
                       AND  CLAVE_TRAN  = :sql_clave_tran
                       AND  FECHA_RECEP = TO_DATE(:sql_fecha_recep, 'YYYYMMDD')
                       AND  TIPO        = :sql_tipo; */ 
#line 934 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            struct sqlexd sqlstm;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlvsn = 13;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.arrsiz = 26;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqladtp = &sqladt;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqltdsp = &sqltds;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.stmt = "update TRANS_PROGRAMADO  set REF_CONCIL=:b0,STATU\
S=(('P'||:b1)||'C'),FECHA_FLUJO=SYSDATE where (((((CLAVE_INST=:b2 and CLAVE_SI\
S=:b3) and CLAVE_RAST=:b4) and CLAVE_TRAN=:b5) and FECHA_RECEP=TO_DATE(:b6,'YY\
YYMMDD')) and TIPO=:b7)";
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.iters = (unsigned int  )1;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.offset = (unsigned int  )997;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.cud = sqlcud0;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlety = (unsigned short)4352;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.occurs = (unsigned int  )0;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[0] = (unsigned char  *)ref_concilG;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[0] = (unsigned long )20;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[0] = (         int  )0;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[0] = (         short *)0;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[0] = (         int  )0;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[0] = (unsigned long )0;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[0] = (unsigned short )0;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[0] = (unsigned short )0;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[1] = (unsigned char  *)loc_tipo;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[1] = (unsigned long )2;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[1] = (         int  )0;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[1] = (         short *)0;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[1] = (         int  )0;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[1] = (unsigned long )0;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[1] = (unsigned short )0;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[1] = (unsigned short )0;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[2] = (unsigned char  *)sql_clave_inst;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[2] = (unsigned long )6;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[2] = (         int  )0;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[2] = (         short *)0;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[2] = (         int  )0;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[2] = (unsigned long )0;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[2] = (unsigned short )0;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[2] = (unsigned short )0;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[3] = (unsigned char  *)sql_clave_sis;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[3] = (unsigned long )6;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[3] = (         int  )0;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[3] = (         short *)0;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[3] = (         int  )0;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[3] = (unsigned long )0;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[3] = (unsigned short )0;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[3] = (unsigned short )0;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[4] = (unsigned char  *)sql_clave_rast;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[4] = (unsigned long )31;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[4] = (         int  )0;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[4] = (         short *)0;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[4] = (         int  )0;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[4] = (unsigned long )0;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[4] = (unsigned short )0;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[4] = (unsigned short )0;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[5] = (unsigned char  *)sql_clave_tran;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[5] = (unsigned long )8;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[5] = (         int  )0;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[5] = (         short *)0;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[5] = (         int  )0;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[5] = (unsigned long )0;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[5] = (unsigned short )0;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[5] = (unsigned short )0;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[6] = (unsigned char  *)sql_fecha_recep;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[6] = (unsigned long )40;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[6] = (         int  )0;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[6] = (         short *)0;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[6] = (         int  )0;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[6] = (unsigned long )0;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[6] = (unsigned short )0;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[6] = (unsigned short )0;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[7] = (unsigned char  *)sql_tipo;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[7] = (unsigned long )2;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[7] = (         int  )0;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[7] = (         short *)0;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[7] = (         int  )0;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[7] = (unsigned long )0;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[7] = (unsigned short )0;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[7] = (unsigned short )0;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqphsv = sqlstm.sqhstv;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqphsl = sqlstm.sqhstl;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqphss = sqlstm.sqhsts;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqpind = sqlstm.sqindv;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqpins = sqlstm.sqinds;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqparm = sqlstm.sqharm;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqparc = sqlstm.sqharc;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqpadto = sqlstm.sqadto;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqptdso = sqlstm.sqtdso;
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 924 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 934 "../../gral/src_db/CTransaccionalCdl.pc"


            resultado = ChecaErrorOracle( 0, 1, &rowcount);
            if (resultado != GFI_ERRC_OK)
            {
              if (NIVEL_TRACE > 5)
                cout << "ERROR AL ACTUALIZAR EN TRANS_PROGRAMADO!" << endl;

              strcpy(err_desc,"R0005");
            }
            else
              swConcOk = 1; // HGT 18-May-2010
          }
          else
          {
            if (NIVEL_TRACE > 9)
              cout << "NO EXISTE PROGRAMADO, INSERTANDOLO..." << endl;

            /* EXEC SQL INSERT INTO TRANS_PROGRAMADO ( CLAVE_INST , CLAVE_SIS , CLAVE_RAST , CLAVE_TRAN ,
                                                    FECHA_RECEP , TIPO , FOLIO_ORIGEN , TIPO_OPER ,
                                                    BANCO_ORD , CVE_BANCO_ORD , NOMBRE_ORD , CUENTA_ORD ,
                                                    BANCO_BEN , CVE_BANCO_BEN , NOMBRE_BEN , CUENTA_BEN ,
                                                    IMPORTE , STATUS , OBSERVACION , REF_CONCIL ,
                                                    REF_MED , FECHA_CAP , FECHA_VENC , FECHA_FLUJO ,
                                                    FECHA_REG , FECHA_BAJA )
                                 VALUES( :sql_clave_inst , :sql_clave_sis , :sql_clave_rast , :sql_clave_tran ,
                                         TO_DATE(:sql_fecha_recep, 'YYYYMMDD') , :sql_tipo , :sql_folio_origen ,
                                         :sql_tipo_oper , :sql_banco_ord , :sql_cve_banco_ord , :sql_nombre_ord ,
                                         :sql_cuenta_ord , :sql_banco_ben , :sql_cve_banco_ben , :sql_nombre_ben ,
                                         :sql_cuenta_ben , ROUND(:sql_importe,2) , 'P' || :sql_tipo || 'C' , :sql_observacion ,
                                         :ref_concilG , :sql_ref_med , TO_DATE(:sql_fecha_cap, 'YYYYMMDD HH24:MI:SS') ,
                                         TO_DATE(:sql_fecha_venc, 'YYYYMMDD HH24:MI:SS') ,
                                         TO_DATE(:sql_fecha_flujo, 'YYYYMMDD HH24:MI:SS') ,
                                         TO_DATE(:sql_fecha_reg, 'YYYYMMDD HH24:MI:SS') ,
                                         TO_DATE(:sql_fecha_baja, 'YYYYMMDD HH24:MI:SS') ); */ 
#line 968 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            struct sqlexd sqlstm;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlvsn = 13;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.arrsiz = 26;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqladtp = &sqladt;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqltdsp = &sqltds;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.stmt = "insert into TRANS_PROGRAMADO (CLAVE_INST,CLAVE_SI\
S,CLAVE_RAST,CLAVE_TRAN,FECHA_RECEP,TIPO,FOLIO_ORIGEN,TIPO_OPER,BANCO_ORD,CVE_\
BANCO_ORD,NOMBRE_ORD,CUENTA_ORD,BANCO_BEN,CVE_BANCO_BEN,NOMBRE_BEN,CUENTA_BEN,\
IMPORTE,STATUS,OBSERVACION,REF_CONCIL,REF_MED,FECHA_CAP,FECHA_VENC,FECHA_FLUJO\
,FECHA_REG,FECHA_BAJA) values (:b0,:b1,:b2,:b3,TO_DATE(:b4,'YYYYMMDD'),:b5,:b6\
,:b7,:b8,:b9,:b10,:b11,:b12,:b13,:b14,:b15,ROUND(:b16,2),(('P'||:b5)||'C'),:b1\
8,:b19,:b20,TO_DATE(:b21,'YYYYMMDD HH24:MI:SS'),TO_DATE(:b22,'YYYYMMDD HH24:MI\
:SS'),TO_DATE(:b23,'YYYYMMDD HH24:MI:SS'),TO_DATE(:b24,'YYYYMMDD HH24:MI:SS'),\
TO_DATE(:b25,'YYYYMMDD HH24:MI:SS'))";
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.iters = (unsigned int  )1;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.offset = (unsigned int  )1044;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.cud = sqlcud0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlety = (unsigned short)4352;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.occurs = (unsigned int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[0] = (unsigned char  *)sql_clave_inst;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[0] = (unsigned long )6;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[0] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[0] = (         short *)0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[0] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[0] = (unsigned long )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[0] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[0] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[1] = (unsigned char  *)sql_clave_sis;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[1] = (unsigned long )6;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[1] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[1] = (         short *)0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[1] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[1] = (unsigned long )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[1] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[1] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[2] = (unsigned char  *)sql_clave_rast;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[2] = (unsigned long )31;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[2] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[2] = (         short *)0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[2] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[2] = (unsigned long )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[2] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[2] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[3] = (unsigned char  *)sql_clave_tran;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[3] = (unsigned long )8;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[3] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[3] = (         short *)0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[3] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[3] = (unsigned long )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[3] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[3] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[4] = (unsigned char  *)sql_fecha_recep;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[4] = (unsigned long )40;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[4] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[4] = (         short *)0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[4] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[4] = (unsigned long )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[4] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[4] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[5] = (unsigned char  *)sql_tipo;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[5] = (unsigned long )2;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[5] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[5] = (         short *)0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[5] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[5] = (unsigned long )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[5] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[5] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[6] = (unsigned char  *)sql_folio_origen;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[6] = (unsigned long )10;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[6] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[6] = (         short *)0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[6] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[6] = (unsigned long )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[6] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[6] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[7] = (unsigned char  *)sql_tipo_oper;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[7] = (unsigned long )5;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[7] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[7] = (         short *)0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[7] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[7] = (unsigned long )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[7] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[7] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[8] = (unsigned char  *)sql_banco_ord;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[8] = (unsigned long )7;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[8] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[8] = (         short *)0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[8] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[8] = (unsigned long )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[8] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[8] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[9] = (unsigned char  *)sql_cve_banco_ord;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[9] = (unsigned long )16;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[9] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[9] = (         short *)0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[9] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[9] = (unsigned long )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[9] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[9] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[10] = (unsigned char  *)sql_nombre_ord;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[10] = (unsigned long )52;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[10] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[10] = (         short *)0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[10] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[10] = (unsigned long )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[10] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[10] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[11] = (unsigned char  *)sql_cuenta_ord;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[11] = (unsigned long )21;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[11] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[11] = (         short *)0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[11] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[11] = (unsigned long )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[11] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[11] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[12] = (unsigned char  *)sql_banco_ben;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[12] = (unsigned long )7;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[12] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[12] = (         short *)0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[12] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[12] = (unsigned long )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[12] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[12] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[13] = (unsigned char  *)sql_cve_banco_ben;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[13] = (unsigned long )16;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[13] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[13] = (         short *)0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[13] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[13] = (unsigned long )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[13] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[13] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[14] = (unsigned char  *)sql_nombre_ben;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[14] = (unsigned long )52;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[14] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[14] = (         short *)0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[14] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[14] = (unsigned long )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[14] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[14] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[15] = (unsigned char  *)sql_cuenta_ben;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[15] = (unsigned long )21;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[15] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[15] = (         short *)0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[15] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[15] = (unsigned long )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[15] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[15] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[16] = (unsigned char  *)&sql_importe;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[16] = (unsigned long )sizeof(double);
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[16] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[16] = (         short *)0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[16] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[16] = (unsigned long )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[16] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[16] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[17] = (unsigned char  *)sql_tipo;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[17] = (unsigned long )2;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[17] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[17] = (         short *)0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[17] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[17] = (unsigned long )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[17] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[17] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[18] = (unsigned char  *)sql_observacion;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[18] = (unsigned long )50;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[18] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[18] = (         short *)0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[18] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[18] = (unsigned long )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[18] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[18] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[19] = (unsigned char  *)ref_concilG;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[19] = (unsigned long )20;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[19] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[19] = (         short *)0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[19] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[19] = (unsigned long )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[19] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[19] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[20] = (unsigned char  *)sql_ref_med;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[20] = (unsigned long )18;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[20] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[20] = (         short *)0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[20] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[20] = (unsigned long )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[20] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[20] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[21] = (unsigned char  *)sql_fecha_cap;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[21] = (unsigned long )40;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[21] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[21] = (         short *)0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[21] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[21] = (unsigned long )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[21] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[21] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[22] = (unsigned char  *)sql_fecha_venc;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[22] = (unsigned long )18;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[22] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[22] = (         short *)0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[22] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[22] = (unsigned long )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[22] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[22] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[23] = (unsigned char  *)sql_fecha_flujo;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[23] = (unsigned long )18;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[23] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[23] = (         short *)0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[23] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[23] = (unsigned long )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[23] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[23] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[24] = (unsigned char  *)sql_fecha_reg;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[24] = (unsigned long )18;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[24] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[24] = (         short *)0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[24] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[24] = (unsigned long )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[24] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[24] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[25] = (unsigned char  *)sql_fecha_baja;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[25] = (unsigned long )18;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[25] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[25] = (         short *)0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[25] = (         int  )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[25] = (unsigned long )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[25] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[25] = (unsigned short )0;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqphsv = sqlstm.sqhstv;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqphsl = sqlstm.sqhstl;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqphss = sqlstm.sqhsts;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqpind = sqlstm.sqindv;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqpins = sqlstm.sqinds;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqparm = sqlstm.sqharm;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqparc = sqlstm.sqharc;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqpadto = sqlstm.sqadto;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqptdso = sqlstm.sqtdso;
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 952 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 968 "../../gral/src_db/CTransaccionalCdl.pc"


            resultado = ChecaErrorOracle( 0, 1, &rowcount);
            if (resultado != GFI_ERRC_OK)
            {
              if (NIVEL_TRACE > 5)
                cout << "ERROR AL INSERTAR EN TRANS_PROGRAMADO!" << endl;

              strcpy(err_desc,"R0006");
            }
            else
              swConcOk = 1; // HGT 18-May-2010
          }

          if (strlen(err_desc) == 0)
          { // 4-3
            if (NIVEL_TRACE > 9)
              cout << "BUSCANDO EL REAL COMO SALA DE ESPERA" << endl;

            loc_status[0] = '\0'; //NULL
            loc_tipo[0] = '\0'; //NULL

            // Index: CLAVE_INST, CLAVE_SIS, CLAVE_RAST, CLAVE_TRAN, FECHA_RECEP, TIPO

            /* EXEC SQL SELECT NVL(STATUS,' '), NVL(TIPO,' ')
                     INTO   :loc_status, :loc_tipo
                     FROM   TRANS_PAGO
                     WHERE  CLAVE_INST  = :sql_clave_inst
                       AND  CLAVE_SIS   = :sql_clave_sis
                       AND  CLAVE_RAST  = :sql_clave_rast
                       AND  CLAVE_TRAN  = :sql_clave_tran
                       AND  FECHA_RECEP = TO_DATE(:sql_fecha_recep, 'YYYYMMDD')
                       AND  TIPO        = :sql_tipo
                       AND  FECHA_BAJA IS NULL; */ 
#line 1001 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            struct sqlexd sqlstm;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlvsn = 13;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.arrsiz = 26;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqladtp = &sqladt;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqltdsp = &sqltds;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.stmt = "select NVL(STATUS,' ') ,NVL(TIPO,' ') into :b0,:b\
1  from TRANS_PAGO where ((((((CLAVE_INST=:b2 and CLAVE_SIS=:b3) and CLAVE_RAS\
T=:b4) and CLAVE_TRAN=:b5) and FECHA_RECEP=TO_DATE(:b6,'YYYYMMDD')) and TIPO=:\
b7) and FECHA_BAJA is null )";
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.iters = (unsigned int  )1;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.offset = (unsigned int  )1163;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.selerr = (unsigned short)1;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlpfmem = (unsigned int  )0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.cud = sqlcud0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlety = (unsigned short)4352;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.occurs = (unsigned int  )0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[0] = (unsigned char  *)loc_status;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[0] = (unsigned long )5;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[0] = (         int  )0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[0] = (         short *)0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[0] = (         int  )0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[0] = (unsigned long )0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[0] = (unsigned short )0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[0] = (unsigned short )0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[1] = (unsigned char  *)loc_tipo;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[1] = (unsigned long )2;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[1] = (         int  )0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[1] = (         short *)0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[1] = (         int  )0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[1] = (unsigned long )0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[1] = (unsigned short )0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[1] = (unsigned short )0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[2] = (unsigned char  *)sql_clave_inst;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[2] = (unsigned long )6;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[2] = (         int  )0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[2] = (         short *)0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[2] = (         int  )0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[2] = (unsigned long )0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[2] = (unsigned short )0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[2] = (unsigned short )0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[3] = (unsigned char  *)sql_clave_sis;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[3] = (unsigned long )6;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[3] = (         int  )0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[3] = (         short *)0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[3] = (         int  )0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[3] = (unsigned long )0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[3] = (unsigned short )0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[3] = (unsigned short )0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[4] = (unsigned char  *)sql_clave_rast;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[4] = (unsigned long )31;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[4] = (         int  )0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[4] = (         short *)0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[4] = (         int  )0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[4] = (unsigned long )0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[4] = (unsigned short )0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[4] = (unsigned short )0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[5] = (unsigned char  *)sql_clave_tran;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[5] = (unsigned long )8;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[5] = (         int  )0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[5] = (         short *)0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[5] = (         int  )0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[5] = (unsigned long )0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[5] = (unsigned short )0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[5] = (unsigned short )0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[6] = (unsigned char  *)sql_fecha_recep;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[6] = (unsigned long )40;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[6] = (         int  )0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[6] = (         short *)0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[6] = (         int  )0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[6] = (unsigned long )0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[6] = (unsigned short )0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[6] = (unsigned short )0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[7] = (unsigned char  *)sql_tipo;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[7] = (unsigned long )2;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[7] = (         int  )0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[7] = (         short *)0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[7] = (         int  )0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[7] = (unsigned long )0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[7] = (unsigned short )0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[7] = (unsigned short )0;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqphsv = sqlstm.sqhstv;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqphsl = sqlstm.sqhstl;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqphss = sqlstm.sqhsts;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqpind = sqlstm.sqindv;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqpins = sqlstm.sqinds;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqparm = sqlstm.sqharm;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqparc = sqlstm.sqharc;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqpadto = sqlstm.sqadto;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqptdso = sqlstm.sqtdso;
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 992 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 1001 "../../gral/src_db/CTransaccionalCdl.pc"

                       //AND  ROWNUM = 1; Propuesto por H.Martinez 19-May-2010

            Gfi_StrRTrim(loc_status);
            if (strlen(loc_status) > 0)
            {
              if (NIVEL_TRACE > 9)
                cout << "ACTUALIZANDO SALA DE ESPERA" << endl;

              /* EXEC SQL UPDATE TRANS_PAGO
                       SET    REF_CONCIL  = :ref_concilG,
                              STATUS      = 'R' || :loc_tipo || 'PC',
                              IMPORTE     = ROUND(:sql_importe,2),
                              FECHA_FLUJO = SYSDATE
                       WHERE  CLAVE_INST  = :sql_clave_inst
                         AND  CLAVE_SIS   = :sql_clave_sis
                         AND  CLAVE_RAST  = :sql_clave_rast
                         AND  CLAVE_TRAN  = :sql_clave_tran
                         AND  FECHA_RECEP = TO_DATE(:sql_fecha_recep, 'YYYYMMDD')
                         AND  TIPO        = :sql_tipo
                         AND  FECHA_BAJA IS NULL; */ 
#line 1021 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              struct sqlexd sqlstm;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqlvsn = 13;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.arrsiz = 26;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqladtp = &sqladt;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqltdsp = &sqltds;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.stmt = "update TRANS_PAGO  set REF_CONCIL=:b0,STATUS=((\
'R'||:b1)||'PC'),IMPORTE=ROUND(:b2,2),FECHA_FLUJO=SYSDATE where ((((((CLAVE_IN\
ST=:b3 and CLAVE_SIS=:b4) and CLAVE_RAST=:b5) and CLAVE_TRAN=:b6) and FECHA_RE\
CEP=TO_DATE(:b7,'YYYYMMDD')) and TIPO=:b8) and FECHA_BAJA is null )";
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.iters = (unsigned int  )1;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.offset = (unsigned int  )1210;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.cud = sqlcud0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqlety = (unsigned short)4352;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.occurs = (unsigned int  )0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[0] = (unsigned char  *)ref_concilG;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[0] = (unsigned long )20;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[0] = (         int  )0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[0] = (         short *)0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[0] = (         int  )0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[0] = (unsigned long )0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[0] = (unsigned short )0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[0] = (unsigned short )0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[1] = (unsigned char  *)loc_tipo;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[1] = (unsigned long )2;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[1] = (         int  )0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[1] = (         short *)0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[1] = (         int  )0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[1] = (unsigned long )0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[1] = (unsigned short )0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[1] = (unsigned short )0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[2] = (unsigned char  *)&sql_importe;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[2] = (unsigned long )sizeof(double);
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[2] = (         int  )0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[2] = (         short *)0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[2] = (         int  )0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[2] = (unsigned long )0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[2] = (unsigned short )0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[2] = (unsigned short )0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[3] = (unsigned char  *)sql_clave_inst;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[3] = (unsigned long )6;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[3] = (         int  )0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[3] = (         short *)0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[3] = (         int  )0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[3] = (unsigned long )0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[3] = (unsigned short )0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[3] = (unsigned short )0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[4] = (unsigned char  *)sql_clave_sis;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[4] = (unsigned long )6;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[4] = (         int  )0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[4] = (         short *)0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[4] = (         int  )0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[4] = (unsigned long )0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[4] = (unsigned short )0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[4] = (unsigned short )0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[5] = (unsigned char  *)sql_clave_rast;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[5] = (unsigned long )31;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[5] = (         int  )0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[5] = (         short *)0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[5] = (         int  )0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[5] = (unsigned long )0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[5] = (unsigned short )0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[5] = (unsigned short )0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[6] = (unsigned char  *)sql_clave_tran;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[6] = (unsigned long )8;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[6] = (         int  )0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[6] = (         short *)0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[6] = (         int  )0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[6] = (unsigned long )0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[6] = (unsigned short )0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[6] = (unsigned short )0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[7] = (unsigned char  *)sql_fecha_recep;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[7] = (unsigned long )40;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[7] = (         int  )0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[7] = (         short *)0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[7] = (         int  )0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[7] = (unsigned long )0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[7] = (unsigned short )0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[7] = (unsigned short )0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[8] = (unsigned char  *)sql_tipo;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[8] = (unsigned long )2;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[8] = (         int  )0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[8] = (         short *)0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[8] = (         int  )0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[8] = (unsigned long )0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[8] = (unsigned short )0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[8] = (unsigned short )0;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqphsv = sqlstm.sqhstv;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqphsl = sqlstm.sqhstl;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqphss = sqlstm.sqhsts;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqpind = sqlstm.sqindv;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqpins = sqlstm.sqinds;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqparm = sqlstm.sqharm;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqparc = sqlstm.sqharc;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqpadto = sqlstm.sqadto;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqptdso = sqlstm.sqtdso;
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 1010 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 1021 "../../gral/src_db/CTransaccionalCdl.pc"


              resultado = ChecaErrorOracle( 0, 1, &rowcount);
              if (resultado != GFI_ERRC_OK)
              {
                if (NIVEL_TRACE > 5)
                  cout << "ERROR AL ACTUALIZAR EN TRANS_PAGO!" << endl;

                strcpy(err_desc,"R0007");
              }
              else
                swConcOk = 1; // HGT 18-May-2010
            }
            else
            {
              if (NIVEL_TRACE > 9)
                cout << "NO EXISTE EL REAL, INSERTANDOLO..." << endl;

              /* EXEC SQL INSERT INTO TRANS_PAGO ( CLAVE_INST , CLAVE_SIS , CLAVE_RAST , CLAVE_TRAN ,
                                                FECHA_RECEP , TIPO , FOLIO_ORIGEN , TIPO_OPER ,
                                                BANCO_ORD , CVE_BANCO_ORD , NOMBRE_ORD , CUENTA_ORD ,
                                                BANCO_BEN , CVE_BANCO_BEN , NOMBRE_BEN , CUENTA_BEN ,
                                                IMPORTE , STATUS , OBSERVACION , REF_CONCIL ,
                                                REF_MED , FECHA_CAP , FECHA_VENC , FECHA_FLUJO ,
                                                FECHA_REG , FECHA_BAJA )
                                   VALUES( :sql_clave_inst , :sql_clave_sis , :sql_clave_rast , :sql_clave_tran ,
                                           TO_DATE(:sql_fecha_recep, 'YYYYMMDD') , :sql_tipo , :sql_folio_origen ,
                                           :sql_tipo_oper , :sql_banco_ord , :sql_cve_banco_ord , :sql_nombre_ord ,
                                           :sql_cuenta_ord , :sql_banco_ben , :sql_cve_banco_ben , :sql_nombre_ben ,
                                           :sql_cuenta_ben , ROUND(:sql_importe,2) , 'R' || :sql_tipo || 'PC' ,
                                           :sql_observacion , :ref_concilG , :sql_ref_med ,
                                           TO_DATE(:sql_fecha_cap, 'YYYYMMDD HH24:MI:SS') ,
                                           TO_DATE(:sql_fecha_venc, 'YYYYMMDD HH24:MI:SS') ,
                                           SYSDATE , TO_DATE(:sql_fecha_reg, 'YYYYMMDD HH24:MI:SS') ,
                                           TO_DATE(:sql_fecha_baja, 'YYYYMMDD HH24:MI:SS') ); */ 
#line 1055 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              struct sqlexd sqlstm;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqlvsn = 13;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.arrsiz = 26;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqladtp = &sqladt;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqltdsp = &sqltds;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.stmt = "insert into TRANS_PAGO (CLAVE_INST,CLAVE_SIS,CL\
AVE_RAST,CLAVE_TRAN,FECHA_RECEP,TIPO,FOLIO_ORIGEN,TIPO_OPER,BANCO_ORD,CVE_BANC\
O_ORD,NOMBRE_ORD,CUENTA_ORD,BANCO_BEN,CVE_BANCO_BEN,NOMBRE_BEN,CUENTA_BEN,IMPO\
RTE,STATUS,OBSERVACION,REF_CONCIL,REF_MED,FECHA_CAP,FECHA_VENC,FECHA_FLUJO,FEC\
HA_REG,FECHA_BAJA) values (:b0,:b1,:b2,:b3,TO_DATE(:b4,'YYYYMMDD'),:b5,:b6,:b7\
,:b8,:b9,:b10,:b11,:b12,:b13,:b14,:b15,ROUND(:b16,2),(('R'||:b5)||'PC'),:b18,:\
b19,:b20,TO_DATE(:b21,'YYYYMMDD HH24:MI:SS'),TO_DATE(:b22,'YYYYMMDD HH24:MI:SS\
'),SYSDATE,TO_DATE(:b23,'YYYYMMDD HH24:MI:SS'),TO_DATE(:b24,'YYYYMMDD HH24:MI:\
SS'))";
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.iters = (unsigned int  )1;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.offset = (unsigned int  )1261;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.cud = sqlcud0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqlety = (unsigned short)4352;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.occurs = (unsigned int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[0] = (unsigned char  *)sql_clave_inst;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[0] = (unsigned long )6;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[0] = (         int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[0] = (         short *)0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[0] = (         int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[0] = (unsigned long )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[0] = (unsigned short )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[0] = (unsigned short )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[1] = (unsigned char  *)sql_clave_sis;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[1] = (unsigned long )6;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[1] = (         int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[1] = (         short *)0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[1] = (         int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[1] = (unsigned long )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[1] = (unsigned short )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[1] = (unsigned short )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[2] = (unsigned char  *)sql_clave_rast;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[2] = (unsigned long )31;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[2] = (         int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[2] = (         short *)0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[2] = (         int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[2] = (unsigned long )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[2] = (unsigned short )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[2] = (unsigned short )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[3] = (unsigned char  *)sql_clave_tran;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[3] = (unsigned long )8;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[3] = (         int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[3] = (         short *)0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[3] = (         int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[3] = (unsigned long )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[3] = (unsigned short )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[3] = (unsigned short )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[4] = (unsigned char  *)sql_fecha_recep;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[4] = (unsigned long )40;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[4] = (         int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[4] = (         short *)0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[4] = (         int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[4] = (unsigned long )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[4] = (unsigned short )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[4] = (unsigned short )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[5] = (unsigned char  *)sql_tipo;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[5] = (unsigned long )2;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[5] = (         int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[5] = (         short *)0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[5] = (         int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[5] = (unsigned long )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[5] = (unsigned short )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[5] = (unsigned short )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[6] = (unsigned char  *)sql_folio_origen;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[6] = (unsigned long )10;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[6] = (         int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[6] = (         short *)0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[6] = (         int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[6] = (unsigned long )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[6] = (unsigned short )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[6] = (unsigned short )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[7] = (unsigned char  *)sql_tipo_oper;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[7] = (unsigned long )5;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[7] = (         int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[7] = (         short *)0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[7] = (         int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[7] = (unsigned long )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[7] = (unsigned short )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[7] = (unsigned short )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[8] = (unsigned char  *)sql_banco_ord;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[8] = (unsigned long )7;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[8] = (         int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[8] = (         short *)0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[8] = (         int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[8] = (unsigned long )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[8] = (unsigned short )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[8] = (unsigned short )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[9] = (unsigned char  *)sql_cve_banco_ord;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[9] = (unsigned long )16;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[9] = (         int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[9] = (         short *)0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[9] = (         int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[9] = (unsigned long )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[9] = (unsigned short )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[9] = (unsigned short )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[10] = (unsigned char  *)sql_nombre_ord;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[10] = (unsigned long )52;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[10] = (         int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[10] = (         short *)0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[10] = (         int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[10] = (unsigned long )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[10] = (unsigned short )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[10] = (unsigned short )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[11] = (unsigned char  *)sql_cuenta_ord;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[11] = (unsigned long )21;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[11] = (         int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[11] = (         short *)0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[11] = (         int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[11] = (unsigned long )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[11] = (unsigned short )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[11] = (unsigned short )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[12] = (unsigned char  *)sql_banco_ben;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[12] = (unsigned long )7;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[12] = (         int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[12] = (         short *)0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[12] = (         int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[12] = (unsigned long )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[12] = (unsigned short )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[12] = (unsigned short )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[13] = (unsigned char  *)sql_cve_banco_ben;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[13] = (unsigned long )16;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[13] = (         int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[13] = (         short *)0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[13] = (         int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[13] = (unsigned long )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[13] = (unsigned short )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[13] = (unsigned short )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[14] = (unsigned char  *)sql_nombre_ben;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[14] = (unsigned long )52;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[14] = (         int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[14] = (         short *)0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[14] = (         int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[14] = (unsigned long )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[14] = (unsigned short )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[14] = (unsigned short )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[15] = (unsigned char  *)sql_cuenta_ben;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[15] = (unsigned long )21;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[15] = (         int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[15] = (         short *)0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[15] = (         int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[15] = (unsigned long )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[15] = (unsigned short )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[15] = (unsigned short )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[16] = (unsigned char  *)&sql_importe;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[16] = (unsigned long )sizeof(double);
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[16] = (         int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[16] = (         short *)0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[16] = (         int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[16] = (unsigned long )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[16] = (unsigned short )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[16] = (unsigned short )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[17] = (unsigned char  *)sql_tipo;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[17] = (unsigned long )2;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[17] = (         int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[17] = (         short *)0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[17] = (         int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[17] = (unsigned long )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[17] = (unsigned short )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[17] = (unsigned short )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[18] = (unsigned char  *)sql_observacion;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[18] = (unsigned long )50;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[18] = (         int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[18] = (         short *)0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[18] = (         int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[18] = (unsigned long )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[18] = (unsigned short )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[18] = (unsigned short )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[19] = (unsigned char  *)ref_concilG;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[19] = (unsigned long )20;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[19] = (         int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[19] = (         short *)0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[19] = (         int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[19] = (unsigned long )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[19] = (unsigned short )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[19] = (unsigned short )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[20] = (unsigned char  *)sql_ref_med;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[20] = (unsigned long )18;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[20] = (         int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[20] = (         short *)0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[20] = (         int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[20] = (unsigned long )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[20] = (unsigned short )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[20] = (unsigned short )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[21] = (unsigned char  *)sql_fecha_cap;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[21] = (unsigned long )40;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[21] = (         int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[21] = (         short *)0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[21] = (         int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[21] = (unsigned long )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[21] = (unsigned short )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[21] = (unsigned short )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[22] = (unsigned char  *)sql_fecha_venc;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[22] = (unsigned long )18;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[22] = (         int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[22] = (         short *)0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[22] = (         int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[22] = (unsigned long )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[22] = (unsigned short )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[22] = (unsigned short )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[23] = (unsigned char  *)sql_fecha_reg;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[23] = (unsigned long )18;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[23] = (         int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[23] = (         short *)0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[23] = (         int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[23] = (unsigned long )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[23] = (unsigned short )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[23] = (unsigned short )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[24] = (unsigned char  *)sql_fecha_baja;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[24] = (unsigned long )18;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[24] = (         int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[24] = (         short *)0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[24] = (         int  )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[24] = (unsigned long )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[24] = (unsigned short )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[24] = (unsigned short )0;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqphsv = sqlstm.sqhstv;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqphsl = sqlstm.sqhstl;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqphss = sqlstm.sqhsts;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqpind = sqlstm.sqindv;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqpins = sqlstm.sqinds;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqparm = sqlstm.sqharm;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqparc = sqlstm.sqharc;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqpadto = sqlstm.sqadto;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqptdso = sqlstm.sqtdso;
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 1039 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 1055 "../../gral/src_db/CTransaccionalCdl.pc"


              resultado = ChecaErrorOracle (0, 1, &rowcount);
              if (resultado != GFI_ERRC_OK)
              {
                if (NIVEL_TRACE > 5)
                  cout << "ERROR AL INSERTAR EN TRANS_PAGO!" << endl;

                strcpy(err_desc,"R0008");
              }
              else
                swConcOk = 1; // HGT 18-May-2010
            }
          } // 4-3
        } // 4-2

        if (NIVEL_TRACE > 9)
          cout << "TERMINO CONCILIACION POR COLOCACION PRIMARIA" << endl;

      } // 4-1

      if (NIVEL_TRACE > 9)
        cout << "BUSCANDO SI SE DEBE ACTUALIZAR LA TABLA IDCD EN OPICS" << endl;

      strcpy(sql_insth,sql_clave_inst); strcpy(sql_sish,"OPICS"); strcpy(sql_nmph,"IDCD");
      strcpy(loc_idcd,"0");
      /* Comentariado a petición H.Martinez para anular actualizacion de tabla referida 16-Feb-2010, CANCELADA EN CODIGO ORIGINAL!!
      EXEC SQL OPEN OBTENPARAM;
      while(1)
      {
          EXEC SQL FETCH OBTENPARAM INTO :sql_valor;
          resultado = ChecaErrorOracle (0, 1, &rowcount);
          if (resultado != GFI_ERRC_OK)
            break;
          Gfi_StrRTrim(sql_valor);
          if (rowcount == 1)
            strcpy(loc_idcd,sql_valor);
          else
            break;
      }
      EXEC SQL CLOSE OBTENPARAM; */

    } // 4

    if ((loc_tpconc == 8) && (swConcOk == 0)) // HGT 18-May-2010
    { // 8
      Gfi_StrRTrim(sql_ref_med);
      if ((strlen(sql_ref_med)>0) && (strcmp(sql_ref_med,"-")))
      { // 8-1
        if (NIVEL_TRACE > 5)
          cout << "CONCILIACION POR REFERENCIA MEDIA" << endl;

        loc_status[0] = '\0'; //NULL
        loc_tipo[0] = '\0'; //NULL
        loc_importe = 0;
        contpro = 0;

        // Index: CLAVE_INST, CLAVE_SIS, CLAVE_RAST, CLAVE_TRAN, FECHA_RECEP, TIPO

        // Correcciones por Conciliacion de Referencia Media Propuesto H.Martinez (14-Jun-2010)
        /* EXEC SQL SELECT COUNT(1)
                   INTO :contpro
                   FROM TRANS_PROGRAMADO
                  WHERE clave_inst  = :sql_clave_inst
                    AND clave_sis   = :sql_clave_sis
                    AND clave_rast  = :sql_ref_med
                    AND clave_tran  = :sql_clave_tran
                    AND fecha_recep = TO_DATE(:sql_fecha_recep, 'YYYYMMDD') //TRUNC(sysdate)
                    AND tipo        = :sql_tipo
                    AND status <> 'P' || :loc_tipo || 'C'
                    AND fecha_baja IS NULL; */ 
#line 1125 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        struct sqlexd sqlstm;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqlvsn = 13;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.arrsiz = 26;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqladtp = &sqladt;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqltdsp = &sqltds;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.stmt = "select count(1) into :b0  from TRANS_PROGRAMADO where\
 (((((((clave_inst=:b1 and clave_sis=:b2) and clave_rast=:b3) and clave_tran=:\
b4) and fecha_recep=TO_DATE(:b5,'YYYYMMDD')) and tipo=:b6) and status<>(('P'||\
:b7)||'C')) and fecha_baja is null )";
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.iters = (unsigned int  )1;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.offset = (unsigned int  )1376;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.selerr = (unsigned short)1;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqlpfmem = (unsigned int  )0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.cud = sqlcud0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqlety = (unsigned short)4352;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.occurs = (unsigned int  )0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[0] = (unsigned char  *)&contpro;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[0] = (unsigned long )sizeof(int);
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[0] = (         int  )0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[0] = (         short *)0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[0] = (         int  )0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[0] = (unsigned long )0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[0] = (unsigned short )0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[0] = (unsigned short )0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[1] = (unsigned char  *)sql_clave_inst;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[1] = (unsigned long )6;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[1] = (         int  )0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[1] = (         short *)0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[1] = (         int  )0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[1] = (unsigned long )0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[1] = (unsigned short )0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[1] = (unsigned short )0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[2] = (unsigned char  *)sql_clave_sis;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[2] = (unsigned long )6;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[2] = (         int  )0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[2] = (         short *)0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[2] = (         int  )0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[2] = (unsigned long )0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[2] = (unsigned short )0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[2] = (unsigned short )0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[3] = (unsigned char  *)sql_ref_med;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[3] = (unsigned long )18;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[3] = (         int  )0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[3] = (         short *)0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[3] = (         int  )0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[3] = (unsigned long )0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[3] = (unsigned short )0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[3] = (unsigned short )0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[4] = (unsigned char  *)sql_clave_tran;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[4] = (unsigned long )8;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[4] = (         int  )0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[4] = (         short *)0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[4] = (         int  )0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[4] = (unsigned long )0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[4] = (unsigned short )0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[4] = (unsigned short )0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[5] = (unsigned char  *)sql_fecha_recep;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[5] = (unsigned long )40;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[5] = (         int  )0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[5] = (         short *)0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[5] = (         int  )0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[5] = (unsigned long )0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[5] = (unsigned short )0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[5] = (unsigned short )0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[6] = (unsigned char  *)sql_tipo;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[6] = (unsigned long )2;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[6] = (         int  )0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[6] = (         short *)0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[6] = (         int  )0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[6] = (unsigned long )0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[6] = (unsigned short )0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[6] = (unsigned short )0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[7] = (unsigned char  *)loc_tipo;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[7] = (unsigned long )2;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[7] = (         int  )0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[7] = (         short *)0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[7] = (         int  )0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[7] = (unsigned long )0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[7] = (unsigned short )0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[7] = (unsigned short )0;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqphsv = sqlstm.sqhstv;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqphsl = sqlstm.sqhstl;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqphss = sqlstm.sqhsts;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqpind = sqlstm.sqindv;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqpins = sqlstm.sqinds;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqparm = sqlstm.sqharm;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqparc = sqlstm.sqharc;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqpadto = sqlstm.sqadto;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqptdso = sqlstm.sqtdso;
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 1115 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 1125 "../../gral/src_db/CTransaccionalCdl.pc"


        if (contpro > 0)
        { // 8-2
            /* EXEC SQL SELECT nvl(status,   ' '),   nvl(tipo,   ' '), NVL(SUM(NVL(importe,0)),0)
                   INTO :loc_status, :loc_tipo, loc_importe
                   FROM TRANS_PROGRAMADO
                  WHERE clave_inst  = :sql_clave_inst
                    AND clave_sis   = :sql_clave_sis
                    AND clave_rast  = :sql_ref_med
                    AND clave_tran  = :sql_clave_tran
                    AND fecha_recep = TO_DATE(:sql_fecha_recep, 'YYYYMMDD') //TRUNC(sysdate)
                    AND tipo        = :sql_tipo
                    AND status <> 'P' || :loc_tipo || 'C'
                    AND fecha_baja IS NULL
                 GROUP BY nvl(status,' '), nvl(tipo,' '); */ 
#line 1140 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            struct sqlexd sqlstm;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlvsn = 13;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.arrsiz = 26;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqladtp = &sqladt;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqltdsp = &sqltds;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.stmt = "select nvl(status,' ') ,nvl(tipo,' ') ,NVL(sum(NV\
L(importe,0)),0) into :b0,:b1,:b2  from TRANS_PROGRAMADO where (((((((clave_in\
st=:b3 and clave_sis=:b4) and clave_rast=:b5) and clave_tran=:b6) and fecha_re\
cep=TO_DATE(:b7,'YYYYMMDD')) and tipo=:b8) and status<>(('P'||:b1)||'C')) and \
fecha_baja is null ) group by nvl(status,' '),nvl(tipo,' ')";
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.iters = (unsigned int  )1;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.offset = (unsigned int  )1423;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.selerr = (unsigned short)1;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlpfmem = (unsigned int  )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.cud = sqlcud0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlety = (unsigned short)4352;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.occurs = (unsigned int  )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[0] = (unsigned char  *)loc_status;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[0] = (unsigned long )5;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[0] = (         int  )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[0] = (         short *)0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[0] = (         int  )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[0] = (unsigned long )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[0] = (unsigned short )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[0] = (unsigned short )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[1] = (unsigned char  *)loc_tipo;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[1] = (unsigned long )2;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[1] = (         int  )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[1] = (         short *)0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[1] = (         int  )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[1] = (unsigned long )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[1] = (unsigned short )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[1] = (unsigned short )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[2] = (unsigned char  *)&loc_importe;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[2] = (unsigned long )sizeof(double);
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[2] = (         int  )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[2] = (         short *)0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[2] = (         int  )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[2] = (unsigned long )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[2] = (unsigned short )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[2] = (unsigned short )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[3] = (unsigned char  *)sql_clave_inst;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[3] = (unsigned long )6;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[3] = (         int  )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[3] = (         short *)0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[3] = (         int  )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[3] = (unsigned long )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[3] = (unsigned short )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[3] = (unsigned short )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[4] = (unsigned char  *)sql_clave_sis;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[4] = (unsigned long )6;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[4] = (         int  )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[4] = (         short *)0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[4] = (         int  )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[4] = (unsigned long )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[4] = (unsigned short )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[4] = (unsigned short )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[5] = (unsigned char  *)sql_ref_med;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[5] = (unsigned long )18;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[5] = (         int  )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[5] = (         short *)0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[5] = (         int  )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[5] = (unsigned long )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[5] = (unsigned short )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[5] = (unsigned short )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[6] = (unsigned char  *)sql_clave_tran;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[6] = (unsigned long )8;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[6] = (         int  )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[6] = (         short *)0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[6] = (         int  )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[6] = (unsigned long )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[6] = (unsigned short )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[6] = (unsigned short )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[7] = (unsigned char  *)sql_fecha_recep;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[7] = (unsigned long )40;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[7] = (         int  )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[7] = (         short *)0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[7] = (         int  )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[7] = (unsigned long )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[7] = (unsigned short )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[7] = (unsigned short )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[8] = (unsigned char  *)sql_tipo;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[8] = (unsigned long )2;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[8] = (         int  )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[8] = (         short *)0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[8] = (         int  )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[8] = (unsigned long )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[8] = (unsigned short )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[8] = (unsigned short )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[9] = (unsigned char  *)loc_tipo;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[9] = (unsigned long )2;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[9] = (         int  )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[9] = (         short *)0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[9] = (         int  )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[9] = (unsigned long )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[9] = (unsigned short )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[9] = (unsigned short )0;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqphsv = sqlstm.sqhstv;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqphsl = sqlstm.sqhstl;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqphss = sqlstm.sqhsts;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqpind = sqlstm.sqindv;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqpins = sqlstm.sqinds;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqparm = sqlstm.sqharm;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqparc = sqlstm.sqharc;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqpadto = sqlstm.sqadto;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqptdso = sqlstm.sqtdso;
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 1129 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 1140 "../../gral/src_db/CTransaccionalCdl.pc"

          /*EXEC SQL SELECT NVL(STATUS,' '), NVL(TIPO,' ')
                 INTO   :loc_status, :loc_tipo
                 FROM   TRANS_PROGRAMADO
                 WHERE  CLAVE_INST    = :sql_clave_inst
                   AND  FECHA_RECEP   = TRUNC(SYSDATE)
                   AND  TIPO          = :sql_tipo
                   AND  TRIM(REF_MED) = :sql_ref_med
                   AND  FECHA_BAJA IS NULL
                   AND  ROWNUM = 1;*/

          resultado = Obtiene_Ref_Concil();
          Gfi_StrRTrim(loc_status);
          if ((resultado == GFI_ERRC_OK) && (loc_importe > 0)) //(strlen(loc_status) > 0)
          { // 8-3
            if (NIVEL_TRACE > 9)
              cout << "ACTUALIZANDO EL PROGRAMADO A CONCILIADO" << endl;

            /* EXEC SQL UPDATE TRANS_PROGRAMADO
                     SET    REF_CONCIL  = :ref_concilG,
                            STATUS      = 'P' || :loc_tipo || 'C',
                            FECHA_FLUJO = SYSDATE
                     WHERE clave_inst  = :sql_clave_inst
                       AND clave_sis   = :sql_clave_sis
                       AND clave_rast  = :sql_ref_med
                       AND clave_tran  = :sql_clave_tran
                       AND fecha_recep = TO_DATE(:sql_fecha_recep, 'YYYYMMDD') //TRUNC(sysdate)
                       AND tipo        = :sql_tipo
                       AND status <> 'P' || :sql_tipo || 'C'
                       AND fecha_baja IS NULL; */ 
#line 1169 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            struct sqlexd sqlstm;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlvsn = 13;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.arrsiz = 26;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqladtp = &sqladt;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqltdsp = &sqltds;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.stmt = "update TRANS_PROGRAMADO  set REF_CONCIL=:b0,STATU\
S=(('P'||:b1)||'C'),FECHA_FLUJO=SYSDATE where (((((((clave_inst=:b2 and clave_\
sis=:b3) and clave_rast=:b4) and clave_tran=:b5) and fecha_recep=TO_DATE(:b6,'\
YYYYMMDD')) and tipo=:b7) and status<>(('P'||:b7)||'C')) and fecha_baja is nul\
l )";
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.iters = (unsigned int  )1;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.offset = (unsigned int  )1478;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.cud = sqlcud0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlety = (unsigned short)4352;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.occurs = (unsigned int  )0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[0] = (unsigned char  *)ref_concilG;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[0] = (unsigned long )20;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[0] = (         int  )0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[0] = (         short *)0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[0] = (         int  )0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[0] = (unsigned long )0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[0] = (unsigned short )0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[0] = (unsigned short )0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[1] = (unsigned char  *)loc_tipo;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[1] = (unsigned long )2;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[1] = (         int  )0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[1] = (         short *)0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[1] = (         int  )0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[1] = (unsigned long )0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[1] = (unsigned short )0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[1] = (unsigned short )0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[2] = (unsigned char  *)sql_clave_inst;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[2] = (unsigned long )6;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[2] = (         int  )0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[2] = (         short *)0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[2] = (         int  )0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[2] = (unsigned long )0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[2] = (unsigned short )0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[2] = (unsigned short )0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[3] = (unsigned char  *)sql_clave_sis;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[3] = (unsigned long )6;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[3] = (         int  )0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[3] = (         short *)0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[3] = (         int  )0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[3] = (unsigned long )0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[3] = (unsigned short )0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[3] = (unsigned short )0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[4] = (unsigned char  *)sql_ref_med;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[4] = (unsigned long )18;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[4] = (         int  )0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[4] = (         short *)0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[4] = (         int  )0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[4] = (unsigned long )0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[4] = (unsigned short )0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[4] = (unsigned short )0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[5] = (unsigned char  *)sql_clave_tran;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[5] = (unsigned long )8;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[5] = (         int  )0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[5] = (         short *)0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[5] = (         int  )0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[5] = (unsigned long )0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[5] = (unsigned short )0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[5] = (unsigned short )0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[6] = (unsigned char  *)sql_fecha_recep;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[6] = (unsigned long )40;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[6] = (         int  )0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[6] = (         short *)0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[6] = (         int  )0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[6] = (unsigned long )0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[6] = (unsigned short )0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[6] = (unsigned short )0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[7] = (unsigned char  *)sql_tipo;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[7] = (unsigned long )2;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[7] = (         int  )0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[7] = (         short *)0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[7] = (         int  )0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[7] = (unsigned long )0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[7] = (unsigned short )0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[7] = (unsigned short )0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[8] = (unsigned char  *)sql_tipo;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[8] = (unsigned long )2;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[8] = (         int  )0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[8] = (         short *)0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[8] = (         int  )0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[8] = (unsigned long )0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[8] = (unsigned short )0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[8] = (unsigned short )0;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqphsv = sqlstm.sqhstv;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqphsl = sqlstm.sqhstl;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqphss = sqlstm.sqhsts;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqpind = sqlstm.sqindv;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqpins = sqlstm.sqinds;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqparm = sqlstm.sqharm;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqparc = sqlstm.sqharc;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqpadto = sqlstm.sqadto;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqptdso = sqlstm.sqtdso;
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 1158 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 1169 "../../gral/src_db/CTransaccionalCdl.pc"

                     /*WHERE  CLAVE_INST    = :sql_clave_inst
                       AND  FECHA_RECEP   = TRUNC(SYSDATE)
                       AND  TIPO          = :sql_tipo
                       AND  TRIM(REF_MED) = :sql_ref_med
                       AND  FECHA_BAJA IS NULL
                       AND  STATUS       <> 'P' || :sql_tipo || 'C';*/

            resultado = ChecaErrorOracle( 0, 1, &rowcount);
            if (resultado != GFI_ERRC_OK)
            {
              if (NIVEL_TRACE > 5)
                cout << "ERROR AL ACTUALIZAR EN TRANS_PROGRAMADO!" << endl;

              strcpy(err_desc,"R0009");
            }
            else
            { // 8-4
              if (NIVEL_TRACE > 9)
                cout << "BUSCANDO EL REAL COMO SALA DE ESPERA" << endl;

              loc_status[0] = '\0';  //NULL
              loc_tipo[0] = '\0';  //NULL
              loc_fecha_baja[0] = '\0';  //NULL
              contpag = 0;

              // Index: CLAVE_INST, CLAVE_SIS, CLAVE_RAST, CLAVE_TRAN, FECHA_RECEP, TIPO

              // Correcciones por Conciliacion de Parcialidades Incompletas HGT (24-Mar-2010)
              /* EXEC SQL SELECT COUNT(1)
                       INTO   :contpag
                       FROM   TRANS_PAGO
                       WHERE  CLAVE_INST  = :sql_clave_inst
                         AND  CLAVE_SIS   = :sql_clave_sis
                         AND  CLAVE_RAST  = :sql_clave_rast
                         AND  CLAVE_TRAN  = :sql_clave_tran
                         AND  FECHA_RECEP = TO_DATE(:sql_fecha_recep, 'YYYYMMDD')
                         AND  TIPO        = :sql_tipo
                         AND  STATUS <> 'R' || TIPO || 'PC'
                         AND  FECHA_BAJA IS NULL; */ 
#line 1208 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              struct sqlexd sqlstm;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqlvsn = 13;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.arrsiz = 26;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqladtp = &sqladt;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqltdsp = &sqltds;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.stmt = "select count(1) into :b0  from TRANS_PAGO where\
 (((((((CLAVE_INST=:b1 and CLAVE_SIS=:b2) and CLAVE_RAST=:b3) and CLAVE_TRAN=:\
b4) and FECHA_RECEP=TO_DATE(:b5,'YYYYMMDD')) and TIPO=:b6) and STATUS<>(('R'||\
TIPO)||'PC')) and FECHA_BAJA is null )";
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.iters = (unsigned int  )1;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.offset = (unsigned int  )1529;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.selerr = (unsigned short)1;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqlpfmem = (unsigned int  )0;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.cud = sqlcud0;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqlety = (unsigned short)4352;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.occurs = (unsigned int  )0;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[0] = (unsigned char  *)&contpag;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[0] = (unsigned long )sizeof(int);
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[0] = (         int  )0;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[0] = (         short *)0;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[0] = (         int  )0;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[0] = (unsigned long )0;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[0] = (unsigned short )0;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[0] = (unsigned short )0;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[1] = (unsigned char  *)sql_clave_inst;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[1] = (unsigned long )6;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[1] = (         int  )0;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[1] = (         short *)0;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[1] = (         int  )0;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[1] = (unsigned long )0;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[1] = (unsigned short )0;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[1] = (unsigned short )0;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[2] = (unsigned char  *)sql_clave_sis;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[2] = (unsigned long )6;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[2] = (         int  )0;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[2] = (         short *)0;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[2] = (         int  )0;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[2] = (unsigned long )0;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[2] = (unsigned short )0;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[2] = (unsigned short )0;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[3] = (unsigned char  *)sql_clave_rast;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[3] = (unsigned long )31;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[3] = (         int  )0;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[3] = (         short *)0;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[3] = (         int  )0;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[3] = (unsigned long )0;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[3] = (unsigned short )0;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[3] = (unsigned short )0;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[4] = (unsigned char  *)sql_clave_tran;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[4] = (unsigned long )8;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[4] = (         int  )0;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[4] = (         short *)0;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[4] = (         int  )0;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[4] = (unsigned long )0;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[4] = (unsigned short )0;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[4] = (unsigned short )0;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[5] = (unsigned char  *)sql_fecha_recep;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[5] = (unsigned long )40;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[5] = (         int  )0;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[5] = (         short *)0;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[5] = (         int  )0;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[5] = (unsigned long )0;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[5] = (unsigned short )0;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[5] = (unsigned short )0;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[6] = (unsigned char  *)sql_tipo;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[6] = (unsigned long )2;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[6] = (         int  )0;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[6] = (         short *)0;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[6] = (         int  )0;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[6] = (unsigned long )0;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[6] = (unsigned short )0;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[6] = (unsigned short )0;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqphsv = sqlstm.sqhstv;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqphsl = sqlstm.sqhstl;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqphss = sqlstm.sqhsts;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqpind = sqlstm.sqindv;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqpins = sqlstm.sqinds;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqparm = sqlstm.sqharm;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqparc = sqlstm.sqharc;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqpadto = sqlstm.sqadto;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqptdso = sqlstm.sqtdso;
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 1198 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 1208 "../../gral/src_db/CTransaccionalCdl.pc"


              if (contpag > 0)
              { // 8-6a
                /* EXEC SQL SELECT NVL(STATUS,' '), NVL(TIPO,' ') //, NVL(TO_CHAR(FECHA_BAJA, 'YYYYMMDD HH24:MI:SS'),' ')
                         INTO   :loc_status, :loc_tipo //, :loc_fecha_baja
                         FROM   TRANS_PAGO
                         WHERE  CLAVE_INST  = :sql_clave_inst
                           AND  CLAVE_SIS   = :sql_clave_sis
                           AND  CLAVE_RAST  = :sql_clave_rast
                           AND  CLAVE_TRAN  = :sql_clave_tran
                           AND  FECHA_RECEP = TO_DATE(:sql_fecha_recep, 'YYYYMMDD')
                           AND  TIPO        = :sql_tipo
                           AND  STATUS <> 'R' || TIPO || 'PC'
                           AND  FECHA_BAJA IS NULL; */ 
#line 1222 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                struct sqlexd sqlstm;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqlvsn = 13;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.arrsiz = 26;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqladtp = &sqladt;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqltdsp = &sqltds;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.stmt = "select NVL(STATUS,' ') ,NVL(TIPO,' ') into :b\
0,:b1  from TRANS_PAGO where (((((((CLAVE_INST=:b2 and CLAVE_SIS=:b3) and CLAV\
E_RAST=:b4) and CLAVE_TRAN=:b5) and FECHA_RECEP=TO_DATE(:b6,'YYYYMMDD')) and T\
IPO=:b7) and STATUS<>(('R'||TIPO)||'PC')) and FECHA_BAJA is null )";
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.iters = (unsigned int  )1;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.offset = (unsigned int  )1572;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.selerr = (unsigned short)1;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqlpfmem = (unsigned int  )0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.cud = sqlcud0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqlety = (unsigned short)4352;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.occurs = (unsigned int  )0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[0] = (unsigned char  *)loc_status;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[0] = (unsigned long )5;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[0] = (         int  )0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[0] = (         short *)0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[0] = (         int  )0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[0] = (unsigned long )0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[0] = (unsigned short )0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[0] = (unsigned short )0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[1] = (unsigned char  *)loc_tipo;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[1] = (unsigned long )2;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[1] = (         int  )0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[1] = (         short *)0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[1] = (         int  )0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[1] = (unsigned long )0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[1] = (unsigned short )0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[1] = (unsigned short )0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[2] = (unsigned char  *)sql_clave_inst;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[2] = (unsigned long )6;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[2] = (         int  )0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[2] = (         short *)0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[2] = (         int  )0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[2] = (unsigned long )0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[2] = (unsigned short )0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[2] = (unsigned short )0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[3] = (unsigned char  *)sql_clave_sis;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[3] = (unsigned long )6;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[3] = (         int  )0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[3] = (         short *)0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[3] = (         int  )0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[3] = (unsigned long )0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[3] = (unsigned short )0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[3] = (unsigned short )0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[4] = (unsigned char  *)sql_clave_rast;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[4] = (unsigned long )31;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[4] = (         int  )0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[4] = (         short *)0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[4] = (         int  )0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[4] = (unsigned long )0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[4] = (unsigned short )0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[4] = (unsigned short )0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[5] = (unsigned char  *)sql_clave_tran;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[5] = (unsigned long )8;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[5] = (         int  )0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[5] = (         short *)0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[5] = (         int  )0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[5] = (unsigned long )0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[5] = (unsigned short )0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[5] = (unsigned short )0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[6] = (unsigned char  *)sql_fecha_recep;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[6] = (unsigned long )40;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[6] = (         int  )0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[6] = (         short *)0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[6] = (         int  )0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[6] = (unsigned long )0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[6] = (unsigned short )0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[6] = (unsigned short )0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[7] = (unsigned char  *)sql_tipo;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[7] = (unsigned long )2;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[7] = (         int  )0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[7] = (         short *)0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[7] = (         int  )0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[7] = (unsigned long )0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[7] = (unsigned short )0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[7] = (unsigned short )0;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqphsv = sqlstm.sqhstv;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqphsl = sqlstm.sqhstl;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqphss = sqlstm.sqhsts;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqpind = sqlstm.sqindv;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqpins = sqlstm.sqinds;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqparm = sqlstm.sqharm;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqparc = sqlstm.sqharc;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqpadto = sqlstm.sqadto;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqptdso = sqlstm.sqtdso;
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 1212 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 1222 "../../gral/src_db/CTransaccionalCdl.pc"

                           //AND  ROWNUM = 1; Propuesto por H.Martinez 19-May-2010

                Gfi_StrRTrim(loc_status);  Gfi_StrRTrim(loc_tipo); //Gfi_StrRTrim(loc_fecha_baja);

                sprintf(loc_status1, "R%sPC",loc_tipo);
                if (strcmp(loc_status,loc_status1)) //&& (strlen(loc_fecha_baja) == 0))
                {
                  if (!strcmp(loc_status2,"C"))
                  {
                    if (NIVEL_TRACE > 9)
                      cout << "CONCILIANDO EL REAL..." << endl;

                    /* EXEC SQL UPDATE TRANS_PAGO
                             SET    REF_CONCIL  = :ref_concilG,
                                    STATUS      = 'R' || :loc_tipo || 'PC',
                                    IMPORTE     = ROUND(:sql_importe,2),
                                    FECHA_FLUJO = SYSDATE
                             WHERE  CLAVE_INST  = :sql_clave_inst
                               AND  CLAVE_SIS   = :sql_clave_sis
                               AND  CLAVE_RAST  = :sql_clave_rast
                               AND  CLAVE_TRAN  = :sql_clave_tran
                               AND  FECHA_RECEP = TO_DATE(:sql_fecha_recep, 'YYYYMMDD')
                               AND  TIPO        = :sql_tipo
                               AND  STATUS <> 'R' || TIPO || 'PC'
                               AND  FECHA_BAJA IS NULL; */ 
#line 1247 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    struct sqlexd sqlstm;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqlvsn = 13;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.arrsiz = 26;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqladtp = &sqladt;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqltdsp = &sqltds;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.stmt = "update TRANS_PAGO  set REF_CONCIL=:b0,STA\
TUS=(('R'||:b1)||'PC'),IMPORTE=ROUND(:b2,2),FECHA_FLUJO=SYSDATE where (((((((C\
LAVE_INST=:b3 and CLAVE_SIS=:b4) and CLAVE_RAST=:b5) and CLAVE_TRAN=:b6) and F\
ECHA_RECEP=TO_DATE(:b7,'YYYYMMDD')) and TIPO=:b8) and STATUS<>(('R'||TIPO)||'P\
C')) and FECHA_BAJA is null )";
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.iters = (unsigned int  )1;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.offset = (unsigned int  )1619;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.cud = sqlcud0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqlety = (unsigned short)4352;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.occurs = (unsigned int  )0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqhstv[0] = (unsigned char  *)ref_concilG;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqhstl[0] = (unsigned long )20;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqhsts[0] = (         int  )0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqindv[0] = (         short *)0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqinds[0] = (         int  )0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqharm[0] = (unsigned long )0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqadto[0] = (unsigned short )0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqtdso[0] = (unsigned short )0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqhstv[1] = (unsigned char  *)loc_tipo;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqhstl[1] = (unsigned long )2;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqhsts[1] = (         int  )0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqindv[1] = (         short *)0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqinds[1] = (         int  )0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqharm[1] = (unsigned long )0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqadto[1] = (unsigned short )0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqtdso[1] = (unsigned short )0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqhstv[2] = (unsigned char  *)&sql_importe;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqhstl[2] = (unsigned long )sizeof(double);
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqhsts[2] = (         int  )0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqindv[2] = (         short *)0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqinds[2] = (         int  )0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqharm[2] = (unsigned long )0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqadto[2] = (unsigned short )0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqtdso[2] = (unsigned short )0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqhstv[3] = (unsigned char  *)sql_clave_inst;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqhstl[3] = (unsigned long )6;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqhsts[3] = (         int  )0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqindv[3] = (         short *)0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqinds[3] = (         int  )0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqharm[3] = (unsigned long )0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqadto[3] = (unsigned short )0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqtdso[3] = (unsigned short )0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqhstv[4] = (unsigned char  *)sql_clave_sis;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqhstl[4] = (unsigned long )6;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqhsts[4] = (         int  )0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqindv[4] = (         short *)0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqinds[4] = (         int  )0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqharm[4] = (unsigned long )0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqadto[4] = (unsigned short )0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqtdso[4] = (unsigned short )0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqhstv[5] = (unsigned char  *)sql_clave_rast;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqhstl[5] = (unsigned long )31;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqhsts[5] = (         int  )0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqindv[5] = (         short *)0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqinds[5] = (         int  )0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqharm[5] = (unsigned long )0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqadto[5] = (unsigned short )0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqtdso[5] = (unsigned short )0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqhstv[6] = (unsigned char  *)sql_clave_tran;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqhstl[6] = (unsigned long )8;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqhsts[6] = (         int  )0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqindv[6] = (         short *)0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqinds[6] = (         int  )0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqharm[6] = (unsigned long )0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqadto[6] = (unsigned short )0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqtdso[6] = (unsigned short )0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqhstv[7] = (unsigned char  *)sql_fecha_recep;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqhstl[7] = (unsigned long )40;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqhsts[7] = (         int  )0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqindv[7] = (         short *)0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqinds[7] = (         int  )0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqharm[7] = (unsigned long )0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqadto[7] = (unsigned short )0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqtdso[7] = (unsigned short )0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqhstv[8] = (unsigned char  *)sql_tipo;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqhstl[8] = (unsigned long )2;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqhsts[8] = (         int  )0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqindv[8] = (         short *)0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqinds[8] = (         int  )0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqharm[8] = (unsigned long )0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqadto[8] = (unsigned short )0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqtdso[8] = (unsigned short )0;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqphsv = sqlstm.sqhstv;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqphsl = sqlstm.sqhstl;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqphss = sqlstm.sqhsts;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqpind = sqlstm.sqindv;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqpins = sqlstm.sqinds;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqparm = sqlstm.sqharm;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqparc = sqlstm.sqharc;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqpadto = sqlstm.sqadto;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlstm.sqptdso = sqlstm.sqtdso;
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
                    sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 1235 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 1247 "../../gral/src_db/CTransaccionalCdl.pc"

                    resultado = ChecaErrorOracle( 0, 1, &rowcount);

                    if (resultado == GFI_ERRC_OK)
                      swConcOk = 1; // HGT 18-May-2010
                    else
                    {
                      if (NIVEL_TRACE > 5)
                        cout << "ERROR AL ACTUALIZAR EN TRANS_PAGO!" << endl;

                      strcpy(err_desc,"R0010");
                    }
                  }
                }
              } // 8-6a
              else
              { // 8-6b
                if (NIVEL_TRACE > 9)
                  cout << "NO EXISTE EL REAL, INSERTANDOLO..." << endl;

                /* EXEC SQL INSERT INTO TRANS_PAGO ( CLAVE_INST , CLAVE_SIS , CLAVE_RAST , CLAVE_TRAN ,
                                                  FECHA_RECEP , TIPO , FOLIO_ORIGEN , TIPO_OPER ,
                                                  BANCO_ORD , CVE_BANCO_ORD , NOMBRE_ORD , CUENTA_ORD ,
                                                  BANCO_BEN , CVE_BANCO_BEN , NOMBRE_BEN , CUENTA_BEN ,
                                                  IMPORTE , STATUS , OBSERVACION , REF_CONCIL ,
                                                  REF_MED , FECHA_CAP , FECHA_VENC , FECHA_FLUJO ,
                                                  FECHA_REG , FECHA_BAJA )
                                     VALUES( :sql_clave_inst , :sql_clave_sis , :sql_clave_rast , :sql_clave_tran ,
                                             TO_DATE(:sql_fecha_recep, 'YYYYMMDD') , :sql_tipo , :sql_folio_origen ,
                                             :sql_tipo_oper , :sql_banco_ord , :sql_cve_banco_ord , :sql_nombre_ord ,
                                             :sql_cuenta_ord , :sql_banco_ben , :sql_cve_banco_ben , :sql_nombre_ben ,
                                             :sql_cuenta_ben , ROUND(:sql_importe,2) , 'R' || :sql_tipo || 'PC' ,
                                             :sql_observacion , :ref_concilG , :sql_ref_med ,
                                             TO_DATE(:sql_fecha_cap, 'YYYYMMDD HH24:MI:SS') ,
                                             TO_DATE(:sql_fecha_venc, 'YYYYMMDD HH24:MI:SS') ,
                                             SYSDATE , TO_DATE(:sql_fecha_reg, 'YYYYMMDD HH24:MI:SS') ,
                                             TO_DATE(:sql_fecha_baja, 'YYYYMMDD HH24:MI:SS') ); */ 
#line 1283 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                struct sqlexd sqlstm;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqlvsn = 13;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.arrsiz = 26;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqladtp = &sqladt;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqltdsp = &sqltds;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.stmt = "insert into TRANS_PAGO (CLAVE_INST,CLAVE_SIS,\
CLAVE_RAST,CLAVE_TRAN,FECHA_RECEP,TIPO,FOLIO_ORIGEN,TIPO_OPER,BANCO_ORD,CVE_BA\
NCO_ORD,NOMBRE_ORD,CUENTA_ORD,BANCO_BEN,CVE_BANCO_BEN,NOMBRE_BEN,CUENTA_BEN,IM\
PORTE,STATUS,OBSERVACION,REF_CONCIL,REF_MED,FECHA_CAP,FECHA_VENC,FECHA_FLUJO,F\
ECHA_REG,FECHA_BAJA) values (:b0,:b1,:b2,:b3,TO_DATE(:b4,'YYYYMMDD'),:b5,:b6,:\
b7,:b8,:b9,:b10,:b11,:b12,:b13,:b14,:b15,ROUND(:b16,2),(('R'||:b5)||'PC'),:b18\
,:b19,:b20,TO_DATE(:b21,'YYYYMMDD HH24:MI:SS'),TO_DATE(:b22,'YYYYMMDD HH24:MI:\
SS'),SYSDATE,TO_DATE(:b23,'YYYYMMDD HH24:MI:SS'),TO_DATE(:b24,'YYYYMMDD HH24:M\
I:SS'))";
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.iters = (unsigned int  )1;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.offset = (unsigned int  )1670;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.cud = sqlcud0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqlety = (unsigned short)4352;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.occurs = (unsigned int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[0] = (unsigned char  *)sql_clave_inst;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[0] = (unsigned long )6;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[0] = (         int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[0] = (         short *)0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[0] = (         int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[0] = (unsigned long )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[0] = (unsigned short )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[0] = (unsigned short )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[1] = (unsigned char  *)sql_clave_sis;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[1] = (unsigned long )6;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[1] = (         int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[1] = (         short *)0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[1] = (         int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[1] = (unsigned long )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[1] = (unsigned short )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[1] = (unsigned short )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[2] = (unsigned char  *)sql_clave_rast;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[2] = (unsigned long )31;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[2] = (         int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[2] = (         short *)0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[2] = (         int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[2] = (unsigned long )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[2] = (unsigned short )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[2] = (unsigned short )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[3] = (unsigned char  *)sql_clave_tran;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[3] = (unsigned long )8;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[3] = (         int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[3] = (         short *)0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[3] = (         int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[3] = (unsigned long )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[3] = (unsigned short )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[3] = (unsigned short )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[4] = (unsigned char  *)sql_fecha_recep;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[4] = (unsigned long )40;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[4] = (         int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[4] = (         short *)0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[4] = (         int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[4] = (unsigned long )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[4] = (unsigned short )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[4] = (unsigned short )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[5] = (unsigned char  *)sql_tipo;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[5] = (unsigned long )2;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[5] = (         int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[5] = (         short *)0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[5] = (         int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[5] = (unsigned long )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[5] = (unsigned short )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[5] = (unsigned short )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[6] = (unsigned char  *)sql_folio_origen;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[6] = (unsigned long )10;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[6] = (         int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[6] = (         short *)0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[6] = (         int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[6] = (unsigned long )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[6] = (unsigned short )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[6] = (unsigned short )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[7] = (unsigned char  *)sql_tipo_oper;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[7] = (unsigned long )5;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[7] = (         int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[7] = (         short *)0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[7] = (         int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[7] = (unsigned long )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[7] = (unsigned short )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[7] = (unsigned short )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[8] = (unsigned char  *)sql_banco_ord;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[8] = (unsigned long )7;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[8] = (         int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[8] = (         short *)0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[8] = (         int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[8] = (unsigned long )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[8] = (unsigned short )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[8] = (unsigned short )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[9] = (unsigned char  *)sql_cve_banco_ord;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[9] = (unsigned long )16;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[9] = (         int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[9] = (         short *)0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[9] = (         int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[9] = (unsigned long )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[9] = (unsigned short )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[9] = (unsigned short )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[10] = (unsigned char  *)sql_nombre_ord;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[10] = (unsigned long )52;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[10] = (         int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[10] = (         short *)0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[10] = (         int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[10] = (unsigned long )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[10] = (unsigned short )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[10] = (unsigned short )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[11] = (unsigned char  *)sql_cuenta_ord;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[11] = (unsigned long )21;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[11] = (         int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[11] = (         short *)0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[11] = (         int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[11] = (unsigned long )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[11] = (unsigned short )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[11] = (unsigned short )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[12] = (unsigned char  *)sql_banco_ben;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[12] = (unsigned long )7;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[12] = (         int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[12] = (         short *)0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[12] = (         int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[12] = (unsigned long )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[12] = (unsigned short )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[12] = (unsigned short )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[13] = (unsigned char  *)sql_cve_banco_ben;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[13] = (unsigned long )16;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[13] = (         int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[13] = (         short *)0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[13] = (         int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[13] = (unsigned long )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[13] = (unsigned short )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[13] = (unsigned short )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[14] = (unsigned char  *)sql_nombre_ben;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[14] = (unsigned long )52;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[14] = (         int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[14] = (         short *)0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[14] = (         int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[14] = (unsigned long )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[14] = (unsigned short )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[14] = (unsigned short )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[15] = (unsigned char  *)sql_cuenta_ben;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[15] = (unsigned long )21;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[15] = (         int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[15] = (         short *)0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[15] = (         int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[15] = (unsigned long )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[15] = (unsigned short )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[15] = (unsigned short )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[16] = (unsigned char  *)&sql_importe;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[16] = (unsigned long )sizeof(double);
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[16] = (         int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[16] = (         short *)0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[16] = (         int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[16] = (unsigned long )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[16] = (unsigned short )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[16] = (unsigned short )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[17] = (unsigned char  *)sql_tipo;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[17] = (unsigned long )2;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[17] = (         int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[17] = (         short *)0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[17] = (         int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[17] = (unsigned long )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[17] = (unsigned short )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[17] = (unsigned short )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[18] = (unsigned char  *)sql_observacion;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[18] = (unsigned long )50;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[18] = (         int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[18] = (         short *)0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[18] = (         int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[18] = (unsigned long )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[18] = (unsigned short )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[18] = (unsigned short )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[19] = (unsigned char  *)ref_concilG;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[19] = (unsigned long )20;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[19] = (         int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[19] = (         short *)0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[19] = (         int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[19] = (unsigned long )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[19] = (unsigned short )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[19] = (unsigned short )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[20] = (unsigned char  *)sql_ref_med;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[20] = (unsigned long )18;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[20] = (         int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[20] = (         short *)0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[20] = (         int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[20] = (unsigned long )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[20] = (unsigned short )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[20] = (unsigned short )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[21] = (unsigned char  *)sql_fecha_cap;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[21] = (unsigned long )40;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[21] = (         int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[21] = (         short *)0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[21] = (         int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[21] = (unsigned long )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[21] = (unsigned short )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[21] = (unsigned short )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[22] = (unsigned char  *)sql_fecha_venc;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[22] = (unsigned long )18;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[22] = (         int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[22] = (         short *)0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[22] = (         int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[22] = (unsigned long )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[22] = (unsigned short )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[22] = (unsigned short )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[23] = (unsigned char  *)sql_fecha_reg;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[23] = (unsigned long )18;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[23] = (         int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[23] = (         short *)0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[23] = (         int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[23] = (unsigned long )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[23] = (unsigned short )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[23] = (unsigned short )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[24] = (unsigned char  *)sql_fecha_baja;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[24] = (unsigned long )18;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[24] = (         int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[24] = (         short *)0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[24] = (         int  )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[24] = (unsigned long )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[24] = (unsigned short )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[24] = (unsigned short )0;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqphsv = sqlstm.sqhstv;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqphsl = sqlstm.sqhstl;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqphss = sqlstm.sqhsts;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqpind = sqlstm.sqindv;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqpins = sqlstm.sqinds;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqparm = sqlstm.sqharm;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqparc = sqlstm.sqharc;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqpadto = sqlstm.sqadto;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqptdso = sqlstm.sqtdso;
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 1267 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 1283 "../../gral/src_db/CTransaccionalCdl.pc"

                resultado = ChecaErrorOracle (0, 1, &rowcount);

                if (resultado == GFI_ERRC_OK)
                  swConcOk = 1; // HGT 18-May-2010
                else
                {
                  if (NIVEL_TRACE > 5)
                  cout << "ERROR AL INSERTAR EN TRANS_PAGO!" << endl;

                  strcpy(err_desc,"R0011");
                }
              } // 8-6b
            } // 8-4
          } // 8-3
        } // 8-2
        else
        {
          if (NIVEL_TRACE > 5)
            cout << "NO EXISTE PROGRAMADO!" << endl;

          strcpy(err_desc,"R0012");
        }

        if (NIVEL_TRACE > 9)
          cout << "TERMINO CONCILIACION POR REFERENCIA MEDIA" << endl;

      } // 8-1
    } // 8

    if ((loc_tpconc == 16) && (swConcOk == 0)); // HGT 18-May-2010
    { // 16

      //if ((strlen(sql_folio_origen)>0) && (atol(sql_folio_origen))>0)  // HGT 18-Jun-2010
      if (strlen(sql_ref_med)>0)
      { // 16-1
        if (NIVEL_TRACE > 5)
          cout << "CONCILIACION POR PARCIALIDADES" << endl;

        strcpy(sql_insth,sql_clave_inst); strcpy(sql_sish,sql_clave_sis); strcpy(sql_nmph,"LIMITE");
        loc_limite=100;
        /* EXEC SQL OPEN OBTENPARAM; */ 
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"
        struct sqlexd sqlstm;
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqlvsn = 13;
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.arrsiz = 26;
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqladtp = &sqladt;
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqltdsp = &sqltds;
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.stmt = sq0002;
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.iters = (unsigned int  )1;
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.offset = (unsigned int  )1785;
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.selerr = (unsigned short)1;
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqlpfmem = (unsigned int  )0;
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.cud = sqlcud0;
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqlety = (unsigned short)4352;
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.occurs = (unsigned int  )0;
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqcmod = (unsigned int )0;
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[0] = (unsigned char  *)sql_insth;
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[0] = (unsigned long )20;
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[0] = (         int  )0;
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[0] = (         short *)0;
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[0] = (         int  )0;
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[0] = (unsigned long )0;
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[0] = (unsigned short )0;
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[0] = (unsigned short )0;
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[1] = (unsigned char  *)sql_sish;
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[1] = (unsigned long )20;
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[1] = (         int  )0;
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[1] = (         short *)0;
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[1] = (         int  )0;
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[1] = (unsigned long )0;
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[1] = (unsigned short )0;
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[1] = (unsigned short )0;
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[2] = (unsigned char  *)sql_nmph;
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[2] = (unsigned long )20;
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[2] = (         int  )0;
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[2] = (         short *)0;
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[2] = (         int  )0;
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[2] = (unsigned long )0;
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[2] = (unsigned short )0;
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[2] = (unsigned short )0;
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqphsv = sqlstm.sqhstv;
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqphsl = sqlstm.sqhstl;
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqphss = sqlstm.sqhsts;
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqpind = sqlstm.sqindv;
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqpins = sqlstm.sqinds;
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqparm = sqlstm.sqharm;
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqparc = sqlstm.sqharc;
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqpadto = sqlstm.sqadto;
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqptdso = sqlstm.sqtdso;
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 1324 "../../gral/src_db/CTransaccionalCdl.pc"

        while(1)
        {
            /* EXEC SQL FETCH OBTENPARAM INTO :sql_valor; */ 
#line 1327 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 1327 "../../gral/src_db/CTransaccionalCdl.pc"
            struct sqlexd sqlstm;
#line 1327 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlvsn = 13;
#line 1327 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.arrsiz = 26;
#line 1327 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqladtp = &sqladt;
#line 1327 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqltdsp = &sqltds;
#line 1327 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.iters = (unsigned int  )1;
#line 1327 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.offset = (unsigned int  )1812;
#line 1327 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.selerr = (unsigned short)1;
#line 1327 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlpfmem = (unsigned int  )0;
#line 1327 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.cud = sqlcud0;
#line 1327 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 1327 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlety = (unsigned short)4352;
#line 1327 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.occurs = (unsigned int  )0;
#line 1327 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqfoff = (         int )0;
#line 1327 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqfmod = (unsigned int )2;
#line 1327 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[0] = (unsigned char  *)sql_valor;
#line 1327 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[0] = (unsigned long )100;
#line 1327 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[0] = (         int  )0;
#line 1327 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[0] = (         short *)0;
#line 1327 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[0] = (         int  )0;
#line 1327 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[0] = (unsigned long )0;
#line 1327 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[0] = (unsigned short )0;
#line 1327 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[0] = (unsigned short )0;
#line 1327 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqphsv = sqlstm.sqhstv;
#line 1327 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqphsl = sqlstm.sqhstl;
#line 1327 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqphss = sqlstm.sqhsts;
#line 1327 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqpind = sqlstm.sqindv;
#line 1327 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqpins = sqlstm.sqinds;
#line 1327 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqparm = sqlstm.sqharm;
#line 1327 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqparc = sqlstm.sqharc;
#line 1327 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqpadto = sqlstm.sqadto;
#line 1327 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqptdso = sqlstm.sqtdso;
#line 1327 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 1327 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 1327 "../../gral/src_db/CTransaccionalCdl.pc"

            resultado = ChecaErrorOracle (0, 1, &rowcount);
            if (resultado != GFI_ERRC_OK)
              break;
            Gfi_StrRTrim(sql_valor);
            if (rowcount == 1)
              loc_limite = atof(sql_valor);
            else
              break;
        }
        /* EXEC SQL CLOSE OBTENPARAM; */ 
#line 1337 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 1337 "../../gral/src_db/CTransaccionalCdl.pc"
        struct sqlexd sqlstm;
#line 1337 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqlvsn = 13;
#line 1337 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.arrsiz = 26;
#line 1337 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqladtp = &sqladt;
#line 1337 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqltdsp = &sqltds;
#line 1337 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.iters = (unsigned int  )1;
#line 1337 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.offset = (unsigned int  )1831;
#line 1337 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.cud = sqlcud0;
#line 1337 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 1337 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqlety = (unsigned short)4352;
#line 1337 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.occurs = (unsigned int  )0;
#line 1337 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 1337 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 1337 "../../gral/src_db/CTransaccionalCdl.pc"


        if (NIVEL_TRACE > 9)
          cout << "BUSCA EL PROGRAMADO" << endl;

        sw = 0;
        loc_status2[0] = '\0';  //NULL
        loc_status[0] = '\0';  //NULL
        loc_tipo[0] = '\0';  //NULL
        loc_importe = 0;

        // Index: CLAVE_INST, CLAVE_SIS, CLAVE_RAST, CLAVE_TRAN, FECHA_RECEP, TIPO

        /* EXEC SQL SELECT NVL(STATUS,' '), NVL(TIPO,' '), NVL(IMPORTE,0)
                 INTO   :loc_status, :loc_tipo, :loc_importe
                 FROM   TRANS_PROGRAMADO
                 WHERE  CLAVE_INST  = :sql_clave_inst
                   AND  CLAVE_SIS   = :sql_clave_sis
                   AND  CLAVE_RAST  = :sql_ref_med
                   AND  CLAVE_TRAN  = :sql_clave_tran
                   AND  FECHA_RECEP = TRUNC(SYSDATE)
                   AND  TIPO        = :sql_tipo
                   //AND  LPAD(FOLIO_ORIGEN, 9, '0') = LPAD(:sql_folio_origen, 9 ,'0') HGT 18-Jun-2010
                   AND  FECHA_BAJA IS NULL; */ 
#line 1360 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        struct sqlexd sqlstm;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqlvsn = 13;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.arrsiz = 26;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqladtp = &sqladt;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqltdsp = &sqltds;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.stmt = "select NVL(STATUS,' ') ,NVL(TIPO,' ') ,NVL(IMPORTE,0)\
 into :b0,:b1,:b2  from TRANS_PROGRAMADO where ((((((CLAVE_INST=:b3 and CLAVE_\
SIS=:b4) and CLAVE_RAST=:b5) and CLAVE_TRAN=:b6) and FECHA_RECEP=TRUNC(SYSDATE\
)) and TIPO=:b7) and FECHA_BAJA is null )";
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.iters = (unsigned int  )1;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.offset = (unsigned int  )1846;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.selerr = (unsigned short)1;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqlpfmem = (unsigned int  )0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.cud = sqlcud0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqlety = (unsigned short)4352;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.occurs = (unsigned int  )0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[0] = (unsigned char  *)loc_status;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[0] = (unsigned long )5;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[0] = (         int  )0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[0] = (         short *)0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[0] = (         int  )0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[0] = (unsigned long )0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[0] = (unsigned short )0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[0] = (unsigned short )0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[1] = (unsigned char  *)loc_tipo;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[1] = (unsigned long )2;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[1] = (         int  )0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[1] = (         short *)0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[1] = (         int  )0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[1] = (unsigned long )0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[1] = (unsigned short )0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[1] = (unsigned short )0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[2] = (unsigned char  *)&loc_importe;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[2] = (unsigned long )sizeof(double);
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[2] = (         int  )0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[2] = (         short *)0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[2] = (         int  )0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[2] = (unsigned long )0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[2] = (unsigned short )0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[2] = (unsigned short )0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[3] = (unsigned char  *)sql_clave_inst;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[3] = (unsigned long )6;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[3] = (         int  )0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[3] = (         short *)0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[3] = (         int  )0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[3] = (unsigned long )0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[3] = (unsigned short )0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[3] = (unsigned short )0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[4] = (unsigned char  *)sql_clave_sis;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[4] = (unsigned long )6;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[4] = (         int  )0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[4] = (         short *)0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[4] = (         int  )0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[4] = (unsigned long )0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[4] = (unsigned short )0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[4] = (unsigned short )0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[5] = (unsigned char  *)sql_ref_med;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[5] = (unsigned long )18;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[5] = (         int  )0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[5] = (         short *)0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[5] = (         int  )0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[5] = (unsigned long )0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[5] = (unsigned short )0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[5] = (unsigned short )0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[6] = (unsigned char  *)sql_clave_tran;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[6] = (unsigned long )8;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[6] = (         int  )0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[6] = (         short *)0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[6] = (         int  )0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[6] = (unsigned long )0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[6] = (unsigned short )0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[6] = (unsigned short )0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[7] = (unsigned char  *)sql_tipo;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[7] = (unsigned long )2;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[7] = (         int  )0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[7] = (         short *)0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[7] = (         int  )0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[7] = (unsigned long )0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[7] = (unsigned short )0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[7] = (unsigned short )0;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqphsv = sqlstm.sqhstv;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqphsl = sqlstm.sqhstl;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqphss = sqlstm.sqhsts;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqpind = sqlstm.sqindv;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqpins = sqlstm.sqinds;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqparm = sqlstm.sqharm;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqparc = sqlstm.sqharc;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqpadto = sqlstm.sqadto;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqptdso = sqlstm.sqtdso;
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 1350 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 1360 "../../gral/src_db/CTransaccionalCdl.pc"

                   // AND  ROWNUM = 1; Propuesto por H.Martinez 19-May-2010

        Gfi_StrRTrim(loc_status);
        if (strlen(loc_status) > 0)
        { // 16-2
          if (NIVEL_TRACE > 9)
            cout << "EXISTE EL PROGRAMADO, STATUS=" << loc_status << endl;

          loc_remanente = 0; sw = 1;
          sprintf(loc_status1, "P%sC",loc_tipo);
          if (strcmp(loc_status,loc_status1))
          { // 16-3
            loc_simporte = 0;

            /* EXEC SQL SELECT NVL(SUM(NVL(IMPORTE,0)),0)
                     INTO   :loc_simporte
                     FROM   TRANS_PAGO
                     WHERE  CLAVE_INST  = :sql_clave_inst
                       AND  CLAVE_SIS   = :sql_clave_sis
                       AND  CLAVE_TRAN  = :sql_clave_tran
                       AND  FECHA_RECEP = TO_DATE(:sql_fecha_recep, 'YYYYMMDD')
                       AND  TIPO        = :sql_tipo
                       //AND  LPAD(FOLIO_ORIGEN, 9, '0') = LPAD(:sql_folio_origen, 9 ,'0') HGT 18-Jun-2010
                       AND  REF_MED  = :sql_ref_med
                       AND  STATUS      = 'R' || :sql_tipo || 'PC'; */ 
#line 1385 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            struct sqlexd sqlstm;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlvsn = 13;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.arrsiz = 26;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqladtp = &sqladt;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqltdsp = &sqltds;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.stmt = "select NVL(sum(NVL(IMPORTE,0)),0) into :b0  from \
TRANS_PAGO where ((((((CLAVE_INST=:b1 and CLAVE_SIS=:b2) and CLAVE_TRAN=:b3) a\
nd FECHA_RECEP=TO_DATE(:b4,'YYYYMMDD')) and TIPO=:b5) and REF_MED=:b6) and STA\
TUS=(('R'||:b5)||'PC'))";
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.iters = (unsigned int  )1;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.offset = (unsigned int  )1893;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.selerr = (unsigned short)1;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlpfmem = (unsigned int  )0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.cud = sqlcud0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlety = (unsigned short)4352;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.occurs = (unsigned int  )0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[0] = (unsigned char  *)&loc_simporte;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[0] = (unsigned long )sizeof(double);
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[0] = (         int  )0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[0] = (         short *)0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[0] = (         int  )0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[0] = (unsigned long )0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[0] = (unsigned short )0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[0] = (unsigned short )0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[1] = (unsigned char  *)sql_clave_inst;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[1] = (unsigned long )6;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[1] = (         int  )0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[1] = (         short *)0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[1] = (         int  )0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[1] = (unsigned long )0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[1] = (unsigned short )0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[1] = (unsigned short )0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[2] = (unsigned char  *)sql_clave_sis;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[2] = (unsigned long )6;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[2] = (         int  )0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[2] = (         short *)0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[2] = (         int  )0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[2] = (unsigned long )0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[2] = (unsigned short )0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[2] = (unsigned short )0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[3] = (unsigned char  *)sql_clave_tran;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[3] = (unsigned long )8;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[3] = (         int  )0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[3] = (         short *)0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[3] = (         int  )0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[3] = (unsigned long )0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[3] = (unsigned short )0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[3] = (unsigned short )0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[4] = (unsigned char  *)sql_fecha_recep;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[4] = (unsigned long )40;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[4] = (         int  )0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[4] = (         short *)0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[4] = (         int  )0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[4] = (unsigned long )0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[4] = (unsigned short )0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[4] = (unsigned short )0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[5] = (unsigned char  *)sql_tipo;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[5] = (unsigned long )2;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[5] = (         int  )0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[5] = (         short *)0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[5] = (         int  )0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[5] = (unsigned long )0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[5] = (unsigned short )0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[5] = (unsigned short )0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[6] = (unsigned char  *)sql_ref_med;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[6] = (unsigned long )18;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[6] = (         int  )0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[6] = (         short *)0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[6] = (         int  )0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[6] = (unsigned long )0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[6] = (unsigned short )0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[6] = (unsigned short )0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[7] = (unsigned char  *)sql_tipo;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[7] = (unsigned long )2;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[7] = (         int  )0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[7] = (         short *)0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[7] = (         int  )0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[7] = (unsigned long )0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[7] = (unsigned short )0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[7] = (unsigned short )0;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqphsv = sqlstm.sqhstv;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqphsl = sqlstm.sqhstl;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqphss = sqlstm.sqhsts;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqpind = sqlstm.sqindv;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqpins = sqlstm.sqinds;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqparm = sqlstm.sqharm;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqparc = sqlstm.sqharc;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqpadto = sqlstm.sqadto;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqptdso = sqlstm.sqtdso;
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 1375 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 1385 "../../gral/src_db/CTransaccionalCdl.pc"


            resultado = ChecaErrorOracle( 0, 1, &rowcount);
            if (resultado != GFI_ERRC_OK)
            {
              if (NIVEL_TRACE > 5)
                cout << "ERROR AL SUMAR IMPORTE PAGADO!" << endl;

              strcpy(err_desc,"R0013");
            }
            else
            {
              loc_remanente = loc_simporte + sql_importe;
              loc_remanente = fabs(loc_remanente);
              //loc_importe = fabs(sql_importe); 18-Jun-2010
              if (NIVEL_TRACE > 9)
                printf ("REMANENTE= %.2lf\n", loc_remanente );

              if ((loc_importe >= (loc_remanente - loc_limite)) && (loc_importe <= (loc_remanente + loc_limite)))
              {
                if (NIVEL_TRACE > 9)
                  cout << "SE PUEDE CONCILIAR EL PROGRAMADO!" << endl;

                strcpy(loc_status2,"C");
              }

              //resultado = Obtiene_Ref_Concil(); Solicitado H.Martinez 21-Jun-2010
            }
          } // 16-3
          else
          {
            if (NIVEL_TRACE > 5)
              cout << "EL PROGRAMADO ESTA CONCILIADO O ESTA DADO DE BAJA!" << endl;

            strcpy(err_desc,"R0014");
          }
        } // 16-2
        else
        {
          if (NIVEL_TRACE > 5)
            cout << "NO EXISTE PROGRAMADO!" << endl;

          strcpy(err_desc,"R0015");
        }

        if (strlen(err_desc) == 0)
        { // 16-3
          if (NIVEL_TRACE > 9)
            cout << "BUSCANDO EL REAL COMO SALA DE ESPERA" << endl;

          loc_status[0] = '\0';  //NULL
          loc_tipo[0] = '\0';  //NULL
          loc_ref_med[0] = '\0';  //NULL
          loc_fecha_baja[0] = '\0';  //NULL

          // Index: CLAVE_INST, CLAVE_SIS, CLAVE_RAST, CLAVE_TRAN, FECHA_RECEP, TIPO

          /* EXEC SQL SELECT NVL(STATUS,' '), NVL(TIPO,' '), NVL(REF_MED,' ') //, NVL(TO_CHAR(FECHA_BAJA, 'YYYYMMDD HH24:MI:SS'),' ')
                   INTO   :loc_status, :loc_tipo, :loc_ref_med //, :loc_fecha_baja
                   FROM   TRANS_PAGO
                   WHERE  CLAVE_INST  = :sql_clave_inst
                     AND  CLAVE_SIS   = :sql_clave_sis
                     AND  CLAVE_RAST  = :sql_clave_rast
                     AND  CLAVE_TRAN  = :sql_clave_tran
                     AND  FECHA_RECEP = TO_DATE(:sql_fecha_recep, 'YYYYMMDD')
                     AND  TIPO        = :sql_tipo
                     AND  FECHA_BAJA IS NULL; */ 
#line 1451 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          struct sqlexd sqlstm;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqlvsn = 13;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.arrsiz = 26;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqladtp = &sqladt;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqltdsp = &sqltds;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.stmt = "select NVL(STATUS,' ') ,NVL(TIPO,' ') ,NVL(REF_MED,\
' ') into :b0,:b1,:b2  from TRANS_PAGO where ((((((CLAVE_INST=:b3 and CLAVE_SI\
S=:b4) and CLAVE_RAST=:b5) and CLAVE_TRAN=:b6) and FECHA_RECEP=TO_DATE(:b7,'YY\
YYMMDD')) and TIPO=:b8) and FECHA_BAJA is null )";
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.iters = (unsigned int  )1;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.offset = (unsigned int  )1940;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.selerr = (unsigned short)1;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqlpfmem = (unsigned int  )0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.cud = sqlcud0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqlety = (unsigned short)4352;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.occurs = (unsigned int  )0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstv[0] = (unsigned char  *)loc_status;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstl[0] = (unsigned long )5;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhsts[0] = (         int  )0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqindv[0] = (         short *)0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqinds[0] = (         int  )0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqharm[0] = (unsigned long )0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqadto[0] = (unsigned short )0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqtdso[0] = (unsigned short )0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstv[1] = (unsigned char  *)loc_tipo;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstl[1] = (unsigned long )2;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhsts[1] = (         int  )0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqindv[1] = (         short *)0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqinds[1] = (         int  )0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqharm[1] = (unsigned long )0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqadto[1] = (unsigned short )0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqtdso[1] = (unsigned short )0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstv[2] = (unsigned char  *)loc_ref_med;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstl[2] = (unsigned long )13;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhsts[2] = (         int  )0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqindv[2] = (         short *)0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqinds[2] = (         int  )0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqharm[2] = (unsigned long )0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqadto[2] = (unsigned short )0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqtdso[2] = (unsigned short )0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstv[3] = (unsigned char  *)sql_clave_inst;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstl[3] = (unsigned long )6;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhsts[3] = (         int  )0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqindv[3] = (         short *)0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqinds[3] = (         int  )0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqharm[3] = (unsigned long )0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqadto[3] = (unsigned short )0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqtdso[3] = (unsigned short )0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstv[4] = (unsigned char  *)sql_clave_sis;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstl[4] = (unsigned long )6;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhsts[4] = (         int  )0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqindv[4] = (         short *)0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqinds[4] = (         int  )0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqharm[4] = (unsigned long )0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqadto[4] = (unsigned short )0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqtdso[4] = (unsigned short )0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstv[5] = (unsigned char  *)sql_clave_rast;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstl[5] = (unsigned long )31;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhsts[5] = (         int  )0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqindv[5] = (         short *)0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqinds[5] = (         int  )0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqharm[5] = (unsigned long )0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqadto[5] = (unsigned short )0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqtdso[5] = (unsigned short )0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstv[6] = (unsigned char  *)sql_clave_tran;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstl[6] = (unsigned long )8;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhsts[6] = (         int  )0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqindv[6] = (         short *)0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqinds[6] = (         int  )0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqharm[6] = (unsigned long )0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqadto[6] = (unsigned short )0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqtdso[6] = (unsigned short )0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstv[7] = (unsigned char  *)sql_fecha_recep;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstl[7] = (unsigned long )40;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhsts[7] = (         int  )0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqindv[7] = (         short *)0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqinds[7] = (         int  )0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqharm[7] = (unsigned long )0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqadto[7] = (unsigned short )0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqtdso[7] = (unsigned short )0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstv[8] = (unsigned char  *)sql_tipo;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstl[8] = (unsigned long )2;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhsts[8] = (         int  )0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqindv[8] = (         short *)0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqinds[8] = (         int  )0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqharm[8] = (unsigned long )0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqadto[8] = (unsigned short )0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqtdso[8] = (unsigned short )0;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqphsv = sqlstm.sqhstv;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqphsl = sqlstm.sqhstl;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqphss = sqlstm.sqhsts;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqpind = sqlstm.sqindv;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqpins = sqlstm.sqinds;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqparm = sqlstm.sqharm;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqparc = sqlstm.sqharc;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqpadto = sqlstm.sqadto;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqptdso = sqlstm.sqtdso;
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 1442 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 1451 "../../gral/src_db/CTransaccionalCdl.pc"

                     // AND  ROWNUM = 1; Propuesto por H.Martinez 19-May-2010

          Gfi_StrRTrim(loc_status);

          if (strlen(loc_status) > 0)
          { // 16-4a
            sprintf(loc_status1, "R%sPC",loc_tipo);
            if (strcmp(loc_status,loc_status1)) //&& (strlen(loc_fecha_baja) == 0))
            { // 16-5
              if (NIVEL_TRACE > 9)
                cout << "ACTUALIZANDO EL REAL..." << endl;

              /* EXEC SQL UPDATE TRANS_PAGO
                       SET    REF_CONCIL  = :sql_ref_med, //:ref_concilG, Solicitado H.Martinez 21-Jun-2010
                              STATUS      = 'R' || :loc_tipo || 'PC',
                              IMPORTE     = ROUND(:sql_importe,2),
                              FECHA_FLUJO = SYSDATE,
                              FOLIO_ORIGEN = :sql_folio_origen,
                              REF_MED     = :loc_ref_med
                       WHERE  CLAVE_INST  = :sql_clave_inst
                         AND  CLAVE_SIS   = :sql_clave_sis
                         AND  CLAVE_RAST  = :sql_clave_rast
                         AND  CLAVE_TRAN  = :sql_clave_tran
                         AND  FECHA_RECEP = TO_DATE(:sql_fecha_recep, 'YYYYMMDD')
                         AND  TIPO        = :sql_tipo
                         AND  FECHA_BAJA IS NULL; */ 
#line 1477 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              struct sqlexd sqlstm;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqlvsn = 13;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.arrsiz = 26;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqladtp = &sqladt;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqltdsp = &sqltds;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.stmt = "update TRANS_PAGO  set REF_CONCIL=:b0,STATUS=((\
'R'||:b1)||'PC'),IMPORTE=ROUND(:b2,2),FECHA_FLUJO=SYSDATE,FOLIO_ORIGEN=:b3,REF\
_MED=:b4 where ((((((CLAVE_INST=:b5 and CLAVE_SIS=:b6) and CLAVE_RAST=:b7) and\
 CLAVE_TRAN=:b8) and FECHA_RECEP=TO_DATE(:b9,'YYYYMMDD')) and TIPO=:b10) and F\
ECHA_BAJA is null )";
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.iters = (unsigned int  )1;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.offset = (unsigned int  )1991;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.cud = sqlcud0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqlety = (unsigned short)4352;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.occurs = (unsigned int  )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[0] = (unsigned char  *)sql_ref_med;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[0] = (unsigned long )18;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[0] = (         int  )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[0] = (         short *)0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[0] = (         int  )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[0] = (unsigned long )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[0] = (unsigned short )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[0] = (unsigned short )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[1] = (unsigned char  *)loc_tipo;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[1] = (unsigned long )2;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[1] = (         int  )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[1] = (         short *)0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[1] = (         int  )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[1] = (unsigned long )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[1] = (unsigned short )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[1] = (unsigned short )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[2] = (unsigned char  *)&sql_importe;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[2] = (unsigned long )sizeof(double);
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[2] = (         int  )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[2] = (         short *)0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[2] = (         int  )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[2] = (unsigned long )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[2] = (unsigned short )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[2] = (unsigned short )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[3] = (unsigned char  *)sql_folio_origen;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[3] = (unsigned long )10;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[3] = (         int  )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[3] = (         short *)0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[3] = (         int  )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[3] = (unsigned long )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[3] = (unsigned short )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[3] = (unsigned short )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[4] = (unsigned char  *)loc_ref_med;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[4] = (unsigned long )13;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[4] = (         int  )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[4] = (         short *)0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[4] = (         int  )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[4] = (unsigned long )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[4] = (unsigned short )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[4] = (unsigned short )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[5] = (unsigned char  *)sql_clave_inst;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[5] = (unsigned long )6;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[5] = (         int  )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[5] = (         short *)0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[5] = (         int  )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[5] = (unsigned long )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[5] = (unsigned short )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[5] = (unsigned short )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[6] = (unsigned char  *)sql_clave_sis;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[6] = (unsigned long )6;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[6] = (         int  )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[6] = (         short *)0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[6] = (         int  )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[6] = (unsigned long )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[6] = (unsigned short )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[6] = (unsigned short )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[7] = (unsigned char  *)sql_clave_rast;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[7] = (unsigned long )31;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[7] = (         int  )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[7] = (         short *)0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[7] = (         int  )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[7] = (unsigned long )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[7] = (unsigned short )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[7] = (unsigned short )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[8] = (unsigned char  *)sql_clave_tran;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[8] = (unsigned long )8;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[8] = (         int  )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[8] = (         short *)0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[8] = (         int  )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[8] = (unsigned long )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[8] = (unsigned short )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[8] = (unsigned short )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[9] = (unsigned char  *)sql_fecha_recep;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[9] = (unsigned long )40;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[9] = (         int  )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[9] = (         short *)0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[9] = (         int  )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[9] = (unsigned long )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[9] = (unsigned short )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[9] = (unsigned short )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[10] = (unsigned char  *)sql_tipo;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[10] = (unsigned long )2;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[10] = (         int  )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[10] = (         short *)0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[10] = (         int  )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[10] = (unsigned long )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[10] = (unsigned short )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[10] = (unsigned short )0;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqphsv = sqlstm.sqhstv;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqphsl = sqlstm.sqhstl;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqphss = sqlstm.sqhsts;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqpind = sqlstm.sqindv;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqpins = sqlstm.sqinds;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqparm = sqlstm.sqharm;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqparc = sqlstm.sqharc;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqpadto = sqlstm.sqadto;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqptdso = sqlstm.sqtdso;
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 1464 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 1477 "../../gral/src_db/CTransaccionalCdl.pc"

              resultado = ChecaErrorOracle( 0, 1, &rowcount);

              if (resultado == GFI_ERRC_OK)
                swConcOk = 1; // HGT 18-May-2010
              else
              {
                if (NIVEL_TRACE > 5)
                  cout << "ERROR AL ACTUALIZAR EN TRANS_PAGO!" << endl;

                strcpy(err_desc,"R0016");
              }

              if (sw == 1)
              {
                /* EXEC SQL UPDATE TRANS_PROGRAMADO
                         SET    REF_CONCIL  = :sql_ref_med, //:ref_concilG, Solicitado H.Martinez 21-Jun-2010,
                                STATUS      = 'P' || :sql_tipo || :loc_status2 ,
                                REMANENTE   = ROUND(:loc_remanente,2) ,
                                FECHA_FLUJO = SYSDATE
                         WHERE  CLAVE_INST  = :sql_clave_inst
                           AND  CLAVE_SIS   = :sql_clave_sis
                           AND  CLAVE_RAST  = :sql_ref_med
                           AND  CLAVE_TRAN  = :sql_clave_tran
                           AND  FECHA_RECEP = TRUNC(SYSDATE)
                           AND  TIPO        = :sql_tipo
                           //AND  LPAD(FOLIO_ORIGEN, 9, '0') = LPAD(:sql_folio_origen, 9 ,'0')  HGT 18-Jun-2010
                           AND  FECHA_BAJA IS NULL; */ 
#line 1504 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                struct sqlexd sqlstm;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqlvsn = 13;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.arrsiz = 26;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqladtp = &sqladt;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqltdsp = &sqltds;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.stmt = "update TRANS_PROGRAMADO  set REF_CONCIL=:b0,S\
TATUS=(('P'||:b1)||:b2),REMANENTE=ROUND(:b3,2),FECHA_FLUJO=SYSDATE where (((((\
(CLAVE_INST=:b4 and CLAVE_SIS=:b5) and CLAVE_RAST=:b0) and CLAVE_TRAN=:b7) and\
 FECHA_RECEP=TRUNC(SYSDATE)) and TIPO=:b1) and FECHA_BAJA is null )";
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.iters = (unsigned int  )1;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.offset = (unsigned int  )2050;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.cud = sqlcud0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqlety = (unsigned short)4352;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.occurs = (unsigned int  )0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[0] = (unsigned char  *)sql_ref_med;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[0] = (unsigned long )18;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[0] = (         int  )0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[0] = (         short *)0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[0] = (         int  )0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[0] = (unsigned long )0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[0] = (unsigned short )0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[0] = (unsigned short )0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[1] = (unsigned char  *)sql_tipo;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[1] = (unsigned long )2;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[1] = (         int  )0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[1] = (         short *)0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[1] = (         int  )0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[1] = (unsigned long )0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[1] = (unsigned short )0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[1] = (unsigned short )0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[2] = (unsigned char  *)loc_status2;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[2] = (unsigned long )9;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[2] = (         int  )0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[2] = (         short *)0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[2] = (         int  )0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[2] = (unsigned long )0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[2] = (unsigned short )0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[2] = (unsigned short )0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[3] = (unsigned char  *)&loc_remanente;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[3] = (unsigned long )sizeof(double);
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[3] = (         int  )0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[3] = (         short *)0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[3] = (         int  )0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[3] = (unsigned long )0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[3] = (unsigned short )0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[3] = (unsigned short )0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[4] = (unsigned char  *)sql_clave_inst;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[4] = (unsigned long )6;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[4] = (         int  )0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[4] = (         short *)0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[4] = (         int  )0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[4] = (unsigned long )0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[4] = (unsigned short )0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[4] = (unsigned short )0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[5] = (unsigned char  *)sql_clave_sis;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[5] = (unsigned long )6;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[5] = (         int  )0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[5] = (         short *)0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[5] = (         int  )0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[5] = (unsigned long )0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[5] = (unsigned short )0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[5] = (unsigned short )0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[6] = (unsigned char  *)sql_ref_med;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[6] = (unsigned long )18;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[6] = (         int  )0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[6] = (         short *)0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[6] = (         int  )0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[6] = (unsigned long )0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[6] = (unsigned short )0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[6] = (unsigned short )0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[7] = (unsigned char  *)sql_clave_tran;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[7] = (unsigned long )8;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[7] = (         int  )0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[7] = (         short *)0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[7] = (         int  )0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[7] = (unsigned long )0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[7] = (unsigned short )0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[7] = (unsigned short )0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[8] = (unsigned char  *)sql_tipo;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[8] = (unsigned long )2;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[8] = (         int  )0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[8] = (         short *)0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[8] = (         int  )0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[8] = (unsigned long )0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[8] = (unsigned short )0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[8] = (unsigned short )0;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqphsv = sqlstm.sqhstv;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqphsl = sqlstm.sqhstl;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqphss = sqlstm.sqhsts;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqpind = sqlstm.sqindv;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqpins = sqlstm.sqinds;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqparm = sqlstm.sqharm;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqparc = sqlstm.sqharc;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqpadto = sqlstm.sqadto;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqptdso = sqlstm.sqtdso;
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 1492 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 1504 "../../gral/src_db/CTransaccionalCdl.pc"


                resultado = ChecaErrorOracle( 0, 1, &rowcount);
                if (resultado != GFI_ERRC_OK)
                {
                  if (NIVEL_TRACE > 5)
                    cout << "ERROR AL ACTUALIZAR EN TRANS_PROGRAMADO!" << endl;

                  strcpy(err_desc,"R0017");
                }
              }
            } // 16-5
          } // 16-4a
          else
          { // 16-4b
            if (NIVEL_TRACE > 9)
              cout << "NO EXISTE EL REAL, INSERTANDOLO..." << endl;

            /* EXEC SQL INSERT INTO TRANS_PAGO ( CLAVE_INST , CLAVE_SIS , CLAVE_RAST , CLAVE_TRAN ,
                                              FECHA_RECEP , TIPO , FOLIO_ORIGEN , TIPO_OPER ,
                                              BANCO_ORD , CVE_BANCO_ORD , NOMBRE_ORD , CUENTA_ORD ,
                                              BANCO_BEN , CVE_BANCO_BEN , NOMBRE_BEN , CUENTA_BEN ,
                                              IMPORTE , STATUS , OBSERVACION , REF_CONCIL ,
                                              REF_MED , FECHA_CAP , FECHA_VENC , FECHA_FLUJO ,
                                              FECHA_REG , FECHA_BAJA )
                                 VALUES( :sql_clave_inst , :sql_clave_sis , :sql_clave_rast , :sql_clave_tran ,
                                         TO_DATE(:sql_fecha_recep, 'YYYYMMDD') , :sql_tipo , :sql_folio_origen ,
                                         :sql_tipo_oper , :sql_banco_ord , :sql_cve_banco_ord , :sql_nombre_ord ,
                                         :sql_cuenta_ord , :sql_banco_ben , :sql_cve_banco_ben , :sql_nombre_ben ,
                                         :sql_cuenta_ben , ROUND(:sql_importe,2) , 'R' || :sql_tipo || 'PC' ,
                                         :sql_observacion , :sql_ref_med, //:ref_concilG, Solicitado H.Martinez 21-Jun-2010,
                                         :sql_ref_med , TO_DATE(:sql_fecha_cap, 'YYYYMMDD HH24:MI:SS') ,
                                         TO_DATE(:sql_fecha_venc, 'YYYYMMDD HH24:MI:SS') ,
                                         SYSDATE , TO_DATE(:sql_fecha_reg, 'YYYYMMDD HH24:MI:SS') ,
                                         TO_DATE(:sql_fecha_baja, 'YYYYMMDD HH24:MI:SS') ); */ 
#line 1538 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            struct sqlexd sqlstm;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlvsn = 13;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.arrsiz = 26;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqladtp = &sqladt;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqltdsp = &sqltds;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.stmt = "insert into TRANS_PAGO (CLAVE_INST,CLAVE_SIS,CLAV\
E_RAST,CLAVE_TRAN,FECHA_RECEP,TIPO,FOLIO_ORIGEN,TIPO_OPER,BANCO_ORD,CVE_BANCO_\
ORD,NOMBRE_ORD,CUENTA_ORD,BANCO_BEN,CVE_BANCO_BEN,NOMBRE_BEN,CUENTA_BEN,IMPORT\
E,STATUS,OBSERVACION,REF_CONCIL,REF_MED,FECHA_CAP,FECHA_VENC,FECHA_FLUJO,FECHA\
_REG,FECHA_BAJA) values (:b0,:b1,:b2,:b3,TO_DATE(:b4,'YYYYMMDD'),:b5,:b6,:b7,:\
b8,:b9,:b10,:b11,:b12,:b13,:b14,:b15,ROUND(:b16,2),(('R'||:b5)||'PC'),:b18,:b1\
9,:b19,TO_DATE(:b21,'YYYYMMDD HH24:MI:SS'),TO_DATE(:b22,'YYYYMMDD HH24:MI:SS')\
,SYSDATE,TO_DATE(:b23,'YYYYMMDD HH24:MI:SS'),TO_DATE(:b24,'YYYYMMDD HH24:MI:SS\
'))";
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.iters = (unsigned int  )1;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.offset = (unsigned int  )2101;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.cud = sqlcud0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlety = (unsigned short)4352;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.occurs = (unsigned int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[0] = (unsigned char  *)sql_clave_inst;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[0] = (unsigned long )6;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[0] = (         int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[0] = (         short *)0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[0] = (         int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[0] = (unsigned long )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[0] = (unsigned short )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[0] = (unsigned short )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[1] = (unsigned char  *)sql_clave_sis;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[1] = (unsigned long )6;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[1] = (         int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[1] = (         short *)0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[1] = (         int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[1] = (unsigned long )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[1] = (unsigned short )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[1] = (unsigned short )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[2] = (unsigned char  *)sql_clave_rast;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[2] = (unsigned long )31;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[2] = (         int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[2] = (         short *)0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[2] = (         int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[2] = (unsigned long )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[2] = (unsigned short )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[2] = (unsigned short )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[3] = (unsigned char  *)sql_clave_tran;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[3] = (unsigned long )8;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[3] = (         int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[3] = (         short *)0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[3] = (         int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[3] = (unsigned long )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[3] = (unsigned short )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[3] = (unsigned short )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[4] = (unsigned char  *)sql_fecha_recep;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[4] = (unsigned long )40;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[4] = (         int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[4] = (         short *)0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[4] = (         int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[4] = (unsigned long )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[4] = (unsigned short )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[4] = (unsigned short )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[5] = (unsigned char  *)sql_tipo;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[5] = (unsigned long )2;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[5] = (         int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[5] = (         short *)0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[5] = (         int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[5] = (unsigned long )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[5] = (unsigned short )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[5] = (unsigned short )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[6] = (unsigned char  *)sql_folio_origen;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[6] = (unsigned long )10;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[6] = (         int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[6] = (         short *)0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[6] = (         int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[6] = (unsigned long )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[6] = (unsigned short )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[6] = (unsigned short )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[7] = (unsigned char  *)sql_tipo_oper;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[7] = (unsigned long )5;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[7] = (         int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[7] = (         short *)0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[7] = (         int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[7] = (unsigned long )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[7] = (unsigned short )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[7] = (unsigned short )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[8] = (unsigned char  *)sql_banco_ord;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[8] = (unsigned long )7;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[8] = (         int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[8] = (         short *)0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[8] = (         int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[8] = (unsigned long )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[8] = (unsigned short )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[8] = (unsigned short )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[9] = (unsigned char  *)sql_cve_banco_ord;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[9] = (unsigned long )16;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[9] = (         int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[9] = (         short *)0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[9] = (         int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[9] = (unsigned long )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[9] = (unsigned short )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[9] = (unsigned short )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[10] = (unsigned char  *)sql_nombre_ord;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[10] = (unsigned long )52;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[10] = (         int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[10] = (         short *)0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[10] = (         int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[10] = (unsigned long )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[10] = (unsigned short )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[10] = (unsigned short )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[11] = (unsigned char  *)sql_cuenta_ord;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[11] = (unsigned long )21;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[11] = (         int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[11] = (         short *)0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[11] = (         int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[11] = (unsigned long )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[11] = (unsigned short )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[11] = (unsigned short )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[12] = (unsigned char  *)sql_banco_ben;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[12] = (unsigned long )7;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[12] = (         int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[12] = (         short *)0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[12] = (         int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[12] = (unsigned long )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[12] = (unsigned short )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[12] = (unsigned short )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[13] = (unsigned char  *)sql_cve_banco_ben;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[13] = (unsigned long )16;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[13] = (         int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[13] = (         short *)0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[13] = (         int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[13] = (unsigned long )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[13] = (unsigned short )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[13] = (unsigned short )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[14] = (unsigned char  *)sql_nombre_ben;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[14] = (unsigned long )52;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[14] = (         int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[14] = (         short *)0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[14] = (         int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[14] = (unsigned long )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[14] = (unsigned short )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[14] = (unsigned short )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[15] = (unsigned char  *)sql_cuenta_ben;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[15] = (unsigned long )21;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[15] = (         int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[15] = (         short *)0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[15] = (         int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[15] = (unsigned long )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[15] = (unsigned short )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[15] = (unsigned short )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[16] = (unsigned char  *)&sql_importe;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[16] = (unsigned long )sizeof(double);
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[16] = (         int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[16] = (         short *)0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[16] = (         int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[16] = (unsigned long )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[16] = (unsigned short )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[16] = (unsigned short )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[17] = (unsigned char  *)sql_tipo;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[17] = (unsigned long )2;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[17] = (         int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[17] = (         short *)0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[17] = (         int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[17] = (unsigned long )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[17] = (unsigned short )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[17] = (unsigned short )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[18] = (unsigned char  *)sql_observacion;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[18] = (unsigned long )50;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[18] = (         int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[18] = (         short *)0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[18] = (         int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[18] = (unsigned long )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[18] = (unsigned short )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[18] = (unsigned short )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[19] = (unsigned char  *)sql_ref_med;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[19] = (unsigned long )18;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[19] = (         int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[19] = (         short *)0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[19] = (         int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[19] = (unsigned long )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[19] = (unsigned short )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[19] = (unsigned short )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[20] = (unsigned char  *)sql_ref_med;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[20] = (unsigned long )18;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[20] = (         int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[20] = (         short *)0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[20] = (         int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[20] = (unsigned long )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[20] = (unsigned short )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[20] = (unsigned short )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[21] = (unsigned char  *)sql_fecha_cap;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[21] = (unsigned long )40;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[21] = (         int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[21] = (         short *)0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[21] = (         int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[21] = (unsigned long )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[21] = (unsigned short )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[21] = (unsigned short )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[22] = (unsigned char  *)sql_fecha_venc;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[22] = (unsigned long )18;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[22] = (         int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[22] = (         short *)0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[22] = (         int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[22] = (unsigned long )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[22] = (unsigned short )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[22] = (unsigned short )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[23] = (unsigned char  *)sql_fecha_reg;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[23] = (unsigned long )18;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[23] = (         int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[23] = (         short *)0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[23] = (         int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[23] = (unsigned long )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[23] = (unsigned short )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[23] = (unsigned short )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[24] = (unsigned char  *)sql_fecha_baja;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[24] = (unsigned long )18;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[24] = (         int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[24] = (         short *)0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[24] = (         int  )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[24] = (unsigned long )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[24] = (unsigned short )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[24] = (unsigned short )0;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqphsv = sqlstm.sqhstv;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqphsl = sqlstm.sqhstl;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqphss = sqlstm.sqhsts;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqpind = sqlstm.sqindv;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqpins = sqlstm.sqinds;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqparm = sqlstm.sqharm;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqparc = sqlstm.sqharc;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqpadto = sqlstm.sqadto;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqptdso = sqlstm.sqtdso;
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 1522 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 1538 "../../gral/src_db/CTransaccionalCdl.pc"


            resultado = ChecaErrorOracle (0, 1, &rowcount);

            if (resultado == GFI_ERRC_OK)
              swConcOk = 1; // HGT 18-May-2010
            else
            {
              if (NIVEL_TRACE > 5)
                cout << "ERROR AL INSERTAR EN TRANS_PAGO!" << endl;

              strcpy(err_desc,"R0018");
            }

            if (sw == 1)
            {
              /* EXEC SQL UPDATE TRANS_PROGRAMADO
                       SET    REF_CONCIL  = :sql_ref_med, //:ref_concilG, Solicitado H.Martinez 21-Jun-2010,
                              STATUS      = 'P' || :sql_tipo || :loc_status2 ,
                              REMANENTE   = ROUND(:loc_remanente,2) ,
                              FECHA_FLUJO = SYSDATE
                       WHERE  CLAVE_INST  = :sql_clave_inst
                         AND  CLAVE_SIS   = :sql_clave_sis
                         AND  CLAVE_RAST  = :sql_ref_med
                         AND  CLAVE_TRAN  = :sql_clave_tran
                         AND  FECHA_RECEP = TRUNC(SYSDATE)
                         AND  TIPO        = :sql_tipo
                         // AND  LPAD(FOLIO_ORIGEN, 9, '0') = LPAD(:sql_folio_origen, 9 ,'0')  HGT 18-Jun-2010
                         AND  FECHA_BAJA IS NULL; */ 
#line 1566 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              struct sqlexd sqlstm;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqlvsn = 13;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.arrsiz = 26;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqladtp = &sqladt;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqltdsp = &sqltds;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.stmt = "update TRANS_PROGRAMADO  set REF_CONCIL=:b0,STA\
TUS=(('P'||:b1)||:b2),REMANENTE=ROUND(:b3,2),FECHA_FLUJO=SYSDATE where ((((((C\
LAVE_INST=:b4 and CLAVE_SIS=:b5) and CLAVE_RAST=:b0) and CLAVE_TRAN=:b7) and F\
ECHA_RECEP=TRUNC(SYSDATE)) and TIPO=:b1) and FECHA_BAJA is null )";
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.iters = (unsigned int  )1;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.offset = (unsigned int  )2216;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.cud = sqlcud0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqlety = (unsigned short)4352;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.occurs = (unsigned int  )0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[0] = (unsigned char  *)sql_ref_med;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[0] = (unsigned long )18;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[0] = (         int  )0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[0] = (         short *)0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[0] = (         int  )0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[0] = (unsigned long )0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[0] = (unsigned short )0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[0] = (unsigned short )0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[1] = (unsigned char  *)sql_tipo;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[1] = (unsigned long )2;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[1] = (         int  )0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[1] = (         short *)0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[1] = (         int  )0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[1] = (unsigned long )0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[1] = (unsigned short )0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[1] = (unsigned short )0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[2] = (unsigned char  *)loc_status2;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[2] = (unsigned long )9;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[2] = (         int  )0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[2] = (         short *)0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[2] = (         int  )0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[2] = (unsigned long )0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[2] = (unsigned short )0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[2] = (unsigned short )0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[3] = (unsigned char  *)&loc_remanente;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[3] = (unsigned long )sizeof(double);
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[3] = (         int  )0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[3] = (         short *)0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[3] = (         int  )0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[3] = (unsigned long )0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[3] = (unsigned short )0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[3] = (unsigned short )0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[4] = (unsigned char  *)sql_clave_inst;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[4] = (unsigned long )6;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[4] = (         int  )0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[4] = (         short *)0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[4] = (         int  )0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[4] = (unsigned long )0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[4] = (unsigned short )0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[4] = (unsigned short )0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[5] = (unsigned char  *)sql_clave_sis;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[5] = (unsigned long )6;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[5] = (         int  )0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[5] = (         short *)0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[5] = (         int  )0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[5] = (unsigned long )0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[5] = (unsigned short )0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[5] = (unsigned short )0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[6] = (unsigned char  *)sql_ref_med;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[6] = (unsigned long )18;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[6] = (         int  )0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[6] = (         short *)0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[6] = (         int  )0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[6] = (unsigned long )0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[6] = (unsigned short )0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[6] = (unsigned short )0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[7] = (unsigned char  *)sql_clave_tran;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[7] = (unsigned long )8;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[7] = (         int  )0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[7] = (         short *)0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[7] = (         int  )0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[7] = (unsigned long )0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[7] = (unsigned short )0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[7] = (unsigned short )0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[8] = (unsigned char  *)sql_tipo;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[8] = (unsigned long )2;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[8] = (         int  )0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[8] = (         short *)0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[8] = (         int  )0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[8] = (unsigned long )0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[8] = (unsigned short )0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[8] = (unsigned short )0;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqphsv = sqlstm.sqhstv;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqphsl = sqlstm.sqhstl;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqphss = sqlstm.sqhsts;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqpind = sqlstm.sqindv;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqpins = sqlstm.sqinds;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqparm = sqlstm.sqharm;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqparc = sqlstm.sqharc;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqpadto = sqlstm.sqadto;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqptdso = sqlstm.sqtdso;
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 1554 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 1566 "../../gral/src_db/CTransaccionalCdl.pc"


              resultado = ChecaErrorOracle( 0, 1, &rowcount);
              if (resultado != GFI_ERRC_OK)
              {
                if (NIVEL_TRACE > 5)
                  cout << "ERROR AL INSERTAR EN TRANS_PROGRAMADO!" << endl;

                strcpy(err_desc,"R0019");
              }
            }
          } // // 16-4b
        } // 16-3

        if (NIVEL_TRACE > 9)
          cout << "TERMINO CONCILIACION POR PARCIALIDADES" << endl;

        if (swConcOk == 1) // HGT 18-May-2010
        { // 16-6
          if (!strcmp(loc_idcd, "1"))
          {
            if (NIVEL_TRACE > 9)
              cout << "ACTUALIZA LA TABLA IDCD EN OPICS" << endl;

            strcpy(sql_insth,sql_clave_inst); strcpy(sql_sish,sql_clave_sis); strcpy(sql_nmph,"BRANCH");
            loc_br[0]='\0';  //NULL
            /* EXEC SQL OPEN OBTENPARAM; */ 
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"
            struct sqlexd sqlstm;
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlvsn = 13;
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.arrsiz = 26;
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqladtp = &sqladt;
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqltdsp = &sqltds;
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.stmt = sq0002;
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.iters = (unsigned int  )1;
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.offset = (unsigned int  )2267;
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.selerr = (unsigned short)1;
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlpfmem = (unsigned int  )0;
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.cud = sqlcud0;
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlety = (unsigned short)4352;
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.occurs = (unsigned int  )0;
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqcmod = (unsigned int )0;
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[0] = (unsigned char  *)sql_insth;
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[0] = (unsigned long )20;
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[0] = (         int  )0;
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[0] = (         short *)0;
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[0] = (         int  )0;
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[0] = (unsigned long )0;
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[0] = (unsigned short )0;
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[0] = (unsigned short )0;
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[1] = (unsigned char  *)sql_sish;
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[1] = (unsigned long )20;
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[1] = (         int  )0;
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[1] = (         short *)0;
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[1] = (         int  )0;
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[1] = (unsigned long )0;
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[1] = (unsigned short )0;
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[1] = (unsigned short )0;
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[2] = (unsigned char  *)sql_nmph;
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[2] = (unsigned long )20;
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[2] = (         int  )0;
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[2] = (         short *)0;
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[2] = (         int  )0;
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[2] = (unsigned long )0;
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[2] = (unsigned short )0;
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[2] = (unsigned short )0;
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqphsv = sqlstm.sqhstv;
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqphsl = sqlstm.sqhstl;
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqphss = sqlstm.sqhsts;
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqpind = sqlstm.sqindv;
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqpins = sqlstm.sqinds;
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqparm = sqlstm.sqharm;
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqparc = sqlstm.sqharc;
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqpadto = sqlstm.sqadto;
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqptdso = sqlstm.sqtdso;
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 1592 "../../gral/src_db/CTransaccionalCdl.pc"

            while(1)
            {
                /* EXEC SQL FETCH OBTENPARAM INTO :sql_valor; */ 
#line 1595 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 1595 "../../gral/src_db/CTransaccionalCdl.pc"
                struct sqlexd sqlstm;
#line 1595 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqlvsn = 13;
#line 1595 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.arrsiz = 26;
#line 1595 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqladtp = &sqladt;
#line 1595 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqltdsp = &sqltds;
#line 1595 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.iters = (unsigned int  )1;
#line 1595 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.offset = (unsigned int  )2294;
#line 1595 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.selerr = (unsigned short)1;
#line 1595 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqlpfmem = (unsigned int  )0;
#line 1595 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.cud = sqlcud0;
#line 1595 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 1595 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqlety = (unsigned short)4352;
#line 1595 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.occurs = (unsigned int  )0;
#line 1595 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqfoff = (         int )0;
#line 1595 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqfmod = (unsigned int )2;
#line 1595 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[0] = (unsigned char  *)sql_valor;
#line 1595 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[0] = (unsigned long )100;
#line 1595 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[0] = (         int  )0;
#line 1595 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[0] = (         short *)0;
#line 1595 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[0] = (         int  )0;
#line 1595 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[0] = (unsigned long )0;
#line 1595 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[0] = (unsigned short )0;
#line 1595 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[0] = (unsigned short )0;
#line 1595 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqphsv = sqlstm.sqhstv;
#line 1595 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqphsl = sqlstm.sqhstl;
#line 1595 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqphss = sqlstm.sqhsts;
#line 1595 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqpind = sqlstm.sqindv;
#line 1595 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqpins = sqlstm.sqinds;
#line 1595 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqparm = sqlstm.sqharm;
#line 1595 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqparc = sqlstm.sqharc;
#line 1595 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqpadto = sqlstm.sqadto;
#line 1595 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqptdso = sqlstm.sqtdso;
#line 1595 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 1595 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 1595 "../../gral/src_db/CTransaccionalCdl.pc"

                resultado = ChecaErrorOracle (0, 1, &rowcount);
                if (resultado != GFI_ERRC_OK)
                  break;
                Gfi_StrRTrim(sql_valor);
                if (rowcount == 1)
                  strcpy(loc_br,sql_valor);
                else
                  break;
            }
            /* EXEC SQL CLOSE OBTENPARAM; */ 
#line 1605 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 1605 "../../gral/src_db/CTransaccionalCdl.pc"
            struct sqlexd sqlstm;
#line 1605 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlvsn = 13;
#line 1605 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.arrsiz = 26;
#line 1605 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqladtp = &sqladt;
#line 1605 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqltdsp = &sqltds;
#line 1605 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.iters = (unsigned int  )1;
#line 1605 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.offset = (unsigned int  )2313;
#line 1605 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.cud = sqlcud0;
#line 1605 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 1605 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlety = (unsigned short)4352;
#line 1605 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.occurs = (unsigned int  )0;
#line 1605 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 1605 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 1605 "../../gral/src_db/CTransaccionalCdl.pc"


            if (strlen(loc_br) > 0)
            {
              /* EXEC SQL UPDATE IDCD@LGTOPICS
                       SET    STATE  = 'LI'
                       WHERE  BR = :loc_br
                         AND  TO_CHAR( DEALDATE, 'YYYYMMDD' ) = :sql_fecha_recep
                         AND  CONFIRMATION = TO_NUMBER(SUBSTR(:sql_clave_rast, 1, 9)); */ 
#line 1613 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 1609 "../../gral/src_db/CTransaccionalCdl.pc"
              struct sqlexd sqlstm;
#line 1609 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqlvsn = 13;
#line 1609 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.arrsiz = 26;
#line 1609 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqladtp = &sqladt;
#line 1609 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqltdsp = &sqltds;
#line 1609 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.stmt = "update IDCD@LGTOPICS  set STATE='LI' where ((BR\
=:b0 and TO_CHAR(DEALDATE,'YYYYMMDD')=:b1) and CONFIRMATION=TO_NUMBER(SUBSTR(:\
b2,1,9)))";
#line 1609 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.iters = (unsigned int  )1;
#line 1609 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.offset = (unsigned int  )2328;
#line 1609 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.cud = sqlcud0;
#line 1609 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 1609 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqlety = (unsigned short)4352;
#line 1609 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.occurs = (unsigned int  )0;
#line 1609 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[0] = (unsigned char  *)loc_br;
#line 1609 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[0] = (unsigned long )10;
#line 1609 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[0] = (         int  )0;
#line 1609 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[0] = (         short *)0;
#line 1609 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[0] = (         int  )0;
#line 1609 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[0] = (unsigned long )0;
#line 1609 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[0] = (unsigned short )0;
#line 1609 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[0] = (unsigned short )0;
#line 1609 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[1] = (unsigned char  *)sql_fecha_recep;
#line 1609 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[1] = (unsigned long )40;
#line 1609 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[1] = (         int  )0;
#line 1609 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[1] = (         short *)0;
#line 1609 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[1] = (         int  )0;
#line 1609 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[1] = (unsigned long )0;
#line 1609 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[1] = (unsigned short )0;
#line 1609 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[1] = (unsigned short )0;
#line 1609 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[2] = (unsigned char  *)sql_clave_rast;
#line 1609 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[2] = (unsigned long )31;
#line 1609 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[2] = (         int  )0;
#line 1609 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[2] = (         short *)0;
#line 1609 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[2] = (         int  )0;
#line 1609 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[2] = (unsigned long )0;
#line 1609 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[2] = (unsigned short )0;
#line 1609 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[2] = (unsigned short )0;
#line 1609 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqphsv = sqlstm.sqhstv;
#line 1609 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqphsl = sqlstm.sqhstl;
#line 1609 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqphss = sqlstm.sqhsts;
#line 1609 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqpind = sqlstm.sqindv;
#line 1609 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqpins = sqlstm.sqinds;
#line 1609 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqparm = sqlstm.sqharm;
#line 1609 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqparc = sqlstm.sqharc;
#line 1609 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqpadto = sqlstm.sqadto;
#line 1609 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqptdso = sqlstm.sqtdso;
#line 1609 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 1609 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 1613 "../../gral/src_db/CTransaccionalCdl.pc"


              if (resultado != GFI_ERRC_OK)
              {
                if (NIVEL_TRACE > 5)
                  cout << "ERROR AL ACTUALIZAR LA TABLA IDCD EN OPICS!" << endl;

                strcpy(err_desc,"R0020");
              }
            }
          }
        } // 16-6
      } // 16-1
    } // 16

    if ((loc_tpconc == 32) && (swConcOk == 0)) // HGT 18-May-2010
    { // 32
      //OPICS CONCILIACION NORMAL
      if (strlen(sql_folio_origen)>0)
      { // 32-1
        if (NIVEL_TRACE > 5)
          cout << "CONCILIACION OPICS" << endl;

        strcpy(sql_insth,sql_clave_inst); strcpy(sql_sish,sql_clave_sis); strcpy(sql_nmph,"LIMITE");
        loc_limite=100;
        /* EXEC SQL OPEN OBTENPARAM; */ 
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"
        struct sqlexd sqlstm;
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqlvsn = 13;
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.arrsiz = 26;
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqladtp = &sqladt;
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqltdsp = &sqltds;
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.stmt = sq0002;
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.iters = (unsigned int  )1;
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.offset = (unsigned int  )2355;
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.selerr = (unsigned short)1;
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqlpfmem = (unsigned int  )0;
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.cud = sqlcud0;
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqlety = (unsigned short)4352;
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.occurs = (unsigned int  )0;
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqcmod = (unsigned int )0;
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[0] = (unsigned char  *)sql_insth;
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[0] = (unsigned long )20;
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[0] = (         int  )0;
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[0] = (         short *)0;
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[0] = (         int  )0;
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[0] = (unsigned long )0;
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[0] = (unsigned short )0;
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[0] = (unsigned short )0;
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[1] = (unsigned char  *)sql_sish;
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[1] = (unsigned long )20;
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[1] = (         int  )0;
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[1] = (         short *)0;
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[1] = (         int  )0;
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[1] = (unsigned long )0;
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[1] = (unsigned short )0;
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[1] = (unsigned short )0;
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[2] = (unsigned char  *)sql_nmph;
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[2] = (unsigned long )20;
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[2] = (         int  )0;
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[2] = (         short *)0;
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[2] = (         int  )0;
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[2] = (unsigned long )0;
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[2] = (unsigned short )0;
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[2] = (unsigned short )0;
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqphsv = sqlstm.sqhstv;
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqphsl = sqlstm.sqhstl;
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqphss = sqlstm.sqhsts;
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqpind = sqlstm.sqindv;
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqpins = sqlstm.sqinds;
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqparm = sqlstm.sqharm;
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqparc = sqlstm.sqharc;
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqpadto = sqlstm.sqadto;
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqptdso = sqlstm.sqtdso;
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 1638 "../../gral/src_db/CTransaccionalCdl.pc"

        while(1)
        {
            /* EXEC SQL FETCH OBTENPARAM INTO :sql_valor; */ 
#line 1641 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 1641 "../../gral/src_db/CTransaccionalCdl.pc"
            struct sqlexd sqlstm;
#line 1641 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlvsn = 13;
#line 1641 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.arrsiz = 26;
#line 1641 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqladtp = &sqladt;
#line 1641 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqltdsp = &sqltds;
#line 1641 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.iters = (unsigned int  )1;
#line 1641 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.offset = (unsigned int  )2382;
#line 1641 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.selerr = (unsigned short)1;
#line 1641 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlpfmem = (unsigned int  )0;
#line 1641 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.cud = sqlcud0;
#line 1641 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 1641 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlety = (unsigned short)4352;
#line 1641 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.occurs = (unsigned int  )0;
#line 1641 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqfoff = (         int )0;
#line 1641 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqfmod = (unsigned int )2;
#line 1641 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[0] = (unsigned char  *)sql_valor;
#line 1641 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[0] = (unsigned long )100;
#line 1641 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[0] = (         int  )0;
#line 1641 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[0] = (         short *)0;
#line 1641 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[0] = (         int  )0;
#line 1641 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[0] = (unsigned long )0;
#line 1641 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[0] = (unsigned short )0;
#line 1641 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[0] = (unsigned short )0;
#line 1641 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqphsv = sqlstm.sqhstv;
#line 1641 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqphsl = sqlstm.sqhstl;
#line 1641 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqphss = sqlstm.sqhsts;
#line 1641 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqpind = sqlstm.sqindv;
#line 1641 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqpins = sqlstm.sqinds;
#line 1641 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqparm = sqlstm.sqharm;
#line 1641 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqparc = sqlstm.sqharc;
#line 1641 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqpadto = sqlstm.sqadto;
#line 1641 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqptdso = sqlstm.sqtdso;
#line 1641 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 1641 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 1641 "../../gral/src_db/CTransaccionalCdl.pc"

            resultado = ChecaErrorOracle (0, 1, &rowcount);
            if (resultado != GFI_ERRC_OK)
              break;
            Gfi_StrRTrim(sql_valor);
            if (rowcount == 1)
              loc_limite = atof(sql_valor);
            else
              break;
        }
        /* EXEC SQL CLOSE OBTENPARAM; */ 
#line 1651 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 1651 "../../gral/src_db/CTransaccionalCdl.pc"
        struct sqlexd sqlstm;
#line 1651 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqlvsn = 13;
#line 1651 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.arrsiz = 26;
#line 1651 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqladtp = &sqladt;
#line 1651 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqltdsp = &sqltds;
#line 1651 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.iters = (unsigned int  )1;
#line 1651 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.offset = (unsigned int  )2401;
#line 1651 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.cud = sqlcud0;
#line 1651 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 1651 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqlety = (unsigned short)4352;
#line 1651 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.occurs = (unsigned int  )0;
#line 1651 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 1651 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 1651 "../../gral/src_db/CTransaccionalCdl.pc"


        if (NIVEL_TRACE > 9)
          cout << "BUSCA EL PROGRAMADO" << endl;

        loc_status[0] = '\0';  //NULL
        loc_tipo[0] = '\0';  //NULL
        loc_fecha_baja[0] = '\0';  //NULL

        /* EXEC SQL SELECT NVL(STATUS,' '), NVL(TIPO,' '), NVL(TO_CHAR(FECHA_BAJA, 'YYYYMMDD HH24:MI:SS'),' ')
                 INTO   :loc_status, :loc_tipo, :loc_fecha_baja
                 FROM   TRANS_PROGRAMADO
                 WHERE  CLAVE_INST  = :sql_clave_inst
                   AND  CLAVE_SIS   = :sql_clave_sis
                   AND  CLAVE_TRAN  = :sql_clave_tran
                   AND  FECHA_RECEP = TRUNC(SYSDATE)
                   AND  TIPO        = :sql_tipo
                   AND  BANCO_ORD = :sql_banco_ord
                   AND  BANCO_BEN = :sql_banco_ben
                   AND ( IMPORTE >= :sql_importe - :loc_limite AND
                         IMPORTE <= :sql_importe + :loc_limite )
                   AND  REMANENTE IS NULL; */ 
#line 1672 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        struct sqlexd sqlstm;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqlvsn = 13;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.arrsiz = 26;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqladtp = &sqladt;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqltdsp = &sqltds;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.stmt = "select NVL(STATUS,' ') ,NVL(TIPO,' ') ,NVL(TO_CHAR(FE\
CHA_BAJA,'YYYYMMDD HH24:MI:SS'),' ') into :b0,:b1,:b2  from TRANS_PROGRAMADO w\
here ((((((((CLAVE_INST=:b3 and CLAVE_SIS=:b4) and CLAVE_TRAN=:b5) and FECHA_R\
ECEP=TRUNC(SYSDATE)) and TIPO=:b6) and BANCO_ORD=:b7) and BANCO_BEN=:b8) and (\
IMPORTE>=(:b9-:b10) and IMPORTE<=(:b9+:b10))) and REMANENTE is null )";
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.iters = (unsigned int  )1;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.offset = (unsigned int  )2416;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.selerr = (unsigned short)1;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqlpfmem = (unsigned int  )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.cud = sqlcud0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqlety = (unsigned short)4352;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.occurs = (unsigned int  )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[0] = (unsigned char  *)loc_status;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[0] = (unsigned long )5;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[0] = (         int  )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[0] = (         short *)0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[0] = (         int  )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[0] = (unsigned long )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[0] = (unsigned short )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[0] = (unsigned short )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[1] = (unsigned char  *)loc_tipo;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[1] = (unsigned long )2;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[1] = (         int  )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[1] = (         short *)0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[1] = (         int  )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[1] = (unsigned long )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[1] = (unsigned short )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[1] = (unsigned short )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[2] = (unsigned char  *)loc_fecha_baja;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[2] = (unsigned long )18;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[2] = (         int  )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[2] = (         short *)0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[2] = (         int  )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[2] = (unsigned long )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[2] = (unsigned short )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[2] = (unsigned short )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[3] = (unsigned char  *)sql_clave_inst;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[3] = (unsigned long )6;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[3] = (         int  )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[3] = (         short *)0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[3] = (         int  )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[3] = (unsigned long )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[3] = (unsigned short )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[3] = (unsigned short )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[4] = (unsigned char  *)sql_clave_sis;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[4] = (unsigned long )6;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[4] = (         int  )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[4] = (         short *)0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[4] = (         int  )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[4] = (unsigned long )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[4] = (unsigned short )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[4] = (unsigned short )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[5] = (unsigned char  *)sql_clave_tran;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[5] = (unsigned long )8;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[5] = (         int  )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[5] = (         short *)0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[5] = (         int  )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[5] = (unsigned long )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[5] = (unsigned short )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[5] = (unsigned short )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[6] = (unsigned char  *)sql_tipo;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[6] = (unsigned long )2;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[6] = (         int  )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[6] = (         short *)0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[6] = (         int  )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[6] = (unsigned long )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[6] = (unsigned short )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[6] = (unsigned short )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[7] = (unsigned char  *)sql_banco_ord;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[7] = (unsigned long )7;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[7] = (         int  )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[7] = (         short *)0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[7] = (         int  )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[7] = (unsigned long )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[7] = (unsigned short )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[7] = (unsigned short )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[8] = (unsigned char  *)sql_banco_ben;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[8] = (unsigned long )7;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[8] = (         int  )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[8] = (         short *)0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[8] = (         int  )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[8] = (unsigned long )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[8] = (unsigned short )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[8] = (unsigned short )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[9] = (unsigned char  *)&sql_importe;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[9] = (unsigned long )sizeof(double);
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[9] = (         int  )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[9] = (         short *)0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[9] = (         int  )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[9] = (unsigned long )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[9] = (unsigned short )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[9] = (unsigned short )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[10] = (unsigned char  *)&loc_limite;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[10] = (unsigned long )sizeof(double);
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[10] = (         int  )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[10] = (         short *)0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[10] = (         int  )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[10] = (unsigned long )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[10] = (unsigned short )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[10] = (unsigned short )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[11] = (unsigned char  *)&sql_importe;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[11] = (unsigned long )sizeof(double);
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[11] = (         int  )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[11] = (         short *)0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[11] = (         int  )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[11] = (unsigned long )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[11] = (unsigned short )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[11] = (unsigned short )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[12] = (unsigned char  *)&loc_limite;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[12] = (unsigned long )sizeof(double);
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[12] = (         int  )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[12] = (         short *)0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[12] = (         int  )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[12] = (unsigned long )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[12] = (unsigned short )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[12] = (unsigned short )0;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqphsv = sqlstm.sqhstv;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqphsl = sqlstm.sqhstl;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqphss = sqlstm.sqhsts;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqpind = sqlstm.sqindv;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqpins = sqlstm.sqinds;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqparm = sqlstm.sqharm;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqparc = sqlstm.sqharc;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqpadto = sqlstm.sqadto;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqptdso = sqlstm.sqtdso;
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 1660 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 1672 "../../gral/src_db/CTransaccionalCdl.pc"

                   //AND  ROWNUM = 1;  Propuesto por H.Martinez 19-May-2010

        Gfi_StrRTrim(loc_status); Gfi_StrRTrim(loc_fecha_baja);
        if (strlen(loc_status) > 0)
        {
          ref_concilG[0] = '\0';  //NULL
          strcpy(loc_status2,"NP");
          if (NIVEL_TRACE > 9)
            cout << "STATUS=" << loc_status << endl;

          sprintf(loc_status1, "P%sC",loc_tipo);
          if (strcmp(loc_status,loc_status1) && (strlen(loc_fecha_baja) == 0))
          {
            if (NIVEL_TRACE > 9)
              cout << "EXISTE EL PROGRAMADO" << endl;

            strcpy(loc_status2,"PC");

            resultado = Obtiene_Ref_Concil();
          }
        }
        else
        {
          if (NIVEL_TRACE > 5)
            cout << "NO EXISTE PROGRAMADO!" << endl;

          strcpy(err_desc,"R0021");
        }

        if (strlen(err_desc) == 0)
        { // 32-2
          if (NIVEL_TRACE > 9)
            cout << "BUSCANDO EL REAL COMO SALA DE ESPERA" << endl;

          loc_status[0] = '\0';  //NULL
          loc_tipo[0] = '\0';  //NULL
          loc_ref_med[0] = '\0';  //NULL
          loc_fecha_baja[0] = '\0';  //NULL

          // Index: CLAVE_INST, CLAVE_SIS, CLAVE_RAST, CLAVE_TRAN, FECHA_RECEP, TIPO

          /* EXEC SQL SELECT NVL(STATUS,' '), NVL(TIPO,' '), NVL(REF_MED,' ') //, NVL(TO_CHAR(FECHA_BAJA, 'YYYYMMDD HH24:MI:SS'),' ')
                   INTO   :loc_status, :loc_tipo, :loc_ref_med //, :loc_fecha_baja
                   FROM   TRANS_PAGO
                   WHERE  CLAVE_INST  = :sql_clave_inst
                     AND  CLAVE_SIS   = :sql_clave_sis
                     AND  CLAVE_RAST  = :sql_clave_rast
                     AND  CLAVE_TRAN  = :sql_clave_tran
                     AND  FECHA_RECEP = TO_DATE(:sql_fecha_recep, 'YYYYMMDD')
                     AND  TIPO        = :sql_tipo
                     AND  FECHA_BAJA IS NULL; */ 
#line 1723 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          struct sqlexd sqlstm;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqlvsn = 13;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.arrsiz = 26;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqladtp = &sqladt;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqltdsp = &sqltds;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.stmt = "select NVL(STATUS,' ') ,NVL(TIPO,' ') ,NVL(REF_MED,\
' ') into :b0,:b1,:b2  from TRANS_PAGO where ((((((CLAVE_INST=:b3 and CLAVE_SI\
S=:b4) and CLAVE_RAST=:b5) and CLAVE_TRAN=:b6) and FECHA_RECEP=TO_DATE(:b7,'YY\
YYMMDD')) and TIPO=:b8) and FECHA_BAJA is null )";
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.iters = (unsigned int  )1;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.offset = (unsigned int  )2483;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.selerr = (unsigned short)1;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqlpfmem = (unsigned int  )0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.cud = sqlcud0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqlety = (unsigned short)4352;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.occurs = (unsigned int  )0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstv[0] = (unsigned char  *)loc_status;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstl[0] = (unsigned long )5;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhsts[0] = (         int  )0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqindv[0] = (         short *)0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqinds[0] = (         int  )0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqharm[0] = (unsigned long )0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqadto[0] = (unsigned short )0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqtdso[0] = (unsigned short )0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstv[1] = (unsigned char  *)loc_tipo;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstl[1] = (unsigned long )2;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhsts[1] = (         int  )0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqindv[1] = (         short *)0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqinds[1] = (         int  )0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqharm[1] = (unsigned long )0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqadto[1] = (unsigned short )0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqtdso[1] = (unsigned short )0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstv[2] = (unsigned char  *)loc_ref_med;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstl[2] = (unsigned long )13;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhsts[2] = (         int  )0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqindv[2] = (         short *)0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqinds[2] = (         int  )0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqharm[2] = (unsigned long )0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqadto[2] = (unsigned short )0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqtdso[2] = (unsigned short )0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstv[3] = (unsigned char  *)sql_clave_inst;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstl[3] = (unsigned long )6;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhsts[3] = (         int  )0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqindv[3] = (         short *)0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqinds[3] = (         int  )0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqharm[3] = (unsigned long )0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqadto[3] = (unsigned short )0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqtdso[3] = (unsigned short )0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstv[4] = (unsigned char  *)sql_clave_sis;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstl[4] = (unsigned long )6;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhsts[4] = (         int  )0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqindv[4] = (         short *)0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqinds[4] = (         int  )0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqharm[4] = (unsigned long )0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqadto[4] = (unsigned short )0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqtdso[4] = (unsigned short )0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstv[5] = (unsigned char  *)sql_clave_rast;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstl[5] = (unsigned long )31;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhsts[5] = (         int  )0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqindv[5] = (         short *)0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqinds[5] = (         int  )0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqharm[5] = (unsigned long )0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqadto[5] = (unsigned short )0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqtdso[5] = (unsigned short )0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstv[6] = (unsigned char  *)sql_clave_tran;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstl[6] = (unsigned long )8;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhsts[6] = (         int  )0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqindv[6] = (         short *)0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqinds[6] = (         int  )0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqharm[6] = (unsigned long )0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqadto[6] = (unsigned short )0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqtdso[6] = (unsigned short )0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstv[7] = (unsigned char  *)sql_fecha_recep;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstl[7] = (unsigned long )40;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhsts[7] = (         int  )0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqindv[7] = (         short *)0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqinds[7] = (         int  )0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqharm[7] = (unsigned long )0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqadto[7] = (unsigned short )0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqtdso[7] = (unsigned short )0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstv[8] = (unsigned char  *)sql_tipo;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstl[8] = (unsigned long )2;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhsts[8] = (         int  )0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqindv[8] = (         short *)0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqinds[8] = (         int  )0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqharm[8] = (unsigned long )0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqadto[8] = (unsigned short )0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqtdso[8] = (unsigned short )0;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqphsv = sqlstm.sqhstv;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqphsl = sqlstm.sqhstl;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqphss = sqlstm.sqhsts;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqpind = sqlstm.sqindv;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqpins = sqlstm.sqinds;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqparm = sqlstm.sqharm;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqparc = sqlstm.sqharc;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqpadto = sqlstm.sqadto;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqptdso = sqlstm.sqtdso;
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 1714 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 1723 "../../gral/src_db/CTransaccionalCdl.pc"

                     //AND  ROWNUM = 1; Propuesto por H.Martinez 19-May-2010

          Gfi_StrRTrim(loc_status);   //Gfi_StrRTrim(loc_fecha_baja);
          if (strlen(loc_status) > 0)
          { // 32-3a
            sprintf(loc_status1, "R%sPC",loc_tipo);
            if (strcmp(loc_status,loc_status1)) //&& (strlen(loc_fecha_baja) == 0))
            { // 32-4
              if (NIVEL_TRACE > 9)
              {
                cout << "EXISTE EL REAL...(" << loc_status2 << ") IMPORTE (";
                printf ("%.2lf)\n", sql_importe );
              }

              /* EXEC SQL UPDATE TRANS_PAGO
                       SET    REF_CONCIL  = :ref_concilG,
                              STATUS      = 'R' || :loc_tipo || :loc_status2,
                              IMPORTE     = ROUND(:sql_importe,2),
                              FOLIO_ORIGEN = :sql_folio_origen,
                              FECHA_FLUJO = SYSDATE
                       WHERE  CLAVE_INST  = :sql_clave_inst
                         AND  CLAVE_SIS   = :sql_clave_sis
                         AND  CLAVE_RAST  = :sql_clave_rast
                         AND  CLAVE_TRAN  = :sql_clave_tran
                         AND  FECHA_RECEP = TO_DATE(:sql_fecha_recep, 'YYYYMMDD')
                         AND  TIPO        = :sql_tipo
                         AND  FECHA_BAJA IS NULL; */ 
#line 1750 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              struct sqlexd sqlstm;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqlvsn = 13;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.arrsiz = 26;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqladtp = &sqladt;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqltdsp = &sqltds;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.stmt = "update TRANS_PAGO  set REF_CONCIL=:b0,STATUS=((\
'R'||:b1)||:b2),IMPORTE=ROUND(:b3,2),FOLIO_ORIGEN=:b4,FECHA_FLUJO=SYSDATE wher\
e ((((((CLAVE_INST=:b5 and CLAVE_SIS=:b6) and CLAVE_RAST=:b7) and CLAVE_TRAN=:\
b8) and FECHA_RECEP=TO_DATE(:b9,'YYYYMMDD')) and TIPO=:b10) and FECHA_BAJA is \
null )";
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.iters = (unsigned int  )1;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.offset = (unsigned int  )2534;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.cud = sqlcud0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqlety = (unsigned short)4352;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.occurs = (unsigned int  )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[0] = (unsigned char  *)ref_concilG;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[0] = (unsigned long )20;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[0] = (         int  )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[0] = (         short *)0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[0] = (         int  )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[0] = (unsigned long )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[0] = (unsigned short )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[0] = (unsigned short )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[1] = (unsigned char  *)loc_tipo;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[1] = (unsigned long )2;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[1] = (         int  )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[1] = (         short *)0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[1] = (         int  )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[1] = (unsigned long )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[1] = (unsigned short )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[1] = (unsigned short )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[2] = (unsigned char  *)loc_status2;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[2] = (unsigned long )9;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[2] = (         int  )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[2] = (         short *)0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[2] = (         int  )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[2] = (unsigned long )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[2] = (unsigned short )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[2] = (unsigned short )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[3] = (unsigned char  *)&sql_importe;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[3] = (unsigned long )sizeof(double);
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[3] = (         int  )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[3] = (         short *)0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[3] = (         int  )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[3] = (unsigned long )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[3] = (unsigned short )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[3] = (unsigned short )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[4] = (unsigned char  *)sql_folio_origen;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[4] = (unsigned long )10;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[4] = (         int  )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[4] = (         short *)0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[4] = (         int  )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[4] = (unsigned long )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[4] = (unsigned short )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[4] = (unsigned short )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[5] = (unsigned char  *)sql_clave_inst;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[5] = (unsigned long )6;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[5] = (         int  )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[5] = (         short *)0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[5] = (         int  )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[5] = (unsigned long )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[5] = (unsigned short )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[5] = (unsigned short )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[6] = (unsigned char  *)sql_clave_sis;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[6] = (unsigned long )6;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[6] = (         int  )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[6] = (         short *)0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[6] = (         int  )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[6] = (unsigned long )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[6] = (unsigned short )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[6] = (unsigned short )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[7] = (unsigned char  *)sql_clave_rast;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[7] = (unsigned long )31;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[7] = (         int  )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[7] = (         short *)0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[7] = (         int  )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[7] = (unsigned long )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[7] = (unsigned short )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[7] = (unsigned short )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[8] = (unsigned char  *)sql_clave_tran;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[8] = (unsigned long )8;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[8] = (         int  )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[8] = (         short *)0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[8] = (         int  )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[8] = (unsigned long )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[8] = (unsigned short )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[8] = (unsigned short )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[9] = (unsigned char  *)sql_fecha_recep;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[9] = (unsigned long )40;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[9] = (         int  )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[9] = (         short *)0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[9] = (         int  )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[9] = (unsigned long )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[9] = (unsigned short )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[9] = (unsigned short )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[10] = (unsigned char  *)sql_tipo;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[10] = (unsigned long )2;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[10] = (         int  )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[10] = (         short *)0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[10] = (         int  )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[10] = (unsigned long )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[10] = (unsigned short )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[10] = (unsigned short )0;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqphsv = sqlstm.sqhstv;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqphsl = sqlstm.sqhstl;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqphss = sqlstm.sqhsts;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqpind = sqlstm.sqindv;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqpins = sqlstm.sqinds;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqparm = sqlstm.sqharm;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqparc = sqlstm.sqharc;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqpadto = sqlstm.sqadto;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqptdso = sqlstm.sqtdso;
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 1738 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 1750 "../../gral/src_db/CTransaccionalCdl.pc"


              resultado = ChecaErrorOracle( 0, 1, &rowcount);
              if (resultado == GFI_ERRC_OK)
                swConcOk = 1; // HGT 18-May-2010
              else
              {
                if (NIVEL_TRACE > 5)
                  cout << "ERROR AL ACTUALIZAR EN TRANS_PAGO!" << endl;

                strcpy(err_desc,"R0022");
              }

              if (!strcmp(loc_status2,"PC"))
              {
                if (NIVEL_TRACE > 9)
                  cout << "CONCILIANDO EL PROGRAMADO" << endl;

                /* EXEC SQL UPDATE TRANS_PROGRAMADO
                         SET    REF_CONCIL  = :ref_concilG,
                                STATUS      = 'P' || :sql_tipo || 'C' ,
                                FECHA_FLUJO = SYSDATE
                         WHERE  CLAVE_INST  = :sql_clave_inst
                           AND  CLAVE_SIS   = :sql_clave_sis
                           AND  CLAVE_TRAN  = :sql_clave_tran
                           AND  FECHA_RECEP = TRUNC(SYSDATE)
                           AND  TIPO        = :sql_tipo
                           AND  BANCO_ORD = :sql_banco_ord
                           AND  BANCO_BEN = :sql_banco_ben
                           AND ( IMPORTE >= :sql_importe - :loc_limite AND
                                 IMPORTE <= :sql_importe + :loc_limite )
                           AND  REMANENTE IS NULL; */ 
#line 1781 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                struct sqlexd sqlstm;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqlvsn = 13;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.arrsiz = 26;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqladtp = &sqladt;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqltdsp = &sqltds;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.stmt = "update TRANS_PROGRAMADO  set REF_CONCIL=:b0,S\
TATUS=(('P'||:b1)||'C'),FECHA_FLUJO=SYSDATE where ((((((((CLAVE_INST=:b2 and C\
LAVE_SIS=:b3) and CLAVE_TRAN=:b4) and FECHA_RECEP=TRUNC(SYSDATE)) and TIPO=:b1\
) and BANCO_ORD=:b6) and BANCO_BEN=:b7) and (IMPORTE>=(:b8-:b9) and IMPORTE<=(\
:b8+:b9))) and REMANENTE is null )";
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.iters = (unsigned int  )1;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.offset = (unsigned int  )2593;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.cud = sqlcud0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqlety = (unsigned short)4352;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.occurs = (unsigned int  )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[0] = (unsigned char  *)ref_concilG;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[0] = (unsigned long )20;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[0] = (         int  )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[0] = (         short *)0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[0] = (         int  )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[0] = (unsigned long )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[0] = (unsigned short )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[0] = (unsigned short )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[1] = (unsigned char  *)sql_tipo;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[1] = (unsigned long )2;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[1] = (         int  )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[1] = (         short *)0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[1] = (         int  )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[1] = (unsigned long )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[1] = (unsigned short )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[1] = (unsigned short )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[2] = (unsigned char  *)sql_clave_inst;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[2] = (unsigned long )6;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[2] = (         int  )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[2] = (         short *)0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[2] = (         int  )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[2] = (unsigned long )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[2] = (unsigned short )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[2] = (unsigned short )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[3] = (unsigned char  *)sql_clave_sis;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[3] = (unsigned long )6;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[3] = (         int  )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[3] = (         short *)0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[3] = (         int  )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[3] = (unsigned long )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[3] = (unsigned short )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[3] = (unsigned short )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[4] = (unsigned char  *)sql_clave_tran;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[4] = (unsigned long )8;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[4] = (         int  )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[4] = (         short *)0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[4] = (         int  )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[4] = (unsigned long )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[4] = (unsigned short )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[4] = (unsigned short )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[5] = (unsigned char  *)sql_tipo;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[5] = (unsigned long )2;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[5] = (         int  )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[5] = (         short *)0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[5] = (         int  )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[5] = (unsigned long )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[5] = (unsigned short )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[5] = (unsigned short )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[6] = (unsigned char  *)sql_banco_ord;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[6] = (unsigned long )7;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[6] = (         int  )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[6] = (         short *)0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[6] = (         int  )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[6] = (unsigned long )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[6] = (unsigned short )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[6] = (unsigned short )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[7] = (unsigned char  *)sql_banco_ben;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[7] = (unsigned long )7;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[7] = (         int  )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[7] = (         short *)0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[7] = (         int  )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[7] = (unsigned long )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[7] = (unsigned short )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[7] = (unsigned short )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[8] = (unsigned char  *)&sql_importe;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[8] = (unsigned long )sizeof(double);
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[8] = (         int  )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[8] = (         short *)0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[8] = (         int  )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[8] = (unsigned long )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[8] = (unsigned short )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[8] = (unsigned short )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[9] = (unsigned char  *)&loc_limite;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[9] = (unsigned long )sizeof(double);
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[9] = (         int  )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[9] = (         short *)0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[9] = (         int  )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[9] = (unsigned long )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[9] = (unsigned short )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[9] = (unsigned short )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[10] = (unsigned char  *)&sql_importe;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[10] = (unsigned long )sizeof(double);
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[10] = (         int  )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[10] = (         short *)0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[10] = (         int  )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[10] = (unsigned long )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[10] = (unsigned short )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[10] = (unsigned short )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[11] = (unsigned char  *)&loc_limite;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[11] = (unsigned long )sizeof(double);
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[11] = (         int  )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[11] = (         short *)0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[11] = (         int  )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[11] = (unsigned long )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[11] = (unsigned short )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[11] = (unsigned short )0;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqphsv = sqlstm.sqhstv;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqphsl = sqlstm.sqhstl;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqphss = sqlstm.sqhsts;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqpind = sqlstm.sqindv;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqpins = sqlstm.sqinds;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqparm = sqlstm.sqharm;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqparc = sqlstm.sqharc;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqpadto = sqlstm.sqadto;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqptdso = sqlstm.sqtdso;
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 1768 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 1781 "../../gral/src_db/CTransaccionalCdl.pc"


                resultado = ChecaErrorOracle( 0, 1, &rowcount);
                if (resultado != GFI_ERRC_OK)
                {
                  if (NIVEL_TRACE > 5)
                    cout << "ERROR AL ACTUALIZAR EN TRANS_PROGRAMADO!" << endl;

                  strcpy(err_desc,"R0023");
                }
              }
            } // 32-4
          } // 32-3a
          else
          { // 32-3b
            if (NIVEL_TRACE > 9)
            {
              cout << "NO EXISTE EL REAL… IMPORTE (";
              printf ("%.2lf)\n", sql_importe );
            }

            /* EXEC SQL INSERT INTO TRANS_PAGO ( CLAVE_INST , CLAVE_SIS , CLAVE_RAST , CLAVE_TRAN ,
                                              FECHA_RECEP , TIPO , FOLIO_ORIGEN , TIPO_OPER ,
                                              BANCO_ORD , CVE_BANCO_ORD , NOMBRE_ORD , CUENTA_ORD ,
                                              BANCO_BEN , CVE_BANCO_BEN , NOMBRE_BEN , CUENTA_BEN ,
                                              IMPORTE , STATUS , OBSERVACION , REF_CONCIL ,
                                              REF_MED , FECHA_CAP , FECHA_VENC , FECHA_FLUJO ,
                                              FECHA_REG , FECHA_BAJA )
                                 VALUES( :sql_clave_inst , :sql_clave_sis , :sql_clave_rast , :sql_clave_tran ,
                                         TO_DATE(:sql_fecha_recep, 'YYYYMMDD') , :sql_tipo , :sql_folio_origen ,
                                         :sql_tipo_oper , :sql_banco_ord , :sql_cve_banco_ord , :sql_nombre_ord ,
                                         :sql_cuenta_ord , :sql_banco_ben , :sql_cve_banco_ben , :sql_nombre_ben ,
                                         :sql_cuenta_ben , ROUND(:sql_importe,2) , 'R' || :sql_tipo || :loc_status2 ,
                                         :sql_observacion , :ref_concilG , :sql_ref_med ,
                                         TO_DATE(:sql_fecha_cap, 'YYYYMMDD HH24:MI:SS') ,
                                         TO_DATE(:sql_fecha_venc, 'YYYYMMDD HH24:MI:SS') ,
                                         SYSDATE , TO_DATE(:sql_fecha_reg, 'YYYYMMDD HH24:MI:SS') ,
                                         TO_DATE(:sql_fecha_baja, 'YYYYMMDD HH24:MI:SS') ); */ 
#line 1818 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            struct sqlexd sqlstm;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlvsn = 13;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.arrsiz = 26;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqladtp = &sqladt;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqltdsp = &sqltds;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.stmt = "insert into TRANS_PAGO (CLAVE_INST,CLAVE_SIS,CLAV\
E_RAST,CLAVE_TRAN,FECHA_RECEP,TIPO,FOLIO_ORIGEN,TIPO_OPER,BANCO_ORD,CVE_BANCO_\
ORD,NOMBRE_ORD,CUENTA_ORD,BANCO_BEN,CVE_BANCO_BEN,NOMBRE_BEN,CUENTA_BEN,IMPORT\
E,STATUS,OBSERVACION,REF_CONCIL,REF_MED,FECHA_CAP,FECHA_VENC,FECHA_FLUJO,FECHA\
_REG,FECHA_BAJA) values (:b0,:b1,:b2,:b3,TO_DATE(:b4,'YYYYMMDD'),:b5,:b6,:b7,:\
b8,:b9,:b10,:b11,:b12,:b13,:b14,:b15,ROUND(:b16,2),(('R'||:b5)||:b18),:b19,:b2\
0,:b21,TO_DATE(:b22,'YYYYMMDD HH24:MI:SS'),TO_DATE(:b23,'YYYYMMDD HH24:MI:SS')\
,SYSDATE,TO_DATE(:b24,'YYYYMMDD HH24:MI:SS'),TO_DATE(:b25,'YYYYMMDD HH24:MI:SS\
'))";
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.iters = (unsigned int  )1;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.offset = (unsigned int  )2656;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.cud = sqlcud0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlety = (unsigned short)4352;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.occurs = (unsigned int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[0] = (unsigned char  *)sql_clave_inst;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[0] = (unsigned long )6;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[0] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[0] = (         short *)0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[0] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[0] = (unsigned long )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[0] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[0] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[1] = (unsigned char  *)sql_clave_sis;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[1] = (unsigned long )6;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[1] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[1] = (         short *)0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[1] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[1] = (unsigned long )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[1] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[1] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[2] = (unsigned char  *)sql_clave_rast;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[2] = (unsigned long )31;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[2] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[2] = (         short *)0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[2] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[2] = (unsigned long )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[2] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[2] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[3] = (unsigned char  *)sql_clave_tran;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[3] = (unsigned long )8;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[3] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[3] = (         short *)0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[3] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[3] = (unsigned long )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[3] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[3] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[4] = (unsigned char  *)sql_fecha_recep;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[4] = (unsigned long )40;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[4] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[4] = (         short *)0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[4] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[4] = (unsigned long )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[4] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[4] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[5] = (unsigned char  *)sql_tipo;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[5] = (unsigned long )2;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[5] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[5] = (         short *)0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[5] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[5] = (unsigned long )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[5] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[5] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[6] = (unsigned char  *)sql_folio_origen;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[6] = (unsigned long )10;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[6] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[6] = (         short *)0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[6] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[6] = (unsigned long )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[6] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[6] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[7] = (unsigned char  *)sql_tipo_oper;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[7] = (unsigned long )5;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[7] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[7] = (         short *)0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[7] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[7] = (unsigned long )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[7] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[7] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[8] = (unsigned char  *)sql_banco_ord;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[8] = (unsigned long )7;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[8] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[8] = (         short *)0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[8] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[8] = (unsigned long )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[8] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[8] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[9] = (unsigned char  *)sql_cve_banco_ord;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[9] = (unsigned long )16;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[9] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[9] = (         short *)0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[9] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[9] = (unsigned long )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[9] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[9] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[10] = (unsigned char  *)sql_nombre_ord;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[10] = (unsigned long )52;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[10] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[10] = (         short *)0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[10] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[10] = (unsigned long )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[10] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[10] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[11] = (unsigned char  *)sql_cuenta_ord;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[11] = (unsigned long )21;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[11] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[11] = (         short *)0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[11] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[11] = (unsigned long )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[11] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[11] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[12] = (unsigned char  *)sql_banco_ben;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[12] = (unsigned long )7;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[12] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[12] = (         short *)0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[12] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[12] = (unsigned long )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[12] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[12] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[13] = (unsigned char  *)sql_cve_banco_ben;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[13] = (unsigned long )16;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[13] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[13] = (         short *)0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[13] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[13] = (unsigned long )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[13] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[13] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[14] = (unsigned char  *)sql_nombre_ben;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[14] = (unsigned long )52;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[14] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[14] = (         short *)0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[14] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[14] = (unsigned long )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[14] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[14] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[15] = (unsigned char  *)sql_cuenta_ben;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[15] = (unsigned long )21;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[15] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[15] = (         short *)0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[15] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[15] = (unsigned long )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[15] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[15] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[16] = (unsigned char  *)&sql_importe;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[16] = (unsigned long )sizeof(double);
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[16] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[16] = (         short *)0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[16] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[16] = (unsigned long )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[16] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[16] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[17] = (unsigned char  *)sql_tipo;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[17] = (unsigned long )2;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[17] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[17] = (         short *)0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[17] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[17] = (unsigned long )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[17] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[17] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[18] = (unsigned char  *)loc_status2;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[18] = (unsigned long )9;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[18] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[18] = (         short *)0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[18] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[18] = (unsigned long )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[18] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[18] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[19] = (unsigned char  *)sql_observacion;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[19] = (unsigned long )50;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[19] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[19] = (         short *)0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[19] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[19] = (unsigned long )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[19] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[19] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[20] = (unsigned char  *)ref_concilG;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[20] = (unsigned long )20;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[20] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[20] = (         short *)0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[20] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[20] = (unsigned long )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[20] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[20] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[21] = (unsigned char  *)sql_ref_med;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[21] = (unsigned long )18;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[21] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[21] = (         short *)0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[21] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[21] = (unsigned long )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[21] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[21] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[22] = (unsigned char  *)sql_fecha_cap;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[22] = (unsigned long )40;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[22] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[22] = (         short *)0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[22] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[22] = (unsigned long )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[22] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[22] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[23] = (unsigned char  *)sql_fecha_venc;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[23] = (unsigned long )18;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[23] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[23] = (         short *)0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[23] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[23] = (unsigned long )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[23] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[23] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[24] = (unsigned char  *)sql_fecha_reg;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[24] = (unsigned long )18;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[24] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[24] = (         short *)0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[24] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[24] = (unsigned long )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[24] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[24] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[25] = (unsigned char  *)sql_fecha_baja;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[25] = (unsigned long )18;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[25] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[25] = (         short *)0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[25] = (         int  )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[25] = (unsigned long )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[25] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[25] = (unsigned short )0;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqphsv = sqlstm.sqhstv;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqphsl = sqlstm.sqhstl;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqphss = sqlstm.sqhsts;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqpind = sqlstm.sqindv;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqpins = sqlstm.sqinds;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqparm = sqlstm.sqharm;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqparc = sqlstm.sqharc;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqpadto = sqlstm.sqadto;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqptdso = sqlstm.sqtdso;
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 1802 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 1818 "../../gral/src_db/CTransaccionalCdl.pc"


            resultado = ChecaErrorOracle (0, 1, &rowcount);
            if (resultado == GFI_ERRC_OK)
              swConcOk = 1; // HGT 18-May-2010
            else
            {
              if (NIVEL_TRACE > 5)
                cout << "ERROR AL INSERTAR EN TRANS_PAGO!" << endl;

              strcpy(err_desc,"R0024");
            }

            if (!strcmp(loc_status2,"PC"))
            {
              /* EXEC SQL UPDATE TRANS_PROGRAMADO
                       SET    REF_CONCIL  = :ref_concilG,
                              STATUS      = 'P' || :sql_tipo || 'C' ,
                              FECHA_FLUJO = SYSDATE
                       WHERE  CLAVE_INST  = :sql_clave_inst
                         AND  CLAVE_SIS   = :sql_clave_sis
                         AND  CLAVE_TRAN  = :sql_clave_tran
                         AND  FECHA_RECEP = TRUNC(SYSDATE)
                         AND  TIPO        = :sql_tipo
                         AND  BANCO_ORD = :sql_banco_ord
                         AND  BANCO_BEN = :sql_banco_ben
                         AND ( IMPORTE >= :sql_importe - :loc_limite AND
                               IMPORTE <= :sql_importe + :loc_limite )
                         AND  REMANENTE IS NULL; */ 
#line 1846 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              struct sqlexd sqlstm;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqlvsn = 13;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.arrsiz = 26;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqladtp = &sqladt;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqltdsp = &sqltds;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.stmt = "update TRANS_PROGRAMADO  set REF_CONCIL=:b0,STA\
TUS=(('P'||:b1)||'C'),FECHA_FLUJO=SYSDATE where ((((((((CLAVE_INST=:b2 and CLA\
VE_SIS=:b3) and CLAVE_TRAN=:b4) and FECHA_RECEP=TRUNC(SYSDATE)) and TIPO=:b1) \
and BANCO_ORD=:b6) and BANCO_BEN=:b7) and (IMPORTE>=(:b8-:b9) and IMPORTE<=(:b\
8+:b9))) and REMANENTE is null )";
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.iters = (unsigned int  )1;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.offset = (unsigned int  )2775;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.cud = sqlcud0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqlety = (unsigned short)4352;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.occurs = (unsigned int  )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[0] = (unsigned char  *)ref_concilG;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[0] = (unsigned long )20;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[0] = (         int  )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[0] = (         short *)0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[0] = (         int  )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[0] = (unsigned long )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[0] = (unsigned short )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[0] = (unsigned short )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[1] = (unsigned char  *)sql_tipo;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[1] = (unsigned long )2;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[1] = (         int  )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[1] = (         short *)0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[1] = (         int  )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[1] = (unsigned long )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[1] = (unsigned short )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[1] = (unsigned short )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[2] = (unsigned char  *)sql_clave_inst;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[2] = (unsigned long )6;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[2] = (         int  )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[2] = (         short *)0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[2] = (         int  )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[2] = (unsigned long )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[2] = (unsigned short )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[2] = (unsigned short )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[3] = (unsigned char  *)sql_clave_sis;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[3] = (unsigned long )6;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[3] = (         int  )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[3] = (         short *)0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[3] = (         int  )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[3] = (unsigned long )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[3] = (unsigned short )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[3] = (unsigned short )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[4] = (unsigned char  *)sql_clave_tran;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[4] = (unsigned long )8;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[4] = (         int  )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[4] = (         short *)0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[4] = (         int  )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[4] = (unsigned long )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[4] = (unsigned short )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[4] = (unsigned short )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[5] = (unsigned char  *)sql_tipo;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[5] = (unsigned long )2;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[5] = (         int  )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[5] = (         short *)0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[5] = (         int  )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[5] = (unsigned long )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[5] = (unsigned short )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[5] = (unsigned short )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[6] = (unsigned char  *)sql_banco_ord;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[6] = (unsigned long )7;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[6] = (         int  )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[6] = (         short *)0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[6] = (         int  )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[6] = (unsigned long )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[6] = (unsigned short )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[6] = (unsigned short )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[7] = (unsigned char  *)sql_banco_ben;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[7] = (unsigned long )7;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[7] = (         int  )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[7] = (         short *)0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[7] = (         int  )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[7] = (unsigned long )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[7] = (unsigned short )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[7] = (unsigned short )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[8] = (unsigned char  *)&sql_importe;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[8] = (unsigned long )sizeof(double);
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[8] = (         int  )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[8] = (         short *)0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[8] = (         int  )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[8] = (unsigned long )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[8] = (unsigned short )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[8] = (unsigned short )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[9] = (unsigned char  *)&loc_limite;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[9] = (unsigned long )sizeof(double);
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[9] = (         int  )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[9] = (         short *)0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[9] = (         int  )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[9] = (unsigned long )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[9] = (unsigned short )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[9] = (unsigned short )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[10] = (unsigned char  *)&sql_importe;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[10] = (unsigned long )sizeof(double);
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[10] = (         int  )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[10] = (         short *)0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[10] = (         int  )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[10] = (unsigned long )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[10] = (unsigned short )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[10] = (unsigned short )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[11] = (unsigned char  *)&loc_limite;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[11] = (unsigned long )sizeof(double);
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[11] = (         int  )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[11] = (         short *)0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[11] = (         int  )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[11] = (unsigned long )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[11] = (unsigned short )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[11] = (unsigned short )0;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqphsv = sqlstm.sqhstv;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqphsl = sqlstm.sqhstl;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqphss = sqlstm.sqhsts;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqpind = sqlstm.sqindv;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqpins = sqlstm.sqinds;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqparm = sqlstm.sqharm;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqparc = sqlstm.sqharc;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqpadto = sqlstm.sqadto;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqptdso = sqlstm.sqtdso;
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 1833 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 1846 "../../gral/src_db/CTransaccionalCdl.pc"


              resultado = ChecaErrorOracle( 0, 1, &rowcount);
              if (resultado != GFI_ERRC_OK)
              {
                if (NIVEL_TRACE > 5)
                  cout << "ERROR AL INSERTAR EN TRANS_PROGRAMADO!" << endl;

                strcpy(err_desc,"R0024");
              }
            }
          } // 32-3b
        } // 32-2

        if (NIVEL_TRACE > 9)
          cout << "TERMINO CONCILIACION OPICS" << endl;

        if (swConcOk == 1) // HGT 18-May-2010
        { // 32-5
          if (!strcmp(loc_idcd, "1"))
          {
            if (NIVEL_TRACE > 9)
              cout << "ACTUALIZA LA TABLA IDCD EN OPICS" << endl;

            strcpy(sql_insth,sql_clave_inst); strcpy(sql_sish,sql_clave_sis); strcpy(sql_nmph,"BRANCH");
            loc_br[0]='\0';  //NULL
            /* EXEC SQL OPEN OBTENPARAM; */ 
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"
            struct sqlexd sqlstm;
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlvsn = 13;
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.arrsiz = 26;
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqladtp = &sqladt;
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqltdsp = &sqltds;
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.stmt = sq0002;
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.iters = (unsigned int  )1;
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.offset = (unsigned int  )2838;
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.selerr = (unsigned short)1;
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlpfmem = (unsigned int  )0;
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.cud = sqlcud0;
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlety = (unsigned short)4352;
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.occurs = (unsigned int  )0;
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqcmod = (unsigned int )0;
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[0] = (unsigned char  *)sql_insth;
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[0] = (unsigned long )20;
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[0] = (         int  )0;
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[0] = (         short *)0;
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[0] = (         int  )0;
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[0] = (unsigned long )0;
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[0] = (unsigned short )0;
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[0] = (unsigned short )0;
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[1] = (unsigned char  *)sql_sish;
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[1] = (unsigned long )20;
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[1] = (         int  )0;
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[1] = (         short *)0;
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[1] = (         int  )0;
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[1] = (unsigned long )0;
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[1] = (unsigned short )0;
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[1] = (unsigned short )0;
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstv[2] = (unsigned char  *)sql_nmph;
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhstl[2] = (unsigned long )20;
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqhsts[2] = (         int  )0;
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqindv[2] = (         short *)0;
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqinds[2] = (         int  )0;
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqharm[2] = (unsigned long )0;
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqadto[2] = (unsigned short )0;
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqtdso[2] = (unsigned short )0;
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqphsv = sqlstm.sqhstv;
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqphsl = sqlstm.sqhstl;
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqphss = sqlstm.sqhsts;
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqpind = sqlstm.sqindv;
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqpins = sqlstm.sqinds;
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqparm = sqlstm.sqharm;
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqparc = sqlstm.sqharc;
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqpadto = sqlstm.sqadto;
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqptdso = sqlstm.sqtdso;
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 1872 "../../gral/src_db/CTransaccionalCdl.pc"

            while(1)
            {
                /* EXEC SQL FETCH OBTENPARAM INTO :sql_valor; */ 
#line 1875 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 1875 "../../gral/src_db/CTransaccionalCdl.pc"
                struct sqlexd sqlstm;
#line 1875 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqlvsn = 13;
#line 1875 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.arrsiz = 26;
#line 1875 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqladtp = &sqladt;
#line 1875 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqltdsp = &sqltds;
#line 1875 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.iters = (unsigned int  )1;
#line 1875 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.offset = (unsigned int  )2865;
#line 1875 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.selerr = (unsigned short)1;
#line 1875 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqlpfmem = (unsigned int  )0;
#line 1875 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.cud = sqlcud0;
#line 1875 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 1875 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqlety = (unsigned short)4352;
#line 1875 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.occurs = (unsigned int  )0;
#line 1875 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqfoff = (         int )0;
#line 1875 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqfmod = (unsigned int )2;
#line 1875 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstv[0] = (unsigned char  *)sql_valor;
#line 1875 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhstl[0] = (unsigned long )100;
#line 1875 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqhsts[0] = (         int  )0;
#line 1875 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqindv[0] = (         short *)0;
#line 1875 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqinds[0] = (         int  )0;
#line 1875 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqharm[0] = (unsigned long )0;
#line 1875 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqadto[0] = (unsigned short )0;
#line 1875 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqtdso[0] = (unsigned short )0;
#line 1875 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqphsv = sqlstm.sqhstv;
#line 1875 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqphsl = sqlstm.sqhstl;
#line 1875 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqphss = sqlstm.sqhsts;
#line 1875 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqpind = sqlstm.sqindv;
#line 1875 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqpins = sqlstm.sqinds;
#line 1875 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqparm = sqlstm.sqharm;
#line 1875 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqparc = sqlstm.sqharc;
#line 1875 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqpadto = sqlstm.sqadto;
#line 1875 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlstm.sqptdso = sqlstm.sqtdso;
#line 1875 "../../gral/src_db/CTransaccionalCdl.pc"
                sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 1875 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 1875 "../../gral/src_db/CTransaccionalCdl.pc"

                resultado = ChecaErrorOracle (0, 1, &rowcount);
                if (resultado != GFI_ERRC_OK)
                  break;
                Gfi_StrRTrim(sql_valor);
                if (rowcount == 1)
                  strcpy(loc_br,sql_valor);
                else
                  break;
            }
            /* EXEC SQL CLOSE OBTENPARAM; */ 
#line 1885 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 1885 "../../gral/src_db/CTransaccionalCdl.pc"
            struct sqlexd sqlstm;
#line 1885 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlvsn = 13;
#line 1885 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.arrsiz = 26;
#line 1885 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqladtp = &sqladt;
#line 1885 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqltdsp = &sqltds;
#line 1885 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.iters = (unsigned int  )1;
#line 1885 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.offset = (unsigned int  )2884;
#line 1885 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.cud = sqlcud0;
#line 1885 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 1885 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.sqlety = (unsigned short)4352;
#line 1885 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlstm.occurs = (unsigned int  )0;
#line 1885 "../../gral/src_db/CTransaccionalCdl.pc"
            sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 1885 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 1885 "../../gral/src_db/CTransaccionalCdl.pc"


            if (strlen(loc_br) > 0)
            {
              /* EXEC SQL UPDATE IDCD@LGTOPICS
                       SET    STATE  = 'LI'
                       WHERE  BR = :loc_br
                         AND  TO_CHAR( DEALDATE, 'YYYYMMDD' ) = :sql_fecha_recep
                         AND  CONFIRMATION = TO_NUMBER(SUBSTR(:sql_clave_rast, 1, 9)); */ 
#line 1893 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 1889 "../../gral/src_db/CTransaccionalCdl.pc"
              struct sqlexd sqlstm;
#line 1889 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqlvsn = 13;
#line 1889 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.arrsiz = 26;
#line 1889 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqladtp = &sqladt;
#line 1889 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqltdsp = &sqltds;
#line 1889 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.stmt = "update IDCD@LGTOPICS  set STATE='LI' where ((BR\
=:b0 and TO_CHAR(DEALDATE,'YYYYMMDD')=:b1) and CONFIRMATION=TO_NUMBER(SUBSTR(:\
b2,1,9)))";
#line 1889 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.iters = (unsigned int  )1;
#line 1889 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.offset = (unsigned int  )2899;
#line 1889 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.cud = sqlcud0;
#line 1889 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 1889 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqlety = (unsigned short)4352;
#line 1889 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.occurs = (unsigned int  )0;
#line 1889 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[0] = (unsigned char  *)loc_br;
#line 1889 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[0] = (unsigned long )10;
#line 1889 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[0] = (         int  )0;
#line 1889 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[0] = (         short *)0;
#line 1889 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[0] = (         int  )0;
#line 1889 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[0] = (unsigned long )0;
#line 1889 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[0] = (unsigned short )0;
#line 1889 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[0] = (unsigned short )0;
#line 1889 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[1] = (unsigned char  *)sql_fecha_recep;
#line 1889 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[1] = (unsigned long )40;
#line 1889 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[1] = (         int  )0;
#line 1889 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[1] = (         short *)0;
#line 1889 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[1] = (         int  )0;
#line 1889 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[1] = (unsigned long )0;
#line 1889 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[1] = (unsigned short )0;
#line 1889 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[1] = (unsigned short )0;
#line 1889 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstv[2] = (unsigned char  *)sql_clave_rast;
#line 1889 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhstl[2] = (unsigned long )31;
#line 1889 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqhsts[2] = (         int  )0;
#line 1889 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqindv[2] = (         short *)0;
#line 1889 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqinds[2] = (         int  )0;
#line 1889 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqharm[2] = (unsigned long )0;
#line 1889 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqadto[2] = (unsigned short )0;
#line 1889 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqtdso[2] = (unsigned short )0;
#line 1889 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqphsv = sqlstm.sqhstv;
#line 1889 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqphsl = sqlstm.sqhstl;
#line 1889 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqphss = sqlstm.sqhsts;
#line 1889 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqpind = sqlstm.sqindv;
#line 1889 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqpins = sqlstm.sqinds;
#line 1889 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqparm = sqlstm.sqharm;
#line 1889 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqparc = sqlstm.sqharc;
#line 1889 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqpadto = sqlstm.sqadto;
#line 1889 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlstm.sqptdso = sqlstm.sqtdso;
#line 1889 "../../gral/src_db/CTransaccionalCdl.pc"
              sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 1889 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 1893 "../../gral/src_db/CTransaccionalCdl.pc"


              if (resultado != GFI_ERRC_OK)
              {
                if (NIVEL_TRACE > 5)
                  cout << "ERROR AL ACTUALIZAR LA TABLA IDCD EN OPICS!" << endl;

                strcpy(err_desc,"R0026");
              }
            }
          }
        } // 32-5
      } // 32-2
    } // 32

   if (swConcOk == 1)
      break;

  } // 1
  /* EXEC SQL CLOSE OBTENPTCONC; */ 
#line 1912 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 1912 "../../gral/src_db/CTransaccionalCdl.pc"
  struct sqlexd sqlstm;
#line 1912 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqlvsn = 13;
#line 1912 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.arrsiz = 26;
#line 1912 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqladtp = &sqladt;
#line 1912 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqltdsp = &sqltds;
#line 1912 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.iters = (unsigned int  )1;
#line 1912 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.offset = (unsigned int  )2926;
#line 1912 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.cud = sqlcud0;
#line 1912 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 1912 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqlety = (unsigned short)4352;
#line 1912 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.occurs = (unsigned int  )0;
#line 1912 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 1912 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 1912 "../../gral/src_db/CTransaccionalCdl.pc"


  if (swConcOk == 0) // HGT 18-May-2010
  { // 64
    // NORMAL - DEFAULT
    err_desc[0] = '\0'; //NULL
    if (NIVEL_TRACE > 5)
      cout << "CONCILIACION NORMAL (DEFAULT)" << endl;

    loc_status[0] = '\0';  //NULL
    loc_tipo[0] = '\0';  //NULL

    /* EXEC SQL SELECT NVL(STATUS,' '), NVL(TIPO,' ')
             INTO   :loc_status, :loc_tipo
             FROM   TRANS_PROGRAMADO
             WHERE  CLAVE_INST  = :sql_clave_inst
               AND  CLAVE_SIS   = :sql_clave_sis
               AND  CLAVE_TRAN  = :sql_clave_tran
               AND  FECHA_RECEP = TRUNC(SYSDATE)
               AND  TIPO        = :sql_tipo
               AND  BANCO_ORD   = :sql_banco_ord
               AND  BANCO_BEN   = :sql_banco_ben
               AND  IMPORTE     = ROUND(:sql_importe,2)
               AND  FECHA_BAJA IS NULL; */ 
#line 1935 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    struct sqlexd sqlstm;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqlvsn = 13;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.arrsiz = 26;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqladtp = &sqladt;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqltdsp = &sqltds;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.stmt = "select NVL(STATUS,' ') ,NVL(TIPO,' ') into :b0,:b1  from \
TRANS_PROGRAMADO where ((((((((CLAVE_INST=:b2 and CLAVE_SIS=:b3) and CLAVE_TRA\
N=:b4) and FECHA_RECEP=TRUNC(SYSDATE)) and TIPO=:b5) and BANCO_ORD=:b6) and BA\
NCO_BEN=:b7) and IMPORTE=ROUND(:b8,2)) and FECHA_BAJA is null )";
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.iters = (unsigned int  )1;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.offset = (unsigned int  )2941;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.selerr = (unsigned short)1;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqlpfmem = (unsigned int  )0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.cud = sqlcud0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqlety = (unsigned short)4352;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.occurs = (unsigned int  )0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[0] = (unsigned char  *)loc_status;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[0] = (unsigned long )5;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[0] = (         int  )0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[0] = (         short *)0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[0] = (         int  )0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[0] = (unsigned long )0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[0] = (unsigned short )0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[0] = (unsigned short )0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[1] = (unsigned char  *)loc_tipo;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[1] = (unsigned long )2;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[1] = (         int  )0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[1] = (         short *)0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[1] = (         int  )0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[1] = (unsigned long )0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[1] = (unsigned short )0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[1] = (unsigned short )0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[2] = (unsigned char  *)sql_clave_inst;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[2] = (unsigned long )6;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[2] = (         int  )0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[2] = (         short *)0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[2] = (         int  )0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[2] = (unsigned long )0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[2] = (unsigned short )0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[2] = (unsigned short )0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[3] = (unsigned char  *)sql_clave_sis;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[3] = (unsigned long )6;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[3] = (         int  )0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[3] = (         short *)0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[3] = (         int  )0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[3] = (unsigned long )0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[3] = (unsigned short )0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[3] = (unsigned short )0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[4] = (unsigned char  *)sql_clave_tran;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[4] = (unsigned long )8;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[4] = (         int  )0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[4] = (         short *)0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[4] = (         int  )0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[4] = (unsigned long )0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[4] = (unsigned short )0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[4] = (unsigned short )0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[5] = (unsigned char  *)sql_tipo;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[5] = (unsigned long )2;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[5] = (         int  )0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[5] = (         short *)0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[5] = (         int  )0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[5] = (unsigned long )0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[5] = (unsigned short )0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[5] = (unsigned short )0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[6] = (unsigned char  *)sql_banco_ord;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[6] = (unsigned long )7;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[6] = (         int  )0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[6] = (         short *)0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[6] = (         int  )0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[6] = (unsigned long )0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[6] = (unsigned short )0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[6] = (unsigned short )0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[7] = (unsigned char  *)sql_banco_ben;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[7] = (unsigned long )7;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[7] = (         int  )0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[7] = (         short *)0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[7] = (         int  )0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[7] = (unsigned long )0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[7] = (unsigned short )0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[7] = (unsigned short )0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[8] = (unsigned char  *)&sql_importe;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[8] = (unsigned long )sizeof(double);
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[8] = (         int  )0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[8] = (         short *)0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[8] = (         int  )0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[8] = (unsigned long )0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[8] = (unsigned short )0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[8] = (unsigned short )0;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqphsv = sqlstm.sqhstv;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqphsl = sqlstm.sqhstl;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqphss = sqlstm.sqhsts;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqpind = sqlstm.sqindv;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqpins = sqlstm.sqinds;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqparm = sqlstm.sqharm;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqparc = sqlstm.sqharc;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqpadto = sqlstm.sqadto;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqptdso = sqlstm.sqtdso;
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 1924 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 1935 "../../gral/src_db/CTransaccionalCdl.pc"

               //AND  ROWNUM = 1; Propuesto por H.Martinez 19-May-2010

    ref_concilG[0] = '\0';  //NULL
    strcpy(loc_status2,"NP");
    Gfi_StrRTrim(loc_status);
    if (strlen(loc_status) > 0)
    { // 64-1
      if (NIVEL_TRACE > 9)
        cout << "BUSCANDO EL PROGRAMADO" << endl;

      sprintf(loc_status1, "P%sC",sql_tipo);
      if (strcmp(loc_status,loc_status1))
      {
        strcpy(loc_status2,"PC"); // HGT 15-Jun-2010

        if (NIVEL_TRACE > 9)
          cout << "ACTUALIZANDO EL PROGRAMADO A CONCILIADO" << endl;

        resultado = Obtiene_Ref_Concil();

        if (resultado == GFI_ERRC_OK)
        {
          /* EXEC SQL UPDATE TRANS_PROGRAMADO
                   SET    REF_CONCIL  = :ref_concilG,
                          STATUS      = 'P' || :loc_tipo || 'C' ,
                          FECHA_FLUJO = SYSDATE
                   WHERE  CLAVE_INST  = :sql_clave_inst
                     AND  CLAVE_SIS   = :sql_clave_sis
                     AND  CLAVE_TRAN  = :sql_clave_tran
                     AND  FECHA_RECEP = TRUNC(SYSDATE)
                     AND  TIPO        = :sql_tipo
                     AND  BANCO_ORD   = :sql_banco_ord
                     AND  BANCO_BEN   = :sql_banco_ben
                     AND  IMPORTE     = ROUND(:sql_importe,2)
                     AND  FECHA_BAJA IS NULL
                     AND  ROWNUM = 1; */ 
#line 1971 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          struct sqlexd sqlstm;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqlvsn = 13;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.arrsiz = 26;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqladtp = &sqladt;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqltdsp = &sqltds;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.stmt = "update TRANS_PROGRAMADO  set REF_CONCIL=:b0,STATUS=\
(('P'||:b1)||'C'),FECHA_FLUJO=SYSDATE where (((((((((CLAVE_INST=:b2 and CLAVE_\
SIS=:b3) and CLAVE_TRAN=:b4) and FECHA_RECEP=TRUNC(SYSDATE)) and TIPO=:b5) and\
 BANCO_ORD=:b6) and BANCO_BEN=:b7) and IMPORTE=ROUND(:b8,2)) and FECHA_BAJA is\
 null ) and ROWNUM=1)";
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.iters = (unsigned int  )1;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.offset = (unsigned int  )2992;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.cud = sqlcud0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqlety = (unsigned short)4352;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.occurs = (unsigned int  )0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstv[0] = (unsigned char  *)ref_concilG;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstl[0] = (unsigned long )20;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhsts[0] = (         int  )0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqindv[0] = (         short *)0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqinds[0] = (         int  )0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqharm[0] = (unsigned long )0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqadto[0] = (unsigned short )0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqtdso[0] = (unsigned short )0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstv[1] = (unsigned char  *)loc_tipo;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstl[1] = (unsigned long )2;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhsts[1] = (         int  )0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqindv[1] = (         short *)0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqinds[1] = (         int  )0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqharm[1] = (unsigned long )0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqadto[1] = (unsigned short )0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqtdso[1] = (unsigned short )0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstv[2] = (unsigned char  *)sql_clave_inst;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstl[2] = (unsigned long )6;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhsts[2] = (         int  )0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqindv[2] = (         short *)0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqinds[2] = (         int  )0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqharm[2] = (unsigned long )0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqadto[2] = (unsigned short )0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqtdso[2] = (unsigned short )0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstv[3] = (unsigned char  *)sql_clave_sis;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstl[3] = (unsigned long )6;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhsts[3] = (         int  )0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqindv[3] = (         short *)0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqinds[3] = (         int  )0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqharm[3] = (unsigned long )0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqadto[3] = (unsigned short )0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqtdso[3] = (unsigned short )0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstv[4] = (unsigned char  *)sql_clave_tran;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstl[4] = (unsigned long )8;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhsts[4] = (         int  )0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqindv[4] = (         short *)0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqinds[4] = (         int  )0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqharm[4] = (unsigned long )0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqadto[4] = (unsigned short )0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqtdso[4] = (unsigned short )0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstv[5] = (unsigned char  *)sql_tipo;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstl[5] = (unsigned long )2;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhsts[5] = (         int  )0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqindv[5] = (         short *)0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqinds[5] = (         int  )0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqharm[5] = (unsigned long )0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqadto[5] = (unsigned short )0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqtdso[5] = (unsigned short )0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstv[6] = (unsigned char  *)sql_banco_ord;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstl[6] = (unsigned long )7;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhsts[6] = (         int  )0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqindv[6] = (         short *)0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqinds[6] = (         int  )0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqharm[6] = (unsigned long )0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqadto[6] = (unsigned short )0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqtdso[6] = (unsigned short )0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstv[7] = (unsigned char  *)sql_banco_ben;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstl[7] = (unsigned long )7;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhsts[7] = (         int  )0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqindv[7] = (         short *)0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqinds[7] = (         int  )0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqharm[7] = (unsigned long )0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqadto[7] = (unsigned short )0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqtdso[7] = (unsigned short )0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstv[8] = (unsigned char  *)&sql_importe;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstl[8] = (unsigned long )sizeof(double);
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhsts[8] = (         int  )0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqindv[8] = (         short *)0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqinds[8] = (         int  )0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqharm[8] = (unsigned long )0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqadto[8] = (unsigned short )0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqtdso[8] = (unsigned short )0;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqphsv = sqlstm.sqhstv;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqphsl = sqlstm.sqhstl;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqphss = sqlstm.sqhsts;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqpind = sqlstm.sqindv;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqpins = sqlstm.sqinds;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqparm = sqlstm.sqharm;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqparc = sqlstm.sqharc;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqpadto = sqlstm.sqadto;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqptdso = sqlstm.sqtdso;
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 1958 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 1971 "../../gral/src_db/CTransaccionalCdl.pc"
 // Solicitado por H.Martinez 15-Jun-2010

          resultado = ChecaErrorOracle( 0, 1, &rowcount);
          if (resultado != GFI_ERRC_OK)
          {
            if (NIVEL_TRACE > 5)
              cout << "ERROR AL ACTUALIZAR EN TRANS_PROGRAMADO!" << endl;

            strcpy(err_desc,"R0027");
          }
        }
      }
    } // 64-1

    if (strlen(err_desc) == 0)
    { // 64-2
      if (NIVEL_TRACE > 9)
        cout << "BUSCANDO EL REAL" << endl;

      loc_status[0] = '\0';  //NULL
      loc_tipo[0] = '\0';  //NULL
      loc_ref_med[0] = '\0';  //NULL

      // Index: CLAVE_INST, CLAVE_SIS, CLAVE_RAST, CLAVE_TRAN, FECHA_RECEP, TIPO

      /* EXEC SQL SELECT NVL(STATUS,' '), NVL(TIPO,' '), NVL(REF_MED,' ')
               INTO   :loc_status, :loc_tipo, :loc_ref_med
               FROM   TRANS_PAGO
               WHERE  CLAVE_INST  = :sql_clave_inst
                 AND  CLAVE_SIS   = :sql_clave_sis
                 AND  CLAVE_RAST  = :sql_clave_rast
                 AND  CLAVE_TRAN  = :sql_clave_tran
                 AND  FECHA_RECEP = TO_DATE(:sql_fecha_recep, 'YYYYMMDD')
                 AND  TIPO        = :sql_tipo
                 AND  FECHA_BAJA IS NULL; */ 
#line 2005 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      struct sqlexd sqlstm;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqlvsn = 13;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.arrsiz = 26;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqladtp = &sqladt;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqltdsp = &sqltds;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.stmt = "select NVL(STATUS,' ') ,NVL(TIPO,' ') ,NVL(REF_MED,' ')\
 into :b0,:b1,:b2  from TRANS_PAGO where ((((((CLAVE_INST=:b3 and CLAVE_SIS=:b\
4) and CLAVE_RAST=:b5) and CLAVE_TRAN=:b6) and FECHA_RECEP=TO_DATE(:b7,'YYYYMM\
DD')) and TIPO=:b8) and FECHA_BAJA is null )";
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.iters = (unsigned int  )1;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.offset = (unsigned int  )3043;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.selerr = (unsigned short)1;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqlpfmem = (unsigned int  )0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.cud = sqlcud0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqlety = (unsigned short)4352;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.occurs = (unsigned int  )0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[0] = (unsigned char  *)loc_status;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[0] = (unsigned long )5;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[0] = (         int  )0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[0] = (         short *)0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[0] = (         int  )0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[0] = (unsigned long )0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[0] = (unsigned short )0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[0] = (unsigned short )0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[1] = (unsigned char  *)loc_tipo;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[1] = (unsigned long )2;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[1] = (         int  )0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[1] = (         short *)0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[1] = (         int  )0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[1] = (unsigned long )0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[1] = (unsigned short )0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[1] = (unsigned short )0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[2] = (unsigned char  *)loc_ref_med;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[2] = (unsigned long )13;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[2] = (         int  )0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[2] = (         short *)0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[2] = (         int  )0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[2] = (unsigned long )0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[2] = (unsigned short )0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[2] = (unsigned short )0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[3] = (unsigned char  *)sql_clave_inst;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[3] = (unsigned long )6;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[3] = (         int  )0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[3] = (         short *)0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[3] = (         int  )0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[3] = (unsigned long )0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[3] = (unsigned short )0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[3] = (unsigned short )0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[4] = (unsigned char  *)sql_clave_sis;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[4] = (unsigned long )6;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[4] = (         int  )0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[4] = (         short *)0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[4] = (         int  )0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[4] = (unsigned long )0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[4] = (unsigned short )0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[4] = (unsigned short )0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[5] = (unsigned char  *)sql_clave_rast;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[5] = (unsigned long )31;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[5] = (         int  )0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[5] = (         short *)0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[5] = (         int  )0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[5] = (unsigned long )0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[5] = (unsigned short )0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[5] = (unsigned short )0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[6] = (unsigned char  *)sql_clave_tran;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[6] = (unsigned long )8;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[6] = (         int  )0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[6] = (         short *)0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[6] = (         int  )0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[6] = (unsigned long )0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[6] = (unsigned short )0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[6] = (unsigned short )0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[7] = (unsigned char  *)sql_fecha_recep;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[7] = (unsigned long )40;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[7] = (         int  )0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[7] = (         short *)0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[7] = (         int  )0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[7] = (unsigned long )0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[7] = (unsigned short )0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[7] = (unsigned short )0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[8] = (unsigned char  *)sql_tipo;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[8] = (unsigned long )2;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[8] = (         int  )0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[8] = (         short *)0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[8] = (         int  )0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[8] = (unsigned long )0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[8] = (unsigned short )0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[8] = (unsigned short )0;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqphsv = sqlstm.sqhstv;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqphsl = sqlstm.sqhstl;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqphss = sqlstm.sqhsts;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqpind = sqlstm.sqindv;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqpins = sqlstm.sqinds;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqparm = sqlstm.sqharm;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqparc = sqlstm.sqharc;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqpadto = sqlstm.sqadto;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqptdso = sqlstm.sqtdso;
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 1996 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 2005 "../../gral/src_db/CTransaccionalCdl.pc"

                 //AND  ROWNUM      = 1;  Propuesto por H.Martinez 19-May-2010

      Gfi_StrRTrim(loc_status);
      if (strlen(loc_status) > 0)
      { // 64-3a
        sprintf(loc_status1, "R%sPC",loc_tipo);
        if (strcmp(loc_status,loc_status1))
        {
          if (NIVEL_TRACE > 9)
            cout << "ACTUALIZANDO EL REAL" << endl;

          /* EXEC SQL UPDATE TRANS_PAGO
                   SET    REF_CONCIL  = :ref_concilG,
                          STATUS      = 'R' || :loc_tipo || :loc_status2,
                          FECHA_FLUJO = SYSDATE,
                          IMPORTE     = ROUND(:sql_importe,2),
                          REF_MED     = :loc_ref_med
                   WHERE  CLAVE_INST  = :sql_clave_inst
                     AND  CLAVE_SIS   = :sql_clave_sis
                     AND  CLAVE_RAST  = :sql_clave_rast
                     AND  CLAVE_TRAN  = :sql_clave_tran
                     AND  FECHA_RECEP = TO_DATE(:sql_fecha_recep, 'YYYYMMDD')
                     AND  TIPO        = :sql_tipo
                     AND  FECHA_BAJA IS NULL; */ 
#line 2029 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          struct sqlexd sqlstm;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqlvsn = 13;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.arrsiz = 26;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqladtp = &sqladt;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqltdsp = &sqltds;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.stmt = "update TRANS_PAGO  set REF_CONCIL=:b0,STATUS=(('R'|\
|:b1)||:b2),FECHA_FLUJO=SYSDATE,IMPORTE=ROUND(:b3,2),REF_MED=:b4 where ((((((C\
LAVE_INST=:b5 and CLAVE_SIS=:b6) and CLAVE_RAST=:b7) and CLAVE_TRAN=:b8) and F\
ECHA_RECEP=TO_DATE(:b9,'YYYYMMDD')) and TIPO=:b10) and FECHA_BAJA is null )";
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.iters = (unsigned int  )1;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.offset = (unsigned int  )3094;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.cud = sqlcud0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqlety = (unsigned short)4352;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.occurs = (unsigned int  )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstv[0] = (unsigned char  *)ref_concilG;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstl[0] = (unsigned long )20;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhsts[0] = (         int  )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqindv[0] = (         short *)0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqinds[0] = (         int  )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqharm[0] = (unsigned long )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqadto[0] = (unsigned short )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqtdso[0] = (unsigned short )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstv[1] = (unsigned char  *)loc_tipo;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstl[1] = (unsigned long )2;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhsts[1] = (         int  )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqindv[1] = (         short *)0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqinds[1] = (         int  )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqharm[1] = (unsigned long )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqadto[1] = (unsigned short )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqtdso[1] = (unsigned short )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstv[2] = (unsigned char  *)loc_status2;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstl[2] = (unsigned long )9;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhsts[2] = (         int  )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqindv[2] = (         short *)0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqinds[2] = (         int  )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqharm[2] = (unsigned long )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqadto[2] = (unsigned short )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqtdso[2] = (unsigned short )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstv[3] = (unsigned char  *)&sql_importe;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstl[3] = (unsigned long )sizeof(double);
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhsts[3] = (         int  )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqindv[3] = (         short *)0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqinds[3] = (         int  )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqharm[3] = (unsigned long )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqadto[3] = (unsigned short )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqtdso[3] = (unsigned short )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstv[4] = (unsigned char  *)loc_ref_med;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstl[4] = (unsigned long )13;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhsts[4] = (         int  )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqindv[4] = (         short *)0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqinds[4] = (         int  )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqharm[4] = (unsigned long )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqadto[4] = (unsigned short )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqtdso[4] = (unsigned short )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstv[5] = (unsigned char  *)sql_clave_inst;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstl[5] = (unsigned long )6;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhsts[5] = (         int  )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqindv[5] = (         short *)0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqinds[5] = (         int  )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqharm[5] = (unsigned long )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqadto[5] = (unsigned short )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqtdso[5] = (unsigned short )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstv[6] = (unsigned char  *)sql_clave_sis;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstl[6] = (unsigned long )6;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhsts[6] = (         int  )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqindv[6] = (         short *)0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqinds[6] = (         int  )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqharm[6] = (unsigned long )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqadto[6] = (unsigned short )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqtdso[6] = (unsigned short )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstv[7] = (unsigned char  *)sql_clave_rast;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstl[7] = (unsigned long )31;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhsts[7] = (         int  )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqindv[7] = (         short *)0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqinds[7] = (         int  )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqharm[7] = (unsigned long )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqadto[7] = (unsigned short )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqtdso[7] = (unsigned short )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstv[8] = (unsigned char  *)sql_clave_tran;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstl[8] = (unsigned long )8;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhsts[8] = (         int  )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqindv[8] = (         short *)0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqinds[8] = (         int  )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqharm[8] = (unsigned long )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqadto[8] = (unsigned short )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqtdso[8] = (unsigned short )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstv[9] = (unsigned char  *)sql_fecha_recep;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstl[9] = (unsigned long )40;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhsts[9] = (         int  )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqindv[9] = (         short *)0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqinds[9] = (         int  )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqharm[9] = (unsigned long )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqadto[9] = (unsigned short )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqtdso[9] = (unsigned short )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstv[10] = (unsigned char  *)sql_tipo;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstl[10] = (unsigned long )2;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhsts[10] = (         int  )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqindv[10] = (         short *)0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqinds[10] = (         int  )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqharm[10] = (unsigned long )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqadto[10] = (unsigned short )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqtdso[10] = (unsigned short )0;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqphsv = sqlstm.sqhstv;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqphsl = sqlstm.sqhstl;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqphss = sqlstm.sqhsts;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqpind = sqlstm.sqindv;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqpins = sqlstm.sqinds;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqparm = sqlstm.sqharm;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqparc = sqlstm.sqharc;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqpadto = sqlstm.sqadto;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqptdso = sqlstm.sqtdso;
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 2017 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 2029 "../../gral/src_db/CTransaccionalCdl.pc"


          resultado = ChecaErrorOracle( 0, 1, &rowcount);
          if (resultado != GFI_ERRC_OK)
          {
            if (NIVEL_TRACE > 5)
              cout << "ERROR AL ACTUALIZAR EN TRANS_PAGO!" << endl;

            strcpy(err_desc,"R0028");
          }
        }
      } // 64-3a
      else
      { // 64-3b
        if (NIVEL_TRACE > 9)
          cout << "NO EXISTE EL REAL, INSERTANDOLO..." << endl;

        /* EXEC SQL INSERT INTO TRANS_PAGO ( CLAVE_INST , CLAVE_SIS , CLAVE_RAST , CLAVE_TRAN ,
                                          FECHA_RECEP , TIPO , FOLIO_ORIGEN , TIPO_OPER ,
                                          BANCO_ORD , CVE_BANCO_ORD , NOMBRE_ORD , CUENTA_ORD ,
                                          BANCO_BEN , CVE_BANCO_BEN , NOMBRE_BEN , CUENTA_BEN ,
                                          IMPORTE , STATUS , OBSERVACION , REF_CONCIL ,
                                          REF_MED , FECHA_CAP , FECHA_VENC , FECHA_FLUJO ,
                                          FECHA_REG , FECHA_BAJA )
                             VALUES( :sql_clave_inst , :sql_clave_sis , :sql_clave_rast , :sql_clave_tran ,
                                     TO_DATE(:sql_fecha_recep, 'YYYYMMDD') , :sql_tipo , :sql_folio_origen ,
                                     :sql_tipo_oper , :sql_banco_ord , :sql_cve_banco_ord , :sql_nombre_ord ,
                                     :sql_cuenta_ord , :sql_banco_ben , :sql_cve_banco_ben , :sql_nombre_ben ,
                                     :sql_cuenta_ben , ROUND(:sql_importe,2) , 'R' || :sql_tipo || :loc_status2 ,
                                     :sql_observacion , :ref_concilG , :sql_ref_med ,
                                     TO_DATE(:sql_fecha_cap, 'YYYYMMDD HH24:MI:SS') ,
                                     TO_DATE(:sql_fecha_venc, 'YYYYMMDD HH24:MI:SS') ,
                                     SYSDATE , TO_DATE(:sql_fecha_reg, 'YYYYMMDD HH24:MI:SS') ,
                                    TO_DATE(:sql_fecha_baja, 'YYYYMMDD HH24:MI:SS') ); */ 
#line 2062 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        struct sqlexd sqlstm;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqlvsn = 13;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.arrsiz = 26;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqladtp = &sqladt;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqltdsp = &sqltds;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.stmt = "insert into TRANS_PAGO (CLAVE_INST,CLAVE_SIS,CLAVE_RA\
ST,CLAVE_TRAN,FECHA_RECEP,TIPO,FOLIO_ORIGEN,TIPO_OPER,BANCO_ORD,CVE_BANCO_ORD,\
NOMBRE_ORD,CUENTA_ORD,BANCO_BEN,CVE_BANCO_BEN,NOMBRE_BEN,CUENTA_BEN,IMPORTE,ST\
ATUS,OBSERVACION,REF_CONCIL,REF_MED,FECHA_CAP,FECHA_VENC,FECHA_FLUJO,FECHA_REG\
,FECHA_BAJA) values (:b0,:b1,:b2,:b3,TO_DATE(:b4,'YYYYMMDD'),:b5,:b6,:b7,:b8,:\
b9,:b10,:b11,:b12,:b13,:b14,:b15,ROUND(:b16,2),(('R'||:b5)||:b18),:b19,:b20,:b\
21,TO_DATE(:b22,'YYYYMMDD HH24:MI:SS'),TO_DATE(:b23,'YYYYMMDD HH24:MI:SS'),SYS\
DATE,TO_DATE(:b24,'YYYYMMDD HH24:MI:SS'),TO_DATE(:b25,'YYYYMMDD HH24:MI:SS'))";
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.iters = (unsigned int  )1;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.offset = (unsigned int  )3153;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.cud = sqlcud0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqlety = (unsigned short)4352;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.occurs = (unsigned int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[0] = (unsigned char  *)sql_clave_inst;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[0] = (unsigned long )6;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[0] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[0] = (         short *)0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[0] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[0] = (unsigned long )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[0] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[0] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[1] = (unsigned char  *)sql_clave_sis;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[1] = (unsigned long )6;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[1] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[1] = (         short *)0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[1] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[1] = (unsigned long )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[1] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[1] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[2] = (unsigned char  *)sql_clave_rast;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[2] = (unsigned long )31;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[2] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[2] = (         short *)0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[2] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[2] = (unsigned long )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[2] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[2] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[3] = (unsigned char  *)sql_clave_tran;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[3] = (unsigned long )8;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[3] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[3] = (         short *)0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[3] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[3] = (unsigned long )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[3] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[3] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[4] = (unsigned char  *)sql_fecha_recep;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[4] = (unsigned long )40;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[4] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[4] = (         short *)0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[4] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[4] = (unsigned long )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[4] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[4] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[5] = (unsigned char  *)sql_tipo;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[5] = (unsigned long )2;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[5] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[5] = (         short *)0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[5] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[5] = (unsigned long )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[5] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[5] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[6] = (unsigned char  *)sql_folio_origen;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[6] = (unsigned long )10;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[6] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[6] = (         short *)0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[6] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[6] = (unsigned long )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[6] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[6] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[7] = (unsigned char  *)sql_tipo_oper;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[7] = (unsigned long )5;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[7] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[7] = (         short *)0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[7] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[7] = (unsigned long )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[7] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[7] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[8] = (unsigned char  *)sql_banco_ord;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[8] = (unsigned long )7;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[8] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[8] = (         short *)0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[8] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[8] = (unsigned long )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[8] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[8] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[9] = (unsigned char  *)sql_cve_banco_ord;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[9] = (unsigned long )16;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[9] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[9] = (         short *)0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[9] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[9] = (unsigned long )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[9] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[9] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[10] = (unsigned char  *)sql_nombre_ord;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[10] = (unsigned long )52;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[10] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[10] = (         short *)0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[10] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[10] = (unsigned long )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[10] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[10] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[11] = (unsigned char  *)sql_cuenta_ord;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[11] = (unsigned long )21;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[11] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[11] = (         short *)0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[11] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[11] = (unsigned long )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[11] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[11] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[12] = (unsigned char  *)sql_banco_ben;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[12] = (unsigned long )7;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[12] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[12] = (         short *)0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[12] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[12] = (unsigned long )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[12] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[12] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[13] = (unsigned char  *)sql_cve_banco_ben;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[13] = (unsigned long )16;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[13] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[13] = (         short *)0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[13] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[13] = (unsigned long )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[13] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[13] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[14] = (unsigned char  *)sql_nombre_ben;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[14] = (unsigned long )52;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[14] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[14] = (         short *)0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[14] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[14] = (unsigned long )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[14] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[14] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[15] = (unsigned char  *)sql_cuenta_ben;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[15] = (unsigned long )21;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[15] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[15] = (         short *)0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[15] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[15] = (unsigned long )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[15] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[15] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[16] = (unsigned char  *)&sql_importe;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[16] = (unsigned long )sizeof(double);
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[16] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[16] = (         short *)0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[16] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[16] = (unsigned long )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[16] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[16] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[17] = (unsigned char  *)sql_tipo;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[17] = (unsigned long )2;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[17] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[17] = (         short *)0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[17] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[17] = (unsigned long )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[17] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[17] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[18] = (unsigned char  *)loc_status2;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[18] = (unsigned long )9;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[18] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[18] = (         short *)0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[18] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[18] = (unsigned long )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[18] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[18] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[19] = (unsigned char  *)sql_observacion;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[19] = (unsigned long )50;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[19] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[19] = (         short *)0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[19] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[19] = (unsigned long )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[19] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[19] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[20] = (unsigned char  *)ref_concilG;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[20] = (unsigned long )20;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[20] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[20] = (         short *)0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[20] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[20] = (unsigned long )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[20] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[20] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[21] = (unsigned char  *)sql_ref_med;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[21] = (unsigned long )18;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[21] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[21] = (         short *)0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[21] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[21] = (unsigned long )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[21] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[21] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[22] = (unsigned char  *)sql_fecha_cap;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[22] = (unsigned long )40;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[22] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[22] = (         short *)0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[22] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[22] = (unsigned long )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[22] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[22] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[23] = (unsigned char  *)sql_fecha_venc;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[23] = (unsigned long )18;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[23] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[23] = (         short *)0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[23] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[23] = (unsigned long )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[23] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[23] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[24] = (unsigned char  *)sql_fecha_reg;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[24] = (unsigned long )18;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[24] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[24] = (         short *)0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[24] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[24] = (unsigned long )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[24] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[24] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[25] = (unsigned char  *)sql_fecha_baja;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[25] = (unsigned long )18;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[25] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[25] = (         short *)0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[25] = (         int  )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[25] = (unsigned long )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[25] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[25] = (unsigned short )0;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqphsv = sqlstm.sqhstv;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqphsl = sqlstm.sqhstl;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqphss = sqlstm.sqhsts;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqpind = sqlstm.sqindv;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqpins = sqlstm.sqinds;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqparm = sqlstm.sqharm;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqparc = sqlstm.sqharc;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqpadto = sqlstm.sqadto;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqptdso = sqlstm.sqtdso;
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 2046 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 2062 "../../gral/src_db/CTransaccionalCdl.pc"


									
        //resultado = ChecaErrorOracle (0, 1, &rowcount);
		
		//if (resultado != GFI_ERRC_OK)
		if(sqlca.sqlcode != 0)
        {
			resultado = 1;
          if (NIVEL_TRACE > 5)
            cout << "ERROR AL INSERTAR EN TRANS_PAGO!" << endl;

          strcpy(err_desc,"R0029");
        }

      } // 64-3b

      if (NIVEL_TRACE > 9)
        cout << "TERMINO CONCILIACION NORMAL (DEFAULT)" << endl;

    } // 64-2
  } // 64

  if (NIVEL_TRACE > 5)
    cout << "Termino REAL" << endl;

  return( resultado);
} // End

/////////////////////////////////////////////////////////
//  Funcion:  ProcesaProg                              //
/////////////////////////////////////////////////////////
int  ProcesaProg( char *clave_inst,  char *clave_sis,     char *clave_rast,   char *clave_tran,
                  char *fecha_recep, char *tipo,          char *folio_origen, char *tipo_oper,
                  char *banco_ord,   char *cve_banco_ord, char *nombre_ord,   char *cuenta_ord,
                  char *banco_ben,   char *cve_banco_ben, char *nombre_ben,   char *cuenta_ben,
                  double importe,    char *observacion,   char *ref_concil,   char *ref_med,
                  char *fecha_cap,   char *fecha_venc,    char *fecha_flujo,  char *fecha_reg,
                  char *fecha_baja,  char *status )
{ // Ini
  /* EXEC SQL BEGIN DECLARE SECTION; */ 
#line 2102 "../../gral/src_db/CTransaccionalCdl.pc"

    char sql_clave_inst[6];
    char sql_clave_sis[6];
    char sql_clave_rast[31];
    char sql_clave_tran[8];
    char sql_fecha_recep[40];
    char sql_tipo[2];
    char sql_folio_origen[10];
    char sql_tipo_oper[5];
    char sql_banco_ord[7];
    char sql_cve_banco_ord[16];
    char sql_nombre_ord[52];
    char sql_cuenta_ord[21];
    char sql_banco_ben[7];
    char sql_cve_banco_ben[16];
    char sql_nombre_ben[52];
    char sql_cuenta_ben[21];
    char sql_observacion[50];
    char sql_ref_concil[16];
    char sql_ref_med[18];
    char sql_fecha_cap[40];
    char sql_fecha_venc[18];
    char sql_fecha_flujo[18];
    char sql_fecha_reg[18];
    char sql_fecha_baja[18];
    char sql_status[5];
    double sql_importe;
    int  conttprog;
//    char loc_ref_med[13];
//    char loc_clave_tran[8];
//    char loc_banco_ben[7];
//    char loc_idcd[10];
//    double loc_limite;
    char loc_status[5];
//    char loc_tipo[2];
    char loc_status2[9];
    double loc_remanente;
//    double loc_importe;
//    double loc_simporte;
//    char loc_br[10];
//    char loc_fecha_baja[18];
  /* EXEC SQL END DECLARE SECTION; */ 
#line 2143 "../../gral/src_db/CTransaccionalCdl.pc"


/*   EXEC SQL INCLUDE SQLCA;
 */ 
#line 1 "/oracle/12.1.0/precomp/public/SQLCA.H"
/*
 * $Header: sqlca.h 24-apr-2003.12:50:58 mkandarp Exp $ sqlca.h 
 */

/* Copyright (c) 1985, 2003, Oracle Corporation.  All rights reserved.  */
 
/*
NAME
  SQLCA : SQL Communications Area.
FUNCTION
  Contains no code. Oracle fills in the SQLCA with status info
  during the execution of a SQL stmt.
NOTES
  **************************************************************
  ***                                                        ***
  *** This file is SOSD.  Porters must change the data types ***
  *** appropriately on their platform.  See notes/pcport.doc ***
  *** for more information.                                  ***
  ***                                                        ***
  **************************************************************

  If the symbol SQLCA_STORAGE_CLASS is defined, then the SQLCA
  will be defined to have this storage class. For example:
 
    #define SQLCA_STORAGE_CLASS extern
 
  will define the SQLCA as an extern.
 
  If the symbol SQLCA_INIT is defined, then the SQLCA will be
  statically initialized. Although this is not necessary in order
  to use the SQLCA, it is a good pgming practice not to have
  unitialized variables. However, some C compilers/OS's don't
  allow automatic variables to be init'd in this manner. Therefore,
  if you are INCLUDE'ing the SQLCA in a place where it would be
  an automatic AND your C compiler/OS doesn't allow this style
  of initialization, then SQLCA_INIT should be left undefined --
  all others can define SQLCA_INIT if they wish.

  If the symbol SQLCA_NONE is defined, then the SQLCA variable will
  not be defined at all.  The symbol SQLCA_NONE should not be defined
  in source modules that have embedded SQL.  However, source modules
  that have no embedded SQL, but need to manipulate a sqlca struct
  passed in as a parameter, can set the SQLCA_NONE symbol to avoid
  creation of an extraneous sqlca variable.
 
MODIFIED
    lvbcheng   07/31/98 -  long to int
    jbasu      12/12/94 -  Bug 217878: note this is an SOSD file
    losborne   08/11/92 -  No sqlca var if SQLCA_NONE macro set 
  Clare      12/06/84 - Ch SQLCA to not be an extern.
  Clare      10/21/85 - Add initialization.
  Bradbury   01/05/86 - Only initialize when SQLCA_INIT set
  Clare      06/12/86 - Add SQLCA_STORAGE_CLASS option.
*/
 
#ifndef SQLCA
#define SQLCA 1
 
struct   sqlca
         {
         /* ub1 */ char    sqlcaid[8];
         /* b4  */ int     sqlabc;
         /* b4  */ int     sqlcode;
         struct
           {
           /* ub2 */ unsigned short sqlerrml;
           /* ub1 */ char           sqlerrmc[70];
           } sqlerrm;
         /* ub1 */ char    sqlerrp[8];
         /* b4  */ int     sqlerrd[6];
         /* ub1 */ char    sqlwarn[8];
         /* ub1 */ char    sqlext[8];
         };

#ifndef SQLCA_NONE 
#ifdef   SQLCA_STORAGE_CLASS
SQLCA_STORAGE_CLASS struct sqlca sqlca
#else
         struct sqlca sqlca
#endif
 
#ifdef  SQLCA_INIT
         = {
         {'S', 'Q', 'L', 'C', 'A', ' ', ' ', ' '},
         sizeof(struct sqlca),
         0,
         { 0, {0}},
         {'N', 'O', 'T', ' ', 'S', 'E', 'T', ' '},
         {0, 0, 0, 0, 0, 0},
         {0, 0, 0, 0, 0, 0, 0, 0},
         {0, 0, 0, 0, 0, 0, 0, 0}
         }
#endif
         ;
#endif
 
#endif
 
/* end SQLCA */

#line 2146 "../../gral/src_db/CTransaccionalCdl.pc"

/////////////////////////////////////////////////////////
// Declaracion de datos para uso dentro de la funcion  //
/////////////////////////////////////////////////////////
  int resultado = GFI_ERRC_OK;
  long rowcount;
  char loc_status1[9];

/////////////////////////////////////////////////////////
//  Inicio de codigo                                   //
/////////////////////////////////////////////////////////
  strcpy( sql_clave_inst,  clave_inst);
  strcpy( sql_clave_sis,  clave_sis);
  strcpy( sql_clave_rast,  clave_rast);
  strcpy( sql_clave_tran,  clave_tran);
  strcpy( sql_fecha_recep,  fecha_recep);
  strcpy( sql_tipo,  tipo);
  strcpy( sql_folio_origen,  folio_origen);
  strcpy( sql_tipo_oper,  tipo_oper);
  strcpy( sql_banco_ord,  banco_ord);
  strcpy( sql_cve_banco_ord,  cve_banco_ord);
  strcpy( sql_nombre_ord,  nombre_ord);
  strcpy( sql_cuenta_ord,  cuenta_ord);
  strcpy( sql_banco_ben,  banco_ben);
  strcpy( sql_cve_banco_ben,  cve_banco_ben);
  strcpy( sql_nombre_ben,  nombre_ben);
  strcpy( sql_cuenta_ben,  cuenta_ben);
  strcpy( sql_observacion,  observacion);
  strcpy( sql_ref_concil,  ref_concil);
  strcpy( sql_ref_med,  ref_med);
  strcpy( sql_fecha_cap,  fecha_cap);
  strcpy( sql_fecha_venc,  fecha_venc);
  strcpy( sql_fecha_flujo,  fecha_flujo);
  strcpy( sql_fecha_reg,  fecha_reg);
  strcpy( sql_fecha_baja,  fecha_baja);
  strcpy( sql_status,  status);
  sql_importe = importe;
  loc_remanente = 0; // HGT 24-Jun-2010

  if (NIVEL_TRACE > 5)
    printf("PROGRAMADO con Clave de Rastreo: >%s<\n", sql_clave_rast);

  conttprog = 0;

  /* EXEC SQL SELECT COUNT(1)
             INTO :conttprog
             FROM TRANS_PROGRAMADO
            WHERE CLAVE_INST  = :sql_clave_inst
              AND CLAVE_SIS   = :sql_clave_sis
              AND CLAVE_RAST  = :sql_clave_rast
              AND CLAVE_TRAN  = :sql_clave_tran
              AND FECHA_RECEP = TO_DATE(:sql_fecha_recep, 'YYYYMMDD')
              AND TIPO        = :sql_tipo; */ 
#line 2198 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  struct sqlexd sqlstm;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqlvsn = 13;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.arrsiz = 26;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqladtp = &sqladt;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqltdsp = &sqltds;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.stmt = "select count(1) into :b0  from TRANS_PROGRAMADO where (((((\
CLAVE_INST=:b1 and CLAVE_SIS=:b2) and CLAVE_RAST=:b3) and CLAVE_TRAN=:b4) and \
FECHA_RECEP=TO_DATE(:b5,'YYYYMMDD')) and TIPO=:b6)";
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.iters = (unsigned int  )1;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.offset = (unsigned int  )3272;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.selerr = (unsigned short)1;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqlpfmem = (unsigned int  )0;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.cud = sqlcud0;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqlety = (unsigned short)4352;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.occurs = (unsigned int  )0;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstv[0] = (unsigned char  *)&conttprog;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstl[0] = (unsigned long )sizeof(int);
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhsts[0] = (         int  )0;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqindv[0] = (         short *)0;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqinds[0] = (         int  )0;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqharm[0] = (unsigned long )0;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqadto[0] = (unsigned short )0;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqtdso[0] = (unsigned short )0;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstv[1] = (unsigned char  *)sql_clave_inst;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstl[1] = (unsigned long )6;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhsts[1] = (         int  )0;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqindv[1] = (         short *)0;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqinds[1] = (         int  )0;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqharm[1] = (unsigned long )0;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqadto[1] = (unsigned short )0;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqtdso[1] = (unsigned short )0;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstv[2] = (unsigned char  *)sql_clave_sis;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstl[2] = (unsigned long )6;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhsts[2] = (         int  )0;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqindv[2] = (         short *)0;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqinds[2] = (         int  )0;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqharm[2] = (unsigned long )0;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqadto[2] = (unsigned short )0;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqtdso[2] = (unsigned short )0;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstv[3] = (unsigned char  *)sql_clave_rast;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstl[3] = (unsigned long )31;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhsts[3] = (         int  )0;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqindv[3] = (         short *)0;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqinds[3] = (         int  )0;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqharm[3] = (unsigned long )0;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqadto[3] = (unsigned short )0;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqtdso[3] = (unsigned short )0;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstv[4] = (unsigned char  *)sql_clave_tran;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstl[4] = (unsigned long )8;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhsts[4] = (         int  )0;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqindv[4] = (         short *)0;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqinds[4] = (         int  )0;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqharm[4] = (unsigned long )0;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqadto[4] = (unsigned short )0;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqtdso[4] = (unsigned short )0;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstv[5] = (unsigned char  *)sql_fecha_recep;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstl[5] = (unsigned long )40;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhsts[5] = (         int  )0;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqindv[5] = (         short *)0;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqinds[5] = (         int  )0;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqharm[5] = (unsigned long )0;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqadto[5] = (unsigned short )0;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqtdso[5] = (unsigned short )0;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstv[6] = (unsigned char  *)sql_tipo;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstl[6] = (unsigned long )2;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhsts[6] = (         int  )0;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqindv[6] = (         short *)0;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqinds[6] = (         int  )0;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqharm[6] = (unsigned long )0;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqadto[6] = (unsigned short )0;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqtdso[6] = (unsigned short )0;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqphsv = sqlstm.sqhstv;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqphsl = sqlstm.sqhstl;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqphss = sqlstm.sqhsts;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqpind = sqlstm.sqindv;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqpins = sqlstm.sqinds;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqparm = sqlstm.sqharm;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqparc = sqlstm.sqharc;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqpadto = sqlstm.sqadto;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqptdso = sqlstm.sqtdso;
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 2190 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 2198 "../../gral/src_db/CTransaccionalCdl.pc"


  if (conttprog > 0)
  { // 1prog
    loc_status[0] = '\0';  //loc_fecha_baja[0] = '\0'; //NULL

    /* EXEC SQL SELECT NVL(STATUS,' ') //, NVL(TO_CHAR(FECHA_BAJA, 'YYYYMMDD'),' ')
               INTO :loc_status //, :loc_fecha_baja
               FROM TRANS_PROGRAMADO
              WHERE CLAVE_INST  = :sql_clave_inst
                AND CLAVE_SIS   = :sql_clave_sis
                AND CLAVE_RAST  = :sql_clave_rast
                AND CLAVE_TRAN  = :sql_clave_tran
                AND FECHA_RECEP = TO_DATE(:sql_fecha_recep, 'YYYYMMDD')
                AND TIPO        = :sql_tipo
                AND FECHA_BAJA IS NULL; */ 
#line 2213 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    struct sqlexd sqlstm;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqlvsn = 13;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.arrsiz = 26;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqladtp = &sqladt;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqltdsp = &sqltds;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.stmt = "select NVL(STATUS,' ') into :b0  from TRANS_PROGRAMADO wh\
ere ((((((CLAVE_INST=:b1 and CLAVE_SIS=:b2) and CLAVE_RAST=:b3) and CLAVE_TRAN\
=:b4) and FECHA_RECEP=TO_DATE(:b5,'YYYYMMDD')) and TIPO=:b6) and FECHA_BAJA is\
 null )";
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.iters = (unsigned int  )1;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.offset = (unsigned int  )3315;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.selerr = (unsigned short)1;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqlpfmem = (unsigned int  )0;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.cud = sqlcud0;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqlety = (unsigned short)4352;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.occurs = (unsigned int  )0;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[0] = (unsigned char  *)loc_status;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[0] = (unsigned long )5;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[0] = (         int  )0;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[0] = (         short *)0;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[0] = (         int  )0;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[0] = (unsigned long )0;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[0] = (unsigned short )0;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[0] = (unsigned short )0;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[1] = (unsigned char  *)sql_clave_inst;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[1] = (unsigned long )6;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[1] = (         int  )0;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[1] = (         short *)0;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[1] = (         int  )0;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[1] = (unsigned long )0;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[1] = (unsigned short )0;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[1] = (unsigned short )0;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[2] = (unsigned char  *)sql_clave_sis;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[2] = (unsigned long )6;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[2] = (         int  )0;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[2] = (         short *)0;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[2] = (         int  )0;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[2] = (unsigned long )0;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[2] = (unsigned short )0;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[2] = (unsigned short )0;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[3] = (unsigned char  *)sql_clave_rast;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[3] = (unsigned long )31;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[3] = (         int  )0;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[3] = (         short *)0;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[3] = (         int  )0;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[3] = (unsigned long )0;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[3] = (unsigned short )0;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[3] = (unsigned short )0;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[4] = (unsigned char  *)sql_clave_tran;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[4] = (unsigned long )8;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[4] = (         int  )0;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[4] = (         short *)0;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[4] = (         int  )0;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[4] = (unsigned long )0;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[4] = (unsigned short )0;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[4] = (unsigned short )0;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[5] = (unsigned char  *)sql_fecha_recep;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[5] = (unsigned long )40;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[5] = (         int  )0;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[5] = (         short *)0;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[5] = (         int  )0;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[5] = (unsigned long )0;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[5] = (unsigned short )0;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[5] = (unsigned short )0;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[6] = (unsigned char  *)sql_tipo;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[6] = (unsigned long )2;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[6] = (         int  )0;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[6] = (         short *)0;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[6] = (         int  )0;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[6] = (unsigned long )0;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[6] = (unsigned short )0;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[6] = (unsigned short )0;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqphsv = sqlstm.sqhstv;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqphsl = sqlstm.sqhstl;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqphss = sqlstm.sqhsts;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqpind = sqlstm.sqindv;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqpins = sqlstm.sqinds;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqparm = sqlstm.sqharm;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqparc = sqlstm.sqharc;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqpadto = sqlstm.sqadto;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqptdso = sqlstm.sqtdso;
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 2204 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 2213 "../../gral/src_db/CTransaccionalCdl.pc"

                //AND ROWNUM      = 1; Propuesto por H.Martinez 19-May-2010

    resultado = Obtiene_Ref_Concil();

    if ((resultado == GFI_ERRC_OK) && (strlen(loc_status) > 0)) // HGT 19-Jul-2010
    { // 2prog
      sprintf(loc_status1, "P%sC",sql_tipo); //Gfi_StrRTrim(loc_fecha_baja);
      Gfi_StrRTrim(loc_status); // HGT 19-Jul-2010

      if (!strcmp(loc_status,loc_status1)) // HGT 19-Jul-2010 && (strlen(loc_fecha_baja) == 0))
      {
        if (NIVEL_TRACE > 5)
          cout << "PROGRAMADO YA CONCILIADO!" << endl;
      }
      else
      { // 3prog
        if (NIVEL_TRACE > 5)
          cout << "ACTUALIZANDO EL PROGRAMADO" << endl;

        /* EXEC SQL UPDATE TRANS_PROGRAMADO
                 SET  FOLIO_ORIGEN = :sql_folio_origen ,
                      TIPO_OPER = :sql_tipo_oper ,
                      BANCO_ORD = :sql_banco_ord ,
                      CVE_BANCO_ORD = :sql_cve_banco_ord ,
                      NOMBRE_ORD = :sql_nombre_ord ,
                      CUENTA_ORD = :sql_cuenta_ord ,
                      BANCO_BEN = :sql_banco_ben ,
                      CVE_BANCO_BEN = :sql_cve_banco_ben ,
                      NOMBRE_BEN = :sql_nombre_ben ,
                      CUENTA_BEN = :sql_cuenta_ben ,
                      IMPORTE = ROUND(:sql_importe,2) ,
                      STATUS  = 'P' || :sql_tipo ,
                      OBSERVACION = :sql_observacion ,
                      REF_CONCIL  = :ref_concilG,
                      REMANENTE = ROUND(:loc_remanente,2) ,
                      TIPO_CTA = NULL , //:sql_tipo_cta ,
                      REF_MED = :sql_ref_med ,
                      FECHA_FLUJO = SYSDATE
                 WHERE CLAVE_INST  = :sql_clave_inst
                   AND CLAVE_SIS   = :sql_clave_sis
                   AND CLAVE_RAST  = :sql_clave_rast
                   AND CLAVE_TRAN  = :sql_clave_tran
                   AND FECHA_RECEP = TO_DATE(:sql_fecha_recep, 'YYYYMMDD')
                   AND TIPO        = :sql_tipo; */ 
#line 2257 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        struct sqlexd sqlstm;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqlvsn = 13;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.arrsiz = 26;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqladtp = &sqladt;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqltdsp = &sqltds;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.stmt = "update TRANS_PROGRAMADO  set FOLIO_ORIGEN=:b0,TIPO_OP\
ER=:b1,BANCO_ORD=:b2,CVE_BANCO_ORD=:b3,NOMBRE_ORD=:b4,CUENTA_ORD=:b5,BANCO_BEN\
=:b6,CVE_BANCO_BEN=:b7,NOMBRE_BEN=:b8,CUENTA_BEN=:b9,IMPORTE=ROUND(:b10,2),STA\
TUS=('P'||:b11),OBSERVACION=:b12,REF_CONCIL=:b13,REMANENTE=ROUND(:b14,2),TIPO_\
CTA=null ,REF_MED=:b15,FECHA_FLUJO=SYSDATE where (((((CLAVE_INST=:b16 and CLAV\
E_SIS=:b17) and CLAVE_RAST=:b18) and CLAVE_TRAN=:b19) and FECHA_RECEP=TO_DATE(\
:b20,'YYYYMMDD')) and TIPO=:b11)";
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.iters = (unsigned int  )1;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.offset = (unsigned int  )3358;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.cud = sqlcud0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqlety = (unsigned short)4352;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.occurs = (unsigned int  )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[0] = (unsigned char  *)sql_folio_origen;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[0] = (unsigned long )10;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[0] = (         int  )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[0] = (         short *)0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[0] = (         int  )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[0] = (unsigned long )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[0] = (unsigned short )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[0] = (unsigned short )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[1] = (unsigned char  *)sql_tipo_oper;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[1] = (unsigned long )5;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[1] = (         int  )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[1] = (         short *)0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[1] = (         int  )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[1] = (unsigned long )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[1] = (unsigned short )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[1] = (unsigned short )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[2] = (unsigned char  *)sql_banco_ord;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[2] = (unsigned long )7;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[2] = (         int  )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[2] = (         short *)0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[2] = (         int  )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[2] = (unsigned long )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[2] = (unsigned short )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[2] = (unsigned short )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[3] = (unsigned char  *)sql_cve_banco_ord;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[3] = (unsigned long )16;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[3] = (         int  )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[3] = (         short *)0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[3] = (         int  )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[3] = (unsigned long )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[3] = (unsigned short )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[3] = (unsigned short )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[4] = (unsigned char  *)sql_nombre_ord;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[4] = (unsigned long )52;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[4] = (         int  )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[4] = (         short *)0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[4] = (         int  )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[4] = (unsigned long )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[4] = (unsigned short )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[4] = (unsigned short )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[5] = (unsigned char  *)sql_cuenta_ord;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[5] = (unsigned long )21;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[5] = (         int  )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[5] = (         short *)0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[5] = (         int  )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[5] = (unsigned long )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[5] = (unsigned short )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[5] = (unsigned short )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[6] = (unsigned char  *)sql_banco_ben;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[6] = (unsigned long )7;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[6] = (         int  )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[6] = (         short *)0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[6] = (         int  )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[6] = (unsigned long )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[6] = (unsigned short )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[6] = (unsigned short )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[7] = (unsigned char  *)sql_cve_banco_ben;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[7] = (unsigned long )16;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[7] = (         int  )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[7] = (         short *)0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[7] = (         int  )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[7] = (unsigned long )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[7] = (unsigned short )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[7] = (unsigned short )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[8] = (unsigned char  *)sql_nombre_ben;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[8] = (unsigned long )52;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[8] = (         int  )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[8] = (         short *)0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[8] = (         int  )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[8] = (unsigned long )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[8] = (unsigned short )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[8] = (unsigned short )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[9] = (unsigned char  *)sql_cuenta_ben;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[9] = (unsigned long )21;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[9] = (         int  )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[9] = (         short *)0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[9] = (         int  )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[9] = (unsigned long )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[9] = (unsigned short )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[9] = (unsigned short )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[10] = (unsigned char  *)&sql_importe;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[10] = (unsigned long )sizeof(double);
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[10] = (         int  )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[10] = (         short *)0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[10] = (         int  )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[10] = (unsigned long )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[10] = (unsigned short )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[10] = (unsigned short )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[11] = (unsigned char  *)sql_tipo;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[11] = (unsigned long )2;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[11] = (         int  )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[11] = (         short *)0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[11] = (         int  )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[11] = (unsigned long )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[11] = (unsigned short )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[11] = (unsigned short )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[12] = (unsigned char  *)sql_observacion;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[12] = (unsigned long )50;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[12] = (         int  )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[12] = (         short *)0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[12] = (         int  )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[12] = (unsigned long )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[12] = (unsigned short )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[12] = (unsigned short )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[13] = (unsigned char  *)ref_concilG;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[13] = (unsigned long )20;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[13] = (         int  )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[13] = (         short *)0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[13] = (         int  )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[13] = (unsigned long )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[13] = (unsigned short )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[13] = (unsigned short )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[14] = (unsigned char  *)&loc_remanente;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[14] = (unsigned long )sizeof(double);
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[14] = (         int  )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[14] = (         short *)0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[14] = (         int  )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[14] = (unsigned long )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[14] = (unsigned short )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[14] = (unsigned short )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[15] = (unsigned char  *)sql_ref_med;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[15] = (unsigned long )18;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[15] = (         int  )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[15] = (         short *)0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[15] = (         int  )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[15] = (unsigned long )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[15] = (unsigned short )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[15] = (unsigned short )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[16] = (unsigned char  *)sql_clave_inst;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[16] = (unsigned long )6;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[16] = (         int  )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[16] = (         short *)0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[16] = (         int  )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[16] = (unsigned long )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[16] = (unsigned short )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[16] = (unsigned short )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[17] = (unsigned char  *)sql_clave_sis;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[17] = (unsigned long )6;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[17] = (         int  )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[17] = (         short *)0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[17] = (         int  )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[17] = (unsigned long )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[17] = (unsigned short )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[17] = (unsigned short )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[18] = (unsigned char  *)sql_clave_rast;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[18] = (unsigned long )31;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[18] = (         int  )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[18] = (         short *)0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[18] = (         int  )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[18] = (unsigned long )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[18] = (unsigned short )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[18] = (unsigned short )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[19] = (unsigned char  *)sql_clave_tran;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[19] = (unsigned long )8;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[19] = (         int  )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[19] = (         short *)0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[19] = (         int  )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[19] = (unsigned long )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[19] = (unsigned short )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[19] = (unsigned short )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[20] = (unsigned char  *)sql_fecha_recep;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[20] = (unsigned long )40;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[20] = (         int  )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[20] = (         short *)0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[20] = (         int  )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[20] = (unsigned long )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[20] = (unsigned short )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[20] = (unsigned short )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstv[21] = (unsigned char  *)sql_tipo;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhstl[21] = (unsigned long )2;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqhsts[21] = (         int  )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqindv[21] = (         short *)0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqinds[21] = (         int  )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqharm[21] = (unsigned long )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqadto[21] = (unsigned short )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqtdso[21] = (unsigned short )0;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqphsv = sqlstm.sqhstv;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqphsl = sqlstm.sqhstl;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqphss = sqlstm.sqhsts;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqpind = sqlstm.sqindv;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqpins = sqlstm.sqinds;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqparm = sqlstm.sqharm;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqparc = sqlstm.sqharc;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqpadto = sqlstm.sqadto;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlstm.sqptdso = sqlstm.sqtdso;
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
        sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 2233 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 2257 "../../gral/src_db/CTransaccionalCdl.pc"


        resultado = ChecaErrorOracle (0, 1, &rowcount);
        if (resultado != GFI_ERRC_OK)
        {
          if (NIVEL_TRACE > 5)
            cout << "ERROR AL ACTUALIZAR EN TRANS_PROGRAMADO!" << endl;

          strcpy(err_desc,"P0003");
        }
      } // 3prog
    } // 2prog
  } // 1prog
  else
  { // 4prog
    if (NIVEL_TRACE > 5)
      cout << "NO EXISTE EL PROGRAMADO, LO INSERTA..." << endl;

    sprintf(loc_status2, "P%s",sql_tipo);

    /* Comentariado en package version Productiva 16-Jul-2010
    if ((!strcmp(sql_clave_sis,"SIDV")) && (strncmp(sql_clave_tran,"4",1)))
    { // 5prog
      strcpy(sql_insth,sql_clave_inst); strcpy(sql_sish,sql_clave_sis); strcpy(sql_nmph,"LIMITE");
      loc_limite=100;
      EXEC SQL OPEN OBTENPARAM;
      while(1)
      {
          EXEC SQL FETCH OBTENPARAM INTO :sql_valor;
          resultado = ChecaErrorOracle (0, 1, &rowcount);
          if (resultado != GFI_ERRC_OK)
            break;
          Gfi_StrRTrim(sql_valor);
          if (rowcount == 1)
            loc_limite = atof(sql_valor);
          else
            break;
      }
      EXEC SQL CLOSE OBTENPARAM;

      if (NIVEL_TRACE > 5)
        cout << "LIMITE : " << loc_limite << endl;
    } // 5prog

    loc_remanente = 0;  loc_simporte = 0;

    EXEC SQL SELECT NVL(SUM(NVL(IMPORTE,0)),0)
             INTO   :loc_simporte
             FROM   TRANS_PAGO
             WHERE	CLAVE_INST  = :sql_clave_inst
               AND	CLAVE_SIS   = :sql_clave_sis
               AND	CLAVE_TRAN  = :sql_clave_tran
               AND	FECHA_RECEP = TO_DATE(:sql_fecha_recep,'YYYYMMDD')
               AND	TIPO        = :sql_tipo
               AND	BANCO_ORD   = :sql_banco_ord
               AND	BANCO_BEN   = :sql_banco_ben
               AND  REF_MED     = :sql_ref_med
               //AND IMPORTE = IN_IMPORTE
               AND	FECHA_BAJA IS NULL
            	 AND STATUS = 'R' || :sql_tipo || 'NP';

    resultado = ChecaErrorOracle( 0, 1, &rowcount);
    if (resultado != GFI_ERRC_OK)
    {
      if (NIVEL_TRACE > 5)
        cout << "ERROR AL SUMAR IMPORTE PAGADO!" << endl;

      strcpy(err_desc,"P0004");
    }
    else if (loc_simporte > 0)
    { // 6prog
      if (NIVEL_TRACE > 5)
        cout << "ACTUALIZANDO EL REAL A CONCILIADO" << endl;

      EXEC SQL UPDATE TRANS_PAGO
                 SET  REF_CONCIL  = FOLIO_ORIGEN,
                      STATUS      = 'R' || TIPO || 'PC',
                      FECHA_FLUJO = SYSDATE
               WHERE	CLAVE_INST  = :sql_clave_inst
                 AND	CLAVE_SIS   = :sql_clave_sis
                 AND	CLAVE_TRAN  = :sql_clave_tran
                 AND	FECHA_RECEP = TO_DATE(:sql_fecha_recep,'YYYYMMDD')
                 AND	TIPO        = :sql_tipo
                 AND	BANCO_ORD   = :sql_banco_ord
                 AND	BANCO_BEN   = :sql_banco_ben
                 AND  REF_MED     = :sql_ref_med
                 AND	FECHA_BAJA IS NULL
            	   AND  STATUS = 'R' || :sql_tipo || 'NP';

      loc_remanente = loc_remanente + loc_simporte;
      loc_remanente = fabs(loc_remanente);
      loc_importe = fabs(sql_importe);

      if (NIVEL_TRACE > 9)
        printf ("TOTAL= %.2lf\n", loc_remanente );

      if ((loc_importe >= (loc_remanente - loc_limite)) &&  (loc_importe <= (loc_remanente + loc_limite)))
      {
        if (NIVEL_TRACE > 9)
          cout << "SE PUEDE CONCILIAR EL PROGRAMADO!" << endl;

        sprintf(loc_status2, "P%sC",sql_tipo);
      }

      resultado = Obtiene_Ref_Concil();

      if (resultado != GFI_ERRC_OK)
        return (resultado);
    } // 6prog      */

    /* EXEC SQL INSERT INTO TRANS_PROGRAMADO ( CLAVE_INST , CLAVE_SIS , CLAVE_RAST , CLAVE_TRAN ,
                                            FECHA_RECEP , TIPO , FOLIO_ORIGEN ,
                                            TIPO_OPER , BANCO_ORD , CVE_BANCO_ORD , NOMBRE_ORD ,
                                            CUENTA_ORD , BANCO_BEN , CVE_BANCO_BEN , NOMBRE_BEN ,
                                            CUENTA_BEN , IMPORTE , STATUS , OBSERVACION , REF_CONCIL ,
                                            REF_MED , FECHA_CAP , FECHA_VENC ,
                                            FECHA_FLUJO , FECHA_REG , FECHA_BAJA )
             VALUES( :sql_clave_inst , :sql_clave_sis , :sql_clave_rast , :sql_clave_tran ,
                     TO_DATE(:sql_fecha_recep,'YYYYMMDD')  , :sql_tipo , :sql_folio_origen ,
                     :sql_tipo_oper , :sql_banco_ord , :sql_cve_banco_ord , :sql_nombre_ord ,
                     :sql_cuenta_ord , :sql_banco_ben , :sql_cve_banco_ben , :sql_nombre_ben ,
                     :sql_cuenta_ben , :sql_importe , :loc_status2 , :sql_observacion , :ref_concilG ,
                     :sql_ref_med , TO_DATE(:sql_fecha_cap,'YYYYMMDD HH24:MI:SS') , :sql_fecha_venc ,
                     :sql_fecha_flujo , :sql_fecha_reg , :sql_fecha_baja ); */ 
#line 2380 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    struct sqlexd sqlstm;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqlvsn = 13;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.arrsiz = 26;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqladtp = &sqladt;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqltdsp = &sqltds;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.stmt = "insert into TRANS_PROGRAMADO (CLAVE_INST,CLAVE_SIS,CLAVE_\
RAST,CLAVE_TRAN,FECHA_RECEP,TIPO,FOLIO_ORIGEN,TIPO_OPER,BANCO_ORD,CVE_BANCO_OR\
D,NOMBRE_ORD,CUENTA_ORD,BANCO_BEN,CVE_BANCO_BEN,NOMBRE_BEN,CUENTA_BEN,IMPORTE,\
STATUS,OBSERVACION,REF_CONCIL,REF_MED,FECHA_CAP,FECHA_VENC,FECHA_FLUJO,FECHA_R\
EG,FECHA_BAJA) values (:b0,:b1,:b2,:b3,TO_DATE(:b4,'YYYYMMDD'),:b5,:b6,:b7,:b8\
,:b9,:b10,:b11,:b12,:b13,:b14,:b15,:b16,:b17,:b18,:b19,:b20,TO_DATE(:b21,'YYYY\
MMDD HH24:MI:SS'),:b22,:b23,:b24,:b25)";
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.iters = (unsigned int  )1;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.offset = (unsigned int  )3461;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.cud = sqlcud0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqlety = (unsigned short)4352;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.occurs = (unsigned int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[0] = (unsigned char  *)sql_clave_inst;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[0] = (unsigned long )6;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[0] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[0] = (         short *)0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[0] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[0] = (unsigned long )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[0] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[0] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[1] = (unsigned char  *)sql_clave_sis;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[1] = (unsigned long )6;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[1] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[1] = (         short *)0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[1] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[1] = (unsigned long )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[1] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[1] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[2] = (unsigned char  *)sql_clave_rast;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[2] = (unsigned long )31;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[2] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[2] = (         short *)0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[2] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[2] = (unsigned long )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[2] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[2] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[3] = (unsigned char  *)sql_clave_tran;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[3] = (unsigned long )8;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[3] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[3] = (         short *)0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[3] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[3] = (unsigned long )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[3] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[3] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[4] = (unsigned char  *)sql_fecha_recep;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[4] = (unsigned long )40;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[4] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[4] = (         short *)0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[4] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[4] = (unsigned long )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[4] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[4] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[5] = (unsigned char  *)sql_tipo;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[5] = (unsigned long )2;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[5] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[5] = (         short *)0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[5] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[5] = (unsigned long )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[5] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[5] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[6] = (unsigned char  *)sql_folio_origen;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[6] = (unsigned long )10;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[6] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[6] = (         short *)0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[6] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[6] = (unsigned long )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[6] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[6] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[7] = (unsigned char  *)sql_tipo_oper;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[7] = (unsigned long )5;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[7] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[7] = (         short *)0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[7] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[7] = (unsigned long )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[7] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[7] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[8] = (unsigned char  *)sql_banco_ord;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[8] = (unsigned long )7;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[8] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[8] = (         short *)0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[8] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[8] = (unsigned long )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[8] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[8] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[9] = (unsigned char  *)sql_cve_banco_ord;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[9] = (unsigned long )16;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[9] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[9] = (         short *)0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[9] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[9] = (unsigned long )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[9] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[9] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[10] = (unsigned char  *)sql_nombre_ord;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[10] = (unsigned long )52;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[10] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[10] = (         short *)0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[10] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[10] = (unsigned long )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[10] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[10] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[11] = (unsigned char  *)sql_cuenta_ord;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[11] = (unsigned long )21;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[11] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[11] = (         short *)0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[11] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[11] = (unsigned long )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[11] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[11] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[12] = (unsigned char  *)sql_banco_ben;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[12] = (unsigned long )7;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[12] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[12] = (         short *)0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[12] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[12] = (unsigned long )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[12] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[12] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[13] = (unsigned char  *)sql_cve_banco_ben;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[13] = (unsigned long )16;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[13] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[13] = (         short *)0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[13] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[13] = (unsigned long )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[13] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[13] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[14] = (unsigned char  *)sql_nombre_ben;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[14] = (unsigned long )52;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[14] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[14] = (         short *)0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[14] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[14] = (unsigned long )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[14] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[14] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[15] = (unsigned char  *)sql_cuenta_ben;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[15] = (unsigned long )21;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[15] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[15] = (         short *)0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[15] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[15] = (unsigned long )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[15] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[15] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[16] = (unsigned char  *)&sql_importe;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[16] = (unsigned long )sizeof(double);
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[16] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[16] = (         short *)0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[16] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[16] = (unsigned long )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[16] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[16] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[17] = (unsigned char  *)loc_status2;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[17] = (unsigned long )9;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[17] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[17] = (         short *)0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[17] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[17] = (unsigned long )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[17] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[17] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[18] = (unsigned char  *)sql_observacion;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[18] = (unsigned long )50;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[18] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[18] = (         short *)0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[18] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[18] = (unsigned long )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[18] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[18] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[19] = (unsigned char  *)ref_concilG;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[19] = (unsigned long )20;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[19] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[19] = (         short *)0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[19] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[19] = (unsigned long )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[19] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[19] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[20] = (unsigned char  *)sql_ref_med;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[20] = (unsigned long )18;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[20] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[20] = (         short *)0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[20] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[20] = (unsigned long )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[20] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[20] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[21] = (unsigned char  *)sql_fecha_cap;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[21] = (unsigned long )40;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[21] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[21] = (         short *)0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[21] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[21] = (unsigned long )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[21] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[21] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[22] = (unsigned char  *)sql_fecha_venc;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[22] = (unsigned long )18;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[22] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[22] = (         short *)0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[22] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[22] = (unsigned long )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[22] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[22] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[23] = (unsigned char  *)sql_fecha_flujo;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[23] = (unsigned long )18;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[23] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[23] = (         short *)0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[23] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[23] = (unsigned long )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[23] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[23] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[24] = (unsigned char  *)sql_fecha_reg;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[24] = (unsigned long )18;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[24] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[24] = (         short *)0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[24] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[24] = (unsigned long )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[24] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[24] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[25] = (unsigned char  *)sql_fecha_baja;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[25] = (unsigned long )18;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[25] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[25] = (         short *)0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[25] = (         int  )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[25] = (unsigned long )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[25] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[25] = (unsigned short )0;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqphsv = sqlstm.sqhstv;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqphsl = sqlstm.sqhstl;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqphss = sqlstm.sqhsts;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqpind = sqlstm.sqindv;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqpins = sqlstm.sqinds;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqparm = sqlstm.sqharm;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqparc = sqlstm.sqharc;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqpadto = sqlstm.sqadto;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqptdso = sqlstm.sqtdso;
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 2367 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 2380 "../../gral/src_db/CTransaccionalCdl.pc"


    resultado = ChecaErrorOracle( 0, 1, &rowcount);
    if (resultado != GFI_ERRC_OK)
    {
      if (NIVEL_TRACE > 5)
        cout << "ERROR AL INSERTAR EN TRANS_PROGRAMADO!" << endl;

      strcpy(err_desc,"P0005");
    }
  } // 4prog

  if (NIVEL_TRACE > 5)
    cout << "TERMINO PROGRAMADO" << endl;

  return( resultado);
} // End

/////////////////////////////////////////////////////////
//  Funcion:  ProcesaProgBaja                          //
/////////////////////////////////////////////////////////
int  ProcesaProgBaja( char *clave_inst,  char *clave_sis, char *clave_rast, char *clave_tran,
                      char *fecha_recep, char *tipo,      char *status )
{ // Ini
  /* EXEC SQL BEGIN DECLARE SECTION; */ 
#line 2404 "../../gral/src_db/CTransaccionalCdl.pc"

  char sql_clave_inst[10];
  char sql_clave_sis[10];
  char sql_clave_rast[31];
  char sql_clave_tran[10];
  char sql_fecha_recep[10];
  char sql_tipo[2];
  char sql_status[5];
  char loc_status[5];

  int  contpro;
  /* EXEC SQL END DECLARE SECTION; */ 
#line 2415 "../../gral/src_db/CTransaccionalCdl.pc"


/*   EXEC SQL INCLUDE SQLCA;
 */ 
#line 1 "/oracle/12.1.0/precomp/public/SQLCA.H"
/*
 * $Header: sqlca.h 24-apr-2003.12:50:58 mkandarp Exp $ sqlca.h 
 */

/* Copyright (c) 1985, 2003, Oracle Corporation.  All rights reserved.  */
 
/*
NAME
  SQLCA : SQL Communications Area.
FUNCTION
  Contains no code. Oracle fills in the SQLCA with status info
  during the execution of a SQL stmt.
NOTES
  **************************************************************
  ***                                                        ***
  *** This file is SOSD.  Porters must change the data types ***
  *** appropriately on their platform.  See notes/pcport.doc ***
  *** for more information.                                  ***
  ***                                                        ***
  **************************************************************

  If the symbol SQLCA_STORAGE_CLASS is defined, then the SQLCA
  will be defined to have this storage class. For example:
 
    #define SQLCA_STORAGE_CLASS extern
 
  will define the SQLCA as an extern.
 
  If the symbol SQLCA_INIT is defined, then the SQLCA will be
  statically initialized. Although this is not necessary in order
  to use the SQLCA, it is a good pgming practice not to have
  unitialized variables. However, some C compilers/OS's don't
  allow automatic variables to be init'd in this manner. Therefore,
  if you are INCLUDE'ing the SQLCA in a place where it would be
  an automatic AND your C compiler/OS doesn't allow this style
  of initialization, then SQLCA_INIT should be left undefined --
  all others can define SQLCA_INIT if they wish.

  If the symbol SQLCA_NONE is defined, then the SQLCA variable will
  not be defined at all.  The symbol SQLCA_NONE should not be defined
  in source modules that have embedded SQL.  However, source modules
  that have no embedded SQL, but need to manipulate a sqlca struct
  passed in as a parameter, can set the SQLCA_NONE symbol to avoid
  creation of an extraneous sqlca variable.
 
MODIFIED
    lvbcheng   07/31/98 -  long to int
    jbasu      12/12/94 -  Bug 217878: note this is an SOSD file
    losborne   08/11/92 -  No sqlca var if SQLCA_NONE macro set 
  Clare      12/06/84 - Ch SQLCA to not be an extern.
  Clare      10/21/85 - Add initialization.
  Bradbury   01/05/86 - Only initialize when SQLCA_INIT set
  Clare      06/12/86 - Add SQLCA_STORAGE_CLASS option.
*/
 
#ifndef SQLCA
#define SQLCA 1
 
struct   sqlca
         {
         /* ub1 */ char    sqlcaid[8];
         /* b4  */ int     sqlabc;
         /* b4  */ int     sqlcode;
         struct
           {
           /* ub2 */ unsigned short sqlerrml;
           /* ub1 */ char           sqlerrmc[70];
           } sqlerrm;
         /* ub1 */ char    sqlerrp[8];
         /* b4  */ int     sqlerrd[6];
         /* ub1 */ char    sqlwarn[8];
         /* ub1 */ char    sqlext[8];
         };

#ifndef SQLCA_NONE 
#ifdef   SQLCA_STORAGE_CLASS
SQLCA_STORAGE_CLASS struct sqlca sqlca
#else
         struct sqlca sqlca
#endif
 
#ifdef  SQLCA_INIT
         = {
         {'S', 'Q', 'L', 'C', 'A', ' ', ' ', ' '},
         sizeof(struct sqlca),
         0,
         { 0, {0}},
         {'N', 'O', 'T', ' ', 'S', 'E', 'T', ' '},
         {0, 0, 0, 0, 0, 0},
         {0, 0, 0, 0, 0, 0, 0, 0},
         {0, 0, 0, 0, 0, 0, 0, 0}
         }
#endif
         ;
#endif
 
#endif
 
/* end SQLCA */

#line 2418 "../../gral/src_db/CTransaccionalCdl.pc"

/////////////////////////////////////////////////////////
// Declaracion de datos para uso dentro de la funcion  //
/////////////////////////////////////////////////////////
  int resultado = GFI_ERRC_OK;
  long rowcount;
  char loc_status1[9];

/////////////////////////////////////////////////////////
//  Inicio de codigo                                   //
/////////////////////////////////////////////////////////
  strcpy( sql_clave_inst,  clave_inst);
  strcpy( sql_clave_sis,   clave_sis);
  strcpy( sql_clave_rast,  clave_rast);
  strcpy( sql_clave_tran,  clave_tran);
  strcpy( sql_fecha_recep, fecha_recep);
  strcpy( sql_tipo,        tipo);
  strcpy( sql_status,      status);

  if (NIVEL_TRACE > 5)
    printf("BAJA DE PROGRAMADO con Clave de Rastreo: >%s<\n", sql_clave_rast);

  contpro = 0;

  /* EXEC SQL SELECT COUNT(1)
             INTO :contpro
             FROM TRANS_PROGRAMADO
            WHERE CLAVE_INST  = :sql_clave_inst
              AND CLAVE_SIS   = :sql_clave_sis
              AND CLAVE_RAST  = :sql_clave_rast
              AND CLAVE_TRAN  = :sql_clave_tran
              AND FECHA_RECEP = TO_DATE(:sql_fecha_recep, 'YYYYMMDD')
              AND TIPO        = :sql_tipo; */ 
#line 2450 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  struct sqlexd sqlstm;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqlvsn = 13;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.arrsiz = 26;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqladtp = &sqladt;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqltdsp = &sqltds;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.stmt = "select count(1) into :b0  from TRANS_PROGRAMADO where (((((\
CLAVE_INST=:b1 and CLAVE_SIS=:b2) and CLAVE_RAST=:b3) and CLAVE_TRAN=:b4) and \
FECHA_RECEP=TO_DATE(:b5,'YYYYMMDD')) and TIPO=:b6)";
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.iters = (unsigned int  )1;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.offset = (unsigned int  )3580;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.selerr = (unsigned short)1;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqlpfmem = (unsigned int  )0;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.cud = sqlcud0;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqlety = (unsigned short)4352;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.occurs = (unsigned int  )0;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstv[0] = (unsigned char  *)&contpro;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstl[0] = (unsigned long )sizeof(int);
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhsts[0] = (         int  )0;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqindv[0] = (         short *)0;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqinds[0] = (         int  )0;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqharm[0] = (unsigned long )0;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqadto[0] = (unsigned short )0;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqtdso[0] = (unsigned short )0;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstv[1] = (unsigned char  *)sql_clave_inst;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstl[1] = (unsigned long )10;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhsts[1] = (         int  )0;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqindv[1] = (         short *)0;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqinds[1] = (         int  )0;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqharm[1] = (unsigned long )0;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqadto[1] = (unsigned short )0;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqtdso[1] = (unsigned short )0;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstv[2] = (unsigned char  *)sql_clave_sis;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstl[2] = (unsigned long )10;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhsts[2] = (         int  )0;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqindv[2] = (         short *)0;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqinds[2] = (         int  )0;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqharm[2] = (unsigned long )0;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqadto[2] = (unsigned short )0;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqtdso[2] = (unsigned short )0;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstv[3] = (unsigned char  *)sql_clave_rast;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstl[3] = (unsigned long )31;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhsts[3] = (         int  )0;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqindv[3] = (         short *)0;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqinds[3] = (         int  )0;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqharm[3] = (unsigned long )0;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqadto[3] = (unsigned short )0;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqtdso[3] = (unsigned short )0;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstv[4] = (unsigned char  *)sql_clave_tran;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstl[4] = (unsigned long )10;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhsts[4] = (         int  )0;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqindv[4] = (         short *)0;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqinds[4] = (         int  )0;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqharm[4] = (unsigned long )0;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqadto[4] = (unsigned short )0;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqtdso[4] = (unsigned short )0;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstv[5] = (unsigned char  *)sql_fecha_recep;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstl[5] = (unsigned long )10;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhsts[5] = (         int  )0;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqindv[5] = (         short *)0;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqinds[5] = (         int  )0;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqharm[5] = (unsigned long )0;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqadto[5] = (unsigned short )0;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqtdso[5] = (unsigned short )0;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstv[6] = (unsigned char  *)sql_tipo;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstl[6] = (unsigned long )2;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhsts[6] = (         int  )0;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqindv[6] = (         short *)0;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqinds[6] = (         int  )0;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqharm[6] = (unsigned long )0;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqadto[6] = (unsigned short )0;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqtdso[6] = (unsigned short )0;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqphsv = sqlstm.sqhstv;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqphsl = sqlstm.sqhstl;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqphss = sqlstm.sqhsts;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqpind = sqlstm.sqindv;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqpins = sqlstm.sqinds;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqparm = sqlstm.sqharm;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqparc = sqlstm.sqharc;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqpadto = sqlstm.sqadto;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqptdso = sqlstm.sqtdso;
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 2442 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 2450 "../../gral/src_db/CTransaccionalCdl.pc"


  if (contpro > 0)
  { // 1progbaja

    loc_status[0] = '\0';

    /* EXEC SQL SELECT NVL(STATUS,' ')
               INTO :loc_status
               FROM TRANS_PROGRAMADO
              WHERE CLAVE_INST  = :sql_clave_inst
                AND CLAVE_SIS   = :sql_clave_sis
                AND CLAVE_RAST  = :sql_clave_rast
                AND CLAVE_TRAN  = :sql_clave_tran
                AND FECHA_RECEP = TO_DATE(:sql_fecha_recep, 'YYYYMMDD')
                AND TIPO        = :sql_tipo; */ 
#line 2465 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    struct sqlexd sqlstm;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqlvsn = 13;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.arrsiz = 26;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqladtp = &sqladt;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqltdsp = &sqltds;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.stmt = "select NVL(STATUS,' ') into :b0  from TRANS_PROGRAMADO wh\
ere (((((CLAVE_INST=:b1 and CLAVE_SIS=:b2) and CLAVE_RAST=:b3) and CLAVE_TRAN=\
:b4) and FECHA_RECEP=TO_DATE(:b5,'YYYYMMDD')) and TIPO=:b6)";
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.iters = (unsigned int  )1;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.offset = (unsigned int  )3623;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.selerr = (unsigned short)1;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqlpfmem = (unsigned int  )0;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.cud = sqlcud0;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqlety = (unsigned short)4352;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.occurs = (unsigned int  )0;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[0] = (unsigned char  *)loc_status;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[0] = (unsigned long )5;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[0] = (         int  )0;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[0] = (         short *)0;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[0] = (         int  )0;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[0] = (unsigned long )0;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[0] = (unsigned short )0;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[0] = (unsigned short )0;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[1] = (unsigned char  *)sql_clave_inst;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[1] = (unsigned long )10;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[1] = (         int  )0;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[1] = (         short *)0;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[1] = (         int  )0;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[1] = (unsigned long )0;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[1] = (unsigned short )0;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[1] = (unsigned short )0;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[2] = (unsigned char  *)sql_clave_sis;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[2] = (unsigned long )10;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[2] = (         int  )0;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[2] = (         short *)0;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[2] = (         int  )0;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[2] = (unsigned long )0;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[2] = (unsigned short )0;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[2] = (unsigned short )0;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[3] = (unsigned char  *)sql_clave_rast;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[3] = (unsigned long )31;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[3] = (         int  )0;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[3] = (         short *)0;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[3] = (         int  )0;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[3] = (unsigned long )0;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[3] = (unsigned short )0;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[3] = (unsigned short )0;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[4] = (unsigned char  *)sql_clave_tran;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[4] = (unsigned long )10;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[4] = (         int  )0;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[4] = (         short *)0;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[4] = (         int  )0;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[4] = (unsigned long )0;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[4] = (unsigned short )0;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[4] = (unsigned short )0;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[5] = (unsigned char  *)sql_fecha_recep;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[5] = (unsigned long )10;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[5] = (         int  )0;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[5] = (         short *)0;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[5] = (         int  )0;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[5] = (unsigned long )0;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[5] = (unsigned short )0;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[5] = (unsigned short )0;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[6] = (unsigned char  *)sql_tipo;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[6] = (unsigned long )2;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[6] = (         int  )0;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[6] = (         short *)0;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[6] = (         int  )0;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[6] = (unsigned long )0;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[6] = (unsigned short )0;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[6] = (unsigned short )0;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqphsv = sqlstm.sqhstv;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqphsl = sqlstm.sqhstl;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqphss = sqlstm.sqhsts;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqpind = sqlstm.sqindv;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqpins = sqlstm.sqinds;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqparm = sqlstm.sqharm;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqparc = sqlstm.sqharc;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqpadto = sqlstm.sqadto;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqptdso = sqlstm.sqtdso;
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 2457 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 2465 "../../gral/src_db/CTransaccionalCdl.pc"

                //AND ROWNUM      = 1;    Propuesto por H.Martinez 19-May-2010

    sprintf(loc_status1, "P%sC",sql_tipo);
    if (strcmp(loc_status,loc_status1))
    { // 2progbaja
      if (NIVEL_TRACE > 5)
        cout << "DANDO DE BAJA EL PROGRAMADO" << endl;

      /* EXEC SQL UPDATE TRANS_PROGRAMADO
               SET    STATUS = 'P' || :sql_tipo || 'B' ,
                      //OBSERVACION = :sql_observacion ,
                      FECHA_BAJA  = SYSDATE
               WHERE  CLAVE_INST  = :sql_clave_inst
                 AND  CLAVE_SIS   = :sql_clave_sis
                 AND  CLAVE_RAST  = :sql_clave_rast
                 AND  CLAVE_TRAN  = :sql_clave_tran
                 AND  FECHA_RECEP = TO_DATE(:sql_fecha_recep, 'YYYYMMDD')
                 AND  TIPO        = :sql_tipo; */ 
#line 2483 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      struct sqlexd sqlstm;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqlvsn = 13;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.arrsiz = 26;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqladtp = &sqladt;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqltdsp = &sqltds;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.stmt = "update TRANS_PROGRAMADO  set STATUS=(('P'||:b0)||'B'),F\
ECHA_BAJA=SYSDATE where (((((CLAVE_INST=:b1 and CLAVE_SIS=:b2) and CLAVE_RAST=\
:b3) and CLAVE_TRAN=:b4) and FECHA_RECEP=TO_DATE(:b5,'YYYYMMDD')) and TIPO=:b0\
)";
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.iters = (unsigned int  )1;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.offset = (unsigned int  )3666;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.cud = sqlcud0;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqlety = (unsigned short)4352;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.occurs = (unsigned int  )0;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[0] = (unsigned char  *)sql_tipo;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[0] = (unsigned long )2;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[0] = (         int  )0;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[0] = (         short *)0;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[0] = (         int  )0;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[0] = (unsigned long )0;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[0] = (unsigned short )0;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[0] = (unsigned short )0;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[1] = (unsigned char  *)sql_clave_inst;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[1] = (unsigned long )10;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[1] = (         int  )0;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[1] = (         short *)0;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[1] = (         int  )0;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[1] = (unsigned long )0;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[1] = (unsigned short )0;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[1] = (unsigned short )0;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[2] = (unsigned char  *)sql_clave_sis;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[2] = (unsigned long )10;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[2] = (         int  )0;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[2] = (         short *)0;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[2] = (         int  )0;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[2] = (unsigned long )0;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[2] = (unsigned short )0;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[2] = (unsigned short )0;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[3] = (unsigned char  *)sql_clave_rast;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[3] = (unsigned long )31;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[3] = (         int  )0;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[3] = (         short *)0;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[3] = (         int  )0;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[3] = (unsigned long )0;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[3] = (unsigned short )0;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[3] = (unsigned short )0;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[4] = (unsigned char  *)sql_clave_tran;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[4] = (unsigned long )10;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[4] = (         int  )0;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[4] = (         short *)0;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[4] = (         int  )0;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[4] = (unsigned long )0;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[4] = (unsigned short )0;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[4] = (unsigned short )0;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[5] = (unsigned char  *)sql_fecha_recep;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[5] = (unsigned long )10;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[5] = (         int  )0;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[5] = (         short *)0;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[5] = (         int  )0;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[5] = (unsigned long )0;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[5] = (unsigned short )0;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[5] = (unsigned short )0;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[6] = (unsigned char  *)sql_tipo;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[6] = (unsigned long )2;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[6] = (         int  )0;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[6] = (         short *)0;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[6] = (         int  )0;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[6] = (unsigned long )0;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[6] = (unsigned short )0;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[6] = (unsigned short )0;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqphsv = sqlstm.sqhstv;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqphsl = sqlstm.sqhstl;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqphss = sqlstm.sqhsts;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqpind = sqlstm.sqindv;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqpins = sqlstm.sqinds;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqparm = sqlstm.sqharm;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqparc = sqlstm.sqharc;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqpadto = sqlstm.sqadto;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqptdso = sqlstm.sqtdso;
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 2474 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 2483 "../../gral/src_db/CTransaccionalCdl.pc"


      resultado = ChecaErrorOracle( 0, 1, &rowcount);
      if (resultado != GFI_ERRC_OK)
      {
        if (NIVEL_TRACE > 5)
          cout << "ERROR AL ACTUALIZAR EN TRANS_PROGRAMADO!" << endl;

        strcpy(err_desc,"P0006");
      }
    } // 2progbaja
  } // 1progbaja

  if (NIVEL_TRACE > 5)
    cout << "TERMINO BAJA DE PROGRAMADO" << endl;

  return( resultado);
} // End

/////////////////////////////////////////////////////////
//  Funcion:  ProcesaSubTipo                           //
/////////////////////////////////////////////////////////
int ProcesaSubTipo( char *clave_inst, char *clave_sis, char *clave_rast, char *clave_tran, char *fecha_recep,
                    char *tipo)
{
  /* EXEC SQL BEGIN DECLARE SECTION; */ 
#line 2508 "../../gral/src_db/CTransaccionalCdl.pc"

  char sql_clave_inst[10];
  char sql_clave_sis[10];
  char sql_clave_rast[31];
  char sql_clave_tran[10];
  char sql_fecha_recep[10];
  char sql_tipo[10];

  int  contpago, contpro;
  /* EXEC SQL END DECLARE SECTION; */ 
#line 2517 "../../gral/src_db/CTransaccionalCdl.pc"


/*   EXEC SQL INCLUDE SQLCA;
 */ 
#line 1 "/oracle/12.1.0/precomp/public/SQLCA.H"
/*
 * $Header: sqlca.h 24-apr-2003.12:50:58 mkandarp Exp $ sqlca.h 
 */

/* Copyright (c) 1985, 2003, Oracle Corporation.  All rights reserved.  */
 
/*
NAME
  SQLCA : SQL Communications Area.
FUNCTION
  Contains no code. Oracle fills in the SQLCA with status info
  during the execution of a SQL stmt.
NOTES
  **************************************************************
  ***                                                        ***
  *** This file is SOSD.  Porters must change the data types ***
  *** appropriately on their platform.  See notes/pcport.doc ***
  *** for more information.                                  ***
  ***                                                        ***
  **************************************************************

  If the symbol SQLCA_STORAGE_CLASS is defined, then the SQLCA
  will be defined to have this storage class. For example:
 
    #define SQLCA_STORAGE_CLASS extern
 
  will define the SQLCA as an extern.
 
  If the symbol SQLCA_INIT is defined, then the SQLCA will be
  statically initialized. Although this is not necessary in order
  to use the SQLCA, it is a good pgming practice not to have
  unitialized variables. However, some C compilers/OS's don't
  allow automatic variables to be init'd in this manner. Therefore,
  if you are INCLUDE'ing the SQLCA in a place where it would be
  an automatic AND your C compiler/OS doesn't allow this style
  of initialization, then SQLCA_INIT should be left undefined --
  all others can define SQLCA_INIT if they wish.

  If the symbol SQLCA_NONE is defined, then the SQLCA variable will
  not be defined at all.  The symbol SQLCA_NONE should not be defined
  in source modules that have embedded SQL.  However, source modules
  that have no embedded SQL, but need to manipulate a sqlca struct
  passed in as a parameter, can set the SQLCA_NONE symbol to avoid
  creation of an extraneous sqlca variable.
 
MODIFIED
    lvbcheng   07/31/98 -  long to int
    jbasu      12/12/94 -  Bug 217878: note this is an SOSD file
    losborne   08/11/92 -  No sqlca var if SQLCA_NONE macro set 
  Clare      12/06/84 - Ch SQLCA to not be an extern.
  Clare      10/21/85 - Add initialization.
  Bradbury   01/05/86 - Only initialize when SQLCA_INIT set
  Clare      06/12/86 - Add SQLCA_STORAGE_CLASS option.
*/
 
#ifndef SQLCA
#define SQLCA 1
 
struct   sqlca
         {
         /* ub1 */ char    sqlcaid[8];
         /* b4  */ int     sqlabc;
         /* b4  */ int     sqlcode;
         struct
           {
           /* ub2 */ unsigned short sqlerrml;
           /* ub1 */ char           sqlerrmc[70];
           } sqlerrm;
         /* ub1 */ char    sqlerrp[8];
         /* b4  */ int     sqlerrd[6];
         /* ub1 */ char    sqlwarn[8];
         /* ub1 */ char    sqlext[8];
         };

#ifndef SQLCA_NONE 
#ifdef   SQLCA_STORAGE_CLASS
SQLCA_STORAGE_CLASS struct sqlca sqlca
#else
         struct sqlca sqlca
#endif
 
#ifdef  SQLCA_INIT
         = {
         {'S', 'Q', 'L', 'C', 'A', ' ', ' ', ' '},
         sizeof(struct sqlca),
         0,
         { 0, {0}},
         {'N', 'O', 'T', ' ', 'S', 'E', 'T', ' '},
         {0, 0, 0, 0, 0, 0},
         {0, 0, 0, 0, 0, 0, 0, 0},
         {0, 0, 0, 0, 0, 0, 0, 0}
         }
#endif
         ;
#endif
 
#endif
 
/* end SQLCA */

#line 2520 "../../gral/src_db/CTransaccionalCdl.pc"

/***********************************************************************
     Declaracion de datos para uso dentro de la funcion
***********************************************************************/
  int resultado = GFI_ERRC_OK;
  long rowcount;

/***********************************************************************
     Inicio de codigo
***********************************************************************/
  if ( NIVEL_TRACE > 5 )
    printf("Inicio BAJA DE SALA DE ESPERA con clave de rastreo: %s\n",clave_rast);

  strcpy( sql_clave_inst,  clave_inst);
  strcpy( sql_clave_sis,   clave_sis);
  strcpy( sql_clave_rast,  clave_rast);
  strcpy( sql_clave_tran,  clave_tran);
  strcpy( sql_fecha_recep, fecha_recep);
  strcpy( sql_tipo,        tipo);

  // Busca la contraparte
  // Index: CLAVE_INST, CLAVE_SIS, CLAVE_RAST, CLAVE_TRAN, FECHA_RECEP, TIPO
  /* EXEC SQL SELECT COUNT(1)
             INTO :contpago
             FROM TRANS_PAGO
            WHERE CLAVE_INST  = :sql_clave_inst
              AND CLAVE_SIS   = :sql_clave_sis
              AND CLAVE_RAST  = :sql_clave_rast
              AND CLAVE_TRAN  = :sql_clave_tran
              AND FECHA_RECEP = TO_DATE(:sql_fecha_recep, 'YYYYMMDD')
              AND TIPO        = :sql_tipo; */ 
#line 2550 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  struct sqlexd sqlstm;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqlvsn = 13;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.arrsiz = 26;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqladtp = &sqladt;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqltdsp = &sqltds;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.stmt = "select count(1) into :b0  from TRANS_PAGO where (((((CLAVE_\
INST=:b1 and CLAVE_SIS=:b2) and CLAVE_RAST=:b3) and CLAVE_TRAN=:b4) and FECHA_\
RECEP=TO_DATE(:b5,'YYYYMMDD')) and TIPO=:b6)";
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.iters = (unsigned int  )1;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.offset = (unsigned int  )3709;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.selerr = (unsigned short)1;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqlpfmem = (unsigned int  )0;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.cud = sqlcud0;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqlety = (unsigned short)4352;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.occurs = (unsigned int  )0;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstv[0] = (unsigned char  *)&contpago;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstl[0] = (unsigned long )sizeof(int);
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhsts[0] = (         int  )0;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqindv[0] = (         short *)0;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqinds[0] = (         int  )0;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqharm[0] = (unsigned long )0;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqadto[0] = (unsigned short )0;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqtdso[0] = (unsigned short )0;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstv[1] = (unsigned char  *)sql_clave_inst;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstl[1] = (unsigned long )10;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhsts[1] = (         int  )0;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqindv[1] = (         short *)0;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqinds[1] = (         int  )0;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqharm[1] = (unsigned long )0;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqadto[1] = (unsigned short )0;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqtdso[1] = (unsigned short )0;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstv[2] = (unsigned char  *)sql_clave_sis;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstl[2] = (unsigned long )10;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhsts[2] = (         int  )0;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqindv[2] = (         short *)0;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqinds[2] = (         int  )0;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqharm[2] = (unsigned long )0;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqadto[2] = (unsigned short )0;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqtdso[2] = (unsigned short )0;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstv[3] = (unsigned char  *)sql_clave_rast;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstl[3] = (unsigned long )31;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhsts[3] = (         int  )0;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqindv[3] = (         short *)0;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqinds[3] = (         int  )0;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqharm[3] = (unsigned long )0;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqadto[3] = (unsigned short )0;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqtdso[3] = (unsigned short )0;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstv[4] = (unsigned char  *)sql_clave_tran;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstl[4] = (unsigned long )10;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhsts[4] = (         int  )0;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqindv[4] = (         short *)0;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqinds[4] = (         int  )0;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqharm[4] = (unsigned long )0;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqadto[4] = (unsigned short )0;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqtdso[4] = (unsigned short )0;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstv[5] = (unsigned char  *)sql_fecha_recep;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstl[5] = (unsigned long )10;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhsts[5] = (         int  )0;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqindv[5] = (         short *)0;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqinds[5] = (         int  )0;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqharm[5] = (unsigned long )0;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqadto[5] = (unsigned short )0;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqtdso[5] = (unsigned short )0;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstv[6] = (unsigned char  *)sql_tipo;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstl[6] = (unsigned long )10;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhsts[6] = (         int  )0;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqindv[6] = (         short *)0;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqinds[6] = (         int  )0;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqharm[6] = (unsigned long )0;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqadto[6] = (unsigned short )0;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqtdso[6] = (unsigned short )0;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqphsv = sqlstm.sqhstv;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqphsl = sqlstm.sqhstl;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqphss = sqlstm.sqhsts;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqpind = sqlstm.sqindv;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqpins = sqlstm.sqinds;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqparm = sqlstm.sqharm;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqparc = sqlstm.sqharc;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqpadto = sqlstm.sqadto;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqptdso = sqlstm.sqtdso;
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 2542 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 2550 "../../gral/src_db/CTransaccionalCdl.pc"


  if ( contpago > 0)
  { // 1subtipo
      /* EXEC SQL DELETE TRANS_PAGO
               WHERE CLAVE_INST  = :sql_clave_inst
                 AND CLAVE_SIS   = :sql_clave_sis
                 AND CLAVE_RAST  = :sql_clave_rast
                 AND CLAVE_TRAN  = :sql_clave_tran
                 AND FECHA_RECEP = TO_DATE(:sql_fecha_recep, 'YYYYMMDD')
                 AND TIPO        = :sql_tipo; */ 
#line 2560 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      struct sqlexd sqlstm;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqlvsn = 13;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.arrsiz = 26;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqladtp = &sqladt;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqltdsp = &sqltds;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.stmt = "delete  from TRANS_PAGO  where (((((CLAVE_INST=:b0 and \
CLAVE_SIS=:b1) and CLAVE_RAST=:b2) and CLAVE_TRAN=:b3) and FECHA_RECEP=TO_DATE\
(:b4,'YYYYMMDD')) and TIPO=:b5)";
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.iters = (unsigned int  )1;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.offset = (unsigned int  )3752;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.cud = sqlcud0;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqlety = (unsigned short)4352;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.occurs = (unsigned int  )0;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[0] = (unsigned char  *)sql_clave_inst;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[0] = (unsigned long )10;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[0] = (         int  )0;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[0] = (         short *)0;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[0] = (         int  )0;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[0] = (unsigned long )0;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[0] = (unsigned short )0;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[0] = (unsigned short )0;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[1] = (unsigned char  *)sql_clave_sis;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[1] = (unsigned long )10;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[1] = (         int  )0;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[1] = (         short *)0;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[1] = (         int  )0;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[1] = (unsigned long )0;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[1] = (unsigned short )0;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[1] = (unsigned short )0;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[2] = (unsigned char  *)sql_clave_rast;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[2] = (unsigned long )31;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[2] = (         int  )0;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[2] = (         short *)0;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[2] = (         int  )0;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[2] = (unsigned long )0;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[2] = (unsigned short )0;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[2] = (unsigned short )0;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[3] = (unsigned char  *)sql_clave_tran;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[3] = (unsigned long )10;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[3] = (         int  )0;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[3] = (         short *)0;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[3] = (         int  )0;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[3] = (unsigned long )0;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[3] = (unsigned short )0;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[3] = (unsigned short )0;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[4] = (unsigned char  *)sql_fecha_recep;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[4] = (unsigned long )10;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[4] = (         int  )0;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[4] = (         short *)0;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[4] = (         int  )0;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[4] = (unsigned long )0;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[4] = (unsigned short )0;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[4] = (unsigned short )0;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[5] = (unsigned char  *)sql_tipo;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[5] = (unsigned long )10;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[5] = (         int  )0;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[5] = (         short *)0;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[5] = (         int  )0;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[5] = (unsigned long )0;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[5] = (unsigned short )0;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[5] = (unsigned short )0;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqphsv = sqlstm.sqhstv;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqphsl = sqlstm.sqhstl;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqphss = sqlstm.sqhsts;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqpind = sqlstm.sqindv;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqpins = sqlstm.sqinds;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqparm = sqlstm.sqharm;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqparc = sqlstm.sqharc;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqpadto = sqlstm.sqadto;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqptdso = sqlstm.sqtdso;
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 2554 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 2560 "../../gral/src_db/CTransaccionalCdl.pc"


    resultado = ChecaErrorOracle( 0, 1, &rowcount);

    if (resultado != GFI_ERRC_OK)
    {
      if (NIVEL_TRACE > 5)
        cout << "ERROR AL ELIMINAR EN TRANS_PAGO!" << endl;

      strcpy(err_desc,"T0001");
    }

    // Busca la contraparte
    /* HGT 26-May-2010 - Inicio cambio Propuesto por H.Martinez 19-May-2010
    contpar = 0;

    EXEC SQL SELECT COUNT(1)
             INTO   :contpar
             FROM   ALLPARAMETER
             WHERE  CLAVE_INST = :sql_clave_inst
               AND  CLAVE_SIS  = :sql_clave_sis
               AND  NOMBRE     = 'CONTRAPARTE'
               AND  VALOR      = :sql_clave_tran;*/
    sprintf(llave_c,"|%s|",sql_clave_tran);

    if (strstr(contrapartes,llave_c) != 0) //if (contpar > 0) HGT 26-May-2010 Fin
    { // 2subtipo
      if (NIVEL_TRACE > 9)
        cout << "VERIFICA SI EXISTE LA CONTRAPARTE EN TRANS_PROGRAMADO" << endl;

      contpro = 0;

      /* EXEC SQL SELECT COUNT(1)
                 INTO :contpro
                 FROM TRANS_PROGRAMADO
                WHERE CLAVE_INST  = :sql_clave_inst
                  AND CLAVE_SIS   = :sql_clave_sis
                  AND CLAVE_RAST  = :sql_clave_rast
                  AND CLAVE_TRAN  = :sql_clave_tran
                  AND FECHA_RECEP = TO_DATE(:sql_fecha_recep, 'YYYYMMDD')
                  AND TIPO        = :sql_tipo; */ 
#line 2600 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      struct sqlexd sqlstm;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqlvsn = 13;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.arrsiz = 26;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqladtp = &sqladt;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqltdsp = &sqltds;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.stmt = "select count(1) into :b0  from TRANS_PROGRAMADO where (\
((((CLAVE_INST=:b1 and CLAVE_SIS=:b2) and CLAVE_RAST=:b3) and CLAVE_TRAN=:b4) \
and FECHA_RECEP=TO_DATE(:b5,'YYYYMMDD')) and TIPO=:b6)";
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.iters = (unsigned int  )1;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.offset = (unsigned int  )3791;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.selerr = (unsigned short)1;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqlpfmem = (unsigned int  )0;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.cud = sqlcud0;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqlety = (unsigned short)4352;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.occurs = (unsigned int  )0;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[0] = (unsigned char  *)&contpro;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[0] = (unsigned long )sizeof(int);
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[0] = (         int  )0;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[0] = (         short *)0;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[0] = (         int  )0;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[0] = (unsigned long )0;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[0] = (unsigned short )0;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[0] = (unsigned short )0;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[1] = (unsigned char  *)sql_clave_inst;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[1] = (unsigned long )10;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[1] = (         int  )0;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[1] = (         short *)0;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[1] = (         int  )0;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[1] = (unsigned long )0;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[1] = (unsigned short )0;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[1] = (unsigned short )0;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[2] = (unsigned char  *)sql_clave_sis;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[2] = (unsigned long )10;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[2] = (         int  )0;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[2] = (         short *)0;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[2] = (         int  )0;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[2] = (unsigned long )0;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[2] = (unsigned short )0;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[2] = (unsigned short )0;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[3] = (unsigned char  *)sql_clave_rast;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[3] = (unsigned long )31;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[3] = (         int  )0;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[3] = (         short *)0;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[3] = (         int  )0;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[3] = (unsigned long )0;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[3] = (unsigned short )0;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[3] = (unsigned short )0;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[4] = (unsigned char  *)sql_clave_tran;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[4] = (unsigned long )10;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[4] = (         int  )0;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[4] = (         short *)0;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[4] = (         int  )0;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[4] = (unsigned long )0;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[4] = (unsigned short )0;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[4] = (unsigned short )0;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[5] = (unsigned char  *)sql_fecha_recep;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[5] = (unsigned long )10;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[5] = (         int  )0;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[5] = (         short *)0;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[5] = (         int  )0;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[5] = (unsigned long )0;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[5] = (unsigned short )0;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[5] = (unsigned short )0;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstv[6] = (unsigned char  *)sql_tipo;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhstl[6] = (unsigned long )10;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqhsts[6] = (         int  )0;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqindv[6] = (         short *)0;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqinds[6] = (         int  )0;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqharm[6] = (unsigned long )0;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqadto[6] = (unsigned short )0;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqtdso[6] = (unsigned short )0;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqphsv = sqlstm.sqhstv;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqphsl = sqlstm.sqhstl;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqphss = sqlstm.sqhsts;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqpind = sqlstm.sqindv;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqpins = sqlstm.sqinds;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqparm = sqlstm.sqharm;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqparc = sqlstm.sqharc;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqpadto = sqlstm.sqadto;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlstm.sqptdso = sqlstm.sqtdso;
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
      sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 2592 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 2600 "../../gral/src_db/CTransaccionalCdl.pc"


      if (contpro > 0)
      { //3subtipo
        if (NIVEL_TRACE > 9)
          cout << "DANDO DE BAJA EL PROGRAMADO" << endl;

          /* EXEC SQL DELETE TRANS_PROGRAMADO
                   WHERE CLAVE_INST  = :sql_clave_inst
                     AND CLAVE_SIS   = :sql_clave_sis
                     AND CLAVE_RAST  = :sql_clave_rast
                     AND CLAVE_TRAN  = :sql_clave_tran
                     AND FECHA_RECEP = TO_DATE(:sql_fecha_recep, 'YYYYMMDD')
                     AND TIPO        = :sql_tipo; */ 
#line 2613 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          struct sqlexd sqlstm;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqlvsn = 13;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.arrsiz = 26;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqladtp = &sqladt;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqltdsp = &sqltds;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.stmt = "delete  from TRANS_PROGRAMADO  where (((((CLAVE_INS\
T=:b0 and CLAVE_SIS=:b1) and CLAVE_RAST=:b2) and CLAVE_TRAN=:b3) and FECHA_REC\
EP=TO_DATE(:b4,'YYYYMMDD')) and TIPO=:b5)";
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.iters = (unsigned int  )1;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.offset = (unsigned int  )3834;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.cud = sqlcud0;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqlety = (unsigned short)4352;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.occurs = (unsigned int  )0;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstv[0] = (unsigned char  *)sql_clave_inst;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstl[0] = (unsigned long )10;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhsts[0] = (         int  )0;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqindv[0] = (         short *)0;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqinds[0] = (         int  )0;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqharm[0] = (unsigned long )0;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqadto[0] = (unsigned short )0;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqtdso[0] = (unsigned short )0;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstv[1] = (unsigned char  *)sql_clave_sis;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstl[1] = (unsigned long )10;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhsts[1] = (         int  )0;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqindv[1] = (         short *)0;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqinds[1] = (         int  )0;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqharm[1] = (unsigned long )0;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqadto[1] = (unsigned short )0;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqtdso[1] = (unsigned short )0;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstv[2] = (unsigned char  *)sql_clave_rast;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstl[2] = (unsigned long )31;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhsts[2] = (         int  )0;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqindv[2] = (         short *)0;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqinds[2] = (         int  )0;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqharm[2] = (unsigned long )0;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqadto[2] = (unsigned short )0;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqtdso[2] = (unsigned short )0;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstv[3] = (unsigned char  *)sql_clave_tran;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstl[3] = (unsigned long )10;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhsts[3] = (         int  )0;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqindv[3] = (         short *)0;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqinds[3] = (         int  )0;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqharm[3] = (unsigned long )0;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqadto[3] = (unsigned short )0;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqtdso[3] = (unsigned short )0;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstv[4] = (unsigned char  *)sql_fecha_recep;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstl[4] = (unsigned long )10;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhsts[4] = (         int  )0;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqindv[4] = (         short *)0;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqinds[4] = (         int  )0;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqharm[4] = (unsigned long )0;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqadto[4] = (unsigned short )0;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqtdso[4] = (unsigned short )0;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstv[5] = (unsigned char  *)sql_tipo;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhstl[5] = (unsigned long )10;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqhsts[5] = (         int  )0;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqindv[5] = (         short *)0;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqinds[5] = (         int  )0;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqharm[5] = (unsigned long )0;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqadto[5] = (unsigned short )0;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqtdso[5] = (unsigned short )0;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqphsv = sqlstm.sqhstv;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqphsl = sqlstm.sqhstl;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqphss = sqlstm.sqhsts;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqpind = sqlstm.sqindv;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqpins = sqlstm.sqinds;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqparm = sqlstm.sqharm;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqparc = sqlstm.sqharc;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqpadto = sqlstm.sqadto;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlstm.sqptdso = sqlstm.sqtdso;
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
          sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 2607 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 2613 "../../gral/src_db/CTransaccionalCdl.pc"


        resultado = ChecaErrorOracle( 0, 1, &rowcount);

        if (resultado != GFI_ERRC_OK)
        {
          if (NIVEL_TRACE > 5)
            cout << "ERROR AL ELIMINAR EN TRANS_PROGRAMADO!" << endl;

          strcpy(err_desc,"T0002");
        }
      } // 3subtipo
    } // 2subtipo
    //else
    //  cout << "llave_c >" << llave_c << "< not found!" << endl;

  } // 1subtipo
  else
    resultado = GFI_ERRC_OK;

  if ( NIVEL_TRACE > 5 )
    cout << "Termino BAJA DE SALA DE ESPERA" << endl;

  return( resultado);
}

///////////////////////////////////////////////////////////////////
//  Funcion: CargaContrapartes                                   //
///////////////////////////////////////////////////////////////////
int CargaContrapartes(char *clave_inst,  char *clave_sis)
{
  int  resultado = GFI_ERRC_OK;
  long rowcount;
  contrapartes[0] = '\0';  //NULL

   if (NIVEL_TRACE > 5)
    cout << "CARGA LAS CONTRAPARTES" << endl;

  strcpy(sql_insth,clave_inst); strcpy(sql_sish,clave_sis); strcpy(sql_nmph,"CONTRAPARTE");
  /* EXEC SQL OPEN OBTENPTCONC; */ 
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"
  struct sqlexd sqlstm;
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqlvsn = 13;
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.arrsiz = 26;
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqladtp = &sqladt;
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqltdsp = &sqltds;
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.stmt = sq0003;
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.iters = (unsigned int  )1;
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.offset = (unsigned int  )3873;
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.selerr = (unsigned short)1;
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqlpfmem = (unsigned int  )0;
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.cud = sqlcud0;
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqlety = (unsigned short)4352;
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.occurs = (unsigned int  )0;
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqcmod = (unsigned int )0;
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstv[0] = (unsigned char  *)sql_insth;
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstl[0] = (unsigned long )20;
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhsts[0] = (         int  )0;
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqindv[0] = (         short *)0;
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqinds[0] = (         int  )0;
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqharm[0] = (unsigned long )0;
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqadto[0] = (unsigned short )0;
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqtdso[0] = (unsigned short )0;
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstv[1] = (unsigned char  *)sql_sish;
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstl[1] = (unsigned long )20;
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhsts[1] = (         int  )0;
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqindv[1] = (         short *)0;
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqinds[1] = (         int  )0;
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqharm[1] = (unsigned long )0;
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqadto[1] = (unsigned short )0;
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqtdso[1] = (unsigned short )0;
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstv[2] = (unsigned char  *)sql_nmph;
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhstl[2] = (unsigned long )20;
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqhsts[2] = (         int  )0;
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqindv[2] = (         short *)0;
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqinds[2] = (         int  )0;
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqharm[2] = (unsigned long )0;
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqadto[2] = (unsigned short )0;
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqtdso[2] = (unsigned short )0;
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqphsv = sqlstm.sqhstv;
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqphsl = sqlstm.sqhstl;
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqphss = sqlstm.sqhsts;
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqpind = sqlstm.sqindv;
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqpins = sqlstm.sqinds;
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqparm = sqlstm.sqharm;
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqparc = sqlstm.sqharc;
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqpadto = sqlstm.sqadto;
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqptdso = sqlstm.sqtdso;
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 2652 "../../gral/src_db/CTransaccionalCdl.pc"

  while(1)
  {
    /* EXEC SQL FETCH OBTENPTCONC INTO :sql_valor; */ 
#line 2655 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 2655 "../../gral/src_db/CTransaccionalCdl.pc"
    struct sqlexd sqlstm;
#line 2655 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqlvsn = 13;
#line 2655 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.arrsiz = 26;
#line 2655 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqladtp = &sqladt;
#line 2655 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqltdsp = &sqltds;
#line 2655 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.iters = (unsigned int  )1;
#line 2655 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.offset = (unsigned int  )3900;
#line 2655 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.selerr = (unsigned short)1;
#line 2655 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqlpfmem = (unsigned int  )0;
#line 2655 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.cud = sqlcud0;
#line 2655 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 2655 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqlety = (unsigned short)4352;
#line 2655 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.occurs = (unsigned int  )0;
#line 2655 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqfoff = (         int )0;
#line 2655 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqfmod = (unsigned int )2;
#line 2655 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstv[0] = (unsigned char  *)sql_valor;
#line 2655 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhstl[0] = (unsigned long )100;
#line 2655 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqhsts[0] = (         int  )0;
#line 2655 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqindv[0] = (         short *)0;
#line 2655 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqinds[0] = (         int  )0;
#line 2655 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqharm[0] = (unsigned long )0;
#line 2655 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqadto[0] = (unsigned short )0;
#line 2655 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqtdso[0] = (unsigned short )0;
#line 2655 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqphsv = sqlstm.sqhstv;
#line 2655 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqphsl = sqlstm.sqhstl;
#line 2655 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqphss = sqlstm.sqhsts;
#line 2655 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqpind = sqlstm.sqindv;
#line 2655 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqpins = sqlstm.sqinds;
#line 2655 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqparm = sqlstm.sqharm;
#line 2655 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqparc = sqlstm.sqharc;
#line 2655 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqpadto = sqlstm.sqadto;
#line 2655 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlstm.sqptdso = sqlstm.sqtdso;
#line 2655 "../../gral/src_db/CTransaccionalCdl.pc"
    sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 2655 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 2655 "../../gral/src_db/CTransaccionalCdl.pc"


    resultado = ChecaErrorOracle (0, 1, &rowcount);
    if (resultado != GFI_ERRC_OK)
      break;

    Gfi_StrRTrim(sql_valor);
    strcat(contrapartes,"|");
    strcat(contrapartes,sql_valor);
    strcat(contrapartes,"|");
  }
  /* EXEC SQL CLOSE OBTENPTCONC; */ 
#line 2666 "../../gral/src_db/CTransaccionalCdl.pc"

{
#line 2666 "../../gral/src_db/CTransaccionalCdl.pc"
  struct sqlexd sqlstm;
#line 2666 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqlvsn = 13;
#line 2666 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.arrsiz = 26;
#line 2666 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqladtp = &sqladt;
#line 2666 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqltdsp = &sqltds;
#line 2666 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.iters = (unsigned int  )1;
#line 2666 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.offset = (unsigned int  )3919;
#line 2666 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.cud = sqlcud0;
#line 2666 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 2666 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.sqlety = (unsigned short)4352;
#line 2666 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlstm.occurs = (unsigned int  )0;
#line 2666 "../../gral/src_db/CTransaccionalCdl.pc"
  sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 2666 "../../gral/src_db/CTransaccionalCdl.pc"
}

#line 2666 "../../gral/src_db/CTransaccionalCdl.pc"


  if (NIVEL_TRACE > 9)
    cout << "\tLista de Contrapartes >" << contrapartes << "<" << endl;

  return (GFI_ERRC_OK);
}