
/* Result Sets Interface */
#ifndef SQL_CRSR
#  define SQL_CRSR
  struct sql_cursor
  {
    unsigned int curocn;
    void *ptr1;
    void *ptr2;
    unsigned int magic;
  };
  typedef struct sql_cursor sql_cursor;
  typedef struct sql_cursor SQL_CURSOR;
#endif /* SQL_CRSR */

/* Thread Safety */
typedef void * sql_context;
typedef void * SQL_CONTEXT;

/* Object support */
struct sqltvn
{
  unsigned char *tvnvsn; 
  unsigned short tvnvsnl; 
  unsigned char *tvnnm;
  unsigned short tvnnml; 
  unsigned char *tvnsnm;
  unsigned short tvnsnml;
};
typedef struct sqltvn sqltvn;

struct sqladts
{
  unsigned int adtvsn; 
  unsigned short adtmode; 
  unsigned short adtnum;  
  sqltvn adttvn[1];       
};
typedef struct sqladts sqladts;

static struct sqladts sqladt = {
  1,1,0,
};

/* Binding to PL/SQL Records */
struct sqltdss
{
  unsigned int tdsvsn; 
  unsigned short tdsnum; 
  unsigned char *tdsval[1]; 
};
typedef struct sqltdss sqltdss;
static struct sqltdss sqltds =
{
  1,
  0,
};

/* File name & Package Name */
struct sqlcxp
{
  unsigned short fillen;
           char  filnam[26];
};
static const struct sqlcxp sqlfpn =
{
    25,
    "../../gral/src_db/admf.pc"
};


static unsigned int sqlctx = 1562302171;


static struct sqlexd {
   unsigned long  sqlvsn;
   unsigned int   arrsiz;
   unsigned int   iters;
   unsigned int   offset;
   unsigned short selerr;
   unsigned short sqlety;
   unsigned int   occurs;
      const short *cud;
   unsigned char  *sqlest;
      const char  *stmt;
   sqladts *sqladtp;
   sqltdss *sqltdsp;
   unsigned char  **sqphsv;
   unsigned long  *sqphsl;
            int   *sqphss;
            short **sqpind;
            int   *sqpins;
   unsigned long  *sqparm;
   unsigned long  **sqparc;
   unsigned short  *sqpadto;
   unsigned short  *sqptdso;
   unsigned int   sqlcmax;
   unsigned int   sqlcmin;
   unsigned int   sqlcincr;
   unsigned int   sqlctimeout;
   unsigned int   sqlcnowait;
            int   sqfoff;
   unsigned int   sqcmod;
   unsigned int   sqfmod;
   unsigned int   sqlpfmem;
   unsigned char  *sqhstv[3];
   unsigned long  sqhstl[3];
            int   sqhsts[3];
            short *sqindv[3];
            int   sqinds[3];
   unsigned long  sqharm[3];
   unsigned long  *sqharc[3];
   unsigned short  sqadto[3];
   unsigned short  sqtdso[3];
} sqlstm = {13,3};

// Prototypes
extern "C" {
  void sqlcxt (void **, unsigned int *,
               struct sqlexd *, const struct sqlcxp *);
  void sqlcx2t(void **, unsigned int *,
               struct sqlexd *, const struct sqlcxp *);
  void sqlbuft(void **, char *);
  void sqlgs2t(void **, char *);
  void sqlorat(void **, unsigned int *, void *);
}

// Forms Interface
static const int IAPSUCC = 0;
static const int IAPFAIL = 1403;
static const int IAPFTL  = 535;
extern "C" { void sqliem(unsigned char *, signed int *); }

typedef struct { unsigned short len; unsigned char arr[1]; } VARCHAR;
typedef struct { unsigned short len; unsigned char arr[1]; } varchar;

/* cud (compilation unit data) array */
static const short sqlcud0[] =
{13,4146,1,0,0,
5,0,0,1,61,0,3,52,0,0,3,3,0,1,0,1,97,0,0,1,97,0,0,1,97,0,0,
};


#line 1 "../../gral/src_db/admf.pc"
/*****************************************************/
/* admf.pc                                           */
/* Autor : Victor Hugo Guti‰rrez Ch.                 */
/* Fecha : 12 de Noviembre de 2002                   */
/*                                                   */
/*****************************************************/
#define SQLCA_STORAGE_CLASS extern
#define ORACA_STORAGE_CLASS extern

#define SEPARADOR            '|'

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <hora.h>
#include <signal.h>
#include <gralsql_oracle.h>
#include <gfi-err.h>

#if defined (__cplusplus) || defined (c_plusplus)
extern "C"
{
#endif

#include <sqlda.h>
#include <sqlcpr.h>

#if defined (__cplusplus) || defined (c_plusplus)
}
#endif

///////////////////////////////////////////////////////////////////
//                                                               //
//  Funcion : pon_cntl_swift                                     //
//                                                               //
///////////////////////////////////////////////////////////////////
int pon_cntl_swift(char *ref1, char *ubic_envio1, char *arch1)
{
 int result = GFI_ERRC_OK;

 /* EXEC SQL BEGIN DECLARE SECTION; */ 
#line 41 "../../gral/src_db/admf.pc"

   char    ubic_envio[15],
           arch[16],
           ref[17];
 /* EXEC SQL END DECLARE SECTION; */ 
#line 45 "../../gral/src_db/admf.pc"


 strcpy(ref,ref1);
 strcpy(ubic_envio,ubic_envio1);
 strcpy(arch,arch1);


 /* EXEC SQL INSERT into admf_cntl_swift
          values(:ref, :ubic_envio, :arch, sysdate, 'E'); */ 
#line 53 "../../gral/src_db/admf.pc"

{
#line 52 "../../gral/src_db/admf.pc"
 struct sqlexd sqlstm;
#line 52 "../../gral/src_db/admf.pc"
 sqlstm.sqlvsn = 13;
#line 52 "../../gral/src_db/admf.pc"
 sqlstm.arrsiz = 3;
#line 52 "../../gral/src_db/admf.pc"
 sqlstm.sqladtp = &sqladt;
#line 52 "../../gral/src_db/admf.pc"
 sqlstm.sqltdsp = &sqltds;
#line 52 "../../gral/src_db/admf.pc"
 sqlstm.stmt = "insert into admf_cntl_swift  values (:b0,:b1,:b2,sysdate,'E'\
)";
#line 52 "../../gral/src_db/admf.pc"
 sqlstm.iters = (unsigned int  )1;
#line 52 "../../gral/src_db/admf.pc"
 sqlstm.offset = (unsigned int  )5;
#line 52 "../../gral/src_db/admf.pc"
 sqlstm.cud = sqlcud0;
#line 52 "../../gral/src_db/admf.pc"
 sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 52 "../../gral/src_db/admf.pc"
 sqlstm.sqlety = (unsigned short)4352;
#line 52 "../../gral/src_db/admf.pc"
 sqlstm.occurs = (unsigned int  )0;
#line 52 "../../gral/src_db/admf.pc"
 sqlstm.sqhstv[0] = (unsigned char  *)ref;
#line 52 "../../gral/src_db/admf.pc"
 sqlstm.sqhstl[0] = (unsigned long )17;
#line 52 "../../gral/src_db/admf.pc"
 sqlstm.sqhsts[0] = (         int  )0;
#line 52 "../../gral/src_db/admf.pc"
 sqlstm.sqindv[0] = (         short *)0;
#line 52 "../../gral/src_db/admf.pc"
 sqlstm.sqinds[0] = (         int  )0;
#line 52 "../../gral/src_db/admf.pc"
 sqlstm.sqharm[0] = (unsigned long )0;
#line 52 "../../gral/src_db/admf.pc"
 sqlstm.sqadto[0] = (unsigned short )0;
#line 52 "../../gral/src_db/admf.pc"
 sqlstm.sqtdso[0] = (unsigned short )0;
#line 52 "../../gral/src_db/admf.pc"
 sqlstm.sqhstv[1] = (unsigned char  *)ubic_envio;
#line 52 "../../gral/src_db/admf.pc"
 sqlstm.sqhstl[1] = (unsigned long )15;
#line 52 "../../gral/src_db/admf.pc"
 sqlstm.sqhsts[1] = (         int  )0;
#line 52 "../../gral/src_db/admf.pc"
 sqlstm.sqindv[1] = (         short *)0;
#line 52 "../../gral/src_db/admf.pc"
 sqlstm.sqinds[1] = (         int  )0;
#line 52 "../../gral/src_db/admf.pc"
 sqlstm.sqharm[1] = (unsigned long )0;
#line 52 "../../gral/src_db/admf.pc"
 sqlstm.sqadto[1] = (unsigned short )0;
#line 52 "../../gral/src_db/admf.pc"
 sqlstm.sqtdso[1] = (unsigned short )0;
#line 52 "../../gral/src_db/admf.pc"
 sqlstm.sqhstv[2] = (unsigned char  *)arch;
#line 52 "../../gral/src_db/admf.pc"
 sqlstm.sqhstl[2] = (unsigned long )16;
#line 52 "../../gral/src_db/admf.pc"
 sqlstm.sqhsts[2] = (         int  )0;
#line 52 "../../gral/src_db/admf.pc"
 sqlstm.sqindv[2] = (         short *)0;
#line 52 "../../gral/src_db/admf.pc"
 sqlstm.sqinds[2] = (         int  )0;
#line 52 "../../gral/src_db/admf.pc"
 sqlstm.sqharm[2] = (unsigned long )0;
#line 52 "../../gral/src_db/admf.pc"
 sqlstm.sqadto[2] = (unsigned short )0;
#line 52 "../../gral/src_db/admf.pc"
 sqlstm.sqtdso[2] = (unsigned short )0;
#line 52 "../../gral/src_db/admf.pc"
 sqlstm.sqphsv = sqlstm.sqhstv;
#line 52 "../../gral/src_db/admf.pc"
 sqlstm.sqphsl = sqlstm.sqhstl;
#line 52 "../../gral/src_db/admf.pc"
 sqlstm.sqphss = sqlstm.sqhsts;
#line 52 "../../gral/src_db/admf.pc"
 sqlstm.sqpind = sqlstm.sqindv;
#line 52 "../../gral/src_db/admf.pc"
 sqlstm.sqpins = sqlstm.sqinds;
#line 52 "../../gral/src_db/admf.pc"
 sqlstm.sqparm = sqlstm.sqharm;
#line 52 "../../gral/src_db/admf.pc"
 sqlstm.sqparc = sqlstm.sqharc;
#line 52 "../../gral/src_db/admf.pc"
 sqlstm.sqpadto = sqlstm.sqadto;
#line 52 "../../gral/src_db/admf.pc"
 sqlstm.sqptdso = sqlstm.sqtdso;
#line 52 "../../gral/src_db/admf.pc"
 sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 52 "../../gral/src_db/admf.pc"
}

#line 53 "../../gral/src_db/admf.pc"

 result = ChecaErrorOracle (1, 0, 0);

 return (result);
}
