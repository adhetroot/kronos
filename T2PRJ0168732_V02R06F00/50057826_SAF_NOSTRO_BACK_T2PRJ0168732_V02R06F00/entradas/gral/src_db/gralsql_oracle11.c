
/* Result Sets Interface */
#ifndef SQL_CRSR
#  define SQL_CRSR
  struct sql_cursor
  {
    unsigned int curocn;
    void *ptr1;
    void *ptr2;
    unsigned int magic;
  };
  typedef struct sql_cursor sql_cursor;
  typedef struct sql_cursor SQL_CURSOR;
#endif /* SQL_CRSR */

/* Thread Safety */
typedef void * sql_context;
typedef void * SQL_CONTEXT;

/* Object support */
struct sqltvn
{
  unsigned char *tvnvsn; 
  unsigned short tvnvsnl; 
  unsigned char *tvnnm;
  unsigned short tvnnml; 
  unsigned char *tvnsnm;
  unsigned short tvnsnml;
};
typedef struct sqltvn sqltvn;

struct sqladts
{
  unsigned int adtvsn; 
  unsigned short adtmode; 
  unsigned short adtnum;  
  sqltvn adttvn[1];       
};
typedef struct sqladts sqladts;

static struct sqladts sqladt = {
  1,1,0,
};

/* Binding to PL/SQL Records */
struct sqltdss
{
  unsigned int tdsvsn; 
  unsigned short tdsnum; 
  unsigned char *tdsval[1]; 
};
typedef struct sqltdss sqltdss;
static struct sqltdss sqltds =
{
  1,
  0,
};

/* File name & Package Name */
struct sqlcxp
{
  unsigned short fillen;
           char  filnam[38];
};
static const struct sqlcxp sqlfpn =
{
    37,
    "../../gral/src_db/gralsql_oracle11.pc"
};


static unsigned int sqlctx = 307757997;


static struct sqlexd {
   unsigned long  sqlvsn;
   unsigned int   arrsiz;
   unsigned int   iters;
   unsigned int   offset;
   unsigned short selerr;
   unsigned short sqlety;
   unsigned int   occurs;
      const short *cud;
   unsigned char  *sqlest;
      const char  *stmt;
   sqladts *sqladtp;
   sqltdss *sqltdsp;
   unsigned char  **sqphsv;
   unsigned long  *sqphsl;
            int   *sqphss;
            short **sqpind;
            int   *sqpins;
   unsigned long  *sqparm;
   unsigned long  **sqparc;
   unsigned short  *sqpadto;
   unsigned short  *sqptdso;
   unsigned int   sqlcmax;
   unsigned int   sqlcmin;
   unsigned int   sqlcincr;
   unsigned int   sqlctimeout;
   unsigned int   sqlcnowait;
            int   sqfoff;
   unsigned int   sqcmod;
   unsigned int   sqfmod;
   unsigned int   sqlpfmem;
   unsigned char  *sqhstv[4];
   unsigned long  sqhstl[4];
            int   sqhsts[4];
            short *sqindv[4];
            int   sqinds[4];
   unsigned long  sqharm[4];
   unsigned long  *sqharc[4];
   unsigned short  sqadto[4];
   unsigned short  sqtdso[4];
} sqlstm = {13,4};

// Prototypes
extern "C" {
  void sqlcxt (void **, unsigned int *,
               struct sqlexd *, const struct sqlcxp *);
  void sqlcx2t(void **, unsigned int *,
               struct sqlexd *, const struct sqlcxp *);
  void sqlbuft(void **, char *);
  void sqlgs2t(void **, char *);
  void sqlorat(void **, unsigned int *, void *);
}

// Forms Interface
static const int IAPSUCC = 0;
static const int IAPFAIL = 1403;
static const int IAPFTL  = 535;
extern "C" { void sqliem(unsigned char *, signed int *); }

typedef struct { unsigned short len; unsigned char arr[1]; } VARCHAR;
typedef struct { unsigned short len; unsigned char arr[1]; } varchar;

/* cud (compilation unit data) array */
static const short sqlcud0[] =
{13,4146,1,0,0,
5,0,0,1,0,0,31,105,0,0,0,0,0,1,0,
20,0,0,2,0,0,31,118,0,0,0,0,0,1,0,
35,0,0,3,0,0,29,142,0,0,0,0,0,1,0,
50,0,0,0,0,0,27,182,0,0,4,4,0,1,0,1,97,0,0,1,10,0,0,1,10,0,0,1,10,0,0,
81,0,0,5,0,0,30,288,0,0,0,0,0,1,0,
96,0,0,6,0,0,31,332,0,0,0,0,0,1,0,
};


#line 1 "../../gral/src_db/gralsql_oracle11.pc"
/**********************************************************************
 *
 *  Banco Santander Mexicano, S.A.
 *
 *  Institucion de Banca Multiple
 *
 *  Grupo Financiero Santander Mexicano
 *
 *  Archivo   : gralsql_oracle.pcc
 *
 *  Proposito : Implementacion de las funciones comunes de interaccion 
 *              con ORACLE.
 *
 *  Lenguaje  : ANSI - C.
 *
 *  Autor     : Antonio Gerardo Cano D.
 *
 *  Historia  : 15/Octubre/1997 -> Creacion
 *
 *              24/Marzo/1999 -> Modificado por Antonio Gerardo Cano D.
 *              para emplear la variable ORACLE_SID si TWO_TASK no esta
 *              definida.
 *
 **********************************************************************/

#include <gralsql_oracle.h>

/*--- Identificacion para el control de versiones RCS ---*/

#if defined (GFI_VERSION)

 static char RCS_GRALSQL_ORACLE_PC[] = "$Id$";

/* Mensajes de Log de RCS
 *
 * $Log$
 * 
 */

#endif

/* EXEC SQL INCLUDE SQLCA;
 */ 
#line 1 "/oracle/12.1.0/precomp/public/SQLCA.H"
/*
 * $Header: sqlca.h 24-apr-2003.12:50:58 mkandarp Exp $ sqlca.h 
 */

/* Copyright (c) 1985, 2003, Oracle Corporation.  All rights reserved.  */
 
/*
NAME
  SQLCA : SQL Communications Area.
FUNCTION
  Contains no code. Oracle fills in the SQLCA with status info
  during the execution of a SQL stmt.
NOTES
  **************************************************************
  ***                                                        ***
  *** This file is SOSD.  Porters must change the data types ***
  *** appropriately on their platform.  See notes/pcport.doc ***
  *** for more information.                                  ***
  ***                                                        ***
  **************************************************************

  If the symbol SQLCA_STORAGE_CLASS is defined, then the SQLCA
  will be defined to have this storage class. For example:
 
    #define SQLCA_STORAGE_CLASS extern
 
  will define the SQLCA as an extern.
 
  If the symbol SQLCA_INIT is defined, then the SQLCA will be
  statically initialized. Although this is not necessary in order
  to use the SQLCA, it is a good pgming practice not to have
  unitialized variables. However, some C compilers/OS's don't
  allow automatic variables to be init'd in this manner. Therefore,
  if you are INCLUDE'ing the SQLCA in a place where it would be
  an automatic AND your C compiler/OS doesn't allow this style
  of initialization, then SQLCA_INIT should be left undefined --
  all others can define SQLCA_INIT if they wish.

  If the symbol SQLCA_NONE is defined, then the SQLCA variable will
  not be defined at all.  The symbol SQLCA_NONE should not be defined
  in source modules that have embedded SQL.  However, source modules
  that have no embedded SQL, but need to manipulate a sqlca struct
  passed in as a parameter, can set the SQLCA_NONE symbol to avoid
  creation of an extraneous sqlca variable.
 
MODIFIED
    lvbcheng   07/31/98 -  long to int
    jbasu      12/12/94 -  Bug 217878: note this is an SOSD file
    losborne   08/11/92 -  No sqlca var if SQLCA_NONE macro set 
  Clare      12/06/84 - Ch SQLCA to not be an extern.
  Clare      10/21/85 - Add initialization.
  Bradbury   01/05/86 - Only initialize when SQLCA_INIT set
  Clare      06/12/86 - Add SQLCA_STORAGE_CLASS option.
*/
 
#ifndef SQLCA
#define SQLCA 1
 
struct   sqlca
         {
         /* ub1 */ char    sqlcaid[8];
         /* b4  */ int     sqlabc;
         /* b4  */ int     sqlcode;
         struct
           {
           /* ub2 */ unsigned short sqlerrml;
           /* ub1 */ char           sqlerrmc[70];
           } sqlerrm;
         /* ub1 */ char    sqlerrp[8];
         /* b4  */ int     sqlerrd[6];
         /* ub1 */ char    sqlwarn[8];
         /* ub1 */ char    sqlext[8];
         };

#ifndef SQLCA_NONE 
#ifdef   SQLCA_STORAGE_CLASS
SQLCA_STORAGE_CLASS struct sqlca sqlca
#else
         struct sqlca sqlca
#endif
 
#ifdef  SQLCA_INIT
         = {
         {'S', 'Q', 'L', 'C', 'A', ' ', ' ', ' '},
         sizeof(struct sqlca),
         0,
         { 0, {0}},
         {'N', 'O', 'T', ' ', 'S', 'E', 'T', ' '},
         {0, 0, 0, 0, 0, 0},
         {0, 0, 0, 0, 0, 0, 0, 0},
         {0, 0, 0, 0, 0, 0, 0, 0}
         }
#endif
         ;
#endif
 
#endif
 
/* end SQLCA */

/* EXEC SQL INCLUDE ORACA;
 */ 
#line 1 "/oracle/12.1.0/precomp/public/ORACA.H"
/*
 * $Header: oraca.h 24-apr-2003.12:50:59 mkandarp Exp $ oraca.h 
 */

/* Copyright (c) 1985, 2003, Oracle Corporation.  All rights reserved.  */
 
/*
NAME
  ORACA : Oracle Communications Area.
FUNCTION
  Contains no code. Provides supplementary communications to/from
  Oracle (in addition to standard SQLCA).
NOTES
  **************************************************************
  ***                                                        ***
  *** This file is SOSD.  Porters must change the data types ***
  *** appropriately on their platform.  See notes/pcport.doc ***
  *** for more information.                                  ***
  ***                                                        ***
  **************************************************************

  oracchf : Check cursor cache consistency flag. If set AND oradbgf
            is set, then directs SQLLIB to perform cursor cache
            consistency checks before every cursor operation
            (OPEN, FETCH, SELECT, INSERT, etc.).
  oradbgf : Master DEBUG flag. Used to turn all DEBUG options
            on or off.
  orahchf : Check Heap consistency flag. If set AND oradbgf is set,
            then directs SQLLIB to perform heap consistency checks
            everytime memory is dynamically allocated/free'd via
            sqlalc/sqlfre/sqlrlc. MUST BE SET BEFORE 1ST CONNECT
            and once set cannot be cleared (subsequent requests
            to change it are ignored).
  orastxtf: Save SQL stmt text flag. If set, then directs SQLLIB
            to save the text of the current SQL stmt in orastxt
            (in VARCHAR format).
  orastxt : Saved len and text of current SQL stmt (in VARCHAR
            format).
  orasfnm : Saved len and text of filename containing current SQL
            stmt (in VARCHAR format).
  oraslnr : Saved line nr within orasfnm of current SQL stmt.
 
  Cursor cache statistics. Set after COMMIT or ROLLBACK. Each
  CONNECT'd DATABASE has its own set of statistics.
 
  orahoc  : Highest Max Open OraCursors requested. Highest value
            for MAXOPENCURSORS by any CONNECT to this DATABASE.
  oramoc  : Max Open OraCursors required. Specifies the max nr
            of OraCursors required to run this pgm. Can be higher
            than orahoc if working set (MAXOPENCURSORS) was set
            too low, thus forcing the PCC to expand the cache.
  oracoc  : Current nr of OraCursors used.
  oranor  : Nr of OraCursor cache reassignments. Can show the
            degree of "thrashing" in the cache. Optimally, this
            nr should be kept as low as possible (time vs space
            optimization).
  oranpr  : Nr of SQL stmt "parses".
  oranex  : Nr of SQL stmt "executes". Optimally, the relation-
            ship of oranex to oranpr should be kept as high as
            possible.
 
 
  If the symbol ORACA_NONE is defined, then there will be no ORACA
  *variable*, although there will still be a struct defined.  This
  macro should not normally be defined in application code.

  If the symbol ORACA_INIT is defined, then the ORACA will be
  statically initialized. Although this is not necessary in order
  to use the ORACA, it is a good pgming practice not to have
  unitialized variables. However, some C compilers/OS's don't
  allow automatic variables to be init'd in this manner. Therefore,
  if you are INCLUDE'ing the ORACA in a place where it would be
  an automatic AND your C compiler/OS doesn't allow this style
  of initialization, then ORACA_INIT should be left undefined --
  all others can define ORACA_INIT if they wish.
 
OWNER
  Clare
DATE
  10/19/85
MODIFIED
    apopat     05/08/02  - [2362423] MVS PE to make lines shorter than 79
    apopat     07/31/99 -  [707588] TAB to blanks for OCCS
    lvbcheng   10/27/98 -  change long to int for oraca
    pccint     10/03/96 -  Add IS_OSD for linting
    jbasu      12/12/94 -  Bug 217878: note this is an SOSD file
    losborne   09/04/92 -  Make oraca variable optional 
    Osborne    05/24/90 - Add ORACA_STORAGE_CLASS construct
  Clare      02/20/86 - PCC [10101l] Feature: Heap consistency check.
  Clare      03/04/86 - PCC [10101r] Port: ORACA init ifdef.
  Clare      03/12/86 - PCC [10101ab] Feature: ORACA cuc statistics.
*/
/* IS_OSD */ 
#ifndef  ORACA
#define  ORACA     1
 
struct   oraca
         {
    /* text */ char oracaid[8];      /* Reserved                            */
    /* ub4  */ int oracabc;          /* Reserved                            */
 
    /*       Flags which are setable by User. */
 
   /* ub4 */ int  oracchf;           /* <> 0 if "check cur cache consistncy"*/
   /* ub4 */ int  oradbgf;           /* <> 0 if "do DEBUG mode checking"    */
   /* ub4 */ int  orahchf;           /* <> 0 if "do Heap consistency check" */
   /* ub4 */ int  orastxtf;          /* SQL stmt text flag                  */
#define  ORASTFNON 0                 /* = don't save text of SQL stmt       */
#define  ORASTFERR 1                 /* = only save on SQLERROR             */
#define  ORASTFWRN 2                 /* = only save on SQLWARNING/SQLERROR  */
#define  ORASTFANY 3                 /* = always save                       */
         struct
           {
  /* ub2  */ unsigned short orastxtl;
  /* text */ char  orastxtc[70];
           } orastxt;                /* text of last SQL stmt               */
         struct
           {
  /* ub2  */   unsigned short orasfnml;
  /* text */   char       orasfnmc[70];
           } orasfnm;                /* name of file containing SQL stmt    */
  /* ub4 */ int   oraslnr;           /* line nr-within-file of SQL stmt     */

  /* ub4 */ int   orahoc;            /* highest max open OraCurs requested  */
  /* ub4 */ int   oramoc;            /* max open OraCursors required        */
  /* ub4 */ int   oracoc;            /* current OraCursors open             */
  /* ub4 */ int   oranor;            /* nr of OraCursor re-assignments      */
  /* ub4 */ int   oranpr;            /* nr of parses                        */
  /* ub4 */ int   oranex;            /* nr of executes                      */
         };

#ifndef ORACA_NONE

#ifdef ORACA_STORAGE_CLASS
ORACA_STORAGE_CLASS struct oraca oraca
#else
struct oraca oraca
#endif
#ifdef ORACA_INIT
         =
         {
         {'O','R','A','C','A',' ',' ',' '},
         sizeof(struct oraca),
         0,0,0,0,
         {0,{0}},
         {0,{0}},
         0,
         0,0,0,0,0,0
         }
#endif
         ;

#endif

#endif
/* end oraca.h */

#line 45 "../../gral/src_db/gralsql_oracle11.pc"


#if defined(c_plusplus) || defined (__cplusplus)
extern "C"
{
#endif

#include <sqlcpr.h>

#if defined(c_plusplus) || defined (__cplusplus)
}
#endif


/*---   Declaraciones SQL   ---*/


#define          MAX_ORACLE_ERROR_MESSAGE_STRING        512
#define          ORACLE_INSERT_DUP_KEY_ERRNO             -1
#define          ORACLE_NO_DB_INST_ERRNO               1403
#define          SQL_LONG_USRID                          30
#define          SQL_LONG_USRID_PASSWD                   30
#define          SQL_LONG_DB_NAME                        30

/*--- EXEC ORACLE OPTION (ORACA=YES); ---*/

/**********************************************************************
 *
 * General ChecaErrorOracle
 *
 **********************************************************************/

int
ChecaErrorOracle (const int commit_or_rollback_work,
		  const int checa_rcount,
		  long *rcount)
{
  int           result         = GFI_ERRC_OK;
  unsigned char          error_message  [ MAX_ORACLE_ERROR_MESSAGE_STRING + 1 ],
   		stmt_buf       [ 700 ];
  /*unsigned int*/ size_t  buffer_length  = MAX_ORACLE_ERROR_MESSAGE_STRING + 1,
		buflen,
		function_code,
                message_length = 0;


  if (checa_rcount == 1)
    if (rcount == 0)
      return GFI_ERRC_NULLPTR;
    else
      *rcount = 0;

  *error_message = '\0';
  buflen = (long) sizeof (stmt_buf);

  oraca.oradbgf  = 1;      /*-- enable debug operations        --*/ 
  oraca.oracchf  = 1;      /*-- gather cursor cache statistics --*/ 
  oraca.orastxtf = 3;      /*-- always save the SQL statement  --*/ 

  if (commit_or_rollback_work == 2)
    /* EXEC SQL ROLLBACK; */ 
#line 105 "../../gral/src_db/gralsql_oracle11.pc"

{
#line 105 "../../gral/src_db/gralsql_oracle11.pc"
    struct sqlexd sqlstm;
#line 105 "../../gral/src_db/gralsql_oracle11.pc"
    sqlstm.sqlvsn = 13;
#line 105 "../../gral/src_db/gralsql_oracle11.pc"
    sqlstm.arrsiz = 0;
#line 105 "../../gral/src_db/gralsql_oracle11.pc"
    sqlstm.sqladtp = &sqladt;
#line 105 "../../gral/src_db/gralsql_oracle11.pc"
    sqlstm.sqltdsp = &sqltds;
#line 105 "../../gral/src_db/gralsql_oracle11.pc"
    sqlstm.iters = (unsigned int  )1;
#line 105 "../../gral/src_db/gralsql_oracle11.pc"
    sqlstm.offset = (unsigned int  )5;
#line 105 "../../gral/src_db/gralsql_oracle11.pc"
    sqlstm.cud = sqlcud0;
#line 105 "../../gral/src_db/gralsql_oracle11.pc"
    sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 105 "../../gral/src_db/gralsql_oracle11.pc"
    sqlstm.sqlety = (unsigned short)4352;
#line 105 "../../gral/src_db/gralsql_oracle11.pc"
    sqlstm.occurs = (unsigned int  )0;
#line 105 "../../gral/src_db/gralsql_oracle11.pc"
    sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 105 "../../gral/src_db/gralsql_oracle11.pc"
}

#line 105 "../../gral/src_db/gralsql_oracle11.pc"

  else
  {
    if (sqlca.sqlcode == ORACLE_INSERT_DUP_KEY_ERRNO)
      return GFI_ERRC_DUPKEY;
    else
      if (sqlca.sqlcode < 0)
      {
	sqlglm (error_message, &buffer_length, &message_length);
        sqlgls ((char *)(stmt_buf), &buflen, &function_code);

	*(error_message + message_length)     = '\0';

	/* EXEC SQL ROLLBACK; */ 
#line 118 "../../gral/src_db/gralsql_oracle11.pc"

{
#line 118 "../../gral/src_db/gralsql_oracle11.pc"
 struct sqlexd sqlstm;
#line 118 "../../gral/src_db/gralsql_oracle11.pc"
 sqlstm.sqlvsn = 13;
#line 118 "../../gral/src_db/gralsql_oracle11.pc"
 sqlstm.arrsiz = 0;
#line 118 "../../gral/src_db/gralsql_oracle11.pc"
 sqlstm.sqladtp = &sqladt;
#line 118 "../../gral/src_db/gralsql_oracle11.pc"
 sqlstm.sqltdsp = &sqltds;
#line 118 "../../gral/src_db/gralsql_oracle11.pc"
 sqlstm.iters = (unsigned int  )1;
#line 118 "../../gral/src_db/gralsql_oracle11.pc"
 sqlstm.offset = (unsigned int  )20;
#line 118 "../../gral/src_db/gralsql_oracle11.pc"
 sqlstm.cud = sqlcud0;
#line 118 "../../gral/src_db/gralsql_oracle11.pc"
 sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 118 "../../gral/src_db/gralsql_oracle11.pc"
 sqlstm.sqlety = (unsigned short)4352;
#line 118 "../../gral/src_db/gralsql_oracle11.pc"
 sqlstm.occurs = (unsigned int  )0;
#line 118 "../../gral/src_db/gralsql_oracle11.pc"
 sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 118 "../../gral/src_db/gralsql_oracle11.pc"
}

#line 118 "../../gral/src_db/gralsql_oracle11.pc"


#if !defined (GFI_NO_ORACLE_ERRORS)

	fprintf (stdout, "Oracle ERROR (%ld): \"%s\"\n",
		 sqlca.sqlcode, error_message);


	fprintf (stdout, "Last ORACLE SQL statement : %d %s \n", 
		 oraca.orastxt.orastxtl, stmt_buf); 
	fprintf (stdout, "\nAt or near line number %ld\n", oraca.oraslnr); 
#endif
	return GFI_ERRC_DBERROR;
      }

    if (checa_rcount == 1)
    {
      if (sqlca.sqlcode == ORACLE_NO_DB_INST_ERRNO)
	result = GFI_ERRC_NODBINST;

      *rcount = sqlca.sqlerrd [ 2 ];
    }

    if (commit_or_rollback_work == 1)
      /* EXEC SQL COMMIT; */ 
#line 142 "../../gral/src_db/gralsql_oracle11.pc"

{
#line 142 "../../gral/src_db/gralsql_oracle11.pc"
      struct sqlexd sqlstm;
#line 142 "../../gral/src_db/gralsql_oracle11.pc"
      sqlstm.sqlvsn = 13;
#line 142 "../../gral/src_db/gralsql_oracle11.pc"
      sqlstm.arrsiz = 0;
#line 142 "../../gral/src_db/gralsql_oracle11.pc"
      sqlstm.sqladtp = &sqladt;
#line 142 "../../gral/src_db/gralsql_oracle11.pc"
      sqlstm.sqltdsp = &sqltds;
#line 142 "../../gral/src_db/gralsql_oracle11.pc"
      sqlstm.iters = (unsigned int  )1;
#line 142 "../../gral/src_db/gralsql_oracle11.pc"
      sqlstm.offset = (unsigned int  )35;
#line 142 "../../gral/src_db/gralsql_oracle11.pc"
      sqlstm.cud = sqlcud0;
#line 142 "../../gral/src_db/gralsql_oracle11.pc"
      sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 142 "../../gral/src_db/gralsql_oracle11.pc"
      sqlstm.sqlety = (unsigned short)4352;
#line 142 "../../gral/src_db/gralsql_oracle11.pc"
      sqlstm.occurs = (unsigned int  )0;
#line 142 "../../gral/src_db/gralsql_oracle11.pc"
      sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 142 "../../gral/src_db/gralsql_oracle11.pc"
}

#line 142 "../../gral/src_db/gralsql_oracle11.pc"

  }

  return result;

}   /*--  Fin de la operacion ChecaErrorOracle  --*/

/*********************************************************************
*
* OpenDB
*
**********************************************************************/

int OpenDB (char *str, int str_len)
{
 
 
 	int			result = GFI_ERRC_OK;
	char		*ptr    = 0;
	static char	*usr_id_pass   = "USR_PASS";

	/* EXEC SQL BEGIN DECLARE SECTION; */ 
#line 163 "../../gral/src_db/gralsql_oracle11.pc"

		char	sql_usrid[SQL_LONG_USRID];
	/* EXEC SQL END DECLARE SECTION; */ 
#line 165 "../../gral/src_db/gralsql_oracle11.pc"


	/* EXEC SQL WHENEVER SQLERROR CONTINUE; */ 
#line 167 "../../gral/src_db/gralsql_oracle11.pc"


	*sql_usrid  = '\0';

	/*-- Obten el Valor del passsword del usuario ----*/
	ptr = getenv (usr_id_pass);
	if ((ptr == 0) || (*ptr == '\0'))
		return -1;

	(void) strncpy (sql_usrid, ptr, SQL_LONG_USRID_PASSWD);
	printf("{%s}",sql_usrid);
	*(sql_usrid + SQL_LONG_USRID_PASSWD - 1) = '\0';

	printf("<<%s>>",sql_usrid);
	
	/* EXEC SQL CONNECT :sql_usrid; */ 
#line 182 "../../gral/src_db/gralsql_oracle11.pc"

{
#line 182 "../../gral/src_db/gralsql_oracle11.pc"
 struct sqlexd sqlstm;
#line 182 "../../gral/src_db/gralsql_oracle11.pc"
 sqlstm.sqlvsn = 13;
#line 182 "../../gral/src_db/gralsql_oracle11.pc"
 sqlstm.arrsiz = 4;
#line 182 "../../gral/src_db/gralsql_oracle11.pc"
 sqlstm.sqladtp = &sqladt;
#line 182 "../../gral/src_db/gralsql_oracle11.pc"
 sqlstm.sqltdsp = &sqltds;
#line 182 "../../gral/src_db/gralsql_oracle11.pc"
 sqlstm.iters = (unsigned int  )10;
#line 182 "../../gral/src_db/gralsql_oracle11.pc"
 sqlstm.offset = (unsigned int  )50;
#line 182 "../../gral/src_db/gralsql_oracle11.pc"
 sqlstm.cud = sqlcud0;
#line 182 "../../gral/src_db/gralsql_oracle11.pc"
 sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 182 "../../gral/src_db/gralsql_oracle11.pc"
 sqlstm.sqlety = (unsigned short)4352;
#line 182 "../../gral/src_db/gralsql_oracle11.pc"
 sqlstm.occurs = (unsigned int  )0;
#line 182 "../../gral/src_db/gralsql_oracle11.pc"
 sqlstm.sqhstv[0] = (unsigned char  *)sql_usrid;
#line 182 "../../gral/src_db/gralsql_oracle11.pc"
 sqlstm.sqhstl[0] = (unsigned long )30;
#line 182 "../../gral/src_db/gralsql_oracle11.pc"
 sqlstm.sqhsts[0] = (         int  )30;
#line 182 "../../gral/src_db/gralsql_oracle11.pc"
 sqlstm.sqindv[0] = (         short *)0;
#line 182 "../../gral/src_db/gralsql_oracle11.pc"
 sqlstm.sqinds[0] = (         int  )0;
#line 182 "../../gral/src_db/gralsql_oracle11.pc"
 sqlstm.sqharm[0] = (unsigned long )0;
#line 182 "../../gral/src_db/gralsql_oracle11.pc"
 sqlstm.sqadto[0] = (unsigned short )0;
#line 182 "../../gral/src_db/gralsql_oracle11.pc"
 sqlstm.sqtdso[0] = (unsigned short )0;
#line 182 "../../gral/src_db/gralsql_oracle11.pc"
 sqlstm.sqphsv = sqlstm.sqhstv;
#line 182 "../../gral/src_db/gralsql_oracle11.pc"
 sqlstm.sqphsl = sqlstm.sqhstl;
#line 182 "../../gral/src_db/gralsql_oracle11.pc"
 sqlstm.sqphss = sqlstm.sqhsts;
#line 182 "../../gral/src_db/gralsql_oracle11.pc"
 sqlstm.sqpind = sqlstm.sqindv;
#line 182 "../../gral/src_db/gralsql_oracle11.pc"
 sqlstm.sqpins = sqlstm.sqinds;
#line 182 "../../gral/src_db/gralsql_oracle11.pc"
 sqlstm.sqparm = sqlstm.sqharm;
#line 182 "../../gral/src_db/gralsql_oracle11.pc"
 sqlstm.sqparc = sqlstm.sqharc;
#line 182 "../../gral/src_db/gralsql_oracle11.pc"
 sqlstm.sqpadto = sqlstm.sqadto;
#line 182 "../../gral/src_db/gralsql_oracle11.pc"
 sqlstm.sqptdso = sqlstm.sqtdso;
#line 182 "../../gral/src_db/gralsql_oracle11.pc"
 sqlstm.sqlcmax = (unsigned int )100;
#line 182 "../../gral/src_db/gralsql_oracle11.pc"
 sqlstm.sqlcmin = (unsigned int )2;
#line 182 "../../gral/src_db/gralsql_oracle11.pc"
 sqlstm.sqlcincr = (unsigned int )1;
#line 182 "../../gral/src_db/gralsql_oracle11.pc"
 sqlstm.sqlctimeout = (unsigned int )0;
#line 182 "../../gral/src_db/gralsql_oracle11.pc"
 sqlstm.sqlcnowait = (unsigned int )0;
#line 182 "../../gral/src_db/gralsql_oracle11.pc"
 sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 182 "../../gral/src_db/gralsql_oracle11.pc"
}

#line 182 "../../gral/src_db/gralsql_oracle11.pc"


	result = ChecaErrorOracle (0, 0, 0);

	if (result != GFI_ERRC_OK){
		fprintf (stdout, "Error en OpenDB_W al conectarse al ambiente : '%s' = %d. \n", str, result);
		return GFI_ERRC_FATAL;
	}
	return result;



/* int          result = GFI_ERRC_OK;
  char        *ptr    = 0;
  static char *usr_id        = "USRID",
              *usr_id_pass   = "USRID_PASS",
	      *db_var_name_1 = "TWO_TASK",
	      *db_var_name_2 = "ORACLE_SID";

  EXEC SQL BEGIN DECLARE SECTION;

  char sql_usrid  [ SQL_LONG_USRID ],
       sql_passwd [ SQL_LONG_USRID_PASSWD ],
       database	  [ SQL_LONG_DB_NAME ];
  
  EXEC SQL END DECLARE SECTION;

  EXEC SQL WHENEVER SQLERROR CONTINUE;

  *sql_usrid  = '\0';
  *sql_passwd = '\0';

  if (str == 0) 
    return GFI_ERRC_NULLPTR;

  if (str_len < 2)
    return GFI_ERRC_BADARG;

  *str = '\0';

  *--- Obten el Valor de la Base de Datos a la que nos estamos  ---
    --- conectando (TWO_TASK tiene preferencia sobre ORACLE_SID) ---*
  ptr = getenv (db_var_name_1);

  *--- TWO_TASK es null ? => prueba con ORACLE_SID  ---*

  if ((ptr == 0) || (*ptr == '\0'))
  {
    ptr = getenv (db_var_name_2);

    if ((ptr == 0) || (*ptr == '\0'))
      return -1;   *--- ORACLE_SID tampoco esta definida => abort ---*
  }

  (void) strncpy (str, ptr, str_len);
  *(str + str_len - 1) = '\0';
  (void) strcpy (database, str);

  *-- Obten el Valor del usuario a conectarse a la Base de Datos----*
  ptr = getenv (usr_id);

  if ((ptr == 0) || (*ptr == '\0'))
    return -1;
	
  (void) strncpy (sql_usrid, ptr, SQL_LONG_USRID);
  *(sql_usrid + SQL_LONG_USRID - 1) = '\0';

  *-- Obten el Valor del passsword del usuario ----*
  ptr = getenv (usr_id_pass);

  if ((ptr == 0) || (*ptr == '\0'))
    return -1;
	
  (void) strncpy (sql_passwd, ptr, SQL_LONG_USRID_PASSWD);
  *(sql_passwd + SQL_LONG_USRID_PASSWD - 1) = '\0';

  printf("Variable USUARIO  es %s\n",sql_usrid); 
*  printf("Variable PASSWD   es %s\n",sql_passwd); *
  printf("Variable DATABASE es %s\n",str); 

  EXEC SQL CONNECT :sql_usrid IDENTIFIED BY :sql_passwd
	USING :database;

  result = ChecaErrorOracle (0, 0, 0);
  
  if (result != GFI_ERRC_OK)
  {
    fprintf (stdout, "Error en OpenDB al conectarse al ambiente : '%s'"
	     "= %d. \n", str, result);

    return GFI_ERRC_FATAL;
  }

  return result;*/

}  /*--- Fin de OpenDB  ---*/

/*********************************************************************
*
* CloseDB
*
**********************************************************************/

int CloseDB (void)
{

  /* EXEC SQL COMMIT WORK RELEASE; */ 
#line 288 "../../gral/src_db/gralsql_oracle11.pc"

{
#line 288 "../../gral/src_db/gralsql_oracle11.pc"
  struct sqlexd sqlstm;
#line 288 "../../gral/src_db/gralsql_oracle11.pc"
  sqlstm.sqlvsn = 13;
#line 288 "../../gral/src_db/gralsql_oracle11.pc"
  sqlstm.arrsiz = 4;
#line 288 "../../gral/src_db/gralsql_oracle11.pc"
  sqlstm.sqladtp = &sqladt;
#line 288 "../../gral/src_db/gralsql_oracle11.pc"
  sqlstm.sqltdsp = &sqltds;
#line 288 "../../gral/src_db/gralsql_oracle11.pc"
  sqlstm.iters = (unsigned int  )1;
#line 288 "../../gral/src_db/gralsql_oracle11.pc"
  sqlstm.offset = (unsigned int  )81;
#line 288 "../../gral/src_db/gralsql_oracle11.pc"
  sqlstm.cud = sqlcud0;
#line 288 "../../gral/src_db/gralsql_oracle11.pc"
  sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 288 "../../gral/src_db/gralsql_oracle11.pc"
  sqlstm.sqlety = (unsigned short)4352;
#line 288 "../../gral/src_db/gralsql_oracle11.pc"
  sqlstm.occurs = (unsigned int  )0;
#line 288 "../../gral/src_db/gralsql_oracle11.pc"
  sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 288 "../../gral/src_db/gralsql_oracle11.pc"
}

#line 288 "../../gral/src_db/gralsql_oracle11.pc"


  return GFI_ERRC_OK;

}  /*-- Fin de CloseDB ---*/

/*********************************************************************
*
* ChecaErrorMsg
*
**********************************************************************/
int ChecaErrorMsg(void)
{
  unsigned char          error_message  [ MAX_ORACLE_ERROR_MESSAGE_STRING + 1 ],
       		stmt_buf       [ 700 ];
  /*unsigned int*/ size_t  buffer_length  = MAX_ORACLE_ERROR_MESSAGE_STRING + 1,
		buflen,
		function_code,
                message_length = 0,
                cod_error = sqlca.sqlcode;

  if ( cod_error != 0 ) 
  {
    *error_message = '\0';
    buflen = (long) sizeof (stmt_buf);

    sqlglm (error_message, &buffer_length, &message_length);
    sqlgls ((char *)(stmt_buf), &buflen, &function_code);

    *(error_message + message_length) = '\0';

#if !defined (GFI_NO_ORACLE_ERRORS)

    fprintf (stdout, "Oracle ERROR (%ld): \"%s\"\n",
	     sqlca.sqlcode, error_message);

    fprintf (stdout, "Ultimo commando de SQL : \n %d '%s' \n", 
	     oraca.orastxt.orastxtl, stmt_buf);

    fprintf (stdout, "\nAt or near line number %ld\n", oraca.oraslnr); 
#endif
    
    /* EXEC SQL WHENEVER SQLERROR CONTINUE; */ 
#line 330 "../../gral/src_db/gralsql_oracle11.pc"


    /* EXEC SQL ROLLBACK WORK; */ 
#line 332 "../../gral/src_db/gralsql_oracle11.pc"

{
#line 332 "../../gral/src_db/gralsql_oracle11.pc"
    struct sqlexd sqlstm;
#line 332 "../../gral/src_db/gralsql_oracle11.pc"
    sqlstm.sqlvsn = 13;
#line 332 "../../gral/src_db/gralsql_oracle11.pc"
    sqlstm.arrsiz = 4;
#line 332 "../../gral/src_db/gralsql_oracle11.pc"
    sqlstm.sqladtp = &sqladt;
#line 332 "../../gral/src_db/gralsql_oracle11.pc"
    sqlstm.sqltdsp = &sqltds;
#line 332 "../../gral/src_db/gralsql_oracle11.pc"
    sqlstm.iters = (unsigned int  )1;
#line 332 "../../gral/src_db/gralsql_oracle11.pc"
    sqlstm.offset = (unsigned int  )96;
#line 332 "../../gral/src_db/gralsql_oracle11.pc"
    sqlstm.cud = sqlcud0;
#line 332 "../../gral/src_db/gralsql_oracle11.pc"
    sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 332 "../../gral/src_db/gralsql_oracle11.pc"
    sqlstm.sqlety = (unsigned short)4352;
#line 332 "../../gral/src_db/gralsql_oracle11.pc"
    sqlstm.occurs = (unsigned int  )0;
#line 332 "../../gral/src_db/gralsql_oracle11.pc"
    sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 332 "../../gral/src_db/gralsql_oracle11.pc"
}

#line 332 "../../gral/src_db/gralsql_oracle11.pc"

  }

 return cod_error;

} /*-- Fin de ChecaErrorMsg ---*/

/*************************************************************************/
