/*
 * Mensajes de Intercomunicacion aplicativa "mia.h"
 */
#ifndef MIA_H
#define MIA_H

#if defined(__cplusplus)
extern "C" {
#endif
extern int CreaMensaje (char *, char *);
extern int Usuario_390 (char *, char *);
extern int AgregaCampo (int, char *);
extern int EjecutaT390 (char *);
#if defined(__cplusplus)
}
#endif

/*       Lista general de codigos de respuesta                  */

#define MIA_BIEN    0     /* Operacion exitosa                  */
#define MIA_CDUP 7100     /* Campo duplicado                    */
#define MIA_CNOE 7101     /* Campo inexistente                  */
#define MIA_MNUL 7105     /* Mensaje nulo                       */
#define MIA_LINV 7110     /* Longitud invalida                  */
#define MIA_DNUL 7115     /* Secc Datos Nula                    */
#define MIA_NMEM 7120     /* Memoria insuficiente               */
#define MIA_LMAX 7125     /* Longitud maxima excedida           */
#define MIA_MINC 7130     /* Mensaje incompleto                 */
#define MIA_QMGR 7135     /* Variable de inicio QMGR indefinida */
#define MIA_QENT 7140     /* Variable de inicio QENT indefinida */
#define MIA_QSAL 7145     /* Variable de inicio QSAL indefinida */
#define MIA_AINV 7150     /* Argumento invalido                 */
#define MIA_NCFG 7155     /* No hay archivo de configuracion    */
#define MIA_TOUT 7160     /* Tempo de espera excedido (Time Out)*/

#endif
