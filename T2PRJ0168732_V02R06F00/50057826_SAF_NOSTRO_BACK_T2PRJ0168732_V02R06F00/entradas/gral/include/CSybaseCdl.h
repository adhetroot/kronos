///////////////////////////////////////////////////////////////////
//                                                               //
//  Programa auxiliar para manejo de MENSAJES y ERRRORES Sybase  //
//                                                               //
//  CSYBASECDL.H                                                 //
//                          Heriberto Guapo (G. Tecnis) Mar-2010 //
//                                                               //
///////////////////////////////////////////////////////////////////

#define SQLCA_STORAGE_CLASS extern
#define ORACA_STORAGE_CLASS extern


#define SEPARADOR            '|'

#if defined (__cplusplus) || defined (c_plusplus)
extern "C"
{
#endif

#include <sqlda.h>
#include <sqlcpr.h>

#if defined (__cplusplus) || defined (c_plusplus)
}
#endif

#include <time.h>
#include <math.h>
#include <gfi-err.h>
#include </opt/sybase/include/sybfront.h>
#include </opt/sybase/include/sybdb.h>
#include </opt/sybase/include/syberror.h>

// Variables Entorno SYBASE
DBPROCESS    *dbproc;     // The connection with SQL Server
LOGINREC     *login;      // The login information
/*DBINT         msgno;
int           msgstate;
int           severity;
char         *msgtext;
char         *srvname;
char         *procname;
DBUSMALLINT   line;
int           dberr;
int           oserr;
char         *oserrstr;
char         *dberrstr;*/

// Variables BANXICO
DBINT        SYBConsec;
DBINT        SYBCveMsg;
DBCHAR       SYBHorMsg[40];
DBCHAR       SYBConten[350];
RETCODE      return_code;

//int err_handler();
//int msg_handler();
int (*err_handler)();
int (*msg_handler)();
/*
int CS_PUBLIC msg_handler(DBPROCESS *dbproc, DBINT msgno, int msgstate, int severity, char msgtext,
                          char *srvname, char *procname, int line)
int CS_PUBLIC msg_handler()
{
  printf ("Msg %ld, Level %d, State %d\n", msgno, severity, msgstate);
  if (strlen(srvname) > 0)
    printf ("Server '%s', ", srvname);

  if (strlen(procname) > 0)
    printf ("Procedure '%s', ", procname);

  if (line > 0)
    printf ("Line %d", line);

  printf("\n\t%s\n", msgtext);
  return(0);
}

int CS_PUBLIC err_handler(DBPROCESS *dbproc, int severity, int dberr, int oserr, char *dberrstr, char *oserrstr)
int CS_PUBLIC err_handler()
{
  if ((dbproc == NULL) || (DBDEAD(dbproc)))
    return(INT_EXIT);
  else
  {
    printf("DB-Library error:\n\t%s\n", dberrstr);

    if (oserr != DBNOERR)
      printf("Operating-system error:\n\t%s\n", oserrstr);

    return(INT_CANCEL);
  }
}
*/
