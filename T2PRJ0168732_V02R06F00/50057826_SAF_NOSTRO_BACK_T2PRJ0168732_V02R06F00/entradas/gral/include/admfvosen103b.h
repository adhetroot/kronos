/* **************************************************************************
    Librería de Envio de MT-103 Envio de pago

    Modulo para envio de mensajes Swift MT103 para pagos For Further Credit

    Creacion 19 de Junio de 2006 por Carlos Jesus Gutierrez Cortazar

//                             DHJ 04-10-2007 (AMX-07-02200-0A)              //

//                        DHJ 27-11-2008 (QUITAR MAC DEL PIE DEL MENSAJE)  //

**************************************************************************** */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include <time.h>

#define SI 1
#define NO 0

int NIVEL_TRACE103 = 1;

char *findNoFile( char *path, char *name);
void setCampo_SC1( char *campo, char *prefijo, char *cuenta_corresponsal_enviador);
void setCampo_SC2( char *campo, char *prefijo, char *datos);

class Mensaje103
{
    private:
        char *Mensaje;

        char CampoF01[45];
        char Campo2[70];
        char Campo20[35];
        char Campo23B[25];
        char Campo23E[60];
        char Campo32A[45];
        char Campo33B[35];
        char Campo50A[70];
        char Campo50F[210]; //DHJ 04-10-2007 (AMX-07-02200-0A)
        char Campo50K[210];
        char Campo52A[75];
        char Campo52D[210];
        char Campo53A[75];
        char Campo54A[49];
        char Campo56A[49];
        char Campo57A[49];
        char Campo59[210];
        char Campo59A[70];
        char Campo70[180];
        char Campo71A[20];
        char Campo71F[35];
        char Campo72[250];

        int CampoF01_llenado;
        int Campo2_llenado;
        int Campo20_llenado;
        int Campo23B_llenado;
        int Campo32A_llenado;
        int Campo33B_llenado;
        int Campo23E_llenado;
        int Campo50A_llenado;
        int Campo50F_llenado; //DHJ 04-10-2007 (AMX-07-02200-0A)
        int Campo50K_llenado;
        int Campo52A_llenado;
        int Campo52D_llenado;
        int Campo53A_llenado;
        int Campo54A_llenado;
        int Campo56A_llenado;
        int Campo57A_llenado;
        int Campo59_llenado;
        int Campo59A_llenado;
        int Campo70_llenado;
        int Campo71A_llenado;
        int Campo71F_llenado;
        int Campo72_llenado;

        int tamanio_mensaje;

    public:
        Mensaje103();
        ~Mensaje103();

        void setMensaje( void );
        const char *getMensaje() { return Mensaje;};

        void setCampoF01( char *banco);
        void setCampo2( char *banco);
        void setCampo20( char *referencia);
        void setCampo23B( char *boc);
        void setCampo23E( char *codigo_instruccion);
        void setCampo32A( char *monto);
        void setCampo33B( char *monto);
        void setCampo50A( char *cliente_ordenante);
        void setCampo50F( char *cliente_ordenante); //DHJ 04-10-2007 (AMX-07-02200-0A)
        void setCampo50K( char *cliente_ordenante);
        void setCampo52A( char *institucion_ordenante);
        void setCampo52A_SC1( char *cuenta_institucion_ordenante, int terminar);
        void setCampo52A_SC2( char *bic, int terminar);
        void setCampo52D( char *institucion_ordenante);
        void setCampo52D_SC1( char *cuenta_institucion_ordenante, int terminar);
        void setCampo52D_SC2( char *nombre_direccion, int terminar);
        void setCampo53A( char *corresponsal_enviador);
        void setCampo53A_SC1( char *cuenta_corresponsal_enviador, int terminar);
        void setCampo53A_SC2( char *bic, int terminar);
        void setCampo54A( char *cliente_ordenante);
        void setCampo56A( char *cliente_ordenante);
        void setCampo57A( char *cliente_ordenante);
        void setCampo59( char *cliente_beneficiario);
        void setCampo59_SC1( char *cuenta_cliente_beneficiario, int terminar);
        void setCampo59_SC2( char *nombre_direccion, int terminar);
        void setCampo59A( char *cliente_beneficiario);
        void setCampo59A_SC1( char *cuenta_cliente_beneficiario, int terminar);
        void setCampo59A_SC2( char *bic, int terminar);
        void setCampo70( char *narrativa);
        void setCampo71A( char *detalle_cargos);
        void setCampo71F( char *cargos_enviador);
        void setCampo72( char *texto);

        void LimpiaMensaje( void);

        int getLongitudMensaje() { return( tamanio_mensaje);};

};

Mensaje103::Mensaje103()
{
    this->CampoF01[ 0] = '\x0';
    this->Campo2[ 0] = '\x0';
    this->Campo20[ 0] = '\x0';
    this->Campo23B[ 0] = '\x0';
    this->Campo23E[ 0] = '\x0';
    this->Campo32A[ 0] = '\x0';
    this->Campo33B[ 0] = '\x0';
    this->Campo50A[ 0] = '\x0';
    this->Campo50F[ 0] = '\x0'; //DHJ 04-10-2007 (AMX-07-02200-0A)
    this->Campo50K[ 0] = '\x0';
    this->Campo52A[ 0] = '\x0';
    this->Campo52D[ 0] = '\x0';
    this->Campo53A[ 0] = '\x0';
    this->Campo54A[ 0] = '\x0';
    this->Campo56A[ 0] = '\x0';
    this->Campo57A[ 0] = '\x0';
    this->Campo59[ 0] = '\x0';
    this->Campo59A[ 0] = '\x0';
    this->Campo70[ 0] = '\x0';
    this->Campo71A[ 0] = '\x0';
    this->Campo71F[ 0] = '\x0';
    this->Campo72[ 0] = '\x0';
}

void Mensaje103::setCampoF01( char *banco)
{
  //sprintf( CampoF01, "%c{1:F01%sAXXX0000000000}", (char) 1, banco);
  sprintf( CampoF01, "{1:F01%sAXXX0000000000}", banco);
  CampoF01_llenado = SI;
}

void Mensaje103::setCampo2( char *banco)
{
  sprintf( Campo2, "{2:%s}", banco);
  Campo2_llenado = SI;
}

void Mensaje103::setCampo20( char *referencia)
{
  sprintf( Campo20, ":20:%s%c%c", referencia, (char) 13, (char) 10);
  Campo20_llenado = SI;
}

void Mensaje103::setCampo23B( char *boc)
{
  sprintf( Campo23B, ":23B:%s%c%c", boc, (char) 13, (char) 10);
  Campo23B_llenado = SI;
}

void Mensaje103::setCampo23E( char *codigo_instruccion)
{
  sprintf( Campo23E, ":23E:%s%c%c", codigo_instruccion, (char) 13, (char) 10);
  Campo23E_llenado = SI;
}

void Mensaje103::setCampo32A( char *monto)
{
  sprintf( Campo32A, ":32A:%s%c%c", monto, (char) 13, (char) 10);
  Campo32A_llenado = SI;
}

void Mensaje103::setCampo33B( char *monto)
{
  sprintf( Campo33B, ":33B:%s%c%c", monto, (char) 13, (char) 10);
  Campo33B_llenado = SI;
}

void Mensaje103::setCampo50A( char *texto)
{
  int i, cC=0, cR=1;
  long cmp_len=0;
  Gfi_StrRTrim(texto);
  cmp_len = strlen(texto);
  sprintf(Campo50A, ":50A:");
  for (i=0; i<cmp_len; i++)
  {
    if( texto[i] != 13 )
      cC++;
    if( texto[i] == 10 )
    {
      if( ++cR > 2 ) break;
      if( texto[i-1] != 13)
        sprintf(Campo50A, "%s%c", Campo50A, toascii(13));
      cC = 0;
    }
    if( cC > 35 )
    {
      if( ++cR > 2 ) break;
      sprintf(Campo50A, "%s%c%c%c", Campo50A, toascii(13), toascii(10), texto[i]);
      cC = 1;
    }
    else
      sprintf(Campo50A, "%s%c", Campo50A, texto[i]);
  }
  sprintf(Campo50A, "%s%c%c", Campo50A, toascii(13), toascii(10));
  //sprintf( Campo50A, ":50A:%s%c%c", texto, (char) 13, (char) 10);
  Campo50A_llenado = SI;
}

void Mensaje103::setCampo50F( char *texto)                        //DHJ 04-10-2007 (AMX-07-02200-0A)
{                                                                             //DHJ 04-10-2007 (AMX-07-02200-0A)
  int i, cC=0, cR=1;
  long cmp_len=0;
  Gfi_StrRTrim(texto);
  cmp_len = strlen(texto);
  sprintf(Campo50F, ":50F:");
  for (i=0; i<cmp_len; i++)
  {
    if( texto[i] != 13 )
      cC++;
    if( texto[i] == 10 )
    {
      if( ++cR > 5 ) break;
      if( texto[i-1] != 13)
        sprintf(Campo50F, "%s%c", Campo50F, toascii(13));
      cC = 0;
    }
    if( cC > 35 )
    {
      if( ++cR > 5 ) break;
      sprintf(Campo50F, "%s%c%c%c", Campo50F, toascii(13), toascii(10), texto[i]);
      cC = 1;
    }
    else
      sprintf(Campo50F, "%s%c", Campo50F, texto[i]);
  }
  sprintf(Campo50F, "%s%c%c", Campo50F, toascii(13), toascii(10));
  //sprintf( Campo50F, ":50F:%s%c%c", texto, (char) 13, (char) 10); //DHJ 04-10-2007 (AMX-07-02200-0A)
  Campo50F_llenado = SI;                                                      //DHJ 04-10-2007 (AMX-07-02200-0A)
}

void Mensaje103::setCampo50K( char *texto)
{
  int i, cC=0, cR=1;
  long cmp_len=0;
  Gfi_StrRTrim(texto);
  cmp_len = strlen(texto);
  sprintf(Campo50K, ":50K:");
  for (i=0; i<cmp_len; i++)
  {
    if( texto[i] != 13 )
      cC++;
    if( texto[i] == 10 )
    {
      if( ++cR > 5 ) break;
      if( texto[i-1] != 13)
        sprintf(Campo50K, "%s%c", Campo50K, toascii(13));
      cC = 0;
    }
    if( cC > 35 )
    {
      if( ++cR > 5 ) break;
      sprintf(Campo50K, "%s%c%c%c", Campo50K, toascii(13), toascii(10), texto[i]);
      cC = 1;
    }
    else
      sprintf(Campo50K, "%s%c", Campo50K, texto[i]);
  }
  sprintf(Campo50K, "%s%c%c", Campo50K, toascii(13), toascii(10));
  //sprintf( Campo50K, ":50K:%s%c%c", texto, (char) 13, (char) 10);
  Campo50K_llenado = SI;
}

void Mensaje103::setCampo52A( char *texto)
{
  int i, cC=0, cR=1;
  long cmp_len=0;
  Gfi_StrRTrim(texto);
  cmp_len = strlen(texto);
  sprintf(Campo52A, ":52A:");
  for (i=0; i<cmp_len; i++)
  {
    if( texto[i] != 13 )
      cC++;
    if( texto[i] == 10 )
    {
      if( ++cR > 2 ) break;
      if( texto[i-1] != 13)
        sprintf(Campo52A, "%s%c", Campo52A, toascii(13));
      cC = 0;
    }
    if( cC > 35 )
    {
      if( ++cR > 2 ) break;
      sprintf(Campo52A, "%s%c%c%c", Campo52A, toascii(13), toascii(10), texto[i]);
      cC = 1;
    }
    else
      sprintf(Campo52A, "%s%c", Campo52A, texto[i]);
  }
  sprintf(Campo52A, "%s%c%c", Campo52A, toascii(13), toascii(10));
  //sprintf( Campo52A, ":52A:%s%c%c", texto, (char) 13, (char) 10);
  Campo52A_llenado = SI;
}

void Mensaje103::setCampo52A_SC1( char *cuenta_institucion_ordenante, int terminar)
{
  setCampo_SC1( Campo52A, "52A", cuenta_institucion_ordenante);
  sprintf( Campo52A, ":52A:%s%c%c", cuenta_institucion_ordenante, (char) 13, (char) 10);

  if ( terminar == SI)
    Campo52A_llenado = SI;
  else
    Campo52A_llenado = NO;
}

void Mensaje103::setCampo52A_SC2( char *bic, int terminar)
{
  setCampo_SC2( Campo52A, "52A", bic);

  if ( terminar == SI)
    Campo52A_llenado = SI;
  else
    Campo52A_llenado = NO;
}

void Mensaje103::setCampo52D( char *texto)
{
  int i, cC=0, cR=1;
  long cmp_len=0;
  Gfi_StrRTrim(texto);
  cmp_len = strlen(texto);
  sprintf(Campo52D, ":52D:");
  for (i=0; i<cmp_len; i++)
  {
    if( texto[i] != 13 )
      cC++;
    if( texto[i] == 10 )
    {
      if( ++cR > 5 ) break;
      if( texto[i-1] != 13)
        sprintf(Campo52D, "%s%c", Campo52D, toascii(13));
      cC = 0;
    }
    if( cC > 35 )
    {
      if( ++cR > 5 ) break;
      sprintf(Campo52D, "%s%c%c%c", Campo52D, toascii(13), toascii(10), texto[i]);
      cC = 1;
    }
    else
      sprintf(Campo52D, "%s%c", Campo52D, texto[i]);
  }
  sprintf(Campo52D, "%s%c%c", Campo52D, toascii(13), toascii(10));
  //sprintf( Campo52D, ":52D:%s%c%c", texto, (char) 13, (char) 10);
  Campo52D_llenado = SI;
}

void Mensaje103::setCampo52D_SC1( char *cuenta_institucion_ordenante, int terminar)
{
  setCampo_SC1( Campo52D, "52D", cuenta_institucion_ordenante);

  if ( terminar == SI)
    Campo52D_llenado = SI;
  else
    Campo52D_llenado = NO;
}

void Mensaje103::setCampo52D_SC2( char *nombre_direccion, int terminar)
{
  setCampo_SC2( Campo52D, "52D", nombre_direccion);

  if ( terminar == SI)
    Campo52D_llenado = SI;
  else
    Campo52D_llenado = NO;
}

void Mensaje103::setCampo53A( char *texto)
{
  int i, cC=0, cR=1;
  long cmp_len=0;
  Gfi_StrRTrim(texto);
  cmp_len = strlen(texto);
  sprintf(Campo53A, ":53A:");
  for (i=0; i<cmp_len; i++)
  {
    if( texto[i] != 13 )
      cC++;
    if( texto[i] == 10 )
    {
      if( ++cR > 2 ) break;
      if( texto[i-1] != 13)
        sprintf(Campo53A, "%s%c", Campo53A, toascii(13));
      cC = 0;
    }
    if( cC > 35 )
    {
      if( ++cR > 2 ) break;
      sprintf(Campo53A, "%s%c%c%c", Campo53A, toascii(13), toascii(10), texto[i]);
      cC = 1;
    }
    else
      sprintf(Campo53A, "%s%c", Campo53A, texto[i]);
  }
  sprintf(Campo53A, "%s%c%c", Campo53A, toascii(13), toascii(10));
  //sprintf( Campo53A, ":53A:%s%c%c", texto, (char) 13, (char) 10);
  Campo53A_llenado = SI;
}

void Mensaje103::setCampo53A_SC1( char *cuenta_corresponsal_enviador, int terminar)
{
  setCampo_SC1( Campo53A, "53A", cuenta_corresponsal_enviador);

  if ( terminar == SI)
    Campo53A_llenado = SI;
  else
    Campo53A_llenado = NO;
}

void Mensaje103::setCampo53A_SC2( char *bic, int terminar)
{
  setCampo_SC2( Campo53A, "53A", bic);

  if ( terminar == SI)
    Campo53A_llenado = SI;
  else
    Campo53A_llenado = NO;
}

void Mensaje103::setCampo54A( char *texto)
{
  int i, cC=0, cR=1;
  long cmp_len=0;
  Gfi_StrRTrim(texto);
  cmp_len = strlen(texto);
  sprintf(Campo54A, ":54A:");
  for (i=0; i<cmp_len; i++)
  {
    if( texto[i] != 13 )
      cC++;
    if( texto[i] == 10 )
    {
      if( ++cR > 2 ) break;
      if( texto[i-1] != 13)
        sprintf(Campo54A, "%s%c", Campo54A, toascii(13));
      cC = 0;
    }
    if( cC > 35 )
    {
      if( ++cR > 2 ) break;
      sprintf(Campo54A, "%s%c%c%c", Campo54A, toascii(13), toascii(10), texto[i]);
      cC = 1;
    }
    else
      sprintf(Campo54A, "%s%c", Campo54A, texto[i]);
  }
  sprintf(Campo54A, "%s%c%c", Campo54A, toascii(13), toascii(10));
  //sprintf( Campo54A, ":54A:%s%c%c", texto, (char) 13, (char) 10);
  Campo54A_llenado = SI;
}

void Mensaje103::setCampo56A( char *texto)
{
  int i, cC=0, cR=1;
  long cmp_len=0;
  Gfi_StrRTrim(texto);
  cmp_len = strlen(texto);
  sprintf(Campo56A, ":56A:");
  for (i=0; i<cmp_len; i++)
  {
    if( texto[i] != 13 )
      cC++;
    if( texto[i] == 10 )
    {
      if( ++cR > 2 ) break;
      if( texto[i-1] != 13)
        sprintf(Campo56A, "%s%c", Campo56A, toascii(13));
      cC = 0;
    }
    if( cC > 35 )
    {
      if( ++cR > 2 ) break;
      sprintf(Campo56A, "%s%c%c%c", Campo56A, toascii(13), toascii(10), texto[i]);
      cC = 1;
    }
    else
      sprintf(Campo56A, "%s%c", Campo56A, texto[i]);
  }
  sprintf(Campo56A, "%s%c%c", Campo56A, toascii(13), toascii(10));
  //sprintf( Campo56A, ":56A:%s%c%c", texto, (char) 13, (char) 10);
  Campo56A_llenado = SI;
}

void Mensaje103::setCampo57A( char *texto)
{
  int i, cC=0, cR=1;
  long cmp_len=0;
  Gfi_StrRTrim(texto);
  cmp_len = strlen(texto);
  sprintf(Campo57A, ":57A:");
  for (i=0; i<cmp_len; i++)
  {
    if( texto[i] != 13 )
      cC++;
    if( texto[i] == 10 )
    {
      if( ++cR > 2 ) break;
      if( texto[i-1] != 13)
        sprintf(Campo57A, "%s%c", Campo57A, toascii(13));
      cC = 0;
    }
    if( cC > 35 )
    {
      if( ++cR > 2 ) break;
      sprintf(Campo57A, "%s%c%c%c", Campo57A, toascii(13), toascii(10), texto[i]);
      cC = 1;
    }
    else
      sprintf(Campo57A, "%s%c", Campo57A, texto[i]);
  }
  sprintf(Campo57A, "%s%c%c", Campo57A, toascii(13), toascii(10));
  //sprintf( Campo57A, ":57A:%s%c%c", texto, (char) 13, (char) 10);
  Campo57A_llenado = SI;
}

void Mensaje103::setCampo59( char *texto)
{
  int i, cC=0, cR=1;
  long cmp_len=0;
  Gfi_StrRTrim(texto);
  cmp_len = strlen(texto);
  sprintf(Campo59, ":59:");
  for (i=0; i<cmp_len; i++)
  {
    if( texto[i] != 13 )
      cC++;
    if( texto[i] == 10 )
    {
      if( ++cR > 5 ) break;
      if( texto[i-1] != 13)
        sprintf(Campo59, "%s%c", Campo59, toascii(13));
      cC = 0;
    }
    if( cC > 35 )
    {
      if( ++cR > 5 ) break;
      sprintf(Campo59, "%s%c%c%c", Campo59, toascii(13), toascii(10), texto[i]);
      cC = 1;
    }
    else
      sprintf(Campo59, "%s%c", Campo59, texto[i]);
  }
  sprintf(Campo59, "%s%c%c", Campo59, toascii(13), toascii(10));
  //sprintf( Campo59, ":59:%s%c%c", texto, (char) 13, (char) 10);
  Campo59_llenado = SI;
}

void Mensaje103::setCampo59_SC1( char *cuenta_cliente_beneficiario, int terminar)
{
  setCampo_SC1( Campo59, "59", cuenta_cliente_beneficiario);

  if ( terminar == SI)
    Campo59_llenado = SI;
  else
    Campo59_llenado = NO;
}

void Mensaje103::setCampo59_SC2( char *nombre_direccion, int terminar)
{
  setCampo_SC2( Campo59, "59", nombre_direccion);

  if ( terminar == SI)
    Campo59_llenado = SI;
  else
    Campo59_llenado = NO;
}

void Mensaje103::setCampo59A( char *texto)
{
  int i, cC=0, cR=1;
  long cmp_len=0;
  Gfi_StrRTrim(texto);
  cmp_len = strlen(texto);
  sprintf(Campo59A, ":59A:");
  for (i=0; i<cmp_len; i++)
  {
    if( texto[i] != 13 )
      cC++;
    if( texto[i] == 10 )
    {
      if( ++cR > 2 ) break;
      if( texto[i-1] != 13)
        sprintf(Campo59A, "%s%c", Campo59A, toascii(13));
      cC = 0;
    }
    if( cC > 35 )
    {
      if( ++cR > 2 ) break;
      sprintf(Campo59A, "%s%c%c%c", Campo59A, toascii(13), toascii(10), texto[i]);
      cC = 1;
    }
    else
      sprintf(Campo59A, "%s%c", Campo59A, texto[i]);
  }
  sprintf(Campo59A, "%s%c%c", Campo59A, toascii(13), toascii(10));
  //sprintf( Campo59A, ":59A:%s%c%c", texto, (char) 13, (char) 10);
  Campo59A_llenado = SI;
}

void Mensaje103::setCampo59A_SC1( char *cuenta_cliente_beneficiario, int terminar)
{
  setCampo_SC1( Campo59A, "59A", cuenta_cliente_beneficiario);

  if ( terminar == SI)
    Campo59A_llenado = SI;
  else
    Campo59A_llenado = NO;
}

void Mensaje103::setCampo59A_SC2( char *bic, int terminar)
{
  setCampo_SC2( Campo59A, "59A", bic);

  if ( terminar == SI)
    Campo59A_llenado = SI;
  else
    Campo59A_llenado = NO;
}

void Mensaje103::setCampo70( char *texto)
{
  int i, cC=0, cR=1;
  long cmp_len=0;
  Gfi_StrRTrim(texto);
  cmp_len = strlen(texto);
  sprintf(Campo70, ":70:");
  for (i=0; i<cmp_len; i++)
  {
    if( texto[i] != 13 )
      cC++;
    if( texto[i] == 10 )
    {
      if( ++cR > 4 ) break;
      if( texto[i-1] != 13)
        sprintf(Campo70, "%s%c", Campo70, toascii(13));
      cC = 0;
    }
    if( cC > 35 )
    {
      if( ++cR > 4 ) break;
      sprintf(Campo70, "%s%c%c%c", Campo70, toascii(13), toascii(10), texto[i]);
      cC = 1;
    }
    else
      sprintf(Campo70, "%s%c", Campo70, texto[i]);
  }
  sprintf(Campo70, "%s%c%c", Campo70, toascii(13), toascii(10));
  //sprintf( Campo70, ":70:%s%c%c", texto, (char) 13, (char) 10);
  Campo70_llenado = SI;
}

void Mensaje103::setCampo71A( char *detalle_cargos)
{
  sprintf( Campo71A, ":71A:%s%c%c", detalle_cargos, (char) 13, (char) 10);
  Campo71A_llenado = SI;
}

void Mensaje103::setCampo71F( char *cargos_enviador)
{
  sprintf( Campo71F, ":71F:%s%c%c", cargos_enviador, (char) 13, (char) 10);
  Campo71F_llenado = SI;
}

void Mensaje103::setCampo72( char *texto)
{
  int i, cC=0, cR=1;
  long cmp_len=0;
  Gfi_StrRTrim(texto);
  cmp_len = strlen(texto);
  sprintf(Campo72, ":72:");
  for (i=0; i<cmp_len; i++)
  {
    if( texto[i] != 13 )
      cC++;
    if( texto[i] == 10 )
    {
      if( ++cR > 6 ) break;
      if( texto[i-1] != 13)
        sprintf(Campo72, "%s%c", Campo72, toascii(13));
      sprintf(Campo72, "%s%c//", Campo72, texto[i]);
      cC = 2;
    }
    if( cC > 35 )
    {
      if( ++cR > 6 ) break;
      sprintf(Campo72, "%s%c%c//%c", Campo72, toascii(13), toascii(10), texto[i]);
      cC = 3;
    }
    else if( texto[i] != 10 )
      sprintf(Campo72, "%s%c", Campo72, texto[i]);
  }
  sprintf(Campo72, "%s%c%c", Campo72, toascii(13), toascii(10));
  //sprintf( Campo72, ":72:%s%c%c", texto, (char) 13, (char) 10);
  Campo72_llenado = SI;
}

void Mensaje103::setMensaje()
{
   char Hora[ 10];
   int i = 0;
   int resultado;

   time_t Fecha;
   tm     *Fecha_1;

   time(&Fecha);
   Fecha_1 = (localtime(&Fecha));

    // Se va a generar el mensaje
    // se reservan 10000 caracteres unicamente del mensaje
    Hora[0] = '\x0';
    sprintf(Hora, "%d%d", Fecha_1->tm_hour, Fecha_1->tm_min);
    Hora[4] = '\x0';

    Mensaje = (char *)malloc( sizeof(char) * 10000);
    if( !Mensaje ) {
        printf("Error: No existe suficiente memoria para crear el mensaje\n");
        printf("el proceso se cancela.\n");
        exit(0);
    }

    if ( NIVEL_TRACE103 > 0)
      printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);

    if ( CampoF01_llenado == SI)
    {
      tamanio_mensaje = sprintf( Mensaje, "%s", CampoF01);

      if ( NIVEL_TRACE103 > 0)
        printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);
    }
    else
      resultado = GFI_ERRC_FATAL;

    if ( Campo2_llenado == SI)
    {
      tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "%s", Campo2);

      if ( NIVEL_TRACE103 > 0)
        printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);
    }
    else
      resultado = GFI_ERRC_FATAL;

    // Se genera el campo 4 cuerpo del mensaje
    tamanio_mensaje += sprintf( Mensaje  + tamanio_mensaje, "{4:%c%c", (char) 13, (char) 10);

    if ( NIVEL_TRACE103 > 0)
      printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);

    if ( Campo20_llenado == SI)
    {
      tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "%s", Campo20);

      if ( NIVEL_TRACE103 > 0)
        printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);
    }
    else
      resultado = GFI_ERRC_FATAL;

    if ( Campo23B_llenado == SI)
    {
      tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "%s", Campo23B);

      if ( NIVEL_TRACE103 > 0)
        printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);
    }
    else
      resultado = GFI_ERRC_FATAL;

    if ( Campo23E_llenado == SI)
    {
      tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "%s", Campo23E);

      if ( NIVEL_TRACE103 > 0)
        printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);
    }
    else
      resultado = GFI_ERRC_FATAL;

    if ( Campo32A_llenado == SI)
    {
      tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "%s", Campo32A);

      if ( NIVEL_TRACE103 > 0)
        printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);
    }
    else
      resultado = GFI_ERRC_FATAL;

    if ( Campo33B_llenado == SI)
    {
      tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "%s", Campo33B);

      if ( NIVEL_TRACE103 > 0)
        printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);
    }
    else
      resultado = GFI_ERRC_FATAL;

   if ( Campo50A_llenado == SI)
   {
      tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "%s", Campo50A);

      if ( NIVEL_TRACE103 > 0)
        printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);
    }

    if ( Campo50F_llenado == SI)                                              //DHJ 04-10-2007 (AMX-07-02200-0A)
    {                                                                         //DHJ 04-10-2007 (AMX-07-02200-0A)
      tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "%s", Campo50F); //DHJ 04-10-2007 (AMX-07-02200-0A)

      if ( NIVEL_TRACE103 > 0)                                                   //DHJ 04-10-2007 (AMX-07-02200-0A)
        printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);            //DHJ 04-10-2007 (AMX-07-02200-0A)
    }                                                                         //DHJ 04-10-2007 (AMX-07-02200-0A)

    if ( Campo50K_llenado == SI)
    {
      tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "%s", Campo50K);

      if ( NIVEL_TRACE103 > 0)
        printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);
    }

    if ( Campo52A_llenado == SI)
    {
      tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "%s", Campo52A);

      if ( NIVEL_TRACE103 > 0)
        printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);
    }

    if ( Campo52D_llenado == SI)
    {
      tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "%s", Campo52D);

      if ( NIVEL_TRACE103 > 0)
        printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);
    }

    if (( Campo52A_llenado != SI) && (Campo52D_llenado != SI))
    {
      resultado = GFI_ERRC_FATAL;

      if ( NIVEL_TRACE103 > 0)
        printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);
    }

    if ( Campo53A_llenado == SI)
    {
      tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "%s", Campo53A);

      if ( NIVEL_TRACE103 > 0)
        printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);
    }
    else
      resultado = GFI_ERRC_FATAL;

    if ( Campo54A_llenado == SI)
    {
      tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "%s", Campo54A);

      if ( NIVEL_TRACE103 > 0)
        printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);
    }
    else
      resultado = GFI_ERRC_FATAL;

    if ( Campo56A_llenado == SI)
    {
      tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "%s", Campo56A);

      if ( NIVEL_TRACE103 > 0)
        printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);
    }
    else
      resultado = GFI_ERRC_FATAL;

    if ( Campo57A_llenado == SI)
    {
      tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "%s", Campo57A);

      if ( NIVEL_TRACE103 > 0)
        printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);
    }
    else
      resultado = GFI_ERRC_FATAL;

    if ( Campo59_llenado == SI)
    {
      tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "%s", Campo59);

      if ( NIVEL_TRACE103 > 0)
        printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);
    }

    if ( Campo59A_llenado == SI)
    {
      tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "%s", Campo59A);

      if ( NIVEL_TRACE103 > 0)
        printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);
    }

    if (( Campo59_llenado != SI) && (Campo59A_llenado != SI))
    {
      resultado = GFI_ERRC_FATAL;

      if ( NIVEL_TRACE103 > 0)
        printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);
    }

    if ( Campo70_llenado == SI)
    {
      tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "%s", Campo70);

      if ( NIVEL_TRACE103 > 0)
        printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);
    }

    if ( Campo71A_llenado == SI)
    {
      tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "%s", Campo71A);

      if ( NIVEL_TRACE103 > 0)
        printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);
    }
    else
      resultado = GFI_ERRC_FATAL;

    if ( Campo71F_llenado == SI)
    {
      tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "%s", Campo71F);

      if ( NIVEL_TRACE103 > 0)
        printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);
    }
    else
      resultado = GFI_ERRC_FATAL;

    /* Segun Richard me falta el campo 72 CGC */

    if ( Campo72_llenado == SI)
    {
      tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "%s", Campo72);

      if ( NIVEL_TRACE103 > 0)
        printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);
    }
    else
      resultado = GFI_ERRC_FATAL;

    // Se cierra el mensaje
//    tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "-}{5:{MAC:1234ABCD}}%c%c%c", '\003', (char) 13, (char) 10);
    //tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "-}{5:{MAC:1234ABCD}}%c", '\003');

    //tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "-}{5:{MAC:1234ABCD}}"); DHJ 27-11-2008 (QUITAR MAC DEL PIE DEL MENSAJE)

    tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "-}%c", '\003'); //DHJ 27-11-2008 (QUITAR MAC DEL PIE DEL MENSAJE)

    if ( NIVEL_TRACE103 > 0)
      printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);

    LimpiaMensaje();

    if ( NIVEL_TRACE103 > 0)
      printf( "Termino de construir el mensaje\n");
}

void Mensaje103::LimpiaMensaje( void)
{
    this->CampoF01[ 0] = '\x0';
    this->Campo2[ 0] = '\x0';
    this->Campo20[ 0] = '\x0';
    this->Campo23B[ 0] = '\x0';
    this->Campo23E[ 0] = '\x0';
    this->Campo32A[ 0] = '\x0';
    this->Campo33B[ 0] = '\x0';
    this->Campo50A[ 0] = '\x0';
    this->Campo50F[ 0] = '\x0'; //DHJ 04-10-2007 (AMX-07-02200-0A)
    this->Campo50K[ 0] = '\x0';
    this->Campo52A[ 0] = '\x0';
    this->Campo52D[ 0] = '\x0';
    this->Campo53A[ 0] = '\x0';
    this->Campo54A[ 0] = '\x0';
    this->Campo56A[ 0] = '\x0';
    this->Campo57A[ 0] = '\x0';
    this->Campo59[ 0] = '\x0';
    this->Campo59A[ 0] = '\x0';
    this->Campo70[ 0] = '\x0';
    this->Campo71A[ 0] = '\x0';
    this->Campo71F[ 0] = '\x0';
    this->Campo72[ 0] = '\x0';

    CampoF01_llenado = 0;
    Campo2_llenado = 0;
    Campo20_llenado = 0;
    Campo23B_llenado = 0;
    Campo32A_llenado = 0;
    Campo33B_llenado = 0;
    Campo23E_llenado = 0;
    Campo50A_llenado = 0;
    Campo50F_llenado = 0; //DHJ 04-10-2007 (AMX-07-02200-0A)
    Campo50K_llenado = 0;
    Campo52A_llenado = 0;
    Campo52D_llenado = 0;
    Campo53A_llenado = 0;
    Campo54A_llenado = 0;
    Campo56A_llenado = 0;
    Campo57A_llenado = 0;
    Campo59_llenado = 0;
    Campo59A_llenado = 0;
    Campo70_llenado = 0;
    Campo71A_llenado = 0;
    Campo71F_llenado = 0;
    Campo72_llenado = 0;
}