/**********************************************************************
 *
 * Grupo Financiero InverMexico
 *
 * Archivo include comun a todos los proyectos
 *
 * Archivo     : gral.h
 *
 * Proposito   : Definicion de includes comunes.
 *
 * Autor       : Enrique Ibarra A.
 *
 * Supervision : Enrique Ibarra A.
 *
 * Historia    :
 *
 **********************************************************************/

#if !defined (_GRALINCL_H_)
#define _GRALINCL_H_

/* Seccion de Includes */

#if defined(__cplusplus) || defined(c_plusplus)
extern "C"
{
#endif

#include  <stdio.h>
#include  <string>
#include  <stdlib.h>
#include  <ctype.h>
#include  <time.h>
#include  "gfi-err.h"

#if defined(__cplusplus) || defined(c_plusplus)
}
#endif

#if defined(__cplusplus) || defined(c_plusplus)
#include <iostream>
#endif

/* Identificacion para el control de versiones RCS */

#if defined (GFSM_VERSION)
static char RCS_GRAL_H[] = "$Id$";

/* Mensajes de log de RCS
 *
 * $Log$
 */

#endif  /*--- GFSM_VERSION ---*/

#if defined(__cplusplus) || defined(c_plusplus)
extern "C"
{
#endif

/* Funciones utilitarias comunes */

  void GfiFree        (void **);
  int  GfiMalloc      (void **, const int);
  int  GfiStrcmp      (const char *, const char *);
  int  Gfi_StrLTrim   (char *);
  int  Gfi_StrRTrim   (char *);

#if defined(__cplusplus) || defined(c_plusplus)
}
#endif

#endif
