/*****************************************************/
/* swift-pc.h                                        */
/* Autor : Victor Hugo Gutiërrez Ch.                 */
/* Fecha : 12 de Noviembre de 2002                   */
/*                                                   */
/*****************************************************/
#define SQLCA_STORAGE_CLASS extern
#define ORACA_STORAGE_CLASS extern


#define SEPARADOR            '|'

#if defined (__cplusplus) || defined (c_plusplus)
extern "C"
{
#endif

#include <sqlda.h>
#include <sqlcpr.h>

#if defined (__cplusplus) || defined (c_plusplus)
}
#endif

#include <time.h>
#include <math.h>
#include <gfi-err.h>

int pon_cab1(char *mensaje, char *bco_env, char *mt);
int pon_cab2(char *mensaje, char *bco_env, char *mt);
int pon_cab3(char *mensaje, char *bco_env, char *mt);
int pon_cab4(char *mensaje, char *bco_env, char *mt);

///////////////////////////////////////////////////////////////////
//                                                               //
//  Funcion : pon_cab1                                           //
//                                                               //
///////////////////////////////////////////////////////////////////
int pon_cab1(char *mensaje, char *bco_env, char *mt)
{
 int result = GFI_ERRC_OK;
 int uno;
 uno = 1;
 sprintf(mensaje, "%c{1:F01SERFMXMMAXXX0000000000}{2:I%s",uno,mt);
 sprintf(mensaje, "%s%sXXXXN}{4:%c%c", mensaje, bco_env, toascii(13), toascii(10));
 return (result);
}

///////////////////////////////////////////////////////////////////
//                                                               //
//  Funcion : pon_cab2                                           //
//                                                               //
///////////////////////////////////////////////////////////////////
int pon_cab2(char *mensaje, char *bco_env, char *mt)
{
 int result = GFI_ERRC_OK;
 int uno;
 uno = 1;
 sprintf(mensaje, "%c{1:F01SERFMXMMAXXX0000000000}{2:I%s",uno,mt);
 sprintf(mensaje, "%s%sN}{4:%c%c", mensaje, bco_env, toascii(13), toascii(10));
 return (result);
}
///////////////////////////////////////////////////////////////////
//                                                               //
//  Funcion : pon_cab3                                           //
//                                                               //
///////////////////////////////////////////////////////////////////
int pon_cab3(char *mensaje, char *bco_env, char *mt)
{
 int result = GFI_ERRC_OK;
 int uno;
 uno = 1;
 sprintf(mensaje, "%c{1:F01BMSXMXMMAXXX0000000000}{2:I%s",uno,mt);
 sprintf(mensaje, "%s%sN}{4:%c%c", mensaje, bco_env, toascii(13), toascii(10));
 return (result);
}
///////////////////////////////////////////////////////////////////
// //
// Funcion : pon_cab4 //
// //
///////////////////////////////////////////////////////////////////
int pon_cab4(char *mensaje, char *bco_env, char *mt)
{
  int result = GFI_ERRC_OK;
  int uno;
  uno = 1;
  sprintf(mensaje, "%c{1:F01BMSXMXMMAXXX0000000000}{2:I%s",uno,mt);
  sprintf(mensaje, "%s%sXXXXN}{4:%c%c", mensaje, bco_env, toascii(13), toascii(10));
  return( result );
}
///////////////////////////////////////////////////////////////////
//                                                               //
//  Funcion : pon_cab_edo_cta                                    //
//                                                               //
///////////////////////////////////////////////////////////////////
int pon_cab_edo_cta(char *mensaje, char *origen, char *destino, char *mt)
{
 int result = GFI_ERRC_OK;
 int uno;
 uno = 1;
 sprintf(mensaje, "%c{1:F01%s0000000000}{2:I%s", uno, origen, mt);
 sprintf(mensaje, "%s%sN}{4:%c%c", mensaje, destino, toascii(13), toascii(10));

 return (result);
}

///////////////////////////////////////////////////////////////////
//                                                               //
//  Funcion : pon_cmp20                                          //
//                                                               //
///////////////////////////////////////////////////////////////////
int pon_cmp20(char *mensaje, char *cmp20)
{
 int result = GFI_ERRC_OK;
 Gfi_StrRTrim(cmp20);

 sprintf(mensaje, "%s:20:%s%c%c", mensaje, cmp20, toascii(13), toascii(10));
 return (result);
}
///////////////////////////////////////////////////////////////////
//                                                               //
//  Funcion : pon_cmp21                                          //
//                                                               //
///////////////////////////////////////////////////////////////////
int pon_cmp21(char *mensaje, char *cmp21)
{
 int result = GFI_ERRC_OK;
 Gfi_StrRTrim(cmp21);

 sprintf(mensaje, "%s:21:%s%c%c", mensaje, cmp21, toascii(13), toascii(10));
 return (result);
}
///////////////////////////////////////////////////////////////////
//                                                               //
//  Funcion : pon_cmp25                                          //
//                                                               //
///////////////////////////////////////////////////////////////////
int pon_cmp25(char *mensaje, char *cmp25)
{
 int result = GFI_ERRC_OK;
 Gfi_StrRTrim(cmp25);

 sprintf(mensaje, "%s:25:%s%c%c", mensaje, cmp25, toascii(13), toascii(10));
 return (result);
}
///////////////////////////////////////////////////////////////////
//                                                               //
//  Funcion : pon_cmp                                            //
//                                                               //
///////////////////////////////////////////////////////////////////
int pon_cmp(char *mensaje, char *campo, char *cmp)
{
 int result = GFI_ERRC_OK;
 Gfi_StrRTrim(cmp);

 if (strcmp(campo,"00") == 0)
   sprintf(mensaje, "%s%s%c%c", mensaje, cmp, toascii(13), toascii(10));
 else
   sprintf(mensaje, "%s:%s:%s%c%c", mensaje, campo, cmp, toascii(13), toascii(10));

 return (result);
}
///////////////////////////////////////////////////////////////////
//                                                               //
//  Funcion : pon_cmp32a                                         //
//                                                               //
///////////////////////////////////////////////////////////////////
int pon_cmp32a(char *mensaje, char *cmp32a)
{
 int result = GFI_ERRC_OK;
 Gfi_StrRTrim(cmp32a);

 sprintf(mensaje, "%s:32A:%s%c%c", mensaje, cmp32a, toascii(13), toascii(10));
 return (result);
}
///////////////////////////////////////////////////////////////////
//                                                               //
//  Funcion : pon_cmp50                                          //
//                                                               //
///////////////////////////////////////////////////////////////////
int pon_cmp50(char *mensaje, char *cmp50)
{
 int result = GFI_ERRC_OK;

 int registro,
     ya,
     tope,
     salir;
 char subcadena[36];
 Gfi_StrRTrim(cmp50);

 ya = 0;
 registro = 0;
 salir = 0;
 if (strlen(cmp50) > 0)
    while (registro < 2 && salir == 0)
    {
        if (strlen(cmp50) < (34*registro+35))
        {
            salir = 1;
            tope = (strlen(cmp50) - (registro*35));
        }
        else
           tope = 35;
        substr(cmp50, (registro*35), tope, subcadena);
        if (ya == 0)
        {
            sprintf(mensaje, "%s:50:%s%c%c", mensaje, subcadena, toascii(13), toascii(10));
            ya++;
        }
        else
           sprintf(mensaje, "%s%s%c%c", mensaje, subcadena, toascii(13), toascii(10));
        registro++;
    }
 return (result);
}
///////////////////////////////////////////////////////////////////
//                                                               //
//  Funcion : pon_cmp52                                          //
//                                                               //
///////////////////////////////////////////////////////////////////
int pon_cmp52(char *mensaje, char *cmp52)
{
 int result = GFI_ERRC_OK;

 int registro, ya, tope, salir, lon;
 char subcadena[36], opcion[2];
 Gfi_StrRTrim(cmp52);

 ya = 0;
 registro = 0;
 strcpy(opcion, " ");
 salir = 0;
 if (strlen(cmp52) > 0)
    while (registro < 5 && salir == 0)
    {
        if ((strlen(cmp52) - 1) < (34*registro+35))
        {
            salir = 1;
            tope = ((strlen(cmp52) - 1) - (registro*35));
        }
        else
           tope = 35;
        substr(cmp52, (registro*35)+1, tope, subcadena);
        lon = strlen(StrRTrim(subcadena));
        if (lon > 0)
           if (ya == 0)
           {
               substr(cmp52, 0, 1, opcion);
               sprintf(mensaje, "%s:52%s:%s%c%c", mensaje, opcion, subcadena, toascii(13), toascii(10));
               ya++;
           }
           else
              sprintf(mensaje, "%s%s%c%c", mensaje, subcadena, toascii(13), toascii(10));
           registro++;
    }
 return (result);
}
///////////////////////////////////////////////////////////////////
//                                                               //
//  Funcion : pon_cmp53                                          //
//                                                               //
///////////////////////////////////////////////////////////////////
int pon_cmp53(char *mensaje, char *cmp53)
{
 int result = GFI_ERRC_OK;

 int registro, ya, tope, salir, lon;
 char subcadena[36], opcion[2];
 Gfi_StrRTrim(cmp53);

 ya = 0;
 registro = 0;
 strcpy(opcion, " ");
 salir = 0;
 if (strlen(cmp53) > 0)
    while (registro < 5 && salir == 0)
    {
        if ((strlen(cmp53) - 1) < (34*registro+35))
        {
            salir = 1;
            tope = ((strlen(cmp53) - 1) - (registro*35));
        }
        else
           tope = 35;
        substr(cmp53, (registro*35)+1, tope, subcadena);
        lon = strlen(StrRTrim(subcadena));
        if (lon > 0)
           if (ya == 0)
           {
               substr(cmp53, 0, 1, opcion);
               sprintf(mensaje, "%s:53%s:%s%c%c", mensaje, opcion, subcadena, toascii(13), toascii(10));
               ya++;
           }
           else
              sprintf(mensaje, "%s%s%c%c", mensaje, subcadena, toascii(13), toascii(10));
        registro++;
    }
 return (result);
}
///////////////////////////////////////////////////////////////////
//                                                               //
//  Funcion : pon_cmp_t0                                         //
//                                                               //
///////////////////////////////////////////////////////////////////
int pon_cmp_t0(char *mensaje, char *cadena, int longitud, char campo[3])
{
 int result = GFI_ERRC_OK;

 int registro, ya, tope, salir;
 char subcadena[36];

 ya = 0;
 registro = 0;
 salir = 0;
 if (longitud > 0 && strcmp(cadena," ") != 0)
    while (registro < 7 && salir == 0)
    {
        if (longitud < (34*registro+35))
        {
            salir = 1;
            tope = (longitud - (registro*35));
        }
        else
           tope = 35;
        substr(cadena, (registro*35), tope, subcadena);
        if (ya == 0)
        {
            sprintf(mensaje, "%s:%s:%s%c%c", mensaje, campo, subcadena, toascii(13), toascii(10));
            ya++;
        }
        else
           sprintf(mensaje, "%s%s%c%c", mensaje, subcadena, toascii(13), toascii(10));
        registro++;
    }
 return (result);
}
///////////////////////////////////////////////////////////////////
//                                                               //
//  Funcion : pon_cmp_t1                                         //
//                                                               //
///////////////////////////////////////////////////////////////////
int pon_cmp_t1(char *mensaje, char *cadena, int longitud, char campo[3])
{
 int result = GFI_ERRC_OK;

 int registro, ya, tope, salir, lon;
 char subcadena[36], opcion[2];

 ya = 0;
 registro = 0;
 strcpy(opcion, " ");
 salir = 0;
 if (longitud > 0)
    while (registro < 5 && salir == 0)
    {
        if ((longitud - 1) < (34*registro+35))
        {
            salir = 1;
            tope = ((longitud - 1) - (registro*35));
        }
        else
           tope = 35;
        substr(cadena, (registro*35)+1, tope, subcadena);
        lon = strlen(StrRTrim(subcadena));
        if (lon > 0)
           if (ya == 0)
           {
               substr(cadena, 0, 1, opcion);
               sprintf(mensaje, "%s:%s%s:%s%c%c", mensaje, campo, opcion, subcadena, toascii(13), toascii(10));
               ya++;
           }
           else
              sprintf(mensaje, "%s%s%c%c", mensaje, subcadena, toascii(13), toascii(10));
        registro++;
    }
 return (result);
}
///////////////////////////////////////////////////////////////////
//                                                               //
//  Funcion : pon_cmp52a                                         //
//                                                               //
///////////////////////////////////////////////////////////////////
int pon_cmp52a(char *mensaje, char *cmp52a)
{
 int result = GFI_ERRC_OK;
 Gfi_StrRTrim(cmp52a);
 sprintf(mensaje, "%s:52A:%s%c%c", mensaje, cmp52a, toascii(13), toascii(10));
 return (result);
}
///////////////////////////////////////////////////////////////////
//                                                               //
//  Funcion : pon_cmp56a                                         //
//                                                               //
///////////////////////////////////////////////////////////////////
int pon_cmp56a(char *mensaje, char *cmp56a)
{
 int result = GFI_ERRC_OK;
 Gfi_StrRTrim(cmp56a);

 sprintf(mensaje, "%s:56A:%s%c%c", mensaje, cmp56a, toascii(13), toascii(10));
 return (result);
}
///////////////////////////////////////////////////////////////////
//                                                               //
//  Funcion : pon_cmp72                                          //
//                                                               //
///////////////////////////////////////////////////////////////////
int pon_cmp72(char *mensaje, char *cmp72)
{
 int result = GFI_ERRC_OK;
 int  fin, salida, ya;
 long cmp72_len = 0;
 char subcadena[36];


 fin = 35;
 ya = 0;
 salida = 0;
 Gfi_StrRTrim(cmp72);

 cmp72_len = strlen(cmp72);

 while (salida == 0)
 {
     if (cmp72_len > fin)
        {
         substr(cmp72, fin-35, 35, subcadena);
         fin = fin + 35;
        }
     else
        {
         substr(cmp72, fin-35, cmp72_len-fin+35, subcadena);
         salida = 1;
        }
     if (ya == 0)
        {
         sprintf(mensaje, "%s:72:%s%c%c", mensaje, subcadena, toascii(13), toascii(10));
         ya = 1;
        }
     else
        sprintf(mensaje, "%s%s%c%c", mensaje, subcadena, toascii(13), toascii(10));
 }
 return (result);
}
///////////////////////////////////////////////////////////////////
//                                                               //
//  Funcion : pon_cmp79                                          //
//                                                               //
///////////////////////////////////////////////////////////////////
int pon_cmp79(char *mensaje, char *cmp79)
{
 int result = GFI_ERRC_OK;
 int  fin, salida, ya;
 long cmp79_len = 0;
 char subcadena[51];
 fin = 50;
 ya = 0;
 salida = 0;
 Gfi_StrRTrim(cmp79);

 cmp79_len = strlen(cmp79);
 while (salida == 0)
 {
     if (cmp79_len > fin)
     {
         substr(cmp79, fin-50, 50, subcadena);

         fin = fin + 50;
     }
     else
     {
         substr(cmp79, fin-50, cmp79_len-fin+50, subcadena);
         salida = 1;
     }
     if (ya == 0)
     {
         sprintf(mensaje, "%s:79:%s%c%c", mensaje, subcadena, toascii(13), toascii(10));
         ya = 1;
     }
     else
        sprintf(mensaje, "%s%s%c%c", mensaje, subcadena, toascii(13), toascii(10));
 }
 return (result);
}

///////////////////////////////////////////////////////////////////
//                                                               //
//  Funcion : pon_pie                                            //
//                                                               //
///////////////////////////////////////////////////////////////////
int pon_pie(char *mensaje)
{
 int result = GFI_ERRC_OK;
 sprintf(mensaje, "%s-}%c", mensaje, '\003');
// sprintf(mensaje, "%s-}{5:{MAC:1234ABCD}}%c", mensaje, '\003');
 return (result);
}