/* **************************************************************************
    Librería de Envio de MT-202 Envio de pago

    Modulo para envio de mensajes Swift MT202 para pagos For Further Credit

    Creacion 19 de Junio de 2006 por Carlos Jesus Gutierrez Cortazar

//                        DHJ 27-11-2008 (QUITAR MAC DEL PIE DEL MENSAJE)  //

**************************************************************************** */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include <time.h>

#define SI 1
#define NO 0

int NIVEL_TRACE202 = 10;

char *findNoFile( char *path, char *name);
void setCampo_SC1( char *campo, char *prefijo, char *cuenta_corresponsal_enviador);
void setCampo_SC2( char *campo, char *prefijo, char *datos);

class Mensaje202
{
    private:
        char *Mensaje;

        char CampoF01[45];
        char Campo2[70];
        char Campo20[35];
        char Campo21[35];
        char Campo32A[45];
        char Campo52A[75];
        char Campo52D[210];
        char Campo53A[75];
        char Campo54A[49];
        char Campo56A[49];
        char Campo57A[49];
        char Campo58A[60];
        char Campo58D[210];
        char Campo72[251];

        int CampoF01_llenado;
        int Campo2_llenado;
        int Campo20_llenado;
        int Campo21_llenado;
        int Campo32A_llenado;
        int Campo52A_llenado;
        int Campo52D_llenado;
        int Campo53A_llenado;
        int Campo54A_llenado;
        int Campo56A_llenado;
        int Campo57A_llenado;
        int Campo58A_llenado;
        int Campo58D_llenado;
        int Campo72_llenado;

        int tamanio_mensaje;

    public:
        Mensaje202();
        ~Mensaje202();

        void setMensaje( void );
        const char *getMensaje() { return Mensaje;};

        void setCampoF01( char *banco);
        void setCampo2( char *banco);
        void setCampo20( char *referencia);
        void setCampo21( char *boc);
        void setCampo32A( char *monto);
        void setCampo52A( char *institucion_ordenante);
        void setCampo52A_SC1( char *cuenta_institucion_ordenante, int terminar);
        void setCampo52A_SC2( char *bic, int terminar);
        void setCampo52D( char *institucion_ordenante);
        void setCampo52D_SC1( char *cuenta_institucion_ordenante, int terminar);
        void setCampo52D_SC2( char *nombre_direccion, int terminar);
        void setCampo53A( char *corresponsal_enviador);
        void setCampo53A_SC1( char *cuenta_corresponsal_enviador, int terminar);
        void setCampo53A_SC2( char *bic, int terminar);
        void setCampo54A( char *institucion_ordenante);
        void setCampo56A( char *institucion_ordenante);
        void setCampo57A( char *institucion_ordenante);
        void setCampo58A( char *institucion_beneficiaria);
        void setCampo58A_SC1( char *cuenta_institucion_beneficiaria, int terminar);
        void setCampo58A_SC2( char *bic, int terminar);
        void setCampo58D( char *institucion_beneficiaria);
        void setCampo58D_SC1( char *cuenta_institucion_beneficiaria, int terminar);
        void setCampo58D_SC2( char *nombre_direccion, int terminar);
        void setCampo72( char *institucion_ordenante);

        void LimpiaMensaje( void);

        int getLongitudMensaje() { return( tamanio_mensaje);};

};

Mensaje202::Mensaje202()
{
    this->CampoF01[ 0] = '\x0';
    this->Campo2[ 0] = '\x0';
    this->Campo20[ 0] = '\x0';
    this->Campo21[ 0] = '\x0';
    this->Campo32A[ 0] = '\x0';
    this->Campo52A[ 0] = '\x0';
    this->Campo52D[ 0] = '\x0';
    this->Campo53A[ 0] = '\x0';
    this->Campo54A[ 0] = '\x0';
    this->Campo56A[ 0] = '\x0';
    this->Campo57A[ 0] = '\x0';
    this->Campo58A[ 0] = '\x0';
    this->Campo58D[ 0] = '\x0';
    this->Campo72[ 0] = '\x0';
}

void Mensaje202::setCampoF01( char *banco)
{
  //sprintf( CampoF01, "%c{1:F01%sAXXX0000000000}", (char) 1, banco);
  sprintf( CampoF01, "{1:F01%sAXXX0000000000}", banco);
  CampoF01_llenado = SI;
}

void Mensaje202::setCampo2( char *banco)
{
  sprintf( Campo2, "{2:%s}", banco);
  Campo2_llenado = SI;
}

void Mensaje202::setCampo20( char *referencia)
{
  sprintf( Campo20, ":20:%s%c%c", referencia, (char) 13, (char) 10);
  Campo20_llenado = SI;
}

void Mensaje202::setCampo21( char *boc)
{
  sprintf( Campo21, ":21:%s%c%c", boc, (char) 13, (char) 10);
  Campo21_llenado = SI;
}

void Mensaje202::setCampo32A( char *monto)
{
  sprintf( Campo32A, ":32A:%s%c%c", monto, (char) 13, (char) 10);
  Campo32A_llenado = SI;
}

void Mensaje202::setCampo52A( char *texto)
{
  int i, cC=0, cR=1;
  long cmp_len=0;
  Gfi_StrRTrim(texto);
  cmp_len = strlen(texto);
  sprintf(Campo52A, ":52A:");
  for (i=0; i<cmp_len; i++)
  {
    if( texto[i] != 13 )
      cC++;
    if( texto[i] == 10 )
    {
      if( ++cR > 2 ) break;
      if( texto[i-1] != 13)
        sprintf(Campo52A, "%s%c", Campo52A, toascii(13));
      cC = 0;
    }
    if( cC > 35 )
    {
      if( ++cR > 2 ) break;
      sprintf(Campo52A, "%s%c%c%c", Campo52A, toascii(13), toascii(10), texto[i]);
      cC = 1;
    }
    else
      sprintf(Campo52A, "%s%c", Campo52A, texto[i]);
  }
  sprintf(Campo52A, "%s%c%c", Campo52A, toascii(13), toascii(10));
  //sprintf( Campo52A, ":52A:%s%c%c", texto, (char) 13, (char) 10);
  Campo52A_llenado = SI;
}

void Mensaje202::setCampo52A_SC1( char *cuenta_institucion_ordenante, int terminar)
{
  sprintf( Campo52A, ":52A:%s%c%c", cuenta_institucion_ordenante, (char) 13, (char) 10);

  if ( terminar == SI)
    Campo52A_llenado = SI;
  else
    Campo52A_llenado = NO;
}

void Mensaje202::setCampo52A_SC2( char *bic, int terminar)
{
  setCampo_SC2( Campo52A, "52A", bic);

  if ( terminar == SI)
    Campo52A_llenado = SI;
  else
    Campo52A_llenado = NO;
}

void Mensaje202::setCampo52D( char *texto)
{
  int i, cC=0, cR=1;
  long cmp_len=0;
  Gfi_StrRTrim(texto);
  cmp_len = strlen(texto);
  sprintf(Campo52D, ":52D:");
  for (i=0; i<cmp_len; i++)
  {
    if( texto[i] != 13 )
      cC++;
    if( texto[i] == 10 )
    {
      if( ++cR > 5 ) break;
      if( texto[i-1] != 13)
        sprintf(Campo52D, "%s%c", Campo52D, toascii(13));
      cC = 0;
    }
    if( cC > 35 )
    {
      if( ++cR > 5 ) break;
      sprintf(Campo52D, "%s%c%c%c", Campo52D, toascii(13), toascii(10), texto[i]);
      cC = 1;
    }
    else
      sprintf(Campo52D, "%s%c", Campo52D, texto[i]);
  }
  sprintf(Campo52D, "%s%c%c", Campo52D, toascii(13), toascii(10));
  //sprintf( Campo52D, ":52D:%s%c%c", texto, (char) 13, (char) 10);
  Campo52D_llenado = SI;
}

void Mensaje202::setCampo52D_SC1( char *cuenta_institucion_ordenante, int terminar)
{
  sprintf( Campo52D, ":52D:%s%c%c", cuenta_institucion_ordenante, (char) 13, (char) 10);

  if ( terminar == SI)
    Campo52D_llenado = SI;
  else
    Campo52D_llenado = NO;
}

void Mensaje202::setCampo52D_SC2( char *nombre_direccion, int terminar)
{
  setCampo_SC2( Campo52D, "52D", nombre_direccion);

  if ( terminar == SI)
    Campo52D_llenado = SI;
  else
    Campo52D_llenado = NO;
}

void Mensaje202::setCampo53A( char *texto)
{
  int i, cC=0, cR=1;
  long cmp_len=0;
  Gfi_StrRTrim(texto);
  cmp_len = strlen(texto);
  sprintf(Campo53A, ":53A:");
  for (i=0; i<cmp_len; i++)
  {
    if( texto[i] != 13 )
      cC++;
    if( texto[i] == 10 )
    {
      if( ++cR > 2 ) break;
      if( texto[i-1] != 13)
        sprintf(Campo53A, "%s%c", Campo53A, toascii(13));
      cC = 0;
    }
    if( cC > 35 )
    {
      if( ++cR > 2 ) break;
      sprintf(Campo53A, "%s%c%c%c", Campo53A, toascii(13), toascii(10), texto[i]);
      cC = 1;
    }
    else
      sprintf(Campo53A, "%s%c", Campo53A, texto[i]);
  }
  sprintf(Campo53A, "%s%c%c", Campo53A, toascii(13), toascii(10));
  //sprintf( Campo53A, ":53A:%s%c%c", texto, (char) 13, (char) 10);
  Campo53A_llenado = SI;
}

void Mensaje202::setCampo53A_SC1( char *cuenta_corresponsal_enviador, int terminar)
{
  setCampo_SC1( Campo53A, "53A", cuenta_corresponsal_enviador);

  if ( terminar == SI)
    Campo53A_llenado = SI;
  else
    Campo53A_llenado = NO;
}

void Mensaje202::setCampo53A_SC2( char *bic, int terminar)
{
  setCampo_SC2( Campo53A, "53A", bic);

  if ( terminar == SI)
    Campo53A_llenado = SI;
  else
    Campo53A_llenado = NO;
}

void Mensaje202::setCampo54A( char *texto)
{
  int i, cC=0, cR=1;
  long cmp_len=0;
  Gfi_StrRTrim(texto);
  cmp_len = strlen(texto);
  sprintf(Campo54A, ":54A:");
  for (i=0; i<cmp_len; i++)
  {
    if( texto[i] != 13 )
      cC++;
    if( texto[i] == 10 )
    {
      if( ++cR > 2 ) break;
      if( texto[i-1] != 13)
        sprintf(Campo54A, "%s%c", Campo54A, toascii(13));
      cC = 0;
    }
    if( cC > 35 )
    {
      if( ++cR > 2 ) break;
      sprintf(Campo54A, "%s%c%c%c", Campo54A, toascii(13), toascii(10), texto[i]);
      cC = 1;
    }
    else
      sprintf(Campo54A, "%s%c", Campo54A, texto[i]);
  }
  sprintf(Campo54A, "%s%c%c", Campo54A, toascii(13), toascii(10));
  //sprintf( Campo54A, ":54A:%s%c%c", texto, (char) 13, (char) 10);
  Campo54A_llenado = SI;
}

void Mensaje202::setCampo56A( char *institucion_ordenante)
{
  sprintf( Campo56A, ":56A:%s%c%c", institucion_ordenante, (char) 13, (char) 10);
  Campo56A_llenado = SI;
}

void Mensaje202::setCampo57A( char *institucion_ordenante)
{
  sprintf( Campo57A, ":57A:%s%c%c", institucion_ordenante, (char) 13, (char) 10);
  Campo57A_llenado = SI;
}

void Mensaje202::setCampo58A( char *texto)
{
  int i, cC=0, cR=1;
  long cmp_len=0;
  Gfi_StrRTrim(texto);
  cmp_len = strlen(texto);
  sprintf(Campo58A, ":58A:");
  for (i=0; i<cmp_len; i++)
  {
    if( texto[i] != 13 )
      cC++;
    if( texto[i] == 10 )
    {
      if( ++cR > 2 ) break;
      if( texto[i-1] != 13)
        sprintf(Campo58A, "%s%c", Campo58A, toascii(13));
      cC = 0;
    }
    if( cC > 35 )
    {
      if( ++cR > 2 ) break;
      sprintf(Campo58A, "%s%c%c%c", Campo58A, toascii(13), toascii(10), texto[i]);
      cC = 1;
    }
    else
      sprintf(Campo58A, "%s%c", Campo58A, texto[i]);
  }
  sprintf(Campo58A, "%s%c%c", Campo58A, toascii(13), toascii(10));
  //sprintf( Campo58A, ":58A:%s%c%c", texto, (char) 13, (char) 10);
  Campo58A_llenado = SI;
}

void Mensaje202::setCampo58A_SC1( char *cuenta_institucion_beneficiaria, int terminar)
{
  sprintf( Campo58A, ":58A:%s%c%c", cuenta_institucion_beneficiaria, (char) 13, (char) 10);

  if ( terminar == SI)
    Campo58A_llenado = SI;
  else
    Campo58A_llenado = NO;
}

void Mensaje202::setCampo58A_SC2( char *bic, int terminar)
{
  setCampo_SC2( Campo58A, "58A", bic);

  if ( terminar == SI)
    Campo58A_llenado = SI;
  else
    Campo58A_llenado = NO;
}

void Mensaje202::setCampo58D( char *texto)
{
  int i, cC=0, cR=1;
  long cmp_len=0;
  Gfi_StrRTrim(texto);
  cmp_len = strlen(texto);
  sprintf(Campo58D, ":58D:");
  for (i=0; i<cmp_len; i++)
  {
    if( texto[i] != 13 )
      cC++;
    if( texto[i] == 10 )
    {
      if( ++cR > 5 ) break;
      if( texto[i-1] != 13)
        sprintf(Campo58D, "%s%c", Campo58D, toascii(13));
      cC = 0;
    }
    if( cC > 35 )
    {
      if( ++cR > 5 ) break;
      sprintf(Campo58D, "%s%c%c%c", Campo58D, toascii(13), toascii(10), texto[i]);
      cC = 1;
    }
    else
      sprintf(Campo58D, "%s%c", Campo58D, texto[i]);
  }
  sprintf(Campo58D, "%s%c%c", Campo58D, toascii(13), toascii(10));
  //sprintf( Campo58D, ":58D:%s%c%c", texto, (char) 13, (char) 10);
  Campo58D_llenado = SI;
}

void Mensaje202::setCampo58D_SC1( char *cuenta_institucion_beneficiaria, int terminar)
{
  sprintf( Campo58D, ":58D:%s%c%c", cuenta_institucion_beneficiaria, (char) 13, (char) 10);

  if ( terminar == SI)
    Campo58D_llenado = SI;
  else
    Campo58D_llenado = NO;
}

void Mensaje202::setCampo58D_SC2( char *nombre_direccion, int terminar)
{
  setCampo_SC2( Campo58D, "58D", nombre_direccion);

  if ( terminar == SI)
    Campo58D_llenado = SI;
  else
    Campo58D_llenado = NO;
}

void Mensaje202::setCampo72( char *texto)
{
  int i, cC=0, cR=1;
  long cmp_len=0;
  Gfi_StrRTrim(texto);
  cmp_len = strlen(texto);
  sprintf(Campo72, ":72:");
  for (i=0; i<cmp_len; i++)
  {
    if( texto[i] != 13 )
      cC++;
    if( texto[i] == 10 )
    {
      if( ++cR > 6 ) break;
      if( texto[i-1] != 13)
        sprintf(Campo72, "%s%c", Campo72, toascii(13));
      sprintf(Campo72, "%s%c//", Campo72, texto[i]);
      cC = 2;
    }
    if( cC > 35 )
    {
      if( ++cR > 6 ) break;
      sprintf(Campo72, "%s%c%c//%c", Campo72, toascii(13), toascii(10), texto[i]);
      cC = 3;
    }
    else if( texto[i] != 10 )
      sprintf(Campo72, "%s%c", Campo72, texto[i]);
  }
  sprintf(Campo72, "%s%c%c", Campo72, toascii(13), toascii(10));
  //sprintf( Campo72, ":72:%s%c%c", texto, (char) 13, (char) 10);
  Campo72_llenado = SI;
}

void Mensaje202::setMensaje()
{
   char Hora[ 10];
   int i = 0;
   int resultado;

   time_t Fecha;
   tm     *Fecha_1;

   time(&Fecha);
   Fecha_1 = (localtime(&Fecha));

    // Se va a generar el mensaje
    // se reservan 10000 caracteres unicamente del mensaje
    Hora[0] = '\x0';
    sprintf(Hora, "%d%d", Fecha_1->tm_hour, Fecha_1->tm_min);
    Hora[4] = '\x0';

    Mensaje = (char *)malloc( sizeof(char) * 10000);
    if( !Mensaje ) {
        printf("Error: No existe suficiente memoria para crear el mensaje\n");
        printf("el proceso se cancela.\n");
        exit(0);
    }

    if ( NIVEL_TRACE202 > 0)
      printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);

    if ( CampoF01_llenado == SI)
    {
      tamanio_mensaje = sprintf( Mensaje, "%s", CampoF01);

      if ( NIVEL_TRACE202 > 0)
        printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);
    }
    else
      resultado = GFI_ERRC_FATAL;

    if ( Campo2_llenado == SI)
    {
      tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "%s", Campo2);

      if ( NIVEL_TRACE202 > 0)
        printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);
    }
    else
      resultado = GFI_ERRC_FATAL;

    // Se genera el campo 4 cuerpo del mensaje
    tamanio_mensaje += sprintf( Mensaje  + tamanio_mensaje, "{4:%c%c", (char) 13, (char) 10);

    if ( NIVEL_TRACE202 > 0)
      printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);

    if ( Campo20_llenado == SI)
    {
      tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "%s", Campo20);

      if ( NIVEL_TRACE202 > 0)
        printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);
    }
    else
      resultado = GFI_ERRC_FATAL;

    if ( Campo21_llenado == SI)
    {
      tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "%s", Campo21);

      if ( NIVEL_TRACE202 > 0)
        printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);
    }
    else
      resultado = GFI_ERRC_FATAL;

    if ( Campo32A_llenado == SI)
    {
      tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "%s", Campo32A);

      if ( NIVEL_TRACE202 > 0)
        printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);
    }
    else
      resultado = GFI_ERRC_FATAL;

    if ( Campo52A_llenado == SI)
    {
      tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "%s", Campo52A);

      if ( NIVEL_TRACE202 > 0)
        printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);
    }

    if ( Campo52D_llenado == SI)
    {
      tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "%s", Campo52D);

      if ( NIVEL_TRACE202 > 0)
        printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);
    }

    if (( Campo52A_llenado != SI) && (Campo52D_llenado != SI))
      resultado = GFI_ERRC_FATAL;

    if ( Campo53A_llenado == SI)
    {
      tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "%s", Campo53A);

      if ( NIVEL_TRACE202 > 0)
        printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);
    }
    else
      resultado = GFI_ERRC_FATAL;

    if ( Campo54A_llenado == SI)
    {
      tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "%s", Campo54A);

      if ( NIVEL_TRACE202 > 0)
        printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);
    }
    else
      resultado = GFI_ERRC_FATAL;

    if ( Campo56A_llenado == SI)
    {
      tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "%s", Campo56A);

      if ( NIVEL_TRACE202 > 0)
        printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);
    }
    else
      resultado = GFI_ERRC_FATAL;

    if ( Campo57A_llenado == SI)
    {
      tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "%s", Campo57A);

      if ( NIVEL_TRACE202 > 0)
        printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);
    }
    else
      resultado = GFI_ERRC_FATAL;

    if ( Campo58A_llenado == SI)
    {
      tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "%s", Campo58A);

      if ( NIVEL_TRACE202 > 0)
        printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);
    }

    if ( Campo58D_llenado == SI)
    {
      tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "%s", Campo58D);

      if ( NIVEL_TRACE202 > 0)
        printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);
    }

    if ( Campo72_llenado == SI)
    {
      tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "%s", Campo72);

      if ( NIVEL_TRACE202 > 0)
        printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);
    }

    // Se cierra el mensaje
//    tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "-}{5:{MAC:1234ABCD}}%c%c%c", '\003', (char) 13, (char) 10);
//    tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "-}{5:{MAC:1234ABCD}}%c", '\003');

    //tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "-}{5:{MAC:1234ABCD}}"); DHJ 27-11-2008 (QUITAR MAC DEL PIE DEL MENSAJE)

    tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "-}%c", '\003'); //DHJ 27-11-2008 (QUITAR MAC DEL PIE DEL MENSAJE)

    if ( NIVEL_TRACE202 > 0)
      printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);

    LimpiaMensaje();

    if ( NIVEL_TRACE202 > 0)
      printf( "Termino de construir el mensaje\n");
}

void Mensaje202::LimpiaMensaje( void)
{
    this->CampoF01[ 0] = '\x0';
    this->Campo2[ 0] = '\x0';
    this->Campo20[ 0] = '\x0';
    this->Campo21[ 0] = '\x0';
    this->Campo32A[ 0] = '\x0';
    this->Campo52A[ 0] = '\x0';
    this->Campo52D[ 0] = '\x0';
    this->Campo53A[ 0] = '\x0';
    this->Campo54A[ 0] = '\x0';
    this->Campo56A[ 0] = '\x0';
    this->Campo57A[ 0] = '\x0';
    this->Campo58A[ 0] = '\x0';
    this->Campo58D[ 0] = '\x0';
    this->Campo72[ 0] = '\x0';

    CampoF01_llenado = 0;
    Campo2_llenado = 0;
    Campo20_llenado = 0;
    Campo21_llenado = 0;
    Campo32A_llenado = 0;
    Campo52A_llenado = 0;
    Campo52D_llenado = 0;
    Campo53A_llenado = 0;
    Campo54A_llenado = 0;
    Campo56A_llenado = 0;
    Campo57A_llenado = 0;
    Campo58A_llenado = 0;
    Campo58D_llenado = 0;
    Campo72_llenado = 0;
}
