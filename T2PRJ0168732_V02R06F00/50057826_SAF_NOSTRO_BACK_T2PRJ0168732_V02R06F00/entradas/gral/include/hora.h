/**********************************************************************
 *
 * Archivo     : hora.h
 *
 * Proposito   : Definicion de la clase GfiHora.
 *
 * Clase       : GfiHora
 *
 * Acronimo
 * de la clase : GfiHora
 *
 * Autor       : Reynaldo Martinez
 *
 * Supervision : Enrique Ibarra
 *
 * Historia    : 11/Nov/93 -> Creacion
 *               25/Nov/93 -> Modificacion por Jose Rafael Torres C.
 *
 **********************************************************************/

#if !defined (_GFI_HORA_H_)
#define _GFI_HORA_H_

#if defined (GFI_VERSION)

/* Identificacion para el control de versiones RCS */

static char RCS_GFI_HORA_H[] = "$Id$";

/* Mensajes de log de RCS
 *
 * $Log$
 */

#endif

/* Definicion de la Clase */

typedef struct GfiHora_attr *GfiHora;

/* Seccion de Includes */

#include "gral.h"

#if defined(__cplusplus) || defined(c_plusplus)
extern "C"
{
#endif

/* Constructores de la clase */

  int GfiHora_Create_1 (GfiHora *, int, int, int);
  int GfiHora_Create_2 (GfiHora *);
  int GfiHora_Create_3 (GfiHora *, const long);
  int GfiHora_Create_4 (GfiHora *, const char *);

/* Destructor de la clase */

  int GfiHora_Delete (GfiHora *);

/* Operaciones Publicas */

  int GfiHora_Hora (const GfiHora, int *);
  int GfiHora_Minutos (const GfiHora, int *);
  int GfiHora_Segundos (const GfiHora, int *);

  int GfiHora_ComparaHoras (const GfiHora, const GfiHora, int *);
  int GfiHora_ComparaConHoraActual (const GfiHora, int *);
  int GfiHora_CopiaHoraNueva (const GfiHora, const GfiHora);
  int GfiHora_2_Long (const GfiHora, long *);
  int GfiHora_2_String (const GfiHora, char *, int);
  int GfiHora_Print (const GfiHora, FILE *);

#if defined(__cplusplus) || defined(c_plusplus)
}
#endif

#endif
