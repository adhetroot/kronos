#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include <time.h>

int NIVEL_TRACE = 0;

void setCampo_SC1( char *campo, char *prefijo, char *cuenta_corresponsal_enviador);
void setCampo_SC2( char *campo, char *prefijo, char *datos);

#define SI 1
#define NO 0

class Mensaje112
{
    private:
        char cve_swift_banco_env[13];
        char ChequeNumber[17];
        char DateOfIssue[7];
        char FechaHoy[7];
        char Currency[4];
        char Ammount[16];
        char Payee[4][36];
        char Answers[6][36];
        char cve_swift_banco_rec[13];

        char CampoF01[45];
        char Campo2[70];
        char Campo20[35];
        char Campo21[35];
        char Campo30[20];
        char Campo32A[45];
        char Campo59[210];
        char Campo76[223]; // 6*37x
        char *Mensaje;

        int CampoF01_llenado;
        int Campo2_llenado;
        int Campo20_llenado;
        int Campo21_llenado;
        int Campo30_llenado;
        int Campo32A_llenado;
        int Campo59_llenado;
        int Campo76_llenado;

    public:
        Mensaje112();
        ~Mensaje112();

        void setCampoF01( char *banco);
        void setCampo2( char *banco);
        void setCampo20( char *referencia);
        void setCampo21( char *num_cheque);
        void setCampo30( char *fecha);
        void setCampo32A( char *fecha, char *moneda, char *monto);
        void setCampo59( char *cliente_beneficiario);
        void setCampo59_SC1( char *cuenta_cliente_beneficiario, int terminar);
        void setCampo59_SC2( char *nombre_direccion, int terminar);
        void setCampo76( char **respuestas);

        void setFechaHoy( void );
        void setMensaje( void );

        char *GetFechaHoy( void);
        const char * getMensaje()      { return Mensaje;};
        void LimpiaMensaje( void);

};

void Mensaje112::setFechaHoy(void)
{
  time_t Fecha;
  tm     *Fecha_1;

  time(&Fecha);
  Fecha_1 = (localtime( &Fecha ));

  sprintf(FechaHoy, "%02d%02d%02d", Fecha_1->tm_year - 100, Fecha_1->tm_mon + 1, Fecha_1->tm_mday);

  FechaHoy[6] = '\0';
}

char *Mensaje112::GetFechaHoy( void)
{
  return( FechaHoy);
}

Mensaje112::Mensaje112()
{
  this->CampoF01[ 0] = '\x0';
  this->Campo2[ 0] = '\x0';
  this->Campo20[ 0] = '\x0';
  this->Campo21[ 0] = '\x0';
  this->Campo30[ 0] = '\x0';
  this->Campo32A[ 0] = '\x0';
  this->Campo59[ 0] = '\x0';
}

void Mensaje112::setCampoF01( char *banco)
{
  sprintf( CampoF01, "%c{1:F01%s0000000000}", (char) 1, banco);
  CampoF01_llenado = SI;
}

void Mensaje112::setCampo2( char *banco)
{
  if ( strlen( banco) != 12)
     strcpy( banco + 8, "XXXX");

  sprintf( Campo2, "{2:I112%sN}", banco);
  Campo2_llenado = SI;
}

void Mensaje112::setCampo20( char *referencia)
{
  sprintf( Campo20, ":20:%s%c%c", referencia, (char) 13, (char) 10);
  Campo20_llenado = SI;
}

void Mensaje112::setCampo21( char *num_cheque)
{
  sprintf( Campo21, ":21:%s%c%c", num_cheque, (char) 13, (char) 10);
  Campo21_llenado = SI;
}

void Mensaje112::setCampo30( char *fecha)
{
  sprintf( Campo30, ":30:%s%c%c", fecha, (char) 13, (char) 10);
  Campo30_llenado = SI;
}

void Mensaje112::setCampo32A( char *fecha, char *moneda, char *monto)
{
  sprintf( Campo32A, ":32A:%s%s%s%c%c", fecha, moneda, monto, (char) 13, (char) 10);
  Campo32A_llenado = SI;
}

void Mensaje112::setCampo59( char *cliente_beneficiario)
{
  sprintf( Campo59, ":59:%s%c%c", cliente_beneficiario, (char) 13, (char) 10);
  Campo59_llenado = SI;
}

void Mensaje112::setCampo59_SC1( char *cuenta_cliente_beneficiario, int terminar)
{
  setCampo_SC1( Campo59, "59", cuenta_cliente_beneficiario);

  if ( terminar == SI)
    Campo59_llenado = SI;
  else
    Campo59_llenado = NO;
}

void Mensaje112::setCampo59_SC2( char *nombre_direccion, int terminar)
{
  setCampo_SC2( Campo59, "59", nombre_direccion);

  if ( terminar == SI)
    Campo59_llenado = SI;
  else
    Campo59_llenado = NO;
}

void Mensaje112::setCampo76( char **respuestas)
{
  int indice;
  int cuantos;
  char pedazo[ 50];
  char temporal[ 100];
  int longitud;

  sprintf( Campo76, ":76:");

  for ( indice = 0; indice < 6; indice++)
  {
    if ( strlen( respuestas[ indice]) > 0)
    {
      Gfi_StrRTrim( respuestas[ indice]);
      strcpy( temporal, respuestas[ indice]);
      longitud = strlen( temporal);

      if ( longitud > 0)
      {
        if ( longitud > 35)
          *(temporal + 34) = '\0';

        sprintf( pedazo, "%s%c%c", temporal, (char) 13, (char) 10);
        strcat( Campo76, pedazo);
      }
    }
  }

  Campo76_llenado = SI;
}

void Mensaje112::setMensaje()
{
   char Hora[ 10];
   int i = 0;
   int resultado;
   int tamanio_mensaje;

   time_t Fecha;
   tm     *Fecha_1;

   time(&Fecha);
   Fecha_1 = (localtime(&Fecha));

    // Se va a generar el mensaje
    // se reservan 10000 caracteres unicamente del mensaje
    Hora[0] = '\x0';
    sprintf(Hora, "%d%d", Fecha_1->tm_hour, Fecha_1->tm_min);
    Hora[4] = '\x0';

    Mensaje = (char *)malloc( sizeof(char) * 10000);
    if( !Mensaje ) {
        printf("Error: No existe suficiente memoria para crear el mensaje\n");
        printf("el proceso se cancela.\n");
        exit(0);
    }

    if ( NIVEL_TRACE > 0)
      printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);

    if ( CampoF01_llenado == SI)
    {
      tamanio_mensaje = sprintf( Mensaje, "%s", CampoF01);

      if ( NIVEL_TRACE > 0)
        printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);
    }
    else
      resultado = GFI_ERRC_FATAL;

    if ( Campo2_llenado == SI)
    {
      tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "%s", Campo2);

      if ( NIVEL_TRACE > 0)
        printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);
    }
    else
      resultado = GFI_ERRC_FATAL;

    // Se genera el campo 4 cuerpo del mensaje
    tamanio_mensaje += sprintf( Mensaje  + tamanio_mensaje, "{4:%c%c", (char) 13, (char) 10);

    if ( NIVEL_TRACE > 0)
      printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);

    if ( Campo20_llenado == SI)
    {
      tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "%s", Campo20);

      if ( NIVEL_TRACE > 0)
        printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);
    }
    else
      resultado = GFI_ERRC_FATAL;

    if ( Campo21_llenado == SI)
    {
      tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "%s", Campo21);

      if ( NIVEL_TRACE > 0)
        printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);
    }
    else
      resultado = GFI_ERRC_FATAL;

    if ( Campo30_llenado == SI)
    {
      tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "%s", Campo30);

      if ( NIVEL_TRACE > 0)
        printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);
    }
    else
      resultado = GFI_ERRC_FATAL;

    if ( Campo32A_llenado == SI)
    {
      tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "%s", Campo32A);

      if ( NIVEL_TRACE > 0)
        printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);
    }
    else
      resultado = GFI_ERRC_FATAL;

/*
    if ( Campo52A_llenado == SI)
    {
      tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "%s", Campo52A);

      if ( NIVEL_TRACE > 0)
        printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);
    }
*/

    if ( Campo59_llenado == SI)
    {
      tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "%s", Campo59);

      if ( NIVEL_TRACE > 0)
        printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);
    }

    if ( Campo76_llenado == SI)
    {
      tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "%s", Campo76);

      if ( NIVEL_TRACE > 0)
        printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);
    }

// Se cierra el mensaje
    tamanio_mensaje += sprintf( Mensaje + tamanio_mensaje, "-}%c", '\003');

    if ( NIVEL_TRACE > 0)
      printf( "Mensaje >%d<, >%s<\n", tamanio_mensaje, Mensaje);

    LimpiaMensaje();

    if ( NIVEL_TRACE > 0)
      printf( "Termino de construir el mensaje\n");
}

void Mensaje112::LimpiaMensaje( void)
{
  this->CampoF01[ 0] = '\x0';
  this->Campo2[ 0] = '\x0';
  this->Campo20[ 0] = '\x0';
  this->Campo21[ 0] = '\x0';
  this->Campo30[ 0] = '\x0';
  this->Campo32A[ 0] = '\x0';
  this->Campo59[ 0] = '\x0';
  this->Campo76[ 0] = '\x0';
}