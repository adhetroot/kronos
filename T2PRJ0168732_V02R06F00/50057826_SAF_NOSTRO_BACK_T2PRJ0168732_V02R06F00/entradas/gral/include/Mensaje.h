/*
////////////////////////////////////////////////////////////
//
//  Headers:
//
//              Mesaje.h
//
//  Creado por:
//
//              German Gutierrez Galan
//
//  Compania:
//
//              SOLTIS
//
////////////////////////////////////////////////////////////
*/

#ifndef MENSAJE_H
#define MENSAJE_H

#include <unistd.h>
#include <sys/msg.h>
#include <errno.h>
#include <fcntl.h>

extern int errno;

struct myMsg
{
   long type;
   char text[64];
};

int open ( int key , int opc );
char * findNoFile( char *path , char * name );
char * cadWithCero( short no );
#ifndef OK
#define OK 0
#endif

#ifndef NOT_OK
#define NOT_OK 1
#endif

#endif
