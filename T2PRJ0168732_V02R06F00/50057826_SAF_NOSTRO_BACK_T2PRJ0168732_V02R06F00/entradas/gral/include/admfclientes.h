#if !defined (_ADMF_EDOSCTA_H_)
 #define _ADMF_EDOSCTA_H_

#include <stdio.h>
#include <sys/types.h>
#include <sys/time.h>
#include <dirent.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <gfi-err.h>
#include <string>
#include <iostream>


#define LONG_LINE        1024
/*==A partir de 9000 carcateres se crea un nuevo archivo==*/
#define LIM_INF_SWF      9000
#define LIM_MSG_SWF      1300
#define PUNTO            '.'
#define COMA             ','
#define ERROR_OPEN_FILE  1000
#define ERROR_FIND_FILE  1010
#define ERROR_OPEN_DIR   1020


/*==Contador de numero de bytes en un Archivo swift==*/
int gi_numbytes;
/*==Indicador de que el Mensaje ha alcanzo el tamanio maximo==*/
int giMaxSizeMsg=0;
/*==Contador de numero de bytes en un mensaje swift==*/
int ms_numbytes=0;
/*==Indicador de que el Mensaje ha alcanzo el tamanio maximo==*/
int msMaxSizeMsg=0;
/*=Nombre archivo Swift=*/
char gcnamefileSwift[255]="";
char gcnamefileSwiftSecuenciaExt[255]="";
/*=Extencion archivo Swift=*/
char gcExtArch[80+1]="";

int gi_first_message=0;
/* Para la funcion EnviaArhivoSwift */
char gcShellFtp[80+1];
char gcDirFtp[80+1];
char gcPathBin[80+1];

FILE *afMsgSw=NULL;

typedef struct _MsgSwift_
{
  char BHB [32],    /*==Basic Header Block==*/
       AHB [22],    /*==Application Header Block==*/
       iBo[4],      /*==Initial Body ==*/
       Field[6],    /*==Field Swift==*/
       Value1[101], /*==Value Field==*/
       Value2[101], /*==Value2 Field ==*/
       eBo[3];      /*==End Body ==*/
  struct _MsgSwift_ *next;
}tMsgSwift;
tMsgSwift *gphead=NULL, *gplist=NULL, *gpptr=NULL;

char *Formatosaldo(char *saldo);
char *BsmStrTok(char *origen,char sep,int ntokens);
tMsgSwift *Creanodo(void);
void BorraLista(void);
int AddFieldSwift(char *campo, char *value1, char *value2);
int CreaHeaderSwift(char *inMsgSwift, char *inCveBicEnv, char *inCveBicSant);
int GeneraMsgSwift(void);
int EnviaArchivoSwift(char *inShellFtp, char *inDirFtp, char *inPathBin, char *inArch);
int EjecucionSystem( char *nombre, char *comando);
int CreaEncSigPag(char *inTypeMsg, char *inBicDest, char *inBicBco, char *inCta,
        char *inFchSist, int inSec, int inNumPag, double inSdoIni, char *inFchsdoini,
        char *inDiv);
int nombreArchivoSig(void);
char *HoraSys (void);

#endif
