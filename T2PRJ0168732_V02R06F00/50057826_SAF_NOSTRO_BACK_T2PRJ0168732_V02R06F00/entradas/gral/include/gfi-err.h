/**********************************************************************
 *
 * Grupo Financiero InverMexico
 *
 * Archivo     : gfi-err.h
 *
 * Proposito   : Definicion de los codigos de error comunes a todas las
 *               aplicaciones del grupo financiero.
 *
 * Autor       : Enrique Ibarra A.
 *
 * Supervision : Enrique Ibarra A.
 *
 *               19/Feb/1999 -> Modificado por Antonio Gerardo Cano
 *                              para agregar errores relacionados con
 *                              DCE, ENCINA, DELIGHT y PPC.
 *
 **********************************************************************/

#if !defined(_GFI_ERRCODES_H_)
#define _GFI_ERRCODES_H_

/* Identificacion para el control de versiones RCS */

/* static char RCS_GFI_ERRCODES_H[] = "$Id$";  */

/* Mensajes de log de RCS

 * $Log$
 */

/*************************************************************
 Definition of common error return codes for the class operations:

 GFI_ERRC_OK     : the operation was executed normally without
 errors.

 int GFI_MyClass_Op(GFI_MyClass self, ..., void** error_info)
 {
 ...

 // The operation performed normally

 return GFI_ERRC_OK;
 }

 GFI_ERRC_NOMEM  : an unsuccessful attempt to allocate RAM memory
 was made within the invoked operation (i.e.,
 through the use of malloc, calloc, alloc, realloc,
 etc.).

 int GFI_MyClass_Op(GFI_MyClass self, ..., void** error_info)
 {
 int *int_array;

 int_array = (int*)calloc(1000,sizeof(int));

 if(int_array == 0) return GFI_ERRC_NOMEM;
 ...
 }

 GFI_ERRC_COMM   : communications error.

 GFI_ERRC_FATAL  : fatal error.

 GFI_ERRC_NULLOBJ: the instance on which the operation is being
 invoked (the first argument passed to the
 operation) has a null value.

 int GFI_MyClass_Op(GFI_MyClass self, ..., void** error_info)
 {
 if(self == 0) return GFI_ERRC_NULLOBJ;
 ...
 }

 GFI_ERRC_NULLOBJPTR:
 a null pointer to an object was passed to a function.

 int GFI_MyClass_Create(GFI_MyClass *obj, ..., void** error_info)
 {
 if(obj == 0) return GFI_ERRC_NULLOBJPTR;
 ...
 }

 GFI_ERRC_NULLPTR: one of the arguments passed by reference to the
 operation has an unexpected null value. If this
 argument is the first argument of the operation
 (the instance on which the operation will be
 invoked), the GFI_ERRC_NULLOBJ return code
 should be used instead.

 int GFI_MyClass_Op(GFI_MyClass self, struct X* ptr, void** error_info)
 {
 if(ptr == 0) return GFI_ERRC_NULLPTR;
 ...
 }

 GFI_ERRC_NULLINF: the pointer (to the pointer) to the error information
 (void** error_info) has an unexpected null value.

 int GFI_MyClass_Op(GFI_MyClass self, ..., void** error_info)
 {
 if(error_info == 0) return GFI_ERRC_NULLINF;
 ...
 }

 GFI_ERRC_BADARG : one of the arguments passed by value to the
 operation contains a value that is out of the
 legal limits within the operation, i.e., a numeric
 variable carrying a negative value when only
 positive values are meaningful.

 int GFI_MyClass_Op(GFI_MyClass self, int size, void** error_info)
 {
 if(size < 0) return GFI_ERRC_BADARG;
 ...
 }

 GFI_ERRC_CANT_DELETE : an attempt was made to delete an instance of a
 class that is linked to other object instances. Since the object is linked
 to other objects, deleting it will violate the integrity of the data model.

 GFI_ERRC_BADDECODECLASS: The first string argument passed by value to
 the operation does not match with the class.

 GFI_ERRC_DCE : An error has occurred on the underlying DCE system.

 GFI_ERRC_ENCINA : An error has occurred with the Encina TP monitor.

 GFI_ERRC_DELIGHT : An error has occurred with the Encina DE-light component.

 *********************************************************************/

#define GFI_ERRC_OK		  0
#define	GFI_ERRC_FATAL		 -1
#define	GFI_ERRC_NOMEM		 -2
#define	GFI_ERRC_COMM		 -3
#define	GFI_ERRC_NULLOBJ	 -4
#define GFI_ERRC_NULLINF	 -5
#define	GFI_ERRC_NULLOBJPTR	 -6
#define	GFI_ERRC_NULLPTR	 -7
#define GFI_ERRC_BADARG          -8
#define GFI_ERRC_NODBINST        -9
#define GFI_ERRC_DBERROR        -10
#define GFI_ERRC_MISSARG        -11
#define GFI_ERRC_CANT_DELETE    -12
#define GFI_ERRC_BADDECODECLASS -13
#define GFI_ERRC_BADTPCALL      -14
#define GFI_ERRC_BADTPINIT      -15
#define GFI_ERRC_BADFCHG        -16
#define GFI_ERRC_BADFGET        -17
#define GFI_ERRC_DUPKEY         -18
#define GFI_ERRC_DCE            -19
#define GFI_ERRC_ENCINA         -20
#define GFI_ERRC_DELIGHT        -21
#define GFI_ERRC_PPC            -22

#endif
